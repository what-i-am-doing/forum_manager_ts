import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { ImageService } from 'src/app/services/image.service';
import { AddressService } from 'src/app/services/address.service';

@Component({
  selector: 'app-search-people-card',
  templateUrl: './search-people-card.component.html',
  styleUrls: ['./search-people-card.component.css'],
  providers: [UserService, AddressService]
})
export class SearchPeopleCardComponent implements OnInit {
  @Input() searchedUser: any;
  userId: string = localStorage.getItem("user");
  public sentFriendRequestListids: Array<any> = [];
  public city: any;
  //current user friends list
  public friendids: Array<any> = [];
  public activeUserPersonalData: any;
  public activeUserEducationalData: any;
  public profilePic: any;
  public sentRequestId = '';
  public gender: any;
  public activeUserId = localStorage.getItem("user");
  userGroup: Array<any> = JSON.parse(localStorage.getItem("user_group"));
  groupId = localStorage.getItem("groupId");

  constructor(private addressService: AddressService, private imageService: ImageService, private router: Router, private service: UserService) {

  }

  ngOnInit() {
    console.log('active userID')
    console.log(this.activeUserId)
    this.getUSerDetails(this.searchedUser.user_id)
    this.getUserEducationalDetails(this.searchedUser.user_id);
    this.getUserPersonalDetails(this.searchedUser.user_id);;
    this.getSentFriendRequestByActiveUser(this.userId);
    this.getFriendListOfActiveUser(this.userId, this.userGroup[0], 0, 100);
  }
  getUSerDetails(userId) {
    this.service.getUserDetails(userId).subscribe(data => {
      if (data.imageService != null || data.image_id != undefined) {
        this.getProfile(data.image_id);
      }

    });

  }

  seeFriendProfile(userId) {

    this.router.navigate(['/home/userprofile'], { queryParams: { userId: userId } });
  }

  sendFriendRequest(frienduserId, group_id) {
    if (this.sentRequestId != frienduserId) {
      this.sentRequestId = frienduserId
      var requestObj = {
        "comments": "string",
        "creation_timestamp": Date.now(),
        "from_user_id": this.userId,
        "group_id": group_id,
        "message": "sending friend Request",
        "relation_type_id": "FRIEND",
        "req_status": "OPEN",
        "status": "ACTIVE",
        "to_user_id": frienduserId
      }
      console.log('requestObj')
      console.log(requestObj);
      this.service.sendFriendRequest(requestObj, this.userId).subscribe(data => {
        console.log(data);
        if (data.id) {
          var element = <HTMLInputElement>document.getElementById(frienduserId);
          document.getElementById(frienduserId).innerText = "Request Sent";
          element.disabled = true;
          setTimeout(() =>
            delete this.searchedUser.user_full_text_name,
            2000)
        }
      })
    } else {
      console.log('friend requst >trying to send same user second time')

      document.getElementById(frienduserId).innerText = "Request Already Sent";
    }
  }



  getSentFriendRequestByActiveUser(userId) {

    this.service.getSentFriendRequestByActiveUser(userId, this.groupId).subscribe(data => {
      console.log("sent friend request  with   open status sent by current user")
      if (data != null || data != undefined)
        data.forEach(element => {
          if (element.req_status == 'OPEN') {
            this.sentFriendRequestListids.push(element.to_user_id);

          }
        });


    })
    console.log(this.sentFriendRequestListids)

  }


  getFriendListOfActiveUser(userId: any, group_id, fromsize, toSize) {
    this.service.getFriendListOfActiveUser(userId, group_id, fromsize, toSize).subscribe(data => {

      if (data != null || data != undefined) {

        data.forEach(element => {
          this.friendids.push(element.friend_user_id);
        });
      }
      console.log("all friends ids");
      console.log(this.friendids);
    });
  }

  getUserPersonalDetails(userId) {

    try {
      this.service.getUserPersonalDetails(userId).subscribe(data => {

        console.log(data);
        this.activeUserPersonalData = data;
        this.gender = data.gender_type
        this.getAdress(data.address_id);
      })
    } catch (err) {

    }

  }


  getUserEducationalDetails(userId) {

    try {
      this.service.getEducationDetailsDataByUserId(userId).subscribe(data => {

        console.log(data);
        this.activeUserEducationalData = data;

      })
    } catch (err) {


    }
  }

  URLChange(img: any) {
    if (img) {
      try {

        return img.replace("https", "http");
      } catch (err) {


      }

    }

    else {

      return '';
    }

  }
  getProfile(image_id) {
    console.log('thumbnail calling')
    console.log(image_id)
    this.imageService.getImageByType(image_id, 'thumbnail').subscribe(data => {
      console.log(data);
      if (data.length > 0) {
        console.log("thumbnail found");
        console.log(data[0].image_storage_url);
        this.profilePic = this.URLChange(data[0].image_storage_url);
      }
      else {
        this.getProfileImage(image_id);
      }
    })
  }
  getProfileImage(image_id) {
    if (image_id != '' || image_id != 'string' || image_id != null) {


      this.imageService.getImage(image_id).subscribe(data => {
        console.log("image found");
        console.log(data.image_storage_url);
        this.profilePic = this.URLChange(data.image_storage_url);

      }

        , err => {
          console.log("image not found")
          switch (this.gender) {
            case 'MALE':
              this.profilePic = 'assets/images/male.jpg'
              break;
            case 'FEMALE':
              this.profilePic = 'assets/images/female.jpg';
              break
          }
        }
      )
    } else {
      switch (this.gender) {
        case 'MALE':
          this.profilePic = 'assets/images/male.jpg'
          break;
        case 'FEMALE':
          this.profilePic = 'assets/images/female.jpg';
          break
      }
    }

  }
















  seeProfile() {
    console.log("see profile clicked");
    this.router.navigateByUrl("home/profile");

  }

  getAdress(adressId) {
    this.addressService.getAdress(adressId).subscribe(data => {
      console.log('adress data found');
      console.log(data);

      this.city = data.city;

    })
  }
}
