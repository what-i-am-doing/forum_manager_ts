export class Role  {

 constructor(){};

private comments: string;
private creation_timestamp:number;
private group_id: string;
private last_updated_timestamp:number;
private permissions_list:string [];
private role_desc: string;
private role_id: string;
private role_name: string;
private role_priority:number;
private status: string;

    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $creation_timestamp
     * @return {number}
     */
	public get $creation_timestamp(): number {
		return this.creation_timestamp;
	}

    /**
     * Getter $group_id
     * @return {string}
     */
	public get $group_id(): string {
		return this.group_id;
	}

    /**
     * Getter $last_updated_timestamp
     * @return {number}
     */
	public get $last_updated_timestamp(): number {
		return this.last_updated_timestamp;
	}

    /**
     * Getter $permissions_list
     * @return {string []}
     */
	public get $permissions_list(): string [] {
		return this.permissions_list;
	}

    /**
     * Getter $role_desc
     * @return {string}
     */
	public get $role_desc(): string {
		return this.role_desc;
	}

    /**
     * Getter $role_id
     * @return {string}
     */
	public get $role_id(): string {
		return this.role_id;
	}

    /**
     * Getter $role_name
     * @return {string}
     */
	public get $role_name(): string {
		return this.role_name;
	}

    /**
     * Getter $role_priority
     * @return {number}
     */
	public get $role_priority(): number {
		return this.role_priority;
	}

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $creation_timestamp
     * @param {number} value
     */
	public set $creation_timestamp(value: number) {
		this.creation_timestamp = value;
	}

    /**
     * Setter $group_id
     * @param {string} value
     */
	public set $group_id(value: string) {
		this.group_id = value;
	}

    /**
     * Setter $last_updated_timestamp
     * @param {number} value
     */
	public set $last_updated_timestamp(value: number) {
		this.last_updated_timestamp = value;
	}

    /**
     * Setter $permissions_list
     * @param {string []} value
     */
	public set $permissions_list(value: string []) {
		this.permissions_list = value;
	}

    /**
     * Setter $role_desc
     * @param {string} value
     */
	public set $role_desc(value: string) {
		this.role_desc = value;
	}

    /**
     * Setter $role_id
     * @param {string} value
     */
	public set $role_id(value: string) {
		this.role_id = value;
	}

    /**
     * Setter $role_name
     * @param {string} value
     */
	public set $role_name(value: string) {
		this.role_name = value;
	}

    /**
     * Setter $role_priority
     * @param {number} value
     */
	public set $role_priority(value: number) {
		this.role_priority = value;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}

}
  