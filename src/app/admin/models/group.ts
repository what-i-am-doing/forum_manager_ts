export class Group {


    constructor( ){};

    private _group_id:string;
    private _group_name:string;
    private _custom_tag_line:string;
    private _group_admin_user_id:string;
    private _hierarchy_id:string;
    private _img_id:string;
    private _stateId:string;
    private _comments:string;
    private _creation_timestamp:number;
    private _last_updated_timestamp:number;


    /**
     * Getter group_id
     * @return {string}
     */
	public get group_id(): string {
		return this._group_id;
	}

    /**
     * Getter group_name
     * @return {string}
     */
	public get group_name(): string {
		return this._group_name;
	}

    /**
     * Getter custom_tag_line
     * @return {string}
     */
	public get custom_tag_line(): string {
		return this._custom_tag_line;
	}

    /**
     * Getter group_admin_user_id
     * @return {string}
     */
	public get group_admin_user_id(): string {
		return this._group_admin_user_id;
	}

    /**
     * Getter hierarchy_id
     * @return {string}
     */
	public get hierarchy_id(): string {
		return this._hierarchy_id;
	}

    /**
     * Getter img_id
     * @return {string}
     */
	public get img_id(): string {
		return this._img_id;
	}

    /**
     * Getter stateId
     * @return {string}
     */
	public get stateId(): string {
		return this._stateId;
	}

    /**
     * Getter comments
     * @return {string}
     */
	public get comments(): string {
		return this._comments;
	}

    /**
     * Getter creation_timestamp
     * @return {number}
     */
	public get creation_timestamp(): number {
		return this._creation_timestamp;
	}

    /**
     * Getter last_updated_timestamp
     * @return {number}
     */
	public get last_updated_timestamp(): number {
		return this._last_updated_timestamp;
	}

    /**
     * Setter group_id
     * @param {string} value
     */
	public set group_id(value: string) {
		this._group_id = value;
	}

    /**
     * Setter group_name
     * @param {string} value
     */
	public set group_name(value: string) {
		this._group_name = value;
	}

    /**
     * Setter custom_tag_line
     * @param {string} value
     */
	public set custom_tag_line(value: string) {
		this._custom_tag_line = value;
	}

    /**
     * Setter group_admin_user_id
     * @param {string} value
     */
	public set group_admin_user_id(value: string) {
		this._group_admin_user_id = value;
	}

    /**
     * Setter hierarchy_id
     * @param {string} value
     */
	public set hierarchy_id(value: string) {
		this._hierarchy_id = value;
	}

    /**
     * Setter img_id
     * @param {string} value
     */
	public set img_id(value: string) {
		this._img_id = value;
	}

    /**
     * Setter stateId
     * @param {string} value
     */
	public set stateId(value: string) {
		this._stateId = value;
	}

    /**
     * Setter comments
     * @param {string} value
     */
	public set comments(value: string) {
		this._comments = value;
	}

    /**
     * Setter creation_timestamp
     * @param {number} value
     */
	public set creation_timestamp(value: number) {
		this._creation_timestamp = value;
	}

    /**
     * Setter last_updated_timestamp
     * @param {number} value
     */
	public set last_updated_timestamp(value: number) {
		this._last_updated_timestamp = value;
	}
    
}