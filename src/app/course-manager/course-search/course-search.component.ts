import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {  Router } from '@angular/router';

@Component({
  selector: 'app-course-search',
  templateUrl: './course-search.component.html',
  styleUrls: ['./course-search.component.css']
})
export class CourseSearchComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  
  @Input() course:any;
  @Output() emitCourse = new EventEmitter<any>();

  sendCourse() {
      /////  this.emitCourse.emit(this.course);
       this.router.navigate(['/appCourseView'],{queryParams:{courses:this.course.course_id}});
  }

}
