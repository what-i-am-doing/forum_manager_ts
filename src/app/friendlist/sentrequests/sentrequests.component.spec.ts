import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentrequestsComponent } from './sentrequests.component';

describe('SentrequestsComponent', () => {
  let component: SentrequestsComponent;
  let fixture: ComponentFixture<SentrequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentrequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentrequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
