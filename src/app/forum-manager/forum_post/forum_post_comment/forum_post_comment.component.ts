import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { CommentService } from 'src/app/services/comment.service';
//import {ForumPostEvent} from 'src/app/model/forum_Event/forumevent';
import { LikeService } from 'src/app/services/like.service';
import { Comment } from 'src/app/model/comment';
import { EventService } from 'src/app/services/event.service';
import { UserService} from 'src/app/services/user.service';
import { ImageService} from 'src/app/services/image.service';

// selector: "app-forum_post",
// templateUrl: "./forum_post.component.html",
// styleUrls: ["./forum_post.component.css"],

@Component({
  selector: "app-forum_post-comment",
  templateUrl: "./forum-post_comment.component.html",
  styleUrls: ["./forum_post_comment.component.css"],
  providers:[CommentService , EventService , LikeService,UserService]
})
export class ForumPostCommentComponent implements OnInit {

  @Input() comment:any
  @Input() counter:number;
  @Output() notReply = new EventEmitter<any>();
  showReplies:boolean = false;
  showBox:boolean = false;
  replies:Array<any> = [];
  reply: Comment = new Comment();
  like_counter:number;
  you_like: boolean = false;
  liked_event: any;
  userGroup: Array<string> = JSON.parse(localStorage.getItem("user_group"));
  userDetail:any;
  tagFound: boolean = false;
  beforePrevious: number = 0;
  previousKey: number = 0;
  currentTag: string;
  taggedUsers: Array<any> = [];
  @ViewChild("replyBox", { read: ElementRef }) replyBox: ElementRef;
  @Input() friendData: any;
  filteredFriends: Array<any> = [];
  start:number;
  end:number;
  profilePic:any;
  userPic:string;
  uFirstName:string;
  uLastName:string;
  firstName:string = localStorage.getItem("first_name");
  lastName:string = localStorage.getItem("last_name");
  constructor(private userService:UserService,private likeService:LikeService,private commentService:CommentService , private eventService:EventService,private imageService:ImageService) { }

  ngOnInit() {
    console.log(localStorage.getItem('activeProfilePic'));
    this.profilePic= localStorage.getItem('activeProfilePic');
     console.log(this.comment);
     this.like_counter = "like_counter" in this.comment?this.comment.like_counter:0;
       console.log(this.counter);
       this.commentService.getCommentByParent(this.comment.entity_id,this.comment.comment_id,0,2).subscribe(comment=>{
          console.log(comment);
          if(Object.keys(comment).length) {
            var key = Object.keys(comment)[0];
            comment[key].forEach(element => {
               this.replies.push(element);
            });
          }
          
          console.log(this.replies);
       });

       this.userService.getUserDetails(this.comment.from_user_id).subscribe(
        user=>{
          console.log(user);
          this.userDetail = user;
        }
      );

      this.likeService
      .isLikedTrue(localStorage.getItem("user"), this.comment.comment_id)
      .subscribe(like => {
        if (like.length == 1) {
          this.you_like = true;
          this.liked_event = like[0].event_id;
        }
      });

      
      this.userService.getUserDetails(this.comment.from_user_id).subscribe(
        user=>{
           this.uFirstName = user.first_name;
           this.uLastName = user.last_name;

           if(user.image_id && user.image_id !="string")
            this.imageService.getImage(user.image_id).subscribe(
              img=> this.userPic = this.URLChange(img.image_storage_url)
            );
        }
      );

  }
 
  submitReply() {
    this.counter = this.counter +1;
    this.reply.$message = this.replyBox.nativeElement.innerHTML;
    if ((this.reply.$message == "")) return;
    this.reply.$entity_id = this.comment.entity_id;
    this.reply.$comment_on_entity = "POST";
    this.reply.$from_user_id = localStorage.getItem('user');
    this.reply.$group_id = this.userGroup[0];
    this.reply.$parent_comment_id = this.comment.comment_id;
    

        this.commentService.submitComment(this.reply).subscribe(reply =>{
            console.log(reply);
            
                       this.reply.$comment_id = reply.id;
                       this.replyBox.nativeElement.innerHTML = "";
                       const tempMyObj = Object.assign({}, this.reply);
                       this.replies.push(tempMyObj);
                       this.reply.$message = '';
                       this.reply.$comment_id ='';

            this.commentService.updatePostAfterComment(this.comment.entity_id , {"comments_counter":this.counter}).subscribe(data=>{

                          var newEvent = new newEvent;
                          newEvent.$from_user_id = localStorage.getItem('user');
                          newEvent.$event_type_id = 'comment';
                          newEvent.$group_id = this.userGroup[0];
                          newEvent.$entity_id = this.comment.comment_id;
                          newEvent.$on_entity_type = 'COMMENT';
                          newEvent.$status = 'ACTIVE';
                          this.eventService.submitEvent(newEvent).subscribe(data=>{
                            console.log(data);
                          });

            });
            
        });
  }

  likeComment() { 
    
        if(this.you_like == true){
          
          this.likeService.likeComment(this.comment.comment_id,{"like_counter":this.comment.like_counter -1}).subscribe(data=>{
            this.comment.like_counter = this.comment.like_counter -1; 
            this.eventService.deleteEvent(this.liked_event).subscribe(data=>{
              console.log(data);
              this.you_like = false;
              this.liked_event = null;
            })
          });

          
        }

       else {
        if (!("like_counter" in this.comment))
        this.comment.like_counter =0;
        
        this.likeService.likeComment(this.comment.comment_id,{"like_counter":this.comment.like_counter +1}).subscribe(data => {
              console.log(data);
              this.comment.like_counter = this.comment.like_counter +1;
              var newEvent = new newEvent;
              newEvent.$from_user_id = localStorage.getItem('user');
              newEvent.$event_type_id = 'like';
              newEvent.$group_id = this.userGroup[0];
              newEvent.$entity_id = this.comment.comment_id;
              newEvent.$on_entity_type = 'COMMENT';
              newEvent.$status = 'ACTIVE';

              this.eventService.submitEvent(newEvent).subscribe(data=>{
                console.log(data);
                this.you_like = true;
                this.liked_event = data.id;
              });
        }); 
       } 

      
  }

  searchFriend(event: any) {
    console.log(this.replyBox.nativeElement.innerHTML);
    if (event.keyCode == 13) {
      event.preventDefault();
      this.replyBox.nativeElement.innerHTML = this.replyBox.nativeElement.innerHTML.replace(
        "<div><br></div>",
        ""
      );
     return this.submitReply();
    }
    this.reply.$message = this.replyBox.nativeElement.innerHTML;
    var str = this.reply.$message;

    if (this.tagFound === true) {
      console.log("found");
      if (str.includes("@")) {
        var str = str.substring(str.lastIndexOf("@") + 1);
        this.currentTag = str;
        console.log(this.filteredFriends);
        this.filteredFriends = this.friendData.filter(word =>
          word.user_full_text_name.startsWith(str)
        );
        console.log(this.filteredFriends);
      } else this.tagFound = false;
    } else if (
      (this.reply.$message == "@" ||
        this.beforePrevious == 32 ||
        this.beforePrevious == 8) &&
      (event.key == 2 && this.previousKey == 16)
    ) {
        this.tagFound = true;
        this.start = str.length-1;
        if(this.friendData.length>6) this.filteredFriends = this.friendData.slice(0,6);
        else this.filteredFriends = this.friendData;
        this.currentTag = str.substring(str.lastIndexOf("@") + 1);
    }
      

    this.beforePrevious = this.previousKey;
    this.previousKey = event.keyCode;
  }

  selectFriend(friend: any) {
    var reply = this.replyBox.nativeElement.innerHTML;
    this.end =  reply.length;
    this.replyBox.nativeElement.innerHTML = this.replaceBetween(this.start , this.end ,"<a contenteditable='false'>@" + friend.user_full_text_name + "</a> &nbsp;" ,reply);
    
    
    this.tagFound = false;
    this.taggedUsers.push({
      content:
        '<a contenteditable="false">@' + friend.user_full_text_name + "</a>",
      id: friend.user_id
    });
    console.log(this.replyBox.nativeElement.innerHTML);
    console.log(this.taggedUsers[0].content);

    console.log(
      this.replyBox.nativeElement.innerHTML.includes(
        this.taggedUsers[0].content
      )
    );
    this.tagFound = false;
    
  }

  replaceBetween = function(start, end, what,string) {
    return string.substring(0, start) + what + string.substring(end);
  };

  emitReply() {
    this.notReply.emit(false);
  }

  URLChange(img: any) {
    return img.replace("https", "http");
  }
}
