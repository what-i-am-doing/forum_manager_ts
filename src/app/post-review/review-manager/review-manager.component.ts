import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { PostService } from 'src/app/services/post.service';

@Component({
  selector: 'app-review-manager',
  templateUrl: './review-manager.component.html',
  styleUrls: ['./review-manager.component.css']
})
export class ReviewManagerComponent implements OnInit {

  constructor(private postService: PostService) { }

  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  dtOptions: DataTables.Settings[] = [];
  dtTrigger: Subject<any>[] = [];

  userPosts: Array<any> = [];
  postReview: any;
  currentPost:any;
  reasonToRej: string;
  ngOnInit() {
    this.dtTrigger["new"] = new Subject<any>();
    this.dtOptions["new"] = {
      pagingType: 'full_numbers',
      pageLength: 10,
      "columnDefs": [
        { "width": "5%", "targets": 0 },
        { "width": "15%", "targets": 1 },
        { "width": "15%", "targets": 2 },
        { "width": "33%", "targets": 3 },
        { "width": "7%", "targets": 4 },
        { "width": "5%", "targets": 5 },
        { "width": "10%", "targets": 6 },
        { "width": "10%", "targets": 7 },
      ]

    };

    this.postService.getAllPosts("public", 0, 10).subscribe(
      data => {
        console.log(data);
        this.userPosts = data;
        setTimeout(() => {
          this.dtTrigger['new'].next();
        });

      }
    );
  }

  approvePost(post:any) {
    if (post.status == "UNDER_REVIEW" || post.status == "BLOCKED") {
      this.postService.updatePost(post.post_id, { "status": "ACTIVE" }).subscribe(
        ()=> post.status = "ACTIVE"
      );
    }
  }

  rejectPost() {
    var post = this.currentPost;
    if (post.status == "UNDER_REVIEW" || post.status == "ACTIVE") {
      this.postService.updatePost(post.post_id, { "status": "BLOCKED" }).subscribe(
        ()=> post.status = "BLOCKED"
      );
    }
    
  }

}
