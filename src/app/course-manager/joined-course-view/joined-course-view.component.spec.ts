import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinedCourseViewComponent } from './joined-course-view.component';

describe('CourseViewComponent', () => {
  let component: JoinedCourseViewComponent;
  let fixture: ComponentFixture<JoinedCourseViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinedCourseViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinedCourseViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
