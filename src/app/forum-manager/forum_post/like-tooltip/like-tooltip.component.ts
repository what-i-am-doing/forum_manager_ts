import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-like-tooltip',
  templateUrl: './like-tooltip.component.html',
  styleUrls: ['./like-tooltip.component.css'],
  providers:[UserService]
})
export class TooltipComponent implements OnInit {
  @Input() likeData: any;
  public userData:any;
  constructor(private userService:UserService,private  router:Router) { }

  ngOnInit() {
    console.log('like tool tip')
    console.log(this.likeData);
   // this.getUsersDetail();
  }

  getUsersDetail(){
    console.log(this.likeData.from_user_id);
    this.userService.getuserDetailsDataByUserId(this.likeData.from_user_id).subscribe(data => {
    console.log(' tooltip user data');
    console.log(data);
    if(data.length){
      console.log(data[0].user_full_text_name);
      this.userData=data[0];
    }
    
    })
  }
  seeFriendProfile(){
    this.router.navigate(['/home/userprofile'], { queryParams: { userId:this.likeData.from_user_id} });
  }
}
