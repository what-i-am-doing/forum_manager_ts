
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class FriendService {
  public url = environment.api_url;
  constructor(private http: HttpClient) { }
  getSentFriendRequestByActiveUser(userId): Observable<any> {
    
    return this.http.get<any>(this.url + '/friend_request/from_user/' + userId + '?size=100', {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'from_user_id': userId,
      })
    })
      .pipe(
        catchError(this.handleError)
      );
  }
  getSentFriendRequestByStatus(userId,groupId,status): Observable<any> {

    return this.http.get<any>(this.url + '/friend_request/from_user/' + userId + '/request_status/'+status+'?group_id='+groupId+'&from='+0+'&size='+100 , {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'from_user_id': userId
      })
    })
      .pipe(
        catchError(this.handleError)
      );

  }

  getSentFriendRequestByStatusAtInterval(userId:string, groupId:string, status:string,starttime:number,endtime:number,from:number, to:number): Observable<any> {


      return this.http.get<any>(this.url +  '/friend_request/for_user/' + userId + '/request_status/'+status+'?group_id='+groupId+'&time_start='+starttime+'&time_end='+endtime+'&from='+from+'&size='+to, {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'from_user_id': userId
        })
      })
        .pipe(
          catchError(this.handleError)
        );
  }

  deleteFriendRequest(friendRequestId, userId): Observable<any> {
    return this.http.delete<any>(this.url + '/friend_request/' + friendRequestId, {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'from_user_id': userId
      })
    })
      .pipe(
        catchError(this.handleError)
      );
  }

  deleteFriend(user:string , friend:string):Observable<any> {

    return this.http.delete<any>(this.url + '/friend/delete_friend/user/'+user+'/friend/'+friend+'?group_id=public', {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'from_user_id': user
      })
    })
      .pipe(
        catchError(this.handleError)
      );

  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

}
