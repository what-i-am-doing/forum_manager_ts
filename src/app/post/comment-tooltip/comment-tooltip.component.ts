import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-comment-tooltip',
  templateUrl: './comment-tooltip.component.html',
  styleUrls: ['./comment-tooltip.component.css'],
  providers:[UserService]
})
export class CommentTooltipComponent implements OnInit {
  @Input() commentData: any;
  public userData:any;
  constructor(private userService:UserService,private router:Router) { }

  ngOnInit() {
    console.log('commentData tool tip')
    console.log(this.commentData);
    this.getUsersDetail();
  }

  getUsersDetail(){
    console.log(this.commentData.from_user_id);
    this.userService.getuserDetailsDataByUserId(this.commentData.from_user_id).subscribe(data => {
    console.log(' tooltip user data');
    console.log(data);
    console.log(data[0].user_id);
    console.log(data[0].user_full_text_name);
    this.userData=data[0];
    this.getComment();
    })
  }
  seeFriendProfile(){
    this.router.navigate(['/home/userprofile'], { queryParams: { userId:this.commentData.from_user_id} });
  }

  @Output() gotComment = new EventEmitter();
  public counter = 0;
  getComment() { 
   
    
      this.counter = this.counter + 1;
      this.gotComment.emit(this.counter);

  }
}
