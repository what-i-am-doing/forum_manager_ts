export class UserInterests {


    constructor( ){};

    private _interest_id:string;
    private _interest_name:string;
    private _img_id:string;
    private _comments:string;
    private _stateId:string;
    private _creation_timestamp:number;
    private _last_updated_timestamp:number;


    /**
     * Getter interest_id
     * @return {string}
     */
	public get interest_id(): string {
		return this._interest_id;
	}

    /**
     * Getter interest_name
     * @return {string}
     */
	public get interest_name(): string {
		return this._interest_name;
	}

    /**
     * Getter img_id
     * @return {string}
     */
	public get img_id(): string {
		return this._img_id;
	}

    /**
     * Getter comments
     * @return {string}
     */
	public get comments(): string {
		return this._comments;
	}

    /**
     * Getter stateId
     * @return {string}
     */
	public get stateId(): string {
		return this._stateId;
	}

    /**
     * Getter creation_timestamp
     * @return {number}
     */
	public get creation_timestamp(): number {
		return this._creation_timestamp;
	}

    /**
     * Getter last_updated_timestamp
     * @return {number}
     */
	public get last_updated_timestamp(): number {
		return this._last_updated_timestamp;
	}

    /**
     * Setter interest_id
     * @param {string} value
     */
	public set interest_id(value: string) {
		this._interest_id = value;
	}

    /**
     * Setter interest_name
     * @param {string} value
     */
	public set interest_name(value: string) {
		this._interest_name = value;
	}

    /**
     * Setter img_id
     * @param {string} value
     */
	public set img_id(value: string) {
		this._img_id = value;
	}

    /**
     * Setter comments
     * @param {string} value
     */
	public set comments(value: string) {
		this._comments = value;
	}

    /**
     * Setter stateId
     * @param {string} value
     */
	public set stateId(value: string) {
		this._stateId = value;
	}

    /**
     * Setter creation_timestamp
     * @param {number} value
     */
	public set creation_timestamp(value: number) {
		this._creation_timestamp = value;
	}

    /**
     * Setter last_updated_timestamp
     * @param {number} value
     */
	public set last_updated_timestamp(value: number) {
		this._last_updated_timestamp = value;
	}
    
}