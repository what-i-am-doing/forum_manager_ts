import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockrequestsComponent } from './blockrequests.component';

describe('BlockrequestsComponent', () => {
  let component: BlockrequestsComponent;
  let fixture: ComponentFixture<BlockrequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockrequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockrequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
