import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {  Router } from '@angular/router';
import { Course } from "../../model/course";
import { CourseService } from "../../services/course.service";
@Component({
  selector: 'app-module-search',
  templateUrl: './module-search.component.html',
  styleUrls: ['./module-search.component.css']
})

export class ModuleSearchComponent implements OnInit {

  constructor(private router:Router,
    private courseService: CourseService,) {

    
   }

  ngOnInit() {
  }
  
  @Input() module:any;
  @Output() emitModule = new EventEmitter<any>();

  sendModule() {
        this.emitModule.emit(this.module);
       this.router.navigate(['/appModuleView'],{queryParams:{modules:this.module.module_id}});
  }

 
    

  }

