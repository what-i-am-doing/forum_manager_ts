import { Pipe,PipeTransform } from "@angular/core";


@Pipe({

    'name':'streamTypes'
})

export class StreamTypesPipe implements PipeTransform{

transform(value , search ){

   return value.filter(stream => stream.stream_id === search)[0].stream_name
}

}