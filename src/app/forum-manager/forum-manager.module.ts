import { NgModule, PipeTransform, Pipe} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { MatIconModule,MatFormFieldModule,MatInputModule } from "@angular/material";
import { InfiniteScrollModule } from 'node_modules/ngx-infinite-scroll';
import { MatCardModule } from '@angular/material';
import { MatButtonModule} from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { MatStepperModule} from '@angular/material/stepper';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ForumManagerComponent } from './forum-manager/forum-manager.component';
import { ForumViewComponent } from './forum-view/forum-view.component';
//import { ForumPostComponent } from './forum_post/forum_post/forum_post.component';
import { ForumPostViewComponent } from './forum_post-view/forum_post-view.component';
import { ForumSearchComponent } from './forum-search/forum-search.component';
import { ForumPostSearchComponent } from './forum_post-search/forum_post-search.component';
import { ForumNavBarComponent } from './forum-nav-bar/forum-nav-bar.component';
import { MatTreeModule } from '@angular/material/tree';
import { Routes ,RouterModule} from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
//import { CreateForumPostComponent } from './forum_post/create-forum_post/create-forum_post.component';
import { ForumPostModule } from './forum_post/forum_post.module';
//import { ThumbnailComponent } from './forum_post/thumbnail/thumbnail.component';
  @Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) { }

  transform(url) {
    var url = url.replace('watch?v=', 'embed/');
    url = url + '' + '?modestbranding=1&;showinfo=0&;autohide=1&;rel=0;'
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}

const routes:Routes=[
  {path:'appforumpostview',component:ForumPostViewComponent},
  { path:'', component:ForumPostViewComponent},  
]
@NgModule({
  declarations: [ForumManagerComponent, ForumViewComponent, ForumNavBarComponent, ForumSearchComponent, ForumPostSearchComponent, ForumPostViewComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule, InfiniteScrollModule,
    MatIconModule,MatFormFieldModule,MatInputModule,MatCardModule,
    MatButtonModule,MatTabsModule,MatStepperModule,NgMultiSelectDropDownModule,MatTreeModule,
    RouterModule.forChild(routes),
//    CreateForumPostComponent,
    ForumPostModule,
  ],
  exports:[ForumManagerComponent, ForumViewComponent, ForumPostSearchComponent, ForumSearchComponent, InfiniteScrollModule, ForumPostViewComponent,ReactiveFormsModule]
})


export class ForumManagerModule {
  
 }
