import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PostComponent } from "./post/post.component";
import { NgxExtendedPdfViewerModule } from "ngx-extended-pdf-viewer";
import { AutoCompleteComponent } from "../auto-complete/auto-complete.component";
import { EmojiModule } from "../../app/lib/emoji/emoji.module";
import { PickerModule } from "../../app/lib/picker/picker.module";
import { ReactiveFormsModule } from "@angular/forms";
import { ThumbnailComponent } from "../shared/thumbnail/thumbnail.component";

import {
  MatIconModule,
  MatButtonModule,
  MatMenuModule,
  MatFormFieldModule,
  MatInputModule,
  MatAutocompleteModule
} from "@angular/material";
import { FormsModule } from "@angular/forms";
import { LightboxModule } from "ngx-lightbox";
import { CommentComponent } from "./comment/comment.component";
import { CreatePostComponent } from "./create-post/create-post.component";
import { UploadThumbnailComponent } from "./upload-thumbnail/upload-thumbnail.component";
import { EditorModule } from "@tinymce/tinymce-angular";
import { MatChipsModule } from "@angular/material/chips";
import { HoverListComponent } from './hover-list/hover-list.component';
import { TooltipComponent } from './like-tooltip/like-tooltip.component';
import { ShareTooltipComponent } from './share-tooltip/share-tooltip.component';
import { CommentTooltipComponent } from './comment-tooltip/comment-tooltip.component';
import { ReportTooltipComponent } from './report-tooltip/report-tooltip.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { LikeModalComponent } from './post/like-modal/like-modal.component';
import { SharePostComponent } from './share-post/share-post.component';
import { PostSnapshotComponent } from './post-snapshot/post-snapshot.component';

@NgModule({
  declarations: [
    PostComponent,
    CommentComponent,
    CreatePostComponent,
    UploadThumbnailComponent,
    AutoCompleteComponent,
    ThumbnailComponent,
    HoverListComponent,
    TooltipComponent,
    ShareTooltipComponent,
    CommentTooltipComponent,
    ReportTooltipComponent,
    LikeModalComponent,
    SharePostComponent,
    PostSnapshotComponent
    
  ],
  imports: [
    CommonModule,
    FormsModule,
    LightboxModule,
    EditorModule,
    NgxExtendedPdfViewerModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    EmojiModule,
    PickerModule,
    MatChipsModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    TooltipModule,
    ModalModule
  ],
  exports: [PostComponent, CreatePostComponent]
})
export class PostModule {}
