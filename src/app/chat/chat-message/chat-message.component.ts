import { Component, OnInit, Input, ViewChild, ElementRef, ChangeDetectorRef, HostListener, Output ,EventEmitter} from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';


@Component({
  selector: 'app-chat-message',
  templateUrl: './chat-message.component.html',
  styleUrls: ['./chat-message.component.css']
})
export class ChatMessageComponent implements OnInit {

  constructor(private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    console.log(this.msg);
  }

  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild("contextMenu", { read: ElementRef }) contextMenu: ElementRef;
  @Input() msg: any;
  @Output() emitmsg = new EventEmitter<any>();
  myID: string = localStorage.getItem("user");
  show:boolean = false;
  
  triggerMenu() { this.trigger.openMenu(); }

  replyMsg(){
      this.emitmsg.emit(this.msg);
  }

  
}
