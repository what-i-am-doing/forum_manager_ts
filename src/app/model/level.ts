export class Level {

constructor(){};

private comments: string;
private creation_timestamp: number;
private custom_tag_line: string;
private group_id: string;
private image_id: string;
private last_updated_timestamp: number;
private level_admin_user_id: string;
private level_id: string;
private level_name: string;
private level_type_id: string;
private sequence_number: number;

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}
private status: string;
    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $creation_timestamp
     * @return {number}
     */
	public get $creation_timestamp(): number {
		return this.creation_timestamp;
	}

    /**
     * Getter $custom_tag_line
     * @return {string}
     */
	public get $custom_tag_line(): string {
		return this.custom_tag_line;
	}

    /**
     * Getter $group_id
     * @return {string}
     */
	public get $group_id(): string {
		return this.group_id;
	}

    /**
     * Getter $image_id
     * @return {string}
     */
	public get $image_id(): string {
		return this.image_id;
	}

    /**
     * Getter $last_updated_timestamp
     * @return {number}
     */
	public get $last_updated_timestamp(): number {
		return this.last_updated_timestamp;
	}

    /**
     * Getter $level_admin_user_id
     * @return {string}
     */
	public get $level_admin_user_id(): string {
		return this.level_admin_user_id;
	}

    /**
     * Getter $level_id
     * @return {string}
     */
	public get $level_id(): string {
		return this.level_id;
	}

    /**
     * Getter $level_name
     * @return {string}
     */
	public get $level_name(): string {
		return this.level_name;
	}

    /**
     * Getter $level_type_id
     * @return {string}
     */
	public get $level_type_id(): string {
		return this.level_type_id;
	}

    /**
     * Getter $sequence_number
     * @return {number}
     */
	public get $sequence_number(): number {
		return this.sequence_number;
	}

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $creation_timestamp
     * @param {number} value
     */
	public set $creation_timestamp(value: number) {
		this.creation_timestamp = value;
	}

    /**
     * Setter $custom_tag_line
     * @param {string} value
     */
	public set $custom_tag_line(value: string) {
		this.custom_tag_line = value;
	}

    /**
     * Setter $group_id
     * @param {string} value
     */
	public set $group_id(value: string) {
		this.group_id = value;
	}

    /**
     * Setter $image_id
     * @param {string} value
     */
	public set $image_id(value: string) {
		this.image_id = value;
	}

    /**
     * Setter $last_updated_timestamp
     * @param {number} value
     */
	public set $last_updated_timestamp(value: number) {
		this.last_updated_timestamp = value;
	}

    /**
     * Setter $level_admin_user_id
     * @param {string} value
     */
	public set $level_admin_user_id(value: string) {
		this.level_admin_user_id = value;
	}

    /**
     * Setter $level_id
     * @param {string} value
     */
	public set $level_id(value: string) {
		this.level_id = value;
	}

    /**
     * Setter $level_name
     * @param {string} value
     */
	public set $level_name(value: string) {
		this.level_name = value;
	}

    /**
     * Setter $level_type_id
     * @param {string} value
     */
	public set $level_type_id(value: string) {
		this.level_type_id = value;
	}

    /**
     * Setter $sequence_number
     * @param {number} value
     */
	public set $sequence_number(value: number) {
		this.sequence_number = value;
	}


}