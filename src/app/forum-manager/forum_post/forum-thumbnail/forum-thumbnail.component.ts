import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-forum-thumbnail',
  templateUrl: './forum-thumbnail.component.html',
  styleUrls: ['./forum-thumbnail.component.css']
})
export class ForumThumbnailComponent implements OnInit {

  constructor() {}

  ngOnInit() {}
  
  @Input() info:any;
  @Input() edit:boolean;
  @Input() index:number;
  @Input() share?:boolean;
  @Input() isPreview?:boolean;
  @Output() emitIndex= new EventEmitter<any>();
  eventType:string;
  showPreview:boolean = false;

  URLChange(pdf: any) {
    return pdf.replace("https", "http");
  }

  changeSelection(event:any){
       console.log(event);
       if(event.target.checked) this.emitIndex.emit({
            index:this.index,
            type:this.eventType
       });
       else this.emitIndex.emit({
            index:0,
            type:this.eventType
   });
  }

}
