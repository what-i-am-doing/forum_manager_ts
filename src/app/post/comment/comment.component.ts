import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { CommentService } from '../../services/comment.service';
import { LikeService } from '../../services/like.service';
import { Comment } from '../../model/comment';
import { tagMapping } from '../../model/tag-mapping';
import { PostEvent } from 'src/app/model/event';
import { EventService } from '../../services/event.service';
import { UserService } from '../../services/user.service';
import { ImageService } from '../../services/image.service';
import { TagMappingService } from '../../services/tag-mapping.service';


@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css'],
  providers: [CommentService, EventService, LikeService, UserService]
})
export class CommentComponent implements OnInit {

  @Input() comment: any;
  @Input() counter: number;
  @Output() notReply = new EventEmitter<any>();
  showReplies: boolean = false;
  showBox: boolean = false;
  replies: Array<any> = [];
  reply: Comment = new Comment();
  like_counter: number;
  you_like: boolean = false;
  liked_event: any;
  userGroup: Array<string> = JSON.parse(localStorage.getItem("user_group"));
  userDetail: any;
  tagFound: boolean = false;
  beforePrevious: number = 0;
  previousKey: number = 0;
  currentTag: string;
  taggedUsers: Array<any> = [];
  @ViewChild("replyBox", { read: ElementRef }) replyBox: ElementRef;
  @Input() friendData: any;
  filteredFriends: Array<any> = [];
  start: number;
  profilePic: any;
  userPic: string;
  uFirstName: string;
  uLastName: string;
  firstName: string = localStorage.getItem("first_name");
  lastName: string = localStorage.getItem("last_name");


  constructor(private userService: UserService, private likeService: LikeService, private commentService: CommentService,
    private eventService: EventService, private imageService: ImageService,
    private tagMappingService: TagMappingService
    ) { }

  ngOnInit() {
    this.profilePic = localStorage.getItem('activeProfilePic');
    this.like_counter = "like_counter" in this.comment ? this.comment.like_counter : 0;

    this.commentService.getCommentByParent(this.comment.entity_id, this.comment.comment_id, 0, 2).subscribe(comment => {
      if (Object.keys(comment).length) {
        var key = Object.keys(comment)[0];
        comment[key].forEach(element => {
          this.replies.push(element);
        });
      }

    });

    this.userService.getUserDetails(this.comment.from_user_id).subscribe(
      user => {
        this.userDetail = user;
      }
    );

    this.likeService
      .isLikedTrue(localStorage.getItem("user"), this.comment.comment_id)
      .subscribe(like => {
        if (like.length == 1) {
          this.you_like = true;
          this.liked_event = like[0].event_id;
        }
      });


    this.userService.getUserDetails(this.comment.from_user_id).subscribe(
      user => {
        this.uFirstName = user.first_name;
        this.uLastName = user.last_name;

        if (user.image_id && user.image_id != "string")
          this.imageService.getImage(user.image_id).subscribe(
            img => this.userPic = this.URLChange(img.image_storage_url)
          );
      }
    );

  }



  submitReply() {
    this.counter = this.counter + 1;
    this.reply.$message = this.replyBox.nativeElement.innerHTML;
    if ((this.reply.$message == "")) return;
    this.reply.$entity_id = this.comment.entity_id;
    this.reply.$comment_on_entity = "POST";
    this.reply.$from_user_id = localStorage.getItem('user');
    this.reply.$group_id = this.userGroup[0];
    this.reply.$parent_comment_id = this.comment.comment_id;


    this.commentService.submitComment(this.reply).subscribe(reply => {
      console.log(reply);

      this.reply.$comment_id = reply.id;
      this.replyBox.nativeElement.innerHTML = "";
      const tempMyObj = Object.assign({}, this.reply);
      this.replies.push(tempMyObj);
      this.reply.$message = '';
      this.reply.$comment_id = '';

      if (this.taggedUsers.length) {
        this.reply.$tagged_mapping_list = [];
        
        var temp_tags = this.taggedUsers;
        this.taggedUsers = [];
        temp_tags.forEach(element => {
          var tag:any[] = Object.values(element);
          if (
            this.reply.$message.includes(tag[0].toString())
          ) {
            
            var tag_mapping = new tagMapping;
            tag_mapping.$creation_timestamp = +new Date;
            tag_mapping.$groupId = this.userGroup[0];
            tag_mapping.$on_entity_type = 'COMMENT';
            tag_mapping.$on_entity_id = reply.id;
            tag_mapping.$status = "ACTIVE";
            tag_mapping.$tag_name = tag[2];
            tag_mapping.$tag_start_index = tag[3];
            tag_mapping.$tagged_entity_id = tag[1];
            tag_mapping.$tagged_entity_type = 'USER';
            this.tagMappingService.createTagMap(tag_mapping).subscribe(
              tag_map => {
                  this.reply.$tagged_mapping_list.push(tag_map.id);
                  this.commentService.updateComment(reply.id, {"tagged_mapping_list":this.reply.$tagged_mapping_list}).subscribe()
              }
            );
            
          }
        });
  
        
      }

      this.commentService.updatePostAfterComment(this.comment.entity_id, { "comments_counter": this.counter }).subscribe(() => {

        var newEvent = new PostEvent;
        newEvent.$from_user_id = localStorage.getItem('user');
        newEvent.$event_type_id = 'comment';
        newEvent.$group_id = this.userGroup[0];
        newEvent.$entity_id = this.comment.comment_id;
        newEvent.$on_entity_type = 'COMMENT';
        newEvent.$status = 'ACTIVE';
        this.eventService.submitEvent(newEvent).subscribe(data => {
          console.log(data);
        });

      });

    });
  }

  likeComment() {

    if (this.you_like == true) {

      this.likeService.likeComment(this.comment.comment_id, { "like_counter": this.comment.like_counter - 1 }).subscribe(data => {
        this.comment.like_counter = this.comment.like_counter - 1;
        this.eventService.deleteEvent(this.liked_event).subscribe(data => {
          console.log(data);
          this.you_like = false;
          this.liked_event = null;
        })
      });


    }

    else {
      if (!("like_counter" in this.comment))
        this.comment.like_counter = 0;

      this.likeService.likeComment(this.comment.comment_id, { "like_counter": this.comment.like_counter + 1 }).subscribe(data => {
        console.log(data);
        this.comment.like_counter = this.comment.like_counter + 1;
        var newEvent = new PostEvent;
        newEvent.$from_user_id = localStorage.getItem('user');
        newEvent.$event_type_id = 'like';
        newEvent.$group_id = this.userGroup[0];
        newEvent.$entity_id = this.comment.comment_id;
        newEvent.$on_entity_type = 'COMMENT';
        newEvent.$status = 'ACTIVE';

        this.eventService.submitEvent(newEvent).subscribe(data => {
          console.log(data);
          this.you_like = true;
          this.liked_event = data.id;
        });
      });
    }


  }

  searchFriend(event: any) {

    if (event.keyCode == 13) {
      event.preventDefault();
      this.replyBox.nativeElement.innerHTML = this.replyBox.nativeElement.innerHTML.replace(
        "<div><br></div>",
        ""
      );
      return this.submitReply();
    }

    this.reply.$message = this.replyBox.nativeElement.innerHTML;
    var reply = this.reply.$message;

    if (this.tagFound === true) {
      console.log("found");
      if (reply.includes("@")) {
        this.start = reply.lastIndexOf("@");
        var tag = reply.substring(this.start + 1); //my name is @vijay then tag = vijay
        this.currentTag = tag;

        this.filteredFriends = this.friendData.filter(word =>
          word.user_full_text_name.startsWith(tag)
        );

      } else this.tagFound = false;

    }

    else if (
      (this.reply.$message == "@" ||
        this.beforePrevious == 32 ||
        this.beforePrevious == 8) &&
      (event.key == 2 && this.previousKey == 16)
    ) {
      this.tagFound = true;
      this.start = reply.lastIndexOf("@");
      //this.start = str.length-1; // my name is @ then start = 11 as length is 12
      if (this.friendData.length > 6) this.filteredFriends = this.friendData.slice(0, 6);
      else this.filteredFriends = this.friendData.slice(0);
      // this.currentTag = str.substring(str.lastIndexOf("@") + 1);

    }

    this.beforePrevious = this.previousKey;
    this.previousKey = event.keyCode;

  }

  selectFriend(friend: any) {
    var reply = this.replyBox.nativeElement.innerHTML;
    this.replyBox.nativeElement.innerHTML = this.replaceBetween(this.start, "<a contenteditable='false'>" + friend.user_full_text_name + "</a> &nbsp;", reply);

    // this.replyBox.nativeElement.innerHTML = reply.replace(
    //   "@" + this.currentTag,
    //   "<a contenteditable='false'>@" + friend.user_full_text_name + "</a> &nbsp;"
    // );

    // this.tagFound = false;
    this.taggedUsers.push({
      content: '<a contenteditable="false">' + friend.user_full_text_name + "</a>",
      id: friend.user_id,
      name:friend.user_full_text_name,
      start: this.start
      
    });

    console.log(this.taggedUsers);

    this.taggedUsers.forEach((element, index) => {
      console.log(this.replyBox.nativeElement.innerHTML.includes(
        this.taggedUsers[index].content
      ))
    })

    this.tagFound = false;

  }

  replaceBetween =  (start, what, string) => {
    console.log(start, what, string);
    let length = this.currentTag?this.currentTag.length:0;
    return string.substring(0, start) + what + string.substring(start + length + 1);
  };

  emitReply() {
    this.notReply.emit(false);
  }

  URLChange(img: any) {
    return img.replace("https", "http");
  }
}
