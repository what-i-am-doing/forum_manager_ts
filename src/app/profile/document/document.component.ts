import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Output, EventEmitter } from '@angular/core'
import { UserDocument } from '../../model/UserDocument';
import { Content } from '../../model/content';
import { PostService } from '../../services/post.service';
import { UserPersonal } from '../../model/userPersonal';
import { ImageService } from '../../services/image.service';
import { PdfService } from '../../services/pdf.service';
import { Subscription, Observable } from 'rxjs';
import { Lightbox, LightboxConfig, LightboxEvent, LIGHTBOX_EVENT, IEvent, IAlbum } from 'ngx-lightbox';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.css'],
  providers: [UserService, ImageService, PdfService, Content, PostService]
})

export class DocumentComponent implements OnInit {

  private api_url = environment.api_url;
  @Output() deleteDocumentEvent = new EventEmitter<string>();
  @Output() updateDocumentEvent = new EventEmitter<string>();
  @Input() receivedUserDocumentData: any;
  public userId: any;
  public user_document_id: any;
  public edittogle: boolean = false;
  public kyc_types: any;
  public mobileFriendlyZoomPercent = false;
  public doc_name_by_user: any;
  private _subscription: Subscription;
  public content = new Content();
  public src: any;
  public hidden: boolean = true;
  ifMax: boolean = false;
  public albums: Array<IAlbum> = [];
  public pdfalbums = [];
  public href: any;
  public pdfhref: any;
  public pdfs: Array<string> = [];
  public userDoc = new UserDocument();
  public deleteDocumentData: boolean = false;
  public doc_description: any;
  public showProgressbar: boolean = false;
  public showUpload: boolean = true;
  public progressvalue: number;
  public uploading_msg: any;
  public uploaddocBtn: boolean = true;
  public documentUpdate: boolean = false;
  public pdfSrc: any;
  public showEdit: boolean = false;
  myControl = new FormControl();
  public doctype: any;
  public docname: any;
  public doc_descrip: any;
  public unsupportedFormat: boolean = false;
  public imageContains = false;
  public pdfContains = false;
  @ViewChild("upload", { read: ElementRef }) upload: ElementRef;

  options: string[] = ['Adhaar Card', 'PAN Card', 'Passport', 'Voter Id', 'Driving License', 'marksheet'];
  filteredOptions: Observable<string[]>;
  constructor(private userService: UserService, private postService: PostService, private pdfService: PdfService, private imageService: ImageService, private _lightbox: Lightbox, private _lightboxEvent: LightboxEvent, private _lighboxConfig: LightboxConfig) {

    var userData = JSON.parse(localStorage.getItem('LoggedInUserData'));

    this.userId = userData.user_id;

    this.content.$content_images_list = [];

  }

  ngOnInit() {

    this.user_document_id = this.receivedUserDocumentData.user_document_id;
    this.docname = this.receivedUserDocumentData.doc_name_by_user;
    this.doc_descrip = this.receivedUserDocumentData.doc_description;
    

    if (this.receivedUserDocumentData) {
      this.myControl.setValue(this.receivedUserDocumentData.doc_type_id);
      this.doctype = this.myControl.value;
      this.getDocumentData();
    }

   this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );


  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
}


  toggleme(): void { this.edittogle = !this.edittogle; }

 updateUserDocument() {
    
 this.postService.updateusercontent(this.receivedUserDocumentData.document_content, this.content).subscribe(
      (data) => {
        var contentId = data.id
        this.receivedUserDocumentData.document_content = contentId;
        this.receivedUserDocumentData.doc_description = this.doc_descrip;
        this.receivedUserDocumentData.doc_name_by_user = this.docname;
        this.myControl.setValue(this.doctype);
         this.doctype = this.myControl.value;
        this.receivedUserDocumentData.user_document_id = this.user_document_id;

        var documentobj = {
          "comments": "updating a document",
          "creation_timestamp": Date.now(),
          "doc_description": this.doc_descrip,
          "doc_name_by_user": this.docname,
          "doc_type_id": this.myControl.value,
          "document_content": contentId,
          "last_updated_timestamp": Date.now(),
          "status": "ACTIVE",
          "user_id": this.userId

        }


        this.userService.updateUserdocument(this.receivedUserDocumentData.user_document_id, documentobj).subscribe(data => {

          this.showEdit = false;
          this.documentUpdate = false;
          this.albums = [];
          this.pdfalbums = [];
          this.getDocumentData();

        })
      })
  }

  open(index: number, album): void {
    this._subscription = this._lightboxEvent.lightboxEvent$.subscribe((event: IEvent) => this._onReceivedEvent(event));
    this._lightbox.open(album, index, { wrapAround: true, showImageNumberLabel: true });
  }

  close(): void {
    this._lightbox.close();
  }

  private _onReceivedEvent(event: IEvent): void {
    if (event.id === LIGHTBOX_EVENT.CLOSE) {
      this._subscription.unsubscribe();
    }
  }

  getDocumentData() {
    
    this.postService.getPostContent(this.receivedUserDocumentData.document_content).subscribe(
      (datalist) => {
        console.log(datalist);
        if (datalist.content_images_list && datalist.content_images_list.length) {
          this.imageContains = true;
          this.content.$content_images_list = datalist.content_images_list;
          datalist.content_images_list.forEach(element => {
            this.imageService.getImage(element).subscribe(
              imgData => {
                console.log(imgData);
                const src = imgData.image_storage_url.replace("https", "http");
                const caption = imgData.image_name;
                const thumb = imgData.image_storage_url.replace("https", "http");
                const album = {
                  src: src,
                  caption: caption,
                  thumb: thumb,
                  id: imgData.image_id
                };
                this.receivedUserDocumentData._albums = [album];
                this.receivedUserDocumentData.image_url = imgData.image_storage_url.replace("https", "http");
                this.src = thumb;
              }
            );
          });
        }

        else if (datalist.content_pdf_list && datalist.content_pdf_list.length) {
          this.pdfContains = true;
          this.content.$content_pdf_list = datalist.content_pdf_list;
          datalist.content_pdf_list.forEach(element => {
            this.pdfService.getPdf(element).subscribe(
              pdfData => { 
                console.log(pdfData);
                this.pdfs.push(pdfData);
                const src = pdfData.pdf_storage_url.replace("https", "http");
                const pdfname = pdfData.pdf_name;
                const pdfalbum = {
                  src: src,
                  pdfname: pdfname,
                  pdfid: pdfData.pdf_id
                };
                this.receivedUserDocumentData.pdf_url = src;
                this.pdfalbums.push(pdfalbum);
                
              }
            );
          });
        }
      }
    );

  }

  downloadImage(imageId) {
    console.log(imageId);
    this.href = this.api_url + '/image/download/' + imageId;

  }

  downloadPdf(pdfId) {

    this.pdfhref = this.api_url + '/pdf/download/' + pdfId;
  }

  deleteDocument(documentId) {

    this.userService.deleteUserDocument(documentId, this.userId).subscribe(data => {

      console.log(data);

      delete this.receivedUserDocumentData.user_document_id,

        setTimeout(() => this.deleteDocumentData = false, 2500);

    })
  }

  public get mobileFriendlyZoom(): string | undefined {
    if (this.mobileFriendlyZoomPercent) {
      return "200%";
    }
    return undefined;
  }

  URLChange(img: any) {

    return img.replace("https", "http");
  }

  showPdf() {
    this.hidden = false;
  }

  clickUpload(): void {
    this.upload.nativeElement.click();
  }

  processFile(imageInput: any) {
    this.content.$content_images_list = [];
    this.content.$content_pdf_list = [];
    this.uploaddocBtn = false;
    this.showProgressbar = true;
    this.showUpload = false;
    var file = imageInput.files[0];
    if (file.name) var fileName = file.name;

    var fileReader = new FileReader();
    if (file.type.match('image')) {
      var image = file;
      this.unsupportedFormat = false;
      fileReader.addEventListener('load', (event: any) => {


        this.imageService.uploadImage(image).subscribe(
          (res: any) => {
            console.log(res);
            if (res.action_type) {
              this.progressvalue = 200;

              this.uploading_msg = 'Uploaded';
              this.uploaddocBtn = true;
              this.showUpload = false;
              setTimeout(() =>
                this.showProgressbar = false,
                2000);
              this.uploading_msg = 'please wait...';
            }
            this.content.$content_images_list.push(res.id);
          },
          (err) => {
            console.log(err);
          });
        this.src = fileReader.result;
      });
      fileReader.readAsDataURL(file);
    }

    else if (file.type.match('pdf')) {
      this.uploaddocBtn == false;
      this.showProgressbar == true;
      this.showUpload = false;
      var pdf = file;
      this.unsupportedFormat = false;

      // const objectURL = window.URL.createObjectURL(file);
      // console.log(objectURL);
      var container = document.createElement('div');
      container.style.width = '100px';
      container.style.height = '100px';
      container.style.cssFloat = 'left';
      container.style.marginRight = "4px";
      container.style.paddingLeft = "2px";
      container.style.paddingTop = "20px";
      container.style.fontSize = "11px";
      container.style.backgroundColor = 'rgb(65, 96, 179)';
      var content = document.createElement('div');
      content.innerHTML = fileName.substring(0, 6) + "..pdf";
      content.style.color = 'white';
      container.appendChild(content);
      document.getElementById('append').appendChild(container);

      fileReader.addEventListener('load', (event: any) => {

        console.log(" pdf uploding")

        this.pdfService.uploadpdf(pdf).subscribe(
          (res: any) => {
            console.log(" pdf uploaded")

            console.log(res);
            if (res.action_type == "UPDATED") {
              console.log('inside if');
              this.uploaddocBtn = true;
              this.showUpload = false;
              this.showProgressbar = false;
              this.content.$content_pdf_list.push(res.id);

            }
          },
          (err) => {
            console.log("error while pdf uploading");
            console.log(err);
          });
        this.pdfSrc = fileReader.result;
      });

      fileReader.readAsDataURL(file);


    }

    else {
      console.log('unsupported document format');
      this.unsupportedFormat = true;

    }
  }
}
