
import { Component, OnInit, Input } from '@angular/core';
import { ImageService } from 'src/app/services/image.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-profile-pic',
  templateUrl: './profile-pic.component.html',
  styleUrls: ['./profile-pic.component.css'],
  providers:[ImageService,UserService]
})
export class ProfilePicComponent implements OnInit {
@Input() userId;
public profilePic:any;
public gender:any;
  constructor(public imageService:ImageService,public  router:Router,private userService:UserService) { }

  ngOnInit() {
 this.getUserDetails(this.userId);
   this.getActiveUserPersonalDetails(this.userId);
  }


  getProfile(image_id){
    console.log('thumbnail calling')
    console.log(image_id)
      this.imageService.getImageByType(image_id,'thumbnail').subscribe(data=>{
       console.log(data);
        if(data.length>0){
        console.log("thumbnail found");
        console.log(data[0].image_storage_url);
      this.profilePic=this.URLChange(data[0].image_storage_url);
        }
        else{
          this.getProfileImage(image_id);
        }
      })
    }
getProfileImage(image_id){
      if(image_id!=''||image_id!='string'||image_id!=null){
        this.imageService.getImage(image_id).subscribe(data=>{
        console.log("image found");
        console.log(data.image_storage_url);
        this.profilePic=this.URLChange(data.image_storage_url);  
      } 
   ,err=>{
    console.log("image not found")
  switch(this.gender){
       case 'MALE':
       this.profilePic='assets/images/male.jpg'
       break;
       case 'FEMALE':
       this.profilePic='assets/images/female.jpg';
       break
  }
   }
   )}else{
    switch(this.gender){
      case 'MALE':
      this.profilePic='assets/images/male.jpg'
      break;
      case 'FEMALE':
      this.profilePic='assets/images/female.jpg';
      break
  }
   }
  
 }

getUserDetails(userId){
  this.userService.getUserDetails(userId).subscribe(data=>{
    console.log('user details ')
       console.log(data);
       this.getProfile(data.image_id);

  })

}
seeFriendProfile(userId){
  this.router.navigate(['/home/userprofile'], { queryParams: { userId:userId} });
}

URLChange(img:any){
  console.log("convert image to http"); 
  console.log(img); 
  return img.replace("https" , "http");
}


getActiveUserPersonalDetails(userId){
this.userService.getUserPersonalDetails(userId).subscribe(data=>{
  console.log("getActiveUserPersonalDetails")
  console.log(data)
   
this.gender=data.gender_type;
console.log(this.gender);
})

}


}
