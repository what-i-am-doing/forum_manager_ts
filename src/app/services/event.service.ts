import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry ,delay, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { EventSourcePolyfill } from 'ng-event-source';

@Injectable({
  providedIn: 'root'
})

export class EventService {

  constructor(private http: HttpClient) { }
  api_url = environment.api_url;
  events:any;
  submitEvent(event:any):Observable<any>{

          return this.http.post<any>(this.api_url+'/event/', event, {headers: new HttpHeaders({

              'Content-Type':  'application/json',
              'Accept':        'application/json',
              'from_user_id':  localStorage.getItem("user")
              
            })
          })
            .pipe(
              catchError(this.handleError)
            );
  }

  eventSearchByEntity(entity_type:string , entity_id:string,from:number,size:number):Observable<any>{
                return this.http.get<any>(this.api_url+'/event/search_by_entity?entity_type='+entity_type+'&entity_id='+entity_id+'&from='+from+'&size='+size,{headers: new HttpHeaders({

                  'Accept':        'application/json',
                  'from_user_id':  localStorage.getItem('user')
                  
                })
              } ).pipe(
                
                retry(1), // retry a failed request up to 1 time
                catchError(this.handleError) // then handle the error
                );
  }
  
  deleteEvent(eventID:string):Observable<any>{
    return this.http.delete<any>(this.api_url + '/event/' + eventID, {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'from_user_id': localStorage.getItem('user')

      })
    }).pipe(

      retry(1),
      catchError(this.handleError)
    );
  }

  searchByEntity(ID:string):Observable<any> {
    return this.http.get<any>(this.api_url+'/event/search_by_entity?group_id=public&entity_type=POST&entity_id='+ID+'&size=10',{
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'from_user_id': localStorage.getItem('user')

      })
    }).pipe(

      retry(1),
      catchError(this.handleError)
    );
  }
  
  evtByEntityNType(grpID:string,entity_type:string,entity_id:string,evt_type:string,from:number,size:number):Observable<any>{
    return this.http.get<any>(this.api_url+'/event/search_by_entity_and_event?group_id='+grpID+'&entity_type='+entity_type+'&entity_id='+entity_id+'&event_type='+evt_type+'&from='+from+'&size='+size,{
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'from_user_id': localStorage.getItem('user')

      })
    }).pipe(

      retry(1),
      catchError(this.handleError)
    );
  }

  evtByEntityNTypeAtInterval(grpID:string,entity_type:string,entity_id:string,evt_type:string,from:number,size:number,time_start:number,time_end:number):Observable<any>{
    return this.http.get<any>(this.api_url+'/event/search_by_entity_and_event?group_id='+grpID+'&entity_type='+entity_type+'&entity_id='+entity_id+'&event_type='+evt_type+'&time_start='+time_start+'&time_end='+time_end+'&from='+from+'&size='+size,{
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'from_user_id': localStorage.getItem('user')

      })
    }).pipe(

      retry(1),
      catchError(this.handleError)
    );
  }
  
  eventSearchByEntitySync(group_id,enitities:string,time_start:number,time_end:number):Observable<any>{
    
    return this.http.get<any>(this.api_url+'/event/search_by_entity?group_id='+group_id+'&entity_type=POST'+enitities+'&time_start='+time_start+'&time_end='+time_end+'&size=100',{
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'from_user_id': localStorage.getItem('user')

      })
    }).pipe(

      retry(1),
      catchError(this.handleError)
    );
  }

  eventSearchByEntityAsync(group_id,enitities:string,time_start:number,time_end:number):Observable<any>{
    
    return this.http.get<any>(this.api_url+'/event/search_by_entity/async?group_id='+group_id+'&entity_type=POST'+enitities+'&time_start='+time_start+'&time_end='+time_end+'&size=100',{
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'from_user_id': localStorage.getItem('user')

      })
    }).pipe(

      retry(1),
      catchError(this.handleError)
    );
  }

getEventsByStream(user):Observable<any>{
    return this.http.get<any>(this.api_url+'/streams/user_monitor_stream/public/'+user,{
      headers: new HttpHeaders({
        'Accept': 'application/stream+json ,text/event-stream',
        'from_user_id': user

      })
      }
    
    ).pipe(
      tap((data)=>console.log(data.data)),
      catchError(this.handleError)
    );
  }
  
  streamEvents(url:string): Observable<any> {
    return new Observable<any>(observer => {
      const eventSource = new EventSourcePolyfill(url,
        {
          headers: { "Accept": 'application/stream+json' ,
                     "from_user_id":"vijay"
                  }
       }
        );
      eventSource.onopen = (a) => {
          console.log("connection opened");
        }; 

      eventSource.onmessage = (event) => {
        console.debug('Received event: ', event);
        let json = JSON.parse(event.data);
        this.events.push(json);
        observer.next(this.events);
      };
      eventSource.onerror = (error) => {
        
        if(eventSource.readyState === 0) {
          console.log('The stream has been closed by the server.');
          eventSource.close();
          observer.complete();
        } else {
          observer.error('EventSource error: ' + error);
        }
      }
      
  });

  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }


}
