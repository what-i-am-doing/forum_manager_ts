import { Pipe,PipeTransform } from "@angular/core";


@Pipe({

    'name':'courseTypes'
})

export class CourseTypesPipe implements PipeTransform{

transform(value , search ){

   return value.filter(course => course.course_id === search)[0].course_name
}

}