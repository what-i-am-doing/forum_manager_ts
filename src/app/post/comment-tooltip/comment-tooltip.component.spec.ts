import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentTooltipComponent } from './comment-tooltip.component';

describe('CommentTooltipComponent', () => {
  let component: CommentTooltipComponent;
  let fixture: ComponentFixture<CommentTooltipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentTooltipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentTooltipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
