import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(private http: HttpClient) { }
  api_url = environment.api_url;

  getSessionByUser(user_id:string):Observable<any>{
    return this.http.get<any>(this.api_url+'/user_session_details/by_user/'+user_id,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  user_id
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
  }

  updateSession(session_id:string,body:any):Observable<any>{
    return this.http.put<any>(this.api_url+'/user_session_details/'+session_id,body,{headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Accept':        'application/json',
      'from_user_id':  session_id
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
  }


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
    error);
  };
}
