import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtraChatBoxContainerComponent } from './extra-chat-box-container.component';

describe('ExtraChatBoxContainerComponent', () => {
  let component: ExtraChatBoxContainerComponent;
  let fixture: ComponentFixture<ExtraChatBoxContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtraChatBoxContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtraChatBoxContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
