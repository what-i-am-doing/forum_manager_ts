import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';
import { ImageService } from 'src/app/services/image.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-like-modal',
  templateUrl: './like-modal.component.html',
  styleUrls: ['./like-modal.component.css'],
  providers:[ImageService,UserService]
})
export class LikeModalComponent implements OnInit {
  @Input() likeModalData: any;
   public  userId:any;
  public sentForumRequestListids: Array<any> = [];
  public city:any;
  public activeUserId;
  //current user friends list
  public friendids: Array<any> = [];
public showSentButton:boolean=true;
public showmsgButton:boolean=false;
    public profilePic:any;
    public sentRequestId='';
    public userName:any;
   
    userGroup: Array<any> = JSON.parse(localStorage.getItem("user_group"));
  forumRequestids: any;
    constructor(private imageService:ImageService,private router:Router,private service:UserService) {
  
     }
  
    ngOnInit() {
      this.activeUserId= localStorage.getItem("user");
      console.log(' userID')
      console.log(this.likeModalData)
      this.userId=this.likeModalData;
      console.log('forum request list got')
    this.forumRequestids= JSON.parse(localStorage.getItem('forum_request_id'))
    console.log(this.forumids)
    console.log('sentForumRequestListids list got')
   this.sentForumRequestListids= JSON.parse(localStorage.getItem('sentForumRequestListids'))
    console.log(this.sentForumRequestListids)
      this.getUserDetails(this.likeModalData)
    
   
    }
  forumids(forumids: any) {
    throw new Error("Method not implemented.");
  }
  getUserDetails(userId){
    this.service.getUserDetails(userId).subscribe(data => {
      console.log('data inside like modal');
      this.userName=data.user_full_text_name;
      if(data.imageService!=null||  data.image_id!=undefined){
        this.getProfile(data.image_id);
     }
     
    });
  
  }
    
    seeFriendProfile(){
  
    this.closePopup();
    }
    @Output() closeModal = new EventEmitter();
   
    closePopup() { 
     console.log("close pop up called");
           this.closeModal.emit('true');
           if(this.userId==this.activeUserId){
            this.router.navigateByUrl("home/profile");
      
          }else
            this.router.navigate(['/home/userprofile'], { queryParams: { userId:this.userId} });
        
       }


    sendForumRequest()
  {
   
      var requestObj= {
  "comments": "string",
  "creation_timestamp": Date.now(),
  "user_group_id":localStorage.getItem("UserGroupID"),
  "from_user_id":localStorage.getItem("user"),
  "forum_id": localStorage.getItem("forum_id"),
  "forum_request_id": localStorage.getItem("forum_request_id"),
  "message": "sending Forum Request",
  "forum_request_status": "OPEN",
  "status": "ACTIVE",
  "last_updated_timestamp": Date.now()
      }
  console.log('requestObj')
  console.log(requestObj);
 // this.service.sendForumRequest(requestObj,this.userId).subscribe(data=>{
//  console.log(data);
//  if(data.id){
//  this.showSentButton=false;
//  this.showmsgButton=true;
//  setTimeout(() =>
//  this.showmsgButton=false,
//  5000)
//  }
 // })
    
  }
    URLChange(img:any){
    
        return img.replace("https" , "http");
     
  }
  getProfile(image_id){
      this.imageService.getImage(image_id).subscribe(data=>{
        console.log("image found");
        console.log(data.image_storage_url);
  this.profilePic=this.URLChange(data.image_storage_url);
  
      })
  
    }
   
    
}
