export class User {

 constructor(){}



        private user_full_text_name: string; 
        public user_id: string; 
        private user_name:string;
        private admin_mapping_list:[string];
        private courses_list:[string];
        private forums_list:[string];
        private roles_list:[string];
        private subscription_list:[string];
        private roll_number: string; 
        private first_name: string; 
        private last_name:string;
        private image_id:string;
        private comments:string;
        private current_status_type_id:string;
        private group_id: Array<string> = ["public"];
    public get $group_id(): Array<string> {
        return this.group_id;
    }
    public set $group_id(value: Array<string>) {
        this. group_id = value;
    }
        private role_type_id:string;
        private level_id:string;
        private tag_id:string;
        private is_group_admin:boolean;
        private admin_type_id:string;
        private status:string;
        private status_message:string;
        private custom_user_id:string;
        private creation_timestamp:number;

    /**
     * Getter $user_full_text_name
     * @return {string}
     */
	public get $user_full_text_name(): string {
		return this.user_full_text_name;
	}

    /**
     * Getter $user_name
     * @return {string}
     */
	public get $user_name(): string {
		return this.user_name;
	}

    /**
     * Getter $admin_mapping_list
     * @return {[string]}
     */
	public get $admin_mapping_list(): [string] {
		return this.admin_mapping_list;
	}

    /**
     * Getter $courses_list
     * @return {[string]}
     */
	public get $courses_list(): [string] {
		return this.courses_list;
	}

    /**
     * Getter $forums_list
     * @return {[string]}
     */
	public get $forums_list(): [string] {
		return this.forums_list;
	}

    /**
     * Getter $roles_list
     * @return {[string]}
     */
	public get $roles_list(): [string] {
		return this.roles_list;
	}

    /**
     * Getter $subscription_list
     * @return {[string]}
     */
	public get $subscription_list(): [string] {
		return this.subscription_list;
	}

    /**
     * Getter $roll_number
     * @return {string}
     */
	public get $roll_number(): string {
		return this.roll_number;
	}

    /**
     * Getter $first_name
     * @return {string}
     */
	public get $first_name(): string {
		return this.first_name;
	}

    /**
     * Getter $last_name
     * @return {string}
     */
	public get $last_name(): string {
		return this.last_name;
	}

    /**
     * Getter $image_id
     * @return {string}
     */
	public get $image_id(): string {
		return this.image_id;
	}

    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $current_status_type_id
     * @return {string}
     */
	public get $current_status_type_id(): string {
		return this.current_status_type_id;
	}

   
    /**
     * Getter $role_type_id
     * @return {string}
     */
	public get $role_type_id(): string {
		return this.role_type_id;
	}

    /**
     * Getter $level_id
     * @return {string}
     */
	public get $level_id(): string {


		return this.level_id;
	}

    /**
     * Getter $tag_id
     * @return {string}
     */
	public get $tag_id(): string {
        return this.tag_id;
        

	}

    /**
     * Getter $is_group_admin
     * @return {boolean}
     */
	public get $is_group_admin(): boolean {
		return this.is_group_admin;
	}

    /**
     * Getter $admin_type_id
     * @return {string}
     */
	public get $admin_type_id(): string {


		return this.admin_type_id;
	}

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
        return this.status;
        
        
	}

    /**
     * Getter $status_message
     * @return {string}
     */
	public get $status_message(): string {
		return this.status_message;
	}

    /**
     * Getter $custom_user_id
     * @return {string}
     */
	public get $custom_user_id(): string {
		return this.custom_user_id;
	}

    /**
     * Getter $creation_timestamp
     * @return {number}
     */
	public get $creation_timestamp(): number {
		return this.creation_timestamp;
	}

    /**
     * Setter $user_full_text_name
     * @param {string} value
     */
	public set $user_full_text_name(value: string) {
		this.user_full_text_name = value;
	}

    /**
     * Setter $user_name
     * @param {string} value
     */
	public set $user_name(value: string) {
		this.user_name = value;
	}

    /**
     * Setter $admin_mapping_list
     * @param {[string]} value
     */
	public set $admin_mapping_list(value: [string]) {
		this.admin_mapping_list = value;
	}

    /**
     * Setter $courses_list
     * @param {[string]} value
     */
	public set $courses_list(value: [string]) {
		this.courses_list = value;
	}

    /**
     * Setter $forums_list
     * @param {[string]} value
     */
	public set $forums_list(value: [string]) {
		this.forums_list = value;
	}

    /**
     * Setter $roles_list
     * @param {[string]} value
     */
	public set $roles_list(value: [string]) {
		this.roles_list = value;
	}

    /**
     * Setter $subscription_list
     * @param {[string]} value
     */
	public set $subscription_list(value: [string]) {
		this.subscription_list = value;
	}

    /**
     * Setter $roll_number
     * @param {string} value
     */
	public set $roll_number(value: string) {
		this.roll_number = value;
	}

    /**
     * Setter $first_name
     * @param {string} value
     */
	public set $first_name(value: string) {
		this.first_name = value;
	}

    /**
     * Setter $last_name
     * @param {string} value
     */
	public set $last_name(value: string) {
		this.last_name = value;
	}

    /**
     * Setter $image_id
     * @param {string} value
     */
	public set $image_id(value: string) {
		this.image_id = value;
	}

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $current_status_type_id
     * @param {string} value
     */
	public set $current_status_type_id(value: string) {
		this.current_status_type_id = value;
	}

  
    /**
     * Setter $role_type_id
     * @param {string} value
     */
	public set $role_type_id(value: string) {
		this.role_type_id = value;
	}

    /**
     * Setter $level_id
     * @param {string} value
     */
	public set $level_id(value: string) {
		this.level_id = value;
	}

    /**
     * Setter $tag_id
     * @param {string} value
     */
	public set $tag_id(value: string) {
		this.tag_id = value;
	}

    /**
     * Setter $is_group_admin
     * @param {boolean} value
     */
	public set $is_group_admin(value: boolean) {
		this.is_group_admin = value;
	}

    /**
     * Setter $admin_type_id
     * @param {string} value
     */
	public set $admin_type_id(value: string) {
		this.admin_type_id = value;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}

    /**
     * Setter $status_message
     * @param {string} value
     */
	public set $status_message(value: string) {
		this.status_message = value;
	}

    /**
     * Setter $custom_user_id
     * @param {string} value
     */
	public set $custom_user_id(value: string) {
		this.custom_user_id = value;
	}

    /**
     * Setter $creation_timestamp
     * @param {number} value
     */
	public set $creation_timestamp(value: number) {
		this.creation_timestamp = value;
	}
       
   
   
   
}