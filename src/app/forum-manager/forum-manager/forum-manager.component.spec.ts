import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumManagerComponent } from './forum-manager.component';

describe('ForumManagerComponent', () => {
  let component: ForumManagerComponent;
  let fixture: ComponentFixture<ForumManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForumManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
