import { Component, OnInit, Inject } from '@angular/core';
import { Router } from "@angular/router"
import { DOCUMENT } from '@angular/common';
import * as Cookies from 'es-cookie';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService]
})

export class AppComponent implements OnInit {
  public navigateduserId: any;
  ngOnInit() { }

  navigateUrl: any;

  constructor(@Inject(DOCUMENT) private document: Document, private router: Router, private userService: UserService) {
    var str = this.document.location.href;
    var lastSlash = str.lastIndexOf('/');
    var url = str.substring(lastSlash + 1);
    if (str.includes('search-result')) {
      url = 'search-result';
    }
    if (str.includes('notification')) {
      url = 'home/notification';
    }
    if (str.includes('notifId')) {
      url = 'notifId';

    }
    if (str.includes('userprofile?userId')) {
      var index = url.lastIndexOf('=');
      var last = url.substring(index + 1);

      this.navigateduserId = last;

      url = 'userprofile';

    }
    if (str.includes('scholars')) {
      var index = url.lastIndexOf('/');
      var last = url.substring(index + 1);
      this.navigateduserId = last;

      url = 'scholars';

    }
    if (str.includes('watch')) {

      var index = url.lastIndexOf('=');
      var last = url.substring(index + 1);
      url = 'watch';
      this.navigateduserId = last;


    }


    switch (url) {
      case 'main':
        this.navigateUrl = 'main';
        this.router.navigate([this.navigateUrl])
          ;
        break;
      case 'login':
        this.navigateUrl = 'login';
        this.router.navigate([this.navigateUrl])
        break;
      case 'about-us':
        this.navigateUrl = 'about-us';
        this.router.navigate([this.navigateUrl])
        break;
      case 'company':
        this.navigateUrl = 'company';
        this.router.navigate([this.navigateUrl])
        break;
      case 'product':
        this.navigateUrl = 'product';
        this.router.navigate([this.navigateUrl])
        break;
      case 'platform':
        this.navigateUrl = 'platform';
        this.router.navigate([this.navigateUrl])
        break;
      case 'user-verification':
        this.navigateUrl = 'user-verification';
        this.router.navigate([this.navigateUrl])
        break;
      case 'home':
        if (sessionStorage.getItem("login") == 'true') {


          this.navigateUrl = 'home';
          this.router.navigate([this.navigateUrl])
        }
        else this.router.navigate(['login'])
        break;
      case 'profile':
        if (sessionStorage.getItem("login") == 'true') {
          this.navigateUrl = 'home/profile';
          this.router.navigate([this.navigateUrl])
        }
        else this.router.navigate(['login'])
        break;
      case 'friendlist':
        if (sessionStorage.getItem("login") == 'true') {


          this.navigateUrl = 'home/friendlist';
          this.router.navigate([this.navigateUrl])
        }
        else this.router.navigate(['login'])
        break;
      case 'profile-setting':
        if (sessionStorage.getItem("login") == 'true') {


          this.navigateUrl = 'home/profile-setting';
          this.router.navigate([this.navigateUrl])
        }
        else this.router.navigate(['login'])
        break;
      case 'home/notification':
        if (sessionStorage.getItem("login") == 'true') {


          this.navigateUrl = 'home/notification';
          this.router.navigate([this.navigateUrl])
        }
        else this.router.navigate(['login'])
        break;
      case 'search-result':
        if (sessionStorage.getItem("login") == 'true') {


          var searchquery = localStorage.getItem("searchquery");
          this.router.navigateByUrl("home/search-result/" + searchquery);
        }
        else this.router.navigate(['login'])
        break;
      case 'notifId':
        if (sessionStorage.getItem("login") == 'true') {


          var test = JSON.parse(localStorage.getItem('notifdata'));
          this.router.navigate(['/home/notif'], { queryParams: { notifId: test.postId } })
        }

        else this.router.navigate(['login'])

        break;
      case 'userprofile':
        if (sessionStorage.getItem("login") == 'true')

          this.router.navigate(['/home/userprofile'], { queryParams: { userId: this.navigateduserId } });
        else this.router.navigate(['login'])
        break;
      case 'course-manager':
        if (sessionStorage.getItem("login") == 'true')

          this.router.navigate(['/home/course-manager']);
        else this.router.navigate(['login'])
        break;
      case 'watch':
        if (sessionStorage.getItem("login") == 'true')

          this.router.navigate(['/home/watch'], { queryParams: { video: this.navigateduserId } });
        else {
          this.router.navigate(['login'])
        }
        break;
      case 'scholars':
        if (sessionStorage.getItem("login") == 'true') {

          this.router.navigate(['/home/watch'], { queryParams: { video: this.navigateduserId } });
          localStorage.setItem('extvideoId', this.navigateduserId);

        }
        else if (Cookies.get('myemail') != undefined || Cookies.get('myemail') != null) {
          this.checkUserExistByEmail(Cookies.get('myemail'));

        }
        else if (Cookies.get('myemail') != undefined && sessionStorage.getItem("login") != 'true') {
          localStorage.setItem('extvideoId', this.navigateduserId);
          localStorage.setItem('watchVideo', 'true')
          this.router.navigate(['login']);

        }



        break;

      default:
        this.navigateUrl = 'main';
        this.router.navigate([this.navigateUrl]);
        break;
    }
  }


  checkUserExistByEmail(email) {
    this.userService.checkUserExistByEmail(email).subscribe(data => {

      localStorage.setItem("LoggedInUserData", JSON.stringify(data));
      localStorage.setItem("user_level", "1");
      localStorage.setItem("user_tag", "e978f067-23ea-4a1d-ae19-c442313b9ce9");
      sessionStorage.setItem("login", "true");
      var userData = JSON.parse(localStorage.getItem("LoggedInUserData"));
      localStorage.setItem("user", userData.user_id);
      localStorage.setItem("all_groups", JSON.stringify(userData.group_id));
      localStorage.setItem("user_group", JSON.stringify(userData.group_id));
      localStorage.setItem("groupId", userData.group_id[0]);
      Cookies.set('myemail', email);
      Cookies.set('mypassword', 'true');
      this.userService.getUserDetails(userData.user_id).subscribe(
        detail => {
          localStorage.setItem("UserAllData", JSON.stringify(detail));
          this.router.navigate(['/home/watch'], { queryParams: { video: this.navigateduserId } });
          localStorage.setItem('extvideoId', this.navigateduserId);

        }
      );


    }, err => {
      console.log(err.messgge);


    })

  }

}