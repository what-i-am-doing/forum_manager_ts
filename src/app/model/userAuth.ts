export class UserAuth {

constructor(){}



private user_id:string;
private username:string;
public password:string;
private cookie_id:string;
//private stateId:string;

private comments:string; 
private status:string;
public email:string;
//private user_auth_id:string;
private group_id:string;
private creation_timestamp:number; 
private last_updated_timestamp:number;

    /**
     * Getter $user_id
     * @return {string}
     */
	public get $user_id(): string {
		return this.user_id;
	}

    /**
     * Getter $username
     * @return {string}
     */
	public get $username(): string {
		return this.username;
	}

    /**
     * Getter $password
     * @return {string}
     */
	public get $password(): string {
		return this.password;
	}

    /**
     * Getter $cookie_id
     * @return {string}
     */
	public get $cookie_id(): string {
		return this.cookie_id;
	}

    /**
     * Getter $stateId
     * @return {string}
     */
	// public get $stateId(): string {
	// 	return this.stateId;
	// }

    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Getter $email
     * @return {string}
     */
	public get $email(): string {
		return this.email;
	}

    /**
     * Getter $user_auth_id
     * @return {string}
     */
	// public get $user_auth_id(): string {
	// 	return this.user_auth_id;
	// }

    /**
     * Getter $group_id
     * @return {string}
     */
	public get $group_id(): string {
		return this.group_id;
	}

    /**
     * Getter $creation_timestamp
     * @return {number}
     */
	public get $creation_timestamp(): number {
		return this.creation_timestamp;
	}

    /**
     * Getter $last_updated_timestamp
     * @return {number}
     */
	public get $last_updated_timestamp(): number {
		return this.last_updated_timestamp;
	}

    /**
     * Setter $user_id
     * @param {string} value
     */
	public set $user_id(value: string) {
		this.user_id = value;
	}

    /**
     * Setter $username
     * @param {string} value
     */
	public set $username(value: string) {
		this.username = value;
	}

    /**
     * Setter $password
     * @param {string} value
     */
	public set $password(value: string) {
		this.password = value;
	}

    /**
     * Setter $cookie_id
     * @param {string} value
     */
	public set $cookie_id(value: string) {
		this.cookie_id = value;
	}

    /**
     * Setter $stateId
     * @param {string} value
     */
	// public set $stateId(value: string) {
	// 	this.stateId = value;
	// }

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}

    /**
     * Setter $email
     * @param {string} value
     */
	public set $email(value: string) {
		this.email = value;
	}

    /**
     * Setter $user_auth_id
     * @param {string} value
     */
	// public set $user_auth_id(value: string) {
	// 	this.user_auth_id = value;
	// }

    /**
     * Setter $group_id
     * @param {string} value
     */
	public set $group_id(value: string) {
		this.group_id = value;
	}

    /**
     * Setter $creation_timestamp
     * @param {number} value
     */
	public set $creation_timestamp(value: number) {
		this.creation_timestamp = value;
	}

    /**
     * Setter $last_updated_timestamp
     * @param {number} value
     */
	public set $last_updated_timestamp(value: number) {
		this.last_updated_timestamp = value;
	}









}