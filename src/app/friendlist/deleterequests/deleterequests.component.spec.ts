import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleterequestsComponent } from './deleterequests.component';

describe('DeleterequestsComponent', () => {
  let component: DeleterequestsComponent;
  let fixture: ComponentFixture<DeleterequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleterequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleterequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
