import { Pipe,PipeTransform } from "@angular/core";


@Pipe({

    'name':'sectionTypes'
})

export class SectionTypesPipe implements PipeTransform{

transform(value , search ){

   return value.filter(section => section.section_id === search)[0].section_name
}

}