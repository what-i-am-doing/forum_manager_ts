import { Injectable } from '@angular/core';
import { Group } from '../model/group';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, delay } from 'rxjs/operators';
import { Level} from '../model/level';
import { Post} from '../model/post';
import { User} from '../model/user';


@Injectable()

export class AdminGroupService {


    //public serverurl:string='http://ec2-13-127-215-157.ap-south-1.compute.amazonaws.com:3000';
   
   serverurl:"http://localhost:3000"
    constructor(private http: HttpClient) { }

    getAdminGroups(userId: string): Observable<Group[]> {

        return this.http.get<Group[]>(this.serverurl+'/groups/?group_admin_user_id=' + userId).pipe(

            retry(3), // retry a failed request up to 3 times
            catchError(this.handleError) // then handle the error
        );
    }

    getGroupInfo(group_id: any) {

        return this.http.get(this.serverurl+'/groups/?group_id=' + group_id);

    }

    getLevelTypes(){

         return this.http.get(this.serverurl+'/level_type');
    }

    submitLevel(level:Level):Observable<Level>{

         return this.http.post<Level>(this.serverurl+'/levels',level).pipe(

            retry(3), // retry a failed request up to 3 times
            catchError(this.handleError) // then handle the error
        );

    }


    getAllLevels(group_id:string):Observable<Level[]>{

                    return this.http.get<Level[]>(this.serverurl+'/levels/?group_id=' + group_id);

             }


    getPostsByGroup(group_id:string):Observable<Post[]> {

                 return this.http.get<Post[]>(this.serverurl+'/posts/?group_id='+group_id);
    }   
    
    getPostsByLevel(level_id:string):Observable<Post[]>{

                return this.http.get<Post[]>(this.serverurl+'/posts/?level_id='+level_id);
    }
    

    getPostsByTag(tag_id:string):Observable<Post[]>{

        return this.http.get<Post[]>(this.serverurl+'/posts/?tag_id='+tag_id);

    }

    getGroupMembers(group_id:string):Observable<User[]>{

                    return this.http.get<User[]>(this.serverurl+'/user/?user.group_id='+group_id);

    }

    getMembersByLevel(level_id:string):Observable<User[]>{

                  return this.http.get<User[]>(this.serverurl+'/user/?user.level_id='+level_id);

    }

    getMembersByTag(tag_id:string):Observable<User[]>{

                return this.http.get<User[]>(this.serverurl+'/user/?user.tag_id='+tag_id);

    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    }

}