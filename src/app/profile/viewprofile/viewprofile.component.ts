import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ImageService } from '../../services/image.service';
import { UserService } from '../../services/user.service';
import { PostService } from '../../services/post.service';
import { NavBarComponent } from '../../nav-bar/nav-bar.component';
import { MatMenuTrigger } from '@angular/material/menu';
import { GroupService } from 'src/app/services/group.service';
import { AddressService } from 'src/app/services/address.service';
import { Router } from '@angular/router';

class ImageSnippet {
  showaddress: boolean = false;
  constructor(public src: string, public file: File) { }
}

@Component({
  selector: 'app-viewprofile',
  templateUrl: './viewprofile.component.html',
  styleUrls: ['./viewprofile.component.css'],
  providers: [ImageService, AddressService, UserService, NavBarComponent, PostService, GroupService],
  host: {
    '(document:click)': 'onClick($event)',
    '(window:mouseup)': 'handleMouseUp($event)'
  },
})

export class ViewprofileComponent implements OnInit {
  @ViewChild('myDiv') myDiv: ElementRef;
  groupId = localStorage.getItem("groupId");
  public showemojis: boolean = false;
  public friends: any;
  public arrowkeyLocation = 0;
  public selectedFile: ImageSnippet;
  public uploadTrue: boolean = false;
  public active: boolean = false;
  public currentItem: any;
  public show_drop_menu: boolean = false;
  public userName: string;
  public receivedUserPersonalData: any;
  public userId: string;
  public user_auth_id: string;
  public status: string;
  public userGroupId: string;
  public displayName: string;
  name = 'Angular 4';
  public url: string = '';
  public receivedFriendList: any;
  public user_friendlist;
  public requestresult;
  public userMainDetails;
  public profilePic: any;
  public allPosts: Array<any> = [];
  public friendData: Array<any> = [];
  public fromSize = 0;
  public toSize = 100;
  Message = "default";
  public receivedUserAuthData: any;
  public gender;
  userDetail: any;
  allUserData: Array<any> = [];
  public city: any;
  activeUserEducationalData: any;
  showEdit: boolean = false;
  firstName: string = localStorage.getItem("first_name");
  lastName: string = localStorage.getItem("last_name");
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  constructor(private addresService: AddressService, private groupService: GroupService, private imageService: ImageService, private service: UserService, private postService: PostService, public eleref: ElementRef, public navbar: NavBarComponent, private router: Router) { }

  public tempUserId: any;
  ngOnInit() {

    this.groupService.getUserlistByGroup(this.groupId, 0, 100).subscribe(data => this.allUserData = data);
    this.defaultProfilepic();
    var userData = JSON.parse(localStorage.getItem('LoggedInUserData'));
    this.userMainDetails = JSON.parse(localStorage.getItem("userMainDetails"));
    this.userName = userData.user_full_text_name;
    this.userId = userData.user_id;
    this.user_auth_id = userData.user_auth_id;
    this.status = userData.status;
    this.userGroupId = JSON.parse(localStorage.getItem('user_group'));
    this.getActiveUserPersonalDetails(this.userId);
    this.getUserEducationalDetails(this.userId);
    this.getUserAuthDetails(this.user_auth_id)
    this.getFriendListOfActiveUser(this.userId, this.userGroupId[0], this.fromSize, this.toSize);
    this.getProfile(this.userMainDetails.image_id);
    this.service.getUserDetails(this.userId).subscribe(data => this.userDetail = data);
    this.postService.getPostByUser(this.userGroupId[0], this.userId).subscribe(
      data => {
        this.allPosts = data;
        this.Message = data.length ? "data got" : "No data found";
      }
    );

  }


  seeUpload() {

    this.uploadTrue = !this.uploadTrue;
  }

  defaultProfilepic() {
    if (this.url == '') this.displayName = this.userName;
  }

  onSelectFile(event: any) {

    var image = event.target.files[0];

    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event: any) => {

        this.imageService.uploadImage(image).subscribe(
          (res: any) => {
            console.log(res);

            if (res) {
              var imgobj = { "image_id": res.id };
              this.service.updateUserDetail(imgobj, this.userId).subscribe(() => {

                this.getProfile(res.id);
              });
            }

          },
          (err) => {
            console.log(err);
          });
      }
    }
  }

  getProfile(image_id) {

    this.imageService.getImageByType(image_id, 'thumbnail').subscribe(data => {

      if (data.length) {

        this.profilePic = this.URLChange(data[0].image_storage_url);
      }
      else {
        this.getProfileImage(image_id);
      }
    })
  }

  getProfileImage(image_id) {

    this.imageService.getImage(image_id).subscribe(data => {

      this.profilePic = this.URLChange(data.image_storage_url);

    })

}

  // users friends list 
  getFriendListOfActiveUser(userId: any, group_id, fromSize, toSize) {

    this.service.getFriendListOfActiveUser(userId, group_id, fromSize, toSize).subscribe(data => {
      this.receivedFriendList = data;


      data.forEach(element => {
        this.service.getUserDetails(element.friend_user_id).subscribe(data => {
          this.friendData.push(data);
        });
      });
    })
  }


  URLChange(img: any) {
    return img.replace("https", "http");
  }



  getUserEducationalDetails(userId) {
    this.service.getEducationDetailsDataByUserId(userId).subscribe(data => {
      this.activeUserEducationalData = data;

    })
  }

  getAdress(adressId) {
    this.addresService.getAdress(adressId).subscribe(data => {
      this.city = data.city;
    })
  }

  getActiveUserPersonalDetails(userId: string): void {
    this.service.getUserPersonalDetails(userId).subscribe(receivedUserPersonalData => {
      this.gender = receivedUserPersonalData.gender_type;
      this.receivedUserPersonalData = receivedUserPersonalData;
      var todate = new Date(receivedUserPersonalData.date_of_birth).getDate();
      var tomonth = new Date(receivedUserPersonalData.date_of_birth).getMonth() + 1;
      var toyear = new Date(receivedUserPersonalData.date_of_birth).getFullYear();
      var original_date = tomonth + '/' + todate + '/' + toyear;
      receivedUserPersonalData.date_of_birth = original_date;
      this.getAdress(receivedUserPersonalData.address_id);

    })
  }

  getUserAuthDetails(user_auth_id: any) {
    this.service.getUserAuthDetails(user_auth_id).subscribe(receivedUserAuthData => {
      this.receivedUserAuthData = receivedUserAuthData;

    })
  }

  goToAccountSetting() {
    this.router.navigate(['/home/profile-setting']);
  }

  gotoAllFriends() {
    this.router.navigate(['/home/all-friends']);
  }
}