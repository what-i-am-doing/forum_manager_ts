import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable()
export class TagService{
     constructor(private http: HttpClient){}
      private api_url = environment.api_url;
     
      createTag(body):Observable<any>{
            return this.http.post<any>(this.api_url+'/tag/', body, {headers: new HttpHeaders({

                  'Content-Type':  'application/json',
                  'Accept':        'application/json',
                  'from_user_id':  localStorage.getItem("user")
                  
                })
              })
                .pipe(
                  catchError(this.handleError)
                );
      }

      getTag(tag:string):Observable<any>{
            return this.http.get<any>(this.api_url+'/tag/'+tag,{headers: new HttpHeaders({

                  'Accept':        'application/json',
                  'from_user_id':  localStorage.getItem('user')
                  
                })
              } ).pipe(
                 
                 // retry a failed request up to 1 time
                 retry(1),
                 catchError(this.handleError) // then handle the error
                ); 

      }

      deleteTag(tag:string):Observable<any>{
            return this.http.delete<any>(this.api_url+'/tag/'+tag, {
                  headers: new HttpHeaders({
                    'Accept': 'application/json',
                    'from_user_id': localStorage.getItem('user')
            
                  })
                }).pipe(
        
                  retry(1),
                  catchError(this.handleError)
                );
          }

      searchTagByGroup(group):Observable<any>{
            return this.http.get<any>(this.api_url+'/tag/search_by_group?group_id='+group+'&from=0&size=10',{headers: new HttpHeaders({

                  'Accept':        'application/json',
                  'from_user_id':  localStorage.getItem('user')
                  
                })
              } ).pipe(
                 
                 // retry a failed request up to 1 time
                 retry(1),
                 catchError(this.handleError) // then handle the error
                ); 

      }
     
      searchTagByLevel(level):Observable<any>{
            return this.http.get<any>(this.api_url+'/tag/search_by_level?level_id='+level+'&from=0&size=10',{headers: new HttpHeaders({

                  'Accept':        'application/json',
                  'from_user_id':  localStorage.getItem('user')
                  
                })
              } ).pipe(
                 
                 // retry a failed request up to 1 time
                 retry(1),
                 catchError(this.handleError) // then handle the error
                ); 
      }

      userBytag(tag,level,group,from,size):Observable<any>{
        return this.http.get<any>(this.api_url+'/user/by_tag?group_id='+group+'&level_id='+level+'&tag_id='+tag+'&from='+from+'&size='+size,{headers: new HttpHeaders({

          'Accept':        'application/json',
          'from_user_id':  localStorage.getItem('user')
          
        })
      } ).pipe(
         
         // retry a failed request up to 1 time
         retry(1),
         catchError(this.handleError) // then handle the error
        ); 
      }

      private handleError(error: HttpErrorResponse) {
            if (error.error instanceof ErrorEvent) {
              // A client-side or network error occurred. Handle it accordingly.
              console.error('An error occurred:', error.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
            }
            // return an observable with a user-facing error message
            return throwError(
              'Something bad happened; please try again later.');
          }
}