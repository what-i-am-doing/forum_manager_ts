import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ImageService } from '../../services/image.service';
import { PdfService } from '../../services/pdf.service';
import { Content } from '../../model/content';
import { FormControl } from '@angular/forms';
import { UserDocument } from '../../model/UserDocument';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-user-document',
  templateUrl: './user-document.component.html',
  styleUrls: ['./user-document.component.css']
})

export class UserDocumentComponent implements OnInit {
  myControl = new FormControl();
  public addDocument: boolean = false;
  public showUpload: boolean = true;
  public doc_src: any;
  public content = new Content();
  public doc_name_by_user: any;
  public document_content: any;
  public document_type: any;
  public doc_description: any;
  public documentToEdit: any;

  public documentName: any;
  public kyc_id_type_name: any;
  public showProgressbar: boolean = false;
  public uploaddocBtn: boolean = true;
  public src: any;
  public documentAdded: boolean = false;
  public edittogle: boolean = false;
  public userDoc = new UserDocument();
  public userId: string = localStorage.getItem("user");
  public progressvalue: number = 0;
  public receivedUserDocumentData: any;
  public uploading_msg: string = "please wait...";
  public unsupportedFormat: boolean = false;
  public curImgID: string;
  public pdfSrc: any;
  docID: string;
  sure: boolean = false;
  
  @ViewChild("upload", { read: ElementRef }) upload: ElementRef;

  options: string[] = ['Adhaar Card', 'PAN Card', 'Passport', 'Voter Id', 'Driving License', 'Marksheet', 'Resume'];
  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  filteredOptions: Observable<string[]>;

  constructor(private userService: UserService, private imageService: ImageService, private pdfService: PdfService) {
    this.getUserDocumentData(this.userId);
    this.content.$content_images_list = [];
    this.content.$content_pdf_list = [];
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [[2, 'desc']],
      columnDefs: [{
        "orderable": false,
        "targets": [0, 4]

      }]
    };

    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
    this.documentName = this.filteredOptions;
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  getUserDocumentData(userId) {
    this.userService.getUserDocumentDetails(userId).subscribe(data => {
      console.log(data);
      this.receivedUserDocumentData = data;
      this.dtTrigger.next();

    })
  }

  visible(addDocument) {
    if (addDocument == false) {
      this.addDocument = true;
      this.showUpload = true;
      this.doc_src = undefined;
      document.getElementById('addbutton').innerText = 'Cancel';

    }
    else {
      this.addDocument = false;
      this.content.$content_pdf_list = [];
      this.doc_name_by_user = null;
      this.document_content = null;
      this.doc_description = null;
      this.kyc_id_type_name = null;
      //this.myControl=null;
      this.content.$content_images_list = [];
      this.content.$content_pdf_list = [];
      this.showProgressbar = false;
      this.showUpload = true;
      this.uploaddocBtn = false;
      this.src = undefined;
      document.getElementById('addbutton').innerText = 'Add a Document';
    }

  }

  uploadUserDocument() {

    var name = this.myControl.value;

    this.imageService.usercontent(this.content).subscribe(
      (data) => {
        var contentId = data.id
        this.src = null;
        this.uploaddocBtn = false;
        this.userDoc.$doc_description = this.doc_description;
        this.userDoc.$doc_name_by_user = this.doc_name_by_user;
        this.userDoc.$doc_type_id = name;
        this.userDoc.$user_id = this.userId;
        this.userDoc.$document_content = contentId;

        this.userService.uploadUserDocument(this.userDoc, this.userId).subscribe(data => {

          this.doc_name_by_user = null;
          this.document_content = null;
          this.doc_description = null;
          this.kyc_id_type_name = null;
          this.content.$content_images_list = [];
          this.content.$content_pdf_list = [];
          this.showProgressbar = false;
          this.progressvalue = 10;
          this.getUserDocumentDataById(data.id);


        })


      })

  }

  processFile(imageInput: any) {
    this.content = new Content;
    this.progressvalue = 10;
    this.showProgressbar = true;
    this.showUpload = false;
    var file = imageInput.files[0];
    var fileName = file.name;
    var fileReader = new FileReader();

    if (file.type.match('image')) {
      this.content.$content_images_list = [];
      this.unsupportedFormat = false;
      var image = file;
      fileReader.addEventListener('load', (event: any) => {

        this.imageService.uploadImage(image).subscribe(
          (res: any) => {
            if (typeof res === 'object') {
              if ('id' in res) {
                console.log(res);
                this.curImgID = res.id;
                if (res.action_type) {

                  this.progressvalue = 200;
                  this.uploading_msg = 'Uploaded';
                  this.uploaddocBtn = true;
                  this.showUpload = false;
                  setTimeout(() =>
                    this.showProgressbar = false,

                    2000);
                  this.uploading_msg = 'please wait...';
                }

                this.content.$content_images_list.push(res.id);

              }
            }

          },
          (err) => {
            console.log(err);
          });


        this.doc_src = fileReader.result;

      });

      fileReader.readAsDataURL(file);


    }

    else if (file.type.match('pdf')) {

      this.content.$content_pdf_list = [];
      this.uploaddocBtn == false;
      this.showProgressbar == true;
      this.showUpload = false;
      var pdf = file;
      this.unsupportedFormat = false;
      var container = document.createElement('div');
      container.style.width = '100px';
      container.style.height = '100px';
      container.style.cssFloat = 'left';
      container.style.marginRight = "4px";
      container.style.paddingLeft = "2px";
      container.style.paddingTop = "20px";
      container.style.fontSize = "11px";
      container.style.backgroundColor = 'rgb(65, 96, 179)';
      var content = document.createElement('div');
      content.innerHTML = fileName.substring(0, 6) + "..pdf";
      content.style.color = 'white';
      container.appendChild(content);
      document.getElementById('append').appendChild(container);

      fileReader.addEventListener('load', (event: any) => {

        console.log(" pdf uploding")

        this.pdfService.uploadpdf(pdf).subscribe(
          (res: any) => {
            if (typeof res === 'object') {
              if ('id' in res) {
                if (res.action_type == "UPDATED") {
                  console.log('inside if');
                  this.uploaddocBtn = true;
                  this.showUpload = false;
                  this.showProgressbar = false;
                  this.content.$content_pdf_list.push(res.id);
                }

              }
            }

          },
          (err) => {
            console.log("error while pdf uploading");
            console.log(err);
          });
        this.pdfSrc = fileReader.result;
      });

      fileReader.readAsDataURL(file);


    }

    else {
      console.log('unsupported document format');
      this.unsupportedFormat = true;
    }
  }

  getUserDocumentDataById(documentId) {

    this.userService.getUserDocumentDataById(documentId).subscribe(data => {

      console.log(data);
      this.receivedUserDocumentData.push(data);
      this.rerender();
      this.addDocument = false;
      this.documentAdded = true;
      document.getElementById('addbutton').innerText = 'Add a Document';
      setTimeout(() => this.documentAdded = false, 2000);

    })

  }

  addDocumentCancel() {
    this.addDocument = true;
    this.showUpload = true;
    document.getElementById('addbutton').innerText = 'Add a Document';

    this.addDocument = false;
    this.doc_name_by_user = null;
    this.document_content = null;
    this.doc_description = null;

    this.documentName = null;
    this.src = null;

  }

  clickUpload(): void {
    this.upload.nativeElement.click();
  }

  tableRow(row: any) {
    console.log(row);
    this.documentToEdit = row;
    this.myControl.setValue(row.doc_type_id);
    if(row.image_url) this.doc_src = row.image_url;
    else this.doc_src = row.pdf_url;
    console.log(this.documentToEdit);
  }
  

  updateUserDocument() {

    var documentobj = {

      "doc_description": this.documentToEdit.doc_description,
      "doc_name_by_user": this.documentToEdit.doc_name_by_user,
      "doc_type_id": this.myControl.value,
      "last_updated_timestamp": Date.now()

    }

    if (this.content.$content_images_list || this.content.$content_pdf_list) {
      this.imageService.updateusercontent(this.documentToEdit.document_content, { "content_images_list": this.content.$content_images_list }).subscribe(
        img => {
          this.imageService.getImage(this.curImgID).subscribe(
            image => {
              const album = {
                src: image.image_storage_url.replace("https", "http"),
                caption: image.image_name,
                thumb: image.image_storage_url.replace("https", "http"),
                id: image.image_id
              };
              this.documentToEdit._albums = [album]

            }
          );
        }
      );
    }

    this.userService.updateUserdocument(this.documentToEdit.user_document_id, documentobj).subscribe(data => console.log(data));


  }

  deleteDocument(document: any) {

    this.userService.deleteUserDocument(document.user_document_id, this.userId).subscribe(data => {
      console.log(data);
      this.receivedUserDocumentData = this.receivedUserDocumentData.filter(
        doc => doc.user_document_id != document.user_document_id
      );

      this.rerender();
    }

    )

  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
}
