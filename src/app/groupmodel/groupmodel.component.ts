import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from '../services/user.service';
import { group } from '@angular/animations';
import { debounce } from 'rxjs/operators';
import { Group } from '../model/group';
@Component({
    selector: 'app-groupmodel',
    templateUrl: './groupmodel.component.html',
    styleUrls: ['./groupmodel.component.css'],
    providers: [UserService],
})
export class GroupmodelComponent implements OnInit {

    public  groups:  Group [];
    public  friends: any[];
    public  groupName: string;
    public  tagName: string;
    public  dropdownList = [];
    public  selectedItems = [];
    public  dropdownSettings = {};
    constructor(
        public dialogRef: MatDialogRef<GroupmodelComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any, public service: UserService) {
        this.friends = ["lavkush", "alam", "pratima yadav", "akhilsesh kumar", "RAj kumar", "Ajay", "Akshay", "Sarah", "Arjuna", "Rohit", "Manish", "Ravi Yadav", "Ravi Shanker", "shiva", "Shiv kumar", "Rachna", "Zoya", "Sarvesh", "Pratima", "Prem", "Abhishek", "Roshan verma", "Seema", "Mr Sharma", "Verma Mukesh", "Sikha", "Sharma Ramesh", "Siddharth", "Rahul", "Rahul singh", "lavi", "Prince", "Vikas", "lavkush Yadav", "Dipak Yadav", "raj", "deepak", "mohit", "john", "Subodh"];
        this.dialogRef.disableClose = true;
   
    } 
   
group=new Group();

    newgroup={         
        "group_id" : null,
        "group_name":this.groupName,
        "custom_tag_line":this.tagName,
        "group_admin_user_id" :null,
        "selectedItems" : this.selectedItems,
        "hierarchy_id" : null,
        "img_id" : null,
        "stateId" : null,
        "comments" : "Group created for DPS, Kanpur !!!",
        "creation_timestamp" : 1541263361513,
        "last_updated_timestamp" : 1541263361513
    
    }
    ngOnInit() {
     
        this.dropdownList = [
            { "id": 1, "itemName": "lavkush", "image": "assets/images/friend.jpg" },
            { "id": 2, "itemName": "raj ", "image": "assets/images/friend.jpg" },
            { "id": 3, "itemName": "shyam tiwari", "image": "assets/images/friend.jpg" },
            { "id": 4, "itemName": " Raunak maurya", "image": "assets/images/friend.jpg" },
            { "id": 5, "itemName": "Akhilesh", "image": "assets/images/friend.jpg" },
            { "id": 6, "itemName": "Akshay kumar", "image": "assets/images/friend.jpg" },
            { "id": 7, "itemName": "Deepak", "image": "assets/images/friend.jpg" },
            { "id": 8, "itemName": "Raj malhotra", "image": "assets/images/friend.jpg" },
            { "id": 9, "itemName": "Dev ", "image": "assets/images/friend.jpg" },
            { "id": 10, "itemName": "Alam", "image": "assets/images/friend.jpg" }
        ];
       this.dropdownSettings = {
            singleSelection: false,
            text: "Select Friends",
            enableSearchFilter: true,
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            classes: "myclass custom-class",
            showCheckbox: false,
        };
    }
    onItemSelect(item: any) {
        console.log(this.selectedItems);
    }
    OnItemDeSelect(item: any) {
        console.log(this.selectedItems);
    }
    onSelectAll(items: any) {
        console.log(this.selectedItems);
    }
    onDeSelectAll(items: any) {
        console.log(this.selectedItems);
    }
    validate() {
        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>");
     console.log(this.groupName);
        if (this.groupName == "null" || this.groupName==undefined ||this.tagName==undefined || this.selectedItems.length==0) {

            alert("please  fill all the fields");    
        }
        else {
           this.createGroup();
           alert("Group Created");
           this.dialogRef.close();
        }
    }
    createGroup() {
     console.log(" create group >>>>>>>>>")    
          
   this.service.createGroup(this.newgroup).subscribe(data => {              
    console.log(data);
     })
      }
  closeModal(){
        this.dialogRef.close();
    }
}