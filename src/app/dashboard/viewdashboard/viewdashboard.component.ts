import { Component, OnInit, ViewChildren, QueryList, ViewChild, ElementRef } from "@angular/core";
import { UserService } from "../../services/user.service";
import { GroupService } from "../../services/group.service";
import { PostService } from "../../services/post.service";
import { ImageService } from "../../services/image.service"
import { CommonService } from "src/app/services/common.service";
import { Router } from "@angular/router";
import { LevelService } from "src/app/services/level.service";
import { TagService } from "src/app/services/tag.service";
import { BsDatepickerConfig } from "ngx-bootstrap/datepicker";
import { AddressService } from "src/app/services/address.service";

@Component({
  selector: "app-viewdashboard",
  templateUrl: "./viewdashboard.component.html",
  styleUrls: ["./viewdashboard.component.css"],
  providers: [UserService, AddressService, GroupService, PostService, ImageService, CommonService, LevelService, TagService]
})

export class ViewdashboardComponent implements OnInit {

  @ViewChild("filterpanel", { read: ElementRef }) filterpanel: ElementRef;
  exFilter:boolean = false;
  friends: Array<any> = [];
  friendData: Array<any> = [];
  userName: string;
  allPosts: Array<any> = [];
  postImages: Array<string>;
  userId: string;
  userGroupId: Array<string>;
  userLevel: string = localStorage.getItem("user_level");
  userTag: string = localStorage.getItem("user_tag");
  active: boolean = false;
  currentThing: any;
  currentProfile: any;
  addFriendsec: boolean = false;
  users: any;
  public user_auth_id: string;
  public status: string;
  userMainDetails: any;
  public friendsForSuggestoion: any = [];
  postTagFriends: Array<any> = [];
  fromSize: number = 1;
  toSize: number = 5;
  public profilePic: any;
  public userGroupList: any = JSON.parse(localStorage.getItem("all_groups"));
  public activeUserAllData: any;
  public allUserData: Array<any> = [];
  public activeUserPersonalData: any;
  public activeUserEducationalData: any;
  public realfriendsForSuggestoionList: Array<any> = [];
  public groupId: any;
  public allLevels: Array<any> = [];
  public allTags: Array<any> = [];
  public curLevel: string = "all";
  public curTag: string = "all";
  datePickerConfig: Partial<BsDatepickerConfig>;
  bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();
  public selected_date: string = "7";
  totalUnchangedPosts: Array<any> = [];
  fromDate: number;
  toDate: number;
  isOnlyFriend: boolean = false;
  friendIDS: Array<string> = [];
  fromPost: number = 0;
  noMorePosts: boolean = false;
  public city: any;
  public profilePercentage: number = 0;
  isLikes: boolean = false;
  isComments: boolean = false;
  isShares: boolean = false;
  likeArrow: boolean = false;
  comArrow: boolean = false;
  shareArrow: boolean = false;
  newPostCreated:boolean = false;
  sByName = "";
  constructor(
    private imageService: ImageService,
    private postService: PostService,
    private groupService: GroupService,
    private service: UserService,
    private router: Router,
    private commonService: CommonService,
    private levelService: LevelService,
    private tagService: TagService,
    private addressService: AddressService


  ) {
    this.datePickerConfig = Object.assign({}, { containerClass: "theme-dark-blue" });
    this.maxDate.setDate(this.maxDate.getDate() - 7);
    this.bsRangeValue = [this.maxDate, this.bsValue];
    var userData = JSON.parse(localStorage.getItem("LoggedInUserData"));
    this.userId = userData.user_id;
    this.user_auth_id = userData.user_auth_id;
    this.status = userData.status;
    this.userGroupId = userData.group_id;
    this.groupId = localStorage.getItem("groupId");

    this.allPostsOfUser(this.groupId);
    this.getUsersforSuggestions(
      this.groupId,
      this.userId,
      this.fromSize,
      this.toSize
    );


  }

  src: string;
  public gender: any;
  public hidden: boolean = true;
  public mobileFriendlyZoomPercent = false;
  ngOnInit() {

    this.groupService.getUserlistByGroup(this.groupId, 0, 100).subscribe(data => this.allUserData = data);
    this.getFriendListOfActiveUser(this.userId, this.userGroupId[0], 0, 100);
    var x = +new Date() / 1000;
    console.log(new Date(x * 1000));
    //need this   API to remove, from here
    this.activeUserAllData = this.commonService.getUserAllDetails(this.userId, this.userGroupId[0]);
    console.log(this.activeUserAllData);
    this.getUsersDetails(this.userId);
    this.getUserEducationalDetails(this.userId);
    this.getUserPersonalDetails(this.userId);
    this.levelService.searchLevelByGroup(this.groupId).subscribe(data => this.allLevels = data);
    this.tagService.searchTagByGroup(this.groupId).subscribe(data => this.allTags = data);

  }
  

  getFriendListOfActiveUser(userId: any, group_id, from, to) {
    this.service.getFriendListOfActiveUser(userId, group_id, 0, 100).subscribe(data => {
      console.log("Real friend List of Logged in users");
      console.log(data);
      data.forEach(element => {
        this.friends.push(element);
      });

      this.friends.forEach(element => {
        this.service.getUserDetails(element.friend_user_id).subscribe(data => {
          console.log(data);
          this.friendData.push(data);
          this.friendIDS.push(data.user_id);
        });
      });
      console.log(this.friendData);
    });
  }

  allPostsOfUser(userGroupId) {
    this.postService.getAllPostsByStatus(userGroupId, "ACTIVE",this.fromPost, 5).subscribe((posts: any) => {
      console.log(posts);
     

      posts.forEach(element => {
        if (!element.comments_counter) {
          element.comments_counter = 0;
        }

        if (!element.likes_counter) {
          element.likes_counter = 0;
        }

        if (!element.shares_counter) {
          element.shares_counter = 0;
        }
      });

      this.allPosts = posts;
      localStorage.setItem("user_posts" , JSON.stringify(posts))
      this.totalUnchangedPosts = posts.slice(0);
      this.fromPost = this.fromPost + 5;
      console.log(this.allPosts);
    });
  }


  getcreatedPost(event: any) {
    // console.log(event);
    // this.allPosts.unshift(event);
    // this.totalUnchangedPosts.unshift(event);
    this.newPostCreated = true;
  }

  public get mobileFriendlyZoom(): string | undefined {
    if (this.mobileFriendlyZoomPercent) {
      return "200%";
    }
    return undefined;
  }

  viewMore(pagenumber, pagesize) {
    console.log(" view more clicked");

    this.toSize = this.toSize + pagesize;

    this.getUsersforSuggestions(
      this.userGroupId,
      this.userId,
      this.fromSize,
      this.toSize
    );
  }

  //showMoreOptions for  sending friend request

  showMoreOptions() {
    this.addFriendsec = true;
  }

  getUsersforSuggestions(userId, group_id, fromsize: Number, tosize: Number) {
    this.service
      .getUsersforSuggestions(userId, group_id, fromsize, tosize)
      .subscribe(data => {


        this.friendsForSuggestoion = data;
        console.log("this.friendsForSuggestoion");
        console.log(data);
      });

  }



  hideMoreOptions() {
    this.addFriendsec = false;
  }

  postMessageFormat(message: string) {
    return message.substring(0, 2000) + "...<a class='see-more'>See More</a>";
  }

  getUsersDetails(userId) {
    this.service.getUserDetails(userId).subscribe(data => {
      this.userMainDetails = data;


      //  console.log(Object(data).values);
      if (this.userMainDetails.first_name != null && this.userMainDetails.first_name != '' && this.userMainDetails.first_name != 'string') {

        this.profilePercentage = this.profilePercentage + 10
      }
      if (this.userMainDetails.last_name != null && this.userMainDetails.last_name != '' && this.userMainDetails.last_name != 'string') {

        this.profilePercentage = this.profilePercentage + 10
      }
      if (this.userMainDetails.image_id != null && this.userMainDetails.image_id != '' && this.userMainDetails.image_id != 'string') {

        this.profilePercentage = this.profilePercentage + 10
      }
      if (this.userMainDetails.roll_number != null && this.userMainDetails.roll_number != '' && this.userMainDetails.roll_number != 'string') {

        this.profilePercentage = this.profilePercentage + 5
      }
      if (this.userMainDetails.user_full_text_name != null && this.userMainDetails.user_full_text_name != '' && this.userMainDetails.user_full_text_name != 'string') {

        this.profilePercentage = this.profilePercentage + 5
      }

      if(data.image_id && data.image_id != 'string') this.getProfile(this.userMainDetails.image_id);
      this.userName = this.userMainDetails.user_full_text_name;
      localStorage.setItem(
        "userMainDetails",
        JSON.stringify(this.userMainDetails)
      );
      this.userMainDetails = JSON.parse(
        localStorage.getItem("userMainDetails")
      );
    });
  }
  userSwitchGroup(groupId) {
    var switchGroupObj = {
      group_id: [groupId],
      user_id: this.userId
    };
    this.groupService
      .userSwitchGroup(switchGroupObj, groupId, this.userId)
      .subscribe(data => {
        console.log(data);
        if (data == true) {
          localStorage.setItem("user_group", JSON.stringify([groupId]));
        }
      });
  }
  openProfilePage(profile) {
    this.router.navigateByUrl("home/profile");
  }
  seeFriendProfile(userId) {
    this.router.navigate(['/home/userprofile'], { queryParams: { userId: userId } });
  }

  URLChange(img: any) {
    return img.replace("https", "http");
  }


  getUserPersonalDetails(userId) {


    this.service.getUserPersonalDetails(userId).subscribe(data => {
      console.log("getActiveUserPersonalDetails");
      console.log(data);
      this.activeUserPersonalData = data;
      this.gender = data.gender_type;
      console.log(this.gender);
      localStorage.setItem('activeGender', this.gender);
      this.getAdress(data.address_id);
      if (data.address_id != null && data.address_id != '' && data.address_id != 'string') {

        this.profilePercentage = 5
      }
      if (data.age != null && data.age != '' && data.age > 0) {

        this.profilePercentage = this.profilePercentage + 5
      }
      if (data.date_of_birth != null && data.date_of_birth != '' && data.date_of_birth > 0) {

        this.profilePercentage = this.profilePercentage + 5
      }
      if (data.mobile_number != null && data.mobile_number != '' && data.mobile_number != 'string') {

        this.profilePercentage = this.profilePercentage + 5
      }
      // if(data.address_id!=null && data.address_id!=''&&data.address_id!='string'){

      //   this.profilePercentage= this.profilePercentage+5
      // }


    })



  }


  getUserEducationalDetails(userId) {


    this.service.getEducationDetailsDataByUserId(userId).subscribe(data => {

      console.log('education data');
      this.activeUserEducationalData = data;

      if (data.class_id != null && data.class_id != '' && data.class_id != 'string') {

        this.profilePercentage = this.profilePercentage + 5
      }
      if (data.course_id != null && data.course_id != '' && data.course_id != 'string') {

        this.profilePercentage = this.profilePercentage + 5
      }
      if (data.education_type_id != null && data.education_type_id != '' && data.education_type_id != 'string') {

        this.profilePercentage = this.profilePercentage + 5
      }
      if (data.institution_id != null && data.institution_id != '' && data.institution_id != 'string') {

        this.profilePercentage = this.profilePercentage + 5
      }
      if (data.section_id != null && data.section_id != '' && data.section_id != 'string') {

        this.profilePercentage = this.profilePercentage + 5
      }
      if (data.stream_id != null && data.stream_id != '' && data.stream_id != 'string') {

        this.profilePercentage = this.profilePercentage + 5
      }


    })
  }

  includeShared(event: any) {
    this.allPosts.unshift(event);
    this.totalUnchangedPosts.unshift(event);
  }

  getProfile(image_id) {
    
    this.imageService.getImageByType(image_id, 'thumbnail').subscribe(data => {
      
      if (data.length) {
        this.profilePic = this.URLChange(data[0].image_storage_url);
        localStorage.setItem('activeProfilePic', this.URLChange(data[0].image_storage_url));
      }

      else {
        this.getProfileImage(image_id);
      }
    })
  }

  getProfileImage(image_id) {

      this.imageService.getImage(image_id).subscribe(data => {

        this.profilePic = this.URLChange(data.image_storage_url);
        localStorage.setItem('activeProfilePic', this.profilePic);

      })
    }


 changeLevel(event: any) {
    alert(this.curLevel);
  }

  changeDate() {
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate() - Number(this.selected_date));
    this.bsRangeValue = [this.maxDate, this.bsValue];
  }

  filterPosts() {
    //console.log(this.bsRangeValue[0].getTime()+ " "+this.bsRangeValue[1].getTime());
    this.fromDate = this.bsRangeValue[0].getTime();
    this.toDate = this.bsRangeValue[1].getTime();

    if (this.curLevel == 'all' || this.curTag == 'all') {
      if (this.curLevel == 'all' && this.curTag != 'all') {
        this.allPosts = this.totalUnchangedPosts.filter(post => (post.tag_id == this.curTag && post.creation_timestamp > this.fromDate && post.creation_timestamp < this.toDate));
      }
      if (this.curTag == 'all' && this.curLevel != 'all') {
        this.allPosts = this.totalUnchangedPosts.filter(post => (post.level_id == this.curLevel && post.creation_timestamp > this.fromDate && post.creation_timestamp < this.toDate));

      }
      if (this.curLevel == 'all' && this.curTag == 'all') {
        this.allPosts = this.totalUnchangedPosts.filter(post => (post.creation_timestamp > this.fromDate && post.creation_timestamp < this.toDate));

      }
    }

    else {
      this.allPosts = this.totalUnchangedPosts.filter(post => (post.tag_id == this.curTag && post.level_id == this.curLevel && post.creation_timestamp > this.fromDate && post.creation_timestamp < this.toDate));
    }

    if (this.isOnlyFriend) {
      this.allPosts = this.allPosts.filter(post => this.friendIDS.includes(post.user_id));
    }

    if (this.isLikes) {

      if (this.likeArrow) {
        this.allPosts.sort(
          (post1, post2) => post1.likes_counter - post2.likes_counter
        );
      }

      else {
        this.allPosts.sort(
          (post1, post2) => post2.likes_counter - post1.likes_counter
        );
      }
    }

    else if (this.isComments) {

      if (this.comArrow) {

        this.allPosts.sort(
          (post1, post2) => post1.comments_counter - post2.comments_counter
        );
      }

      else {

        this.allPosts.sort(
          (post1, post2) => post2.comments_counter - post1.comments_counter
        );
      }
    }

    else if (this.isShares) {

      if (this.shareArrow) {
        this.allPosts.sort(
          (post1, post2) => post1.shares_counter - post2.shares_counter
        );
      }

      else {
        this.allPosts.sort(
          (post1, post2) => post2.shares_counter - post1.shares_counter
        );
      }
    }

    if (this.sByName != "") {
      console.log(this.allPosts);
      this.allPosts = this.allPosts.filter(post => post.user_name.toLowerCase().startsWith(this.sByName.toLowerCase()));
    }
  }

  onScroll(event: any) {
    console.log(event);
    this.postService.getAllPostsByStatus(this.groupId,"ACTIVE",this.fromPost,5).subscribe((posts: any) => {
      console.log(posts);

      if(posts.length){

        posts.forEach(element => {
          if (!element.comments_counter) {
            element.comments_counter = 0;
          }
  
          if (!element.likes_counter) {
            element.likes_counter = 0;
          }
  
          if (!element.shares_counter) {
            element.shares_counter = 0;
          }
        });

        this.allPosts = this.allPosts.concat(posts).slice(0);
        this.totalUnchangedPosts = this.allPosts.slice(0);
        this.fromPost = this.fromPost + 5;
        console.log(this.allPosts);
      }

      else this.noMorePosts = true;

    });

  }


  openSettingPage() {
    this.router.navigate(['/home/profile-setting']);

  }


  getAdress(adressId) {
    this.addressService.getAdress(adressId).subscribe(data => {
      console.log('adress data found');
      console.log(data);

      this.city = data.city;

      if (data.area != null && data.area != '' && data.area != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.city != null && data.city != '' && data.city != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.country != null && data.country != '' && data.country != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.landmark != null && data.landmark != '' && data.landmark != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.plot_number != null && data.plot_number != '' && data.plot_number != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.state != null && data.state != '' && data.state != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.street_name != null && data.street_name != '' && data.street_name != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.zip != null && data.zip != '' && data.zip != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }

    })
    if (this.profilePercentage > 100) {
      this.profilePercentage = 100;
    }
  }

  onClickOutside(event:any) {
  
     if(event && event['value'] === true) {
      if(this.exFilter) {
        this.filterpanel.nativeElement.click();
      }
   }
    
  }

}
