export class Course{

    constructor(){};

private books_list:string[];
private category: string;
private chapter: string;
private class: string;
private comments: string;
private concept: string;
private course: string;
private course_admin_list:string[];
private course_content_list:string[];
private course_custom_id: string;
private course_description: string;
private course_id: string;
private course_modules_list:string[];
private course_name: string;
private course_type_id: string;
private created_by: string;
private creation_timestamp: number;
private details: string;
private education_level: string;
private enabled_for_groups:string[];
private formatted_course_description: string;
private heading: string;
private image_id: string;
private is_education_content: true;
private language: string;
private last_updated_timestamp:number;
private mapped_to_tags:string[];
private question_set_list:string[];
private status: string;
private stream: string;
private sub_class: string;
private subject: string;
private syllabus_entry_list:string[];
private syllabus_pdf: string[];


    /**
     * Getter $books_list
     * @return {string[]}
     */
	public get $books_list(): string[] {
		return this.books_list;
	}

    /**
     * Getter $category
     * @return {string}
     */
	public get $category(): string {
		return this.category;
	}

    /**
     * Getter $chapter
     * @return {string}
     */
	public get $chapter(): string {
		return this.chapter;
	}

    /**
     * Getter $class
     * @return {string}
     */
	public get $class(): string {
		return this.class;
	}

    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $concept
     * @return {string}
     */
	public get $concept(): string {
		return this.concept;
	}

    /**
     * Getter $course
     * @return {string}
     */
	public get $course(): string {
		return this.course;
	}

    /**
     * Getter $course_admin_list
     * @return {string[]}
     */
	public get $course_admin_list(): string[] {
		return this.course_admin_list;
	}

    /**
     * Getter $course_content_list
     * @return {string[]}
     */
	public get $course_content_list(): string[] {
		return this.course_content_list;
	}

    /**
     * Getter $course_custom_id
     * @return {string}
     */
	public get $course_custom_id(): string {
		return this.course_custom_id;
	}

    /**
     * Getter $course_description
     * @return {string}
     */
	public get $course_description(): string {
		return this.course_description;
	}

    /**
     * Getter $course_id
     * @return {string}
     */
	public get $course_id(): string {
		return this.course_id;
	}

    /**
     * Getter $course_modules_list
     * @return {string[]}
     */
	public get $course_modules_list(): string[] {
		return this.course_modules_list;
	}

    /**
     * Getter $course_name
     * @return {string}
     */
	public get $course_name(): string {
		return this.course_name;
	}

    /**
     * Getter $course_type_id
     * @return {string}
     */
	public get $course_type_id(): string {
		return this.course_type_id;
	}

    /**
     * Getter $created_by
     * @return {string}
     */
	public get $created_by(): string {
		return this.created_by;
	}

    /**
     * Getter $creation_timestamp
     * @return {number}
     */
	public get $creation_timestamp(): number {
		return this.creation_timestamp;
	}

    /**
     * Getter $details
     * @return {string}
     */
	public get $details(): string {
		return this.details;
	}

    /**
     * Getter $education_level
     * @return {string}
     */
	public get $education_level(): string {
		return this.education_level;
	}

    /**
     * Getter $enabled_for_groups
     * @return {string[]}
     */
	public get $enabled_for_groups(): string[] {
		return this.enabled_for_groups;
	}

    /**
     * Getter $formatted_course_description
     * @return {string}
     */
	public get $formatted_course_description(): string {
		return this.formatted_course_description;
	}

    /**
     * Getter $heading
     * @return {string}
     */
	public get $heading(): string {
		return this.heading;
	}

    /**
     * Getter $image_id
     * @return {string}
     */
	public get $image_id(): string {
		return this.image_id;
	}

    /**
     * Getter $is_education_content
     * @return {true}
     */
	public get $is_education_content(): true {
		return this.is_education_content;
	}

    /**
     * Getter $language
     * @return {string}
     */
	public get $language(): string {
		return this.language;
	}

    /**
     * Getter $last_updated_timestamp
     * @return {number}
     */
	public get $last_updated_timestamp(): number {
		return this.last_updated_timestamp;
	}

    /**
     * Getter $mapped_to_tags
     * @return {string[]}
     */
	public get $mapped_to_tags(): string[] {
		return this.mapped_to_tags;
	}

    /**
     * Getter $question_set_list
     * @return {string[]}
     */
	public get $question_set_list(): string[] {
		return this.question_set_list;
	}

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Getter $stream
     * @return {string}
     */
	public get $stream(): string {
		return this.stream;
	}

    /**
     * Getter $sub_class
     * @return {string}
     */
	public get $sub_class(): string {
		return this.sub_class;
	}

    /**
     * Getter $subject
     * @return {string}
     */
	public get $subject(): string {
		return this.subject;
	}

    /**
     * Getter $syllabus_entry_list
     * @return {string[]}
     */
	public get $syllabus_entry_list(): string[] {
		return this.syllabus_entry_list;
	}

    /**
     * Getter $syllabus_pdf
     * @return {string[]}
     */
	public get $syllabus_pdf(): string[] {
		return this.syllabus_pdf;
	}

    /**
     * Setter $books_list
     * @param {string[]} value
     */
	public set $books_list(value: string[]) {
		this.books_list = value;
	}

    /**
     * Setter $category
     * @param {string} value
     */
	public set $category(value: string) {
		this.category = value;
	}

    /**
     * Setter $chapter
     * @param {string} value
     */
	public set $chapter(value: string) {
		this.chapter = value;
	}

    /**
     * Setter $class
     * @param {string} value
     */
	public set $class(value: string) {
		this.class = value;
	}

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $concept
     * @param {string} value
     */
	public set $concept(value: string) {
		this.concept = value;
	}

    /**
     * Setter $course
     * @param {string} value
     */
	public set $course(value: string) {
		this.course = value;
	}

    /**
     * Setter $course_admin_list
     * @param {string[]} value
     */
	public set $course_admin_list(value: string[]) {
		this.course_admin_list = value;
	}

    /**
     * Setter $course_content_list
     * @param {string[]} value
     */
	public set $course_content_list(value: string[]) {
		this.course_content_list = value;
	}

    /**
     * Setter $course_custom_id
     * @param {string} value
     */
	public set $course_custom_id(value: string) {
		this.course_custom_id = value;
	}

    /**
     * Setter $course_description
     * @param {string} value
     */
	public set $course_description(value: string) {
		this.course_description = value;
	}

    /**
     * Setter $course_id
     * @param {string} value
     */
	public set $course_id(value: string) {
		this.course_id = value;
	}

    /**
     * Setter $course_modules_list
     * @param {string[]} value
     */
	public set $course_modules_list(value: string[]) {
		this.course_modules_list = value;
	}

    /**
     * Setter $course_name
     * @param {string} value
     */
	public set $course_name(value: string) {
		this.course_name = value;
	}

    /**
     * Setter $course_type_id
     * @param {string} value
     */
	public set $course_type_id(value: string) {
		this.course_type_id = value;
	}

    /**
     * Setter $created_by
     * @param {string} value
     */
	public set $created_by(value: string) {
		this.created_by = value;
	}

    /**
     * Setter $creation_timestamp
     * @param {number} value
     */
	public set $creation_timestamp(value: number) {
		this.creation_timestamp = value;
	}

    /**
     * Setter $details
     * @param {string} value
     */
	public set $details(value: string) {
		this.details = value;
	}

    /**
     * Setter $education_level
     * @param {string} value
     */
	public set $education_level(value: string) {
		this.education_level = value;
	}

    /**
     * Setter $enabled_for_groups
     * @param {string[]} value
     */
	public set $enabled_for_groups(value: string[]) {
		this.enabled_for_groups = value;
	}

    /**
     * Setter $formatted_course_description
     * @param {string} value
     */
	public set $formatted_course_description(value: string) {
		this.formatted_course_description = value;
	}

    /**
     * Setter $heading
     * @param {string} value
     */
	public set $heading(value: string) {
		this.heading = value;
	}

    /**
     * Setter $image_id
     * @param {string} value
     */
	public set $image_id(value: string) {
		this.image_id = value;
	}

    /**
     * Setter $is_education_content
     * @param {true} value
     */
	public set $is_education_content(value: true) {
		this.is_education_content = value;
	}

    /**
     * Setter $language
     * @param {string} value
     */
	public set $language(value: string) {
		this.language = value;
	}

    /**
     * Setter $last_updated_timestamp
     * @param {number} value
     */
	public set $last_updated_timestamp(value: number) {
		this.last_updated_timestamp = value;
	}

    /**
     * Setter $mapped_to_tags
     * @param {string[]} value
     */
	public set $mapped_to_tags(value: string[]) {
		this.mapped_to_tags = value;
	}

    /**
     * Setter $question_set_list
     * @param {string[]} value
     */
	public set $question_set_list(value: string[]) {
		this.question_set_list = value;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}

    /**
     * Setter $stream
     * @param {string} value
     */
	public set $stream(value: string) {
		this.stream = value;
	}

    /**
     * Setter $sub_class
     * @param {string} value
     */
	public set $sub_class(value: string) {
		this.sub_class = value;
	}

    /**
     * Setter $subject
     * @param {string} value
     */
	public set $subject(value: string) {
		this.subject = value;
	}

    /**
     * Setter $syllabus_entry_list
     * @param {string[]} value
     */
	public set $syllabus_entry_list(value: string[]) {
		this.syllabus_entry_list = value;
	}

    /**
     * Setter $syllabus_pdf
     * @param {string[]} value
     */
	public set $syllabus_pdf(value: string[]) {
		this.syllabus_pdf = value;
	}


}
    
  