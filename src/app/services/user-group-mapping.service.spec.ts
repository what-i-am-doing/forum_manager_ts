import { TestBed } from '@angular/core/testing';

import { UserGroupMappingService } from './user-group-mapping.service';

describe('UserGroupMappingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserGroupMappingService = TestBed.get(UserGroupMappingService);
    expect(service).toBeTruthy();
  });
});
