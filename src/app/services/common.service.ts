import { Injectable, Component } from '@angular/core';
import { UserService } from './user.service';
import { ImageService } from './image.service';


@Injectable({
  providedIn: 'root'
})

export class CommonService {
  public userAllData: Array<any> = [];
  public suggestionsAllData: Array<any> = [];

constructor(private service:UserService,private imageservice:ImageService) { }



getUserAllDetails(userId, grouopId){
this.getUserDetails(userId);


return this.userAllData;
}




getUserDetails(userId):any{
  this.service.getUserDetails(userId).subscribe(getUserDetailsdata=>{

   console.log(getUserDetailsdata);
     this.userAllData.push(getUserDetailsdata);

    
     if(getUserDetailsdata.image_id!='string'){
      try{

        this.getProfilePic(getUserDetailsdata.image_id);
      
      }catch(err){


      }
     
    }
    


  })



}



getProfilePic(imageId){
this.imageservice.getImage(imageId).subscribe(imagedata=>{

this.userAllData.push(imagedata);

})


}




}
