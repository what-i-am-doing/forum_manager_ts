import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from "@angular/core";
import { Lightbox, LightboxConfig, LightboxEvent, LIGHTBOX_EVENT, IEvent, IAlbum } from "ngx-lightbox";
import { CommentService } from "../../services/comment.service";
import { PostService } from "../../services/post.service";
import { EventService } from "../../services/event.service";
import { LikeService } from "../../services/like.service";
import { ReportService } from "../../services/report.service";
import { ShareService } from "../../services/share.service";
import { OpengraphService } from "../../services/opengraph.service";
import { Comment } from "../../model/comment";
import { Subscription, timer } from "rxjs";
import { PdfService } from "src/app/services/pdf.service";
import { PostEvent } from "src/app/model/event";
import { UserService } from "../../services/user.service";
import { ImageService } from "../../services/image.service";
import { VideoService } from "../../services/video.service";
import { MatMenuTrigger } from "@angular/material/menu";
import { Router } from "@angular/router";
import { Post } from "src/app/model/post";
import * as pdfjs from "pdfjs-dist";
import { TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: "app-post",
  templateUrl: "./post.component.html",
  styleUrls: ["./post.component.css"],
  providers: [CommentService, PostService, BsModalService, EventService, LikeService, UserService
  ]
})

export class PostComponent implements OnInit {
  modalRef: BsModalRef;
  noevent: boolean = false;
  constructor(
    private imageService: ImageService,
    private pdfService: PdfService,
    private videoService: VideoService,
    private userService: UserService,
    private shareService: ShareService,
    private opengraphService: OpengraphService,
    private likeService: LikeService,
    private reportService: ReportService,
    private eventService: EventService,
    private postService: PostService,
    private _lightbox: Lightbox,
    private _lightboxEvent: LightboxEvent,
    private _lighboxConfig: LightboxConfig,
    private commentService: CommentService,
    private router: Router,
    private modalService: BsModalService
  ) {
    this._lighboxConfig.fadeDuration = 1;
  }

  @ViewChild("msgpara", { read: ElementRef }) msgpara: ElementRef;
  @ViewChild("appendOGraph", { read: ElementRef }) appendOGraph: ElementRef;
  @Output() editPost = new EventEmitter<any>();
  @Output() emitShared = new EventEmitter<any>();
  edit: boolean = false;
  share: boolean = false;
  comments_counter: number;
  shares_counter: number;
  userDetail: any;
  lastLink: string;
  tagFound: boolean = false;
  beforePrevious: number = 0;
  previousKey: number = 0;
  currentTag: string;
  taggedUsers: Array<any> = [];
  likeUser: Array<any> = [];
  userID: string = localStorage.getItem("user");
  userGroupID: string = JSON.parse(localStorage.getItem("user_group"))[0];
  you_like: boolean = false;
  liked_event: any;
  you_shared: boolean = false;
  shared_event: any;
  likedData: Array<any> = [];
  likedDataForTooltip: Array<any> = [];
  sharedDataForTooltip: Array<any> = [];
  commentdataDataForTooltip: Array<any> = [];
  reportdataDataForTooltip: Array<any> = [];
  showList: boolean = false;
  notReply: boolean = true;
  sub: Subscription;
  sub1: Subscription;
  start: number;
  end: number;
  startIndex: number = 0;
  endIndex: number = 5;
  originalUser: any;
  previewPdf: any;
  previewVideo: any;
  showVideo: boolean = false;
  showPdf: boolean = false;
  profilePic: string;
  firstName: string = "";
  lastName: string = "";
  uFirstName: string = localStorage.getItem("first_name");
  uLastName: string = localStorage.getItem("last_name");
  public activeprofilePic: any;
  public Activegender: any;
  public showCommentLoader: any;
  public sentFriendRequestListids: Array<any> = [];
  public friendids: Array<any> = [];

  ngOnInit() {
    console.log(this.singlePost);
    if (this.isNotif) this.showComments = true;
    this.getUsersDetailsOfActiveUser(this.userId);
    this.getActiveUserPersonalDetailsOfActiveUser(this.userId);
    this.getAllEventonPost(this.userGroupID, this.singlePost.post_id);
    //this.getAllEventonPostByInterval(this.userGroupID, this.singlePost.post_id, +new Date(), +new Date() + Number(600000)), 60 * 1000


    this.comments_counter =
      "comments_counter" in this.singlePost
        ? this.singlePost.comments_counter
        : 0;
    this.shares_counter =
      "shares_counter" in this.singlePost ? this.singlePost.shares_counter : 0;
    if (this.singlePost.post_content) {
      this.postService
        .getPostContent(this.singlePost.post_content)
        .subscribe(datalist => {
          console.log(datalist);

          if (datalist.content_images_list) {
            this.singlePost._albums = [];
            this.singlePost._real_albums = [];
            datalist.content_images_list.forEach(element => {
              this.imageService.getImage(element).subscribe(imgData => {
                console.log(imgData);
                this.imageService.getImageByType(element, 'thumbnail').subscribe(
                  img => {
                    if (img.length) var thumb = img[0].image_storage_url.replace(
                                                                              "https",
                                                                              "http"
                                                                            );

                    else var thumb = imgData.image_storage_url.replace(
                                                                          "https",
                                                                          "http"
                                                                        );


                    const src = imgData.image_storage_url.replace("https", "http");
                    const caption = imgData.image_name;

                    if (imgData.image_id == this.singlePost.img_id) {
                      var is_preview = true;
                      imgData.is_preview = true;
                    }
                    else is_preview = false;
                    const album = {
                      src: src,
                      caption: caption,
                      thumb: thumb,
                      is_preview: is_preview
                    };
                    console.log(album);
                    (album.is_preview) ? this.singlePost._albums.unshift(album) : this.singlePost._albums.push(album);
                    (album.is_preview) ? this.singlePost._real_albums.unshift(imgData) : this.singlePost._real_albums.push(imgData);
                  }
                );
              });
            });
          }

          if (datalist.content_pdf_list) {
            if (datalist.content_pdf_list.includes(this.singlePost.img_id)) {
              this.pdfService.getPdf(this.singlePost.img_id).subscribe(
                data => {
                  this.previewPdf = data;
                  this.showPDF(this.URLChange(data.pdf_storage_url));
                }

              );
            }
            this.singlePost._pdfs = [];
            datalist.content_pdf_list.forEach(element => {
              this.pdfService.getPdf(element).subscribe(pdfData => {
                console.log(pdfData);
                this.singlePost._pdfs.push(pdfData);
              });
            });
          }

          if (datalist.content_video_list) {
            if (datalist.content_video_list.includes(this.singlePost.img_id)) {
              this.videoService.getVideo(this.singlePost.img_id).subscribe(
                data => this.previewVideo = data
              );
            }
            this.singlePost._videos = [];
            datalist.content_video_list.forEach(element => {
              this.videoService.getVideo(element).subscribe(videoData => {
                console.log(videoData);
                this.singlePost._videos.push(videoData);
              });
            });
          }
        });
    }


    this.commentService
      .getCommentByParent(this.singlePost.post_id, 'top_level', 0, 10)
      .subscribe(data => {
        console.log(this.singlePost.post_id + ":" + data);
        if (Object.keys(data).length) {
          var key = Object.keys(data)[0];
          data[key].forEach(element => {
            console.log(element);
            this.comments.push(element);
          });
        }
      });

    this.userService.getUserDetails(this.singlePost.user_id).subscribe(user => {
      console.log(user);
      this.singlePost.user_name = user.user_full_text_name;
      this.userDetail = user;
      this.firstName = user.first_name;
      this.lastName = user.last_name;
      if (this.singlePost.post_type == "SHARED") {

        this.userService.getUserDetails(this.singlePost.original_user_id).subscribe(
          data => this.originalUser = data
        );

        this.postService.getPostById(this.singlePost.original_post_id).subscribe(
          post => this.originalPost = post
        );

      }
      if (user.image_id && user.image_id != 'string') this.getProfile(user.image_id);

    });

    if (this.singlePost.shares_counter) {
      this.postService
        .isSharedTrue(localStorage.getItem("user"), this.singlePost.post_id)
        .subscribe(share => {
          if (share.length >= 1) {
            this.you_shared = true;
            this.shared_event = share[0].event_id;
          }
        });
    }

    if (this.singlePost.likes_counter) {
      this.postService
        .isLikedTrue(localStorage.getItem("user"), this.singlePost.post_id)
        .subscribe(like => {
          if (like.length == 1) {
            this.you_like = true;
            this.liked_event = like[0].event_id;
          }
        });
    }


    this.eventService.evtByEntityNType(this.userGroupID, "POST", this.singlePost.post_id, "like", 0, 10).subscribe(
      data => {
        console.log(data);
        this.likedData = data;
        //this.getLikesAtInterval();
      }
    );

    this.getOpenGraphLinks();

  }

  ngOnDestroy() {
    if (this.sub)
      this.sub.unsubscribe();
    if (this.sub1)
      this.sub1.unsubscribe();
  }

  @ViewChild("commentBox", { read: ElementRef }) commentBox: ElementRef;
  expanded: boolean = false;
  comments: Array<any> = [];
  public mobileFriendlyZoomPercent = false;
  active: boolean = false;
  showComments: boolean = false;
  friends: Array<any> = [];
  filteredFriends: Array<any> = [];
  totalComments: number = 0;
  viewAbleComments: number = 0;
  userId: string = localStorage.getItem("user");
  userGroup: Array<string> = JSON.parse(localStorage.getItem("user_group"));
  userLevel: string = localStorage.getItem("user_level");
  userTag: string = localStorage.getItem("user_tag");
  public hidden: boolean = true;
  public ifMax: boolean = false;
  private _subscription: Subscription;
  originalPost: any;
  comment: Comment = new Comment();
  @Input() singlePost: any;
  @Input() friendData: any;
  @Input() isNotif?: boolean;
  @Input() isSearch?: boolean;

  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;

  triggerMenu() {
    this.trigger.openMenu();
  }

  public get mobileFriendlyZoom(): string | undefined {
    if (this.mobileFriendlyZoomPercent) {
      return "200%";
    }
    return undefined;
  }

  open(index: number, album): void {
    this._subscription = this._lightboxEvent.lightboxEvent$.subscribe(
      (event: IEvent) => this._onReceivedEvent(event)
    );

    // override the default config
    this._lightbox.open(album, index, {
      wrapAround: true,
      showImageNumberLabel: true
    });
  }

  close(): void {
    // close lightbox programmatically
    this._lightbox.close();
  }

  private _onReceivedEvent(event: IEvent): void {
    if (event.id === LIGHTBOX_EVENT.CLOSE) {
      this._subscription.unsubscribe();
    }
  }

  getOpenGraphLinks() {
    console.log(this.singlePost.message);
    var filter = this.singlePost.message.replace(/&nbsp;/g, "");
    var links = this.findLinksInPost(filter);
    console.log(links);

    if (links != null) {
      this.lastLink = links[0];
      var reqURL = encodeURIComponent(this.lastLink);
      console.log(reqURL);
      this.opengraphService.getOgData(reqURL).subscribe(data => {
        data = data.hybridGraph;
        var main_container = document.createElement("div");
        main_container.style.border = "1px solid #c5c5c5";
        main_container.style.padding = "5px";
        main_container.style.borderRadius = "5px";
        var footer_container = document.createElement("div");
        var heading = document.createElement("h4");
        heading.textContent = data.title;
        footer_container.appendChild(heading);
        var link = document.createElement("h5");
        var link_content = document.createElement("a");
        link_content.textContent = data.url;
        link.classList.add("cursor");
        link_content.setAttribute("href", data.url);
        link_content.setAttribute("title", "click with ctrl to open in a new window");
        link.appendChild(link_content);
        footer_container.appendChild(link);
        var describe = document.createElement("div");
        describe.textContent = data.description.substring(0, 200) + "...";
        footer_container.appendChild(describe);
        var img = document.createElement("img");
        img.src = data.image;
        img.width = 150;
        img.height = 70;
        main_container.appendChild(img);
        main_container.appendChild(footer_container);
        var container = this.appendOGraph.nativeElement;
        container.appendChild(main_container);
      });
    }
  }

  findLinksInPost(str) {
    var pattern = /(?:(?:https?|ftp):\/\/|\b(?:[a-z\d]+\.))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))?/gi;
    var result = str.match(pattern);
    console.log(result);
    return result;
  }

  searchFriend(event: any) {
    console.log(this.commentBox.nativeElement.innerHTML);
    if (event.keyCode == 13) {
      event.preventDefault();
      this.commentBox.nativeElement.innerHTML = this.commentBox.nativeElement.innerHTML.replace(
        "<div><br></div>",
        ""
      );
      return this.submitComment();
    }

    this.comment.$message = this.commentBox.nativeElement.innerHTML;
    var str = this.comment.$message;

    if (this.tagFound === true) {
      console.log("found");
      if (str.includes("@")) {
        var str = str.substring(str.lastIndexOf("@") + 1);
        this.currentTag = str;
        console.log(this.filteredFriends);
        this.filteredFriends = this.friendData.filter(word =>
          word.user_full_text_name.startsWith(str)
        );
        console.log(this.filteredFriends);
      } else this.tagFound = false;
    } else if (
      (this.comment.$message == "@" ||
        this.beforePrevious == 32 ||
        this.beforePrevious == 8) &&
      (event.key == 2 && this.previousKey == 16)
    ) {
      this.tagFound = true;
      this.start = str.length - 1;
      if (this.friendData.length > 6) this.filteredFriends = this.friendData.slice(0, 6);
      else this.filteredFriends = this.friendData;
      this.currentTag = str.substring(str.lastIndexOf("@") + 1);
    }


    this.beforePrevious = this.previousKey;
    this.previousKey = event.keyCode;
  }

  keydown(e: any) {
    if (e.keyCode == 40) {
      //If the arrow DOWN key is pressed,
      alert("arrow down");
    } else if (e.keyCode == 38) {
      //up
      //If the arrow UP key is pressed
      alert("arrow up");
    }
  }

  focusComment() {
    this.notReply = true;
    this.commentBox.nativeElement.focus();
  }

  submitComment() {
    var post_id = this.singlePost.post_id;
    this.comment.$message = this.commentBox.nativeElement.innerHTML;
    if ((this.comment.$message == "")) return;
    console.log(this.taggedUsers);
    if (this.taggedUsers.length) {
     // this.comment.$tagged_user_id = [];
      var temp_tags = this.taggedUsers;
      this.taggedUsers = [];
      temp_tags.forEach(element => {
        if (
          this.comment.$message.includes(Object.values(element)[0].toString())
        ) {
          this.taggedUsers.push(Object.values(element)[1]);
        }
      });

     // this.comment.$tagged_user_id = this.taggedUsers;
    }

    this.comment.$comment_on_entity = "POST";
    this.comment.$entity_id = post_id;
    this.comment.$from_user_id = localStorage.getItem("user");
    this.comment.$group_id = this.userGroup[0];
    this.comment.$parent_comment_id = 'top_level';
    if (!("comments_counter" in this.singlePost)) this.singlePost.comments_counter = 0;
    this.commentService.submitComment(this.comment).subscribe(data => {
      console.log(data);
      this.commentBox.nativeElement.innerHTML = "";
      //this.comments_counter = this.singlePost.comments_counter = this.comments_counter + 1;
      console.log(data);
      this.comment.$comment_id = data.id;
      const tempMyObj = Object.assign({}, this.comment);
      this.comments.push(tempMyObj);
      this.comment.$message = "";
      this.comment.$comment_id = "";

      this.postService
        .updatePost(post_id, { comments_counter: this.singlePost.comments_counter + 1 })
        .subscribe(data => {
          console.log(data);
          this.singlePost.comments_counter = this.singlePost.comments_counter + 1;
          var newEvent = new PostEvent();
          newEvent.$from_user_id = localStorage.getItem("user");
          newEvent.$group_id = this.userGroup[0];
          newEvent.$event_type_id = "comment";
          newEvent.$entity_id = post_id;
          newEvent.$on_entity_type = "POST";
          newEvent.$status = "ACTIVE";
          this.eventService.submitEvent(newEvent).subscribe(data => {
            console.log(data);
          });
        });
    });
    this.showComments = true;
  }

  increaseLikes(post_id: string) {


    if (this.you_like == true) {
      this.likeService
        .increaseLike(post_id, {
          likes_counter: this.singlePost.likes_counter - 1
        })
        .subscribe(res => {
          console.log(res);
          console.log(this.singlePost.likes_counter);
          this.singlePost.likes_counter = this.singlePost.likes_counter - 1;
          console.log(this.singlePost.likes_counter);

          this.eventService
            .deleteEvent(this.liked_event)
            .subscribe(data => {
              console.log(data);
              this.you_like = false;
              this.liked_event = null;
            });
        });
    } else {

      if (!("likes_counter" in this.singlePost))
        this.singlePost.likes_counter = 0;

      this.likeService
        .increaseLike(post_id, {
          likes_counter: this.singlePost.likes_counter + 1
        })
        .subscribe(data => {
          console.log(data);
          console.log(this.singlePost.likes_counter);
          this.singlePost.likes_counter = this.singlePost.likes_counter + 1;
          var newEvent = new PostEvent();
          newEvent.$from_user_id = localStorage.getItem("user");
          newEvent.$event_type_id = "like";
          newEvent.$group_id = this.userGroup[0];
          newEvent.$entity_id = post_id;
          newEvent.$on_entity_type = "POST";
          newEvent.$status = "ACTIVE";
          this.eventService.submitEvent(newEvent).subscribe(data => {
            console.log(data);
            this.you_like = true;
            this.liked_event = data.id;
          });
        });
    }

  }

  increaseShares(event: any) {


    var shared_post = new Post;
    shared_post.$creation_timestamp = +new Date;
    shared_post.$group_id = this.userGroup;
    shared_post.$heading = event.heading;
    shared_post.$level_id = this.userLevel;
    shared_post.$tag_id = this.userTag;
    shared_post.$formatted_message = event.formatted_message;
    shared_post.$message = event.message;
    shared_post.$original_user_id = this.singlePost.original_user_id;
    shared_post.$original_post_id = this.singlePost.post_id;
    shared_post.$post_type = "SHARED";
    shared_post.$post_has_data = false;
    shared_post.$user_id = this.userID;


    console.log(shared_post);
    this.postService.userpost(shared_post).subscribe(shared => {
      console.log(shared);

      if (!("shares_counter" in this.singlePost)) this.singlePost.shares_counter = 0;

      this.shareService
        .increaseShare(this.singlePost.post_id, {
          shares_counter: this.singlePost.shares_counter + 1
        })
        .subscribe(data => {
          console.log(data);
          this.shares_counter = this.singlePost.shares_counter = this.shares_counter + 1;
          var newEvent = new PostEvent();
          newEvent.$from_user_id = localStorage.getItem("user");
          newEvent.$event_type_id = "share";
          newEvent.$group_id = this.userGroup[0];
          newEvent.$entity_id = this.singlePost.post_id;
          newEvent.$on_entity_type = "POST";
          newEvent.$status = "ACTIVE";
          this.eventService.submitEvent(newEvent).subscribe(data => {
            console.log(data);
            this.getCreatedPost(shared.id);
          });
        });
    });

  }

  getCreatedPost(postID: string) {
    this.postService.getPostById(postID).subscribe(post => {
      console.log(post);
      this.share = !this.share;
      var emitPost = Object.assign({}, post);
      this.emitShared.emit(emitPost);
    });
  }

  loadMoreComments() { }
  URLChange(pdf: any) {
    return pdf.replace("https", "http");
  }

  expandMsg(event: any) {
    console.log(event);
    this.msgpara.nativeElement.innerHTML = this.singlePost.formatted_message;
    this.expanded = !this.expanded;
  }
  // *this method need to be fixed
  selectFriend(friend: any) {
    var comment = this.commentBox.nativeElement.innerHTML;
    this.end = comment.length;
    this.commentBox.nativeElement.innerHTML = this.replaceBetween(this.start, this.end, "<a contenteditable='false'>@" + friend.user_full_text_name + "</a> &nbsp;", comment);

    // comment.replace(
    //   "@" + this.currentTag,
    //   "<a contenteditable='false'>@" + friend.user_full_text_name + "</a> &nbsp;"
    // );

    this.tagFound = false;
    this.taggedUsers.push({
      content:
        '<a contenteditable="false">@' + friend.user_full_text_name + "</a>",
      id: friend.user_id
    });
    console.log(this.commentBox.nativeElement.innerHTML);
    console.log(this.taggedUsers[0].content);

    console.log(
      this.commentBox.nativeElement.innerHTML.includes(
        this.taggedUsers[0].content
      )
    );
    this.tagFound = false;

  }
  emitEdit() {
    this.editPost.emit(this.singlePost);
  }

  submitReport() {

    if (!("reports_counter" in this.singlePost)) this.singlePost.reports_counter = 0;
    this.reportService
      .increaseReport(this.singlePost.post_id, {
        reports_counter: this.singlePost.reports_counter + 1
      })
      .subscribe(data => {
        console.log(data);
        this.singlePost.reports_counter = this.singlePost.reports_counter + 1;
        var newEvent = new PostEvent();
        newEvent.$from_user_id = localStorage.getItem("user");
        newEvent.$event_type_id = "report";
        newEvent.$group_id = this.userGroup[0];
        newEvent.$entity_id = this.singlePost.post_id;
        newEvent.$on_entity_type = "POST";
        newEvent.$status = "ACTIVE";
        this.eventService.submitEvent(newEvent).subscribe(data => {
          console.log(data);
        });
      });
  }

  updatePost(event: any) {
    console.log(event);
    this.singlePost.heading = event.heading;
    this.singlePost.formatted_message = event.formatted_message;
    this.singlePost.message = event.message;
    this.edit = !this.edit;
  }

  seeFriendProfile(userId) {

    this.router.navigate(['/home/userprofile'], { queryParams: { userId: userId } });
  }

  getLikesAtInterval() {
    this.sub = timer(0, 5000).subscribe(() => this.eventService.evtByEntityNTypeAtInterval(this.userGroupID, "POST", this.singlePost.post_id, "like", 0, 10, +new Date() - Number(5000),
      +new Date()).subscribe(
        data => console.log(data)
      )
    );
  }

  replaceBetween = function (start, end, what, string) {
    return string.substring(0, start) + what + string.substring(end);
  };

  getProfile(image_id) {

    this.imageService.getImageByType(image_id, 'thumbnail').subscribe(data => {
      if(data.length) this.profilePic = this.URLChange(data[0].image_storage_url);
      this.getProfileImage(image_id);
    })

  }

  getProfileImage(image_id) {

    this.imageService.getImage(image_id).subscribe(data => this.profilePic = this.URLChange(data.image_storage_url))
    
  }

  getUsersDetailsOfActiveUser(userId) {
    this.userService.getUserDetails(userId).subscribe(data => {
      if (data.image_id && data.image_id != 'string') this.getProfileOfActiveUser(data.image_id);

    })
  }

  getProfileOfActiveUser(image_id: string) {
    this.imageService.getImage(image_id).subscribe(data => {
      this.activeprofilePic = this.URLChange(data.image_storage_url);
    })
  }

  getActiveUserPersonalDetailsOfActiveUser(userId) {
    this.userService.getUserPersonalDetails(userId).subscribe(data => {

      this.Activegender = data.gender_type;
      console.log(data.gender_type);


    })
  }

  public _PDF_DOC;

  // PDF.JS renders PDF in a <canvas> element
  // _CANVAS = <HTMLCanvasElement>document.querySelector('#pdf-preview');
  @ViewChild("pdfpreview", { read: ElementRef }) _CANVAS: ElementRef;

  // will hold object url of chosen PDF
  _OBJECT_URL;

  // load the PDF
  showPDF(pdf_url) {


    pdfjs.getDocument({ url: pdf_url }).promise.then((pdf_doc) => {
      this._PDF_DOC = pdf_doc;

      // show the first page of PDF
      this.showPage(1);

      // destroy previous object url
      URL.revokeObjectURL(this._OBJECT_URL);
    }), (reason) => { console.error(reason); }

  }

  showPage(page_no: any) {
    this._PDF_DOC.getPage(page_no).then((page) => {
      // set the scale of viewport
      var scale_required = this._CANVAS.nativeElement.width / page.getViewport(1).width;

      // get viewport of the page at required scale
      var viewport = page.getViewport(scale_required);

      // set canvas height
      this._CANVAS.nativeElement.height = viewport.height;

      var renderContext = {
        canvasContext: this._CANVAS.nativeElement.getContext('2d'),
        viewport: viewport
      };

      // render the page contents in the canvas
      page.render(renderContext).then(() => {
        this._CANVAS.nativeElement.style.display = 'inline-block';

      });
    });

  }





  getAllEventonPost(groupId, entityId) {


    this.postService.getAllEventonPost(groupId, entityId).subscribe(data => {
      console.log('all event received on a single post');
      console.log(data);
      if (!data.length) {
        this.noevent = true;
      }

      console.log(data.like);
      var likeddata = [];
      likeddata = data.like;
      var shareddata = [];
      shareddata = data.share;
      var commentdata = [];
      commentdata = data.comment;
      var reportdata = [];
      reportdata = data.report;
      if (likeddata != undefined && likeddata.length != undefined && likeddata.length != null) {
        console.log('inside like if');
        const uniqueArray = this.removeDuplicates(likeddata, 'from_user_id')
        this.likedDataForTooltip = uniqueArray;
        this.likedDataForTooltip.sort(
          (post1, post2) => post2.creation_timestamp - post1.creation_timestamp
        );

      }
      if (shareddata != undefined && shareddata.length != undefined && shareddata.length != null) {
        console.log('inside share if');
        const uniqueArray = this.removeDuplicates(shareddata, 'from_user_id')
        this.sharedDataForTooltip = uniqueArray;
        this.sharedDataForTooltip.sort(
          (post1, post2) => post2.creation_timestamp - post1.creation_timestamp
        );
      }
      if (commentdata != undefined && commentdata.length != undefined && commentdata.length != null) {

        const uniqueArray = this.removeDuplicates(commentdata, 'from_user_id')

        this.commentdataDataForTooltip = uniqueArray;
        this.commentdataDataForTooltip.sort(
          (post1, post2) => post2.creation_timestamp - post1.creation_timestamp
        );
      }
      if (reportdata != undefined && reportdata.length != undefined && reportdata.length != null) {
        console.log('inside report if');
        const uniqueArray = this.removeDuplicates(reportdata, 'from_user_id')
        this.reportdataDataForTooltip = uniqueArray;

        this.reportdataDataForTooltip.sort(
          (post1, post2) => post2.creation_timestamp - post1.creation_timestamp
        );
      }
    })
  }

  removeDuplicates(myArr, prop) {
    console.log('duplicates called')
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }



  seeMore(startIndex, endIndex) {
    console.log('see more clicked');
    console.log(startIndex);
    console.log(endIndex);
    if (this.endIndex < this.sharedDataForTooltip.length) {
      this.startIndex = this.startIndex + 5;
      this.endIndex = this.endIndex + 5;
    }
    else {
      this.startIndex = 0;
      this.endIndex = 5;
    }
  }

  getAllEventonPostByInterval(groupId, entityId, start, end) {

    this.sub1 = this.postService.getAllEventonPostatInterval(groupId, entityId, start, end).subscribe(data => {
      console.log(data);
      if (Object.keys(data).length) {

        var timestamp = [];
        if (data.like) {
          data.like.forEach(element => {
            this.likedDataForTooltip.push(element);
          });

          this.likedDataForTooltip = this.removeDuplicates(this.likedDataForTooltip, 'from_user_id')
          this.likedDataForTooltip.sort(
            (post1, post2) => post2.creation_timestamp - post1.creation_timestamp
          );

          timestamp.push(this.likedDataForTooltip[0].creation_timestamp)

        }
        if (data.share) {

          data.share.forEach(element => {
            this.sharedDataForTooltip.push(element);
          });

          this.sharedDataForTooltip = this.removeDuplicates(this.sharedDataForTooltip, 'from_user_id')
          this.sharedDataForTooltip.sort(
            (post1, post2) => post2.creation_timestamp - post1.creation_timestamp
          );
          timestamp.push(this.sharedDataForTooltip[0].creation_timestamp)
        }
        if (data.comment) {

          data.comment.forEach(element => {
            this.commentdataDataForTooltip.push(element);
          });

          this.commentdataDataForTooltip = this.removeDuplicates(this.commentdataDataForTooltip, 'from_user_id')
          this.commentdataDataForTooltip.sort(
            (post1, post2) => post2.creation_timestamp - post1.creation_timestamp
          );
          timestamp.push(this.commentdataDataForTooltip[0].creation_timestamp)
        }
        if (data.report) {
          data.report.forEach(element => {
            this.reportdataDataForTooltip.push(element);
          });
          this.reportdataDataForTooltip = this.removeDuplicates(this.reportdataDataForTooltip, 'from_user_id')
          this.reportdataDataForTooltip.sort(
            (post1, post2) => post2.creation_timestamp - post1.creation_timestamp
          );
          timestamp.push(this.reportdataDataForTooltip[0].creation_timestamp)
        }


        var max = Math.max.apply(null, timestamp);
        console.log(max);
        this.getAllEventonPostByInterval(groupId, entityId, max + 1, max + 1 + Number(600000))


      }
      else {

        this.getAllEventonPostByInterval(groupId, entityId, +new Date(), +new Date() + Number(600000))
      }


    })


  }

  displayCommentCounter(comment) {
    console.log('comment data');
    this.showCommentLoader = false;

  }

  openModal(template: TemplateRef<any>) {

    this.getSentFriendRequestByActiveUser(localStorage.getItem("user"), this.userGroupID)
    this.getFriendListOfactiveUser(localStorage.getItem("user"), localStorage.getItem("groupId"), 0, 100)
    this.modalRef = this.modalService.show(template);

  }


  getSentFriendRequestByActiveUser(userId, groupId) {

    this.userService.getSentFriendRequestByActiveUser(userId, groupId).subscribe(data => {
      console.log("sent friend request  with   open status sent by current user")
      if (data != null || data != undefined)
        data.forEach(element => {
          if (element.req_status == 'OPEN') {
            this.sentFriendRequestListids.push(element.to_user_id);

          }
        });


    })
    console.log(this.sentFriendRequestListids)
    localStorage.setItem('sentFriendRequestListids', JSON.stringify(this.sentFriendRequestListids))

  }


  getFriendListOfactiveUser(userId: any, group_id, fromsize, toSize) {
    this.userService.getFriendListOfActiveUser(userId, group_id, fromsize, toSize).subscribe(data => {

      if (data != null || data != undefined) {

        data.forEach(element => {
          this.friendids.push(element.friend_user_id);
        });
      }
      console.log("all friends ids");
      console.log(this.friendids);
      localStorage.setItem('friendids', JSON.stringify(this.friendids))
    });
  }

  closeModal(event) {
    console.log(event);
    console.log('modal closed');
    this.modalRef.hide()

  }


}
