export class OgData {

                         constructor(
                             
                            public  data : {
                                "_id": string,
                                hybridGraph: {
                                    "title": string,
                                    "description": string,
                                    "type": string,
                                    "url": string,
                                    "favicon": string,
                                    "site_name": string,
                                    "image": string
                                },
                                "openGraph": {
                                    "title": string,
                                    "description": string,
                                    "type": string,
                                    "url": string,
                                    "site_name": string
                                },
                                "htmlInferred": {
                                    "title": string,
                                    "description": string,
                                    "type": string,
                                    "url": string,
                                    "favicon": string,
                                    "site_name": string,
                                    "images": [],
                                    "image_guess": string
                                },
                                "requestInfo": {
                                    "redirects": number,
                                    "host": string,
                                    "responseCode": number,
                                    "cache_ok": boolean,
                                    "max_cache_age": number,
                                    "accept_lang": string,
                                    "url": string,
                                    "full_render": boolean,
                                    "responseContentType": string
                                },
                                "accept_lang": string,
                                "url": string,
                                "__v": number,
                                "accessed": number,
                                "updated": string,
                                "created": string,
                                "is_cache": boolean
                            }

                                         
                         
                         ){};

               }