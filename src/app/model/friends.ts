export class Friends{

    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter firstname
     * @return {string}
     */
	public get firstname(): string {
		return this._firstname;
	}

    /**
     * Getter lastname
     * @return {string}
     */
	public get lastname(): string {
		return this._lastname;
	}

    /**
     * Getter mobile
     * @return {string}
     */
	public get mobile(): string {
		return this._mobile;
	}

    /**
     * Getter email
     * @return {string}
     */
	public get email(): string {
		return this._email;
	}

    /**
     * Getter address
     * @return {string}
     */
	public get address(): string {
		return this._address;
	}

    /**
     * Getter password
     * @return {string}
     */
	public get password(): string {
		return this._password;
	}

    /**
     * Getter birthday
     * @return {number}
     */
	public get birthday(): number {
		return this._birthday;
	}

    /**
     * Getter birthmonth
     * @return {string}
     */
	public get birthmonth(): string {
		return this._birthmonth;
	}

    /**
     * Getter birthyear
     * @return {number}
     */
	public get birthyear(): number {
		return this._birthyear;
	}

    /**
     * Getter gender
     * @return {string}
     */
	public get gender(): string {
		return this._gender;
	}

    /**
     * Getter isActive
     * @return {boolean}
     */
	public get isActive(): boolean {
		return this._isActive;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter firstname
     * @param {string} value
     */
	public set firstname(value: string) {
		this._firstname = value;
	}

    /**
     * Setter lastname
     * @param {string} value
     */
	public set lastname(value: string) {
		this._lastname = value;
	}

    /**
     * Setter mobile
     * @param {string} value
     */
	public set mobile(value: string) {
		this._mobile = value;
	}

    /**
     * Setter email
     * @param {string} value
     */
	public set email(value: string) {
		this._email = value;
	}

    /**
     * Setter address
     * @param {string} value
     */
	public set address(value: string) {
		this._address = value;
	}

    /**
     * Setter password
     * @param {string} value
     */
	public set password(value: string) {
		this._password = value;
	}

    /**
     * Setter birthday
     * @param {number} value
     */
	public set birthday(value: number) {
		this._birthday = value;
	}

    /**
     * Setter birthmonth
     * @param {string} value
     */
	public set birthmonth(value: string) {
		this._birthmonth = value;
	}

    /**
     * Setter birthyear
     * @param {number} value
     */
	public set birthyear(value: number) {
		this._birthyear = value;
	}

    /**
     * Setter gender
     * @param {string} value
     */
	public set gender(value: string) {
		this._gender = value;
	}

    /**
     * Setter isActive
     * @param {boolean} value
     */
	public set isActive(value: boolean) {
		this._isActive = value;
	}

  private _id:number;
  private _firstname: string;
  private _lastname:string;
  private _mobile:string;
  private _email:string;
  private _address:string;
  private _password:string;
  private _birthday:number;
  private _birthmonth:string;
  private _birthyear:number;
  private _gender:string;
  private _isActive:boolean;


constructor(
   
  

){}




}