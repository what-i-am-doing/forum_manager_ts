import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { UserInterests } from "../model/user_interests";
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { UserPersonal } from '../model/userPersonal';
import { environment } from '../../environments/environment';



@Injectable()
export class UserService {
  public url = environment.api_url;
  public serverurl: string = 'http://ec2-13-127-215-157.ap-south-1.compute.amazonaws.com:3000';
  //public serverurl:string="http://localhost:3000"


  youtubeurl = "https://www.googleapis.com/youtube/v3/search/";
  key = "AIzaSyAITmelOebWwViGEiWsnty5SqpsczeZlAE";

  youtubevideourl: string = "https://www.googleapis.com/youtube/v3/videos/";

  constructor(private http: HttpClient) {


  }


  //  create new user 
  addUser(userRegisterObj: any, userId: string): Observable<any> {

    console.log(" user register body");
    console.log("userId");
    console.log(userId);

    return this.http.post<any>(this.url +'/user/register?notify_user=true', userRegisterObj, {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'from_user_id': userId

      })
    })
      .pipe(
        catchError(this.handleError)
      );
  }
  // add user auth details
  addUserAuthDetail(authdetail: any, userId: string): Observable<any> {
    return this.http.post<any>(this.url + '/user_auth_details/', authdetail, {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'from_user_id': userId

      })
    })
      .pipe(
        catchError(this.handleError)
      );
  }

  // add users personal detail
  addUserPersonalDetails(personaldetail: any, userId: string): Observable<any> {
    console.log(" inside user service file");

    console.log(personaldetail);
    console.log(userId);

    return this.http.post<any>(this.url + '/user_personal_details/', personaldetail, {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'from_user_id': userId,

      })
    })
      .pipe(
        catchError(this.handleError)
      );
  }



  loginUserviaEmail(loginObj) {

    return this.http.post(this.url + '/user_auth_details/login/', loginObj,
      {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json',


        })
      }).pipe(

        retry(3),
        catchError(this.handleError)

      )


  }




  getUserDetails(userId):Observable<any> {
console.log(" get user details for>>>>>>>>>>>"+userId);
    return this.http.get<any>(this.url + '/user/' + userId,
      {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'from_user_id': userId,
        })
      }).pipe(
        catchError(this.handleError)
      )
  }


  getUserAuthDetails(userAuthlId: any):Observable<any> {

    return this.http.get<any>(this.url + '/user_auth_details/' + userAuthlId,
      {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'from_user_id': userAuthlId,

        })
      }).pipe(

        retry(3),
        catchError(this.handleError)

      )
  }


  getUserPersonalDetails(userId: any) :Observable<any>{
    return this.http.get<any>(this.url + '/user_personal_details/by_user?user_id=' + userId,
      {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'from_user_id': userId,

        })
      }).pipe(

    
        catchError(this.handleError)

      )

  }

  // to get educational details of the users
  getUserEducationDetails(userId: any):Observable<any> {
    return this.http.get<any>(this.url + '/user_education_details/by_user?user_id=' + userId,
      {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'from_user_id': userId,

        })
      }).pipe(

        retry(3),
        catchError(this.handleError)

      )

  }
  loginUserMobile(mobile, password) {

    return this.http.get(this.serverurl + '/user?user_personal.mobile=' + mobile + '&&user_auth.password=' + password).pipe(
      retry(3), // retry a failed request up to 3 times
      catchError(this.handleError) // then handle the error
    );

  }


  userInterests(): Observable<UserInterests[]> {

    return this.http.get<UserInterests[]>(this.serverurl + '/interest_type').pipe(

      retry(3),
      catchError(this.handleError)

    )
  }

    // update users new password after verify the OTP
    updateUserDetail(userDetailObj, userId):Observable<any> {
      console.log(" update detail in service");
  
  
      return this.http.put(this.url + '/user/'+userId, userDetailObj,
  
        {
          headers: new HttpHeaders({
  
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'from_user_id': userId,
  
          })
  
  
  
        }
      ).pipe(
  
        retry(3),
        catchError(this.handleError)
  
      )
  
    }
    updateUserAuthDetail(userAuthObj:any, authID:any) {
      console.log(" update auth detail in service");
  
  
      return this.http.put(this.url + '/user_auth_details/'+authID,userAuthObj,
  
        {
          headers: new HttpHeaders({
  
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'from_user_id': authID,
  
          })
  
  
  
        }
      ).pipe(
  
        retry(3),
        catchError(this.handleError)
  
      )
  
    }
    updateUsercourseDetail(userDetailObj :any, authID:any) {
      console.log(" update auth detail in service");
  
  
      return this.http.put(this.url + '/user/'+authID,userDetailObj,
  
        {
          headers: new HttpHeaders({
  
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'from_user_id': authID,
  
          })
  
  
  
        }
      ).pipe(
  
        retry(3),
        catchError(this.handleError)
  
      )
  
    }

    updateUserPersonalDetail(userPersonalObj:any, personalId:any) {
      console.log(" update personal detail in service");
  
  
      return this.http.put(this.url + '/user_personal_details/'+personalId,userPersonalObj, 
  
        {
          headers: new HttpHeaders({
  
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'from_user_id': personalId
  
          })
  
  
  
        }
      ).pipe(
  
        retry(3),
        catchError(this.handleError)
  
      )
  
    }


  



  getEducationType() {

    return this.http.get(this.serverurl + '/education_type').pipe(

      retry(3),
      catchError(this.handleError)

    )

  }

  getInstituteType() {

    return this.http.get(this.serverurl + '/institution_type').pipe(

      retry(3),
      catchError(this.handleError)

    )

  }

  getCourseType() {

    return this.http.get(this.serverurl + '/course_type').pipe(

      retry(3),
      catchError(this.handleError)

    )

  }

  getStreamType() {

    return this.http.get(this.serverurl + '/stream_type').pipe(

      retry(3),
      catchError(this.handleError)

    )

  }

  getClassType() {

    return this.http.get(this.serverurl + '/class_type').pipe(

      retry(3),
      catchError(this.handleError)

    )

  }

  getSectionType() {

    return this.http.get(this.serverurl + '/section_type').pipe(

      retry(3),
      catchError(this.handleError)

    )

  }
  getKycType() {

    return this.http.get(this.serverurl + '/kyc_type').pipe(

      retry(3),
      catchError(this.handleError)
    )

  }

  checkUserMobile(mobile) {

    return this.http.get(this.serverurl + '/user/?mobile=' + mobile).pipe(
      retry(3), // retry a failed request up to 3 times
      catchError(this.handleError) // then handle the error
    );

  }


  checkUserEmail(emailobj: any): Observable<any> {
    console.log(" inside user service file");

    console.log(emailobj);




    return this.http.post<any>(this.url + '/user_auth_details/otp/', emailobj, {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',

      })
    })
      .pipe(
        catchError(this.handleError)
      );
  }




  // update users new password after verify the OTP
  updateUserPassword(userAuthdetailObj, OtpId) {
    console.log(" update user password in service");


    return this.http.put(this.url + '/user_auth_details/update_by_otp/' + OtpId, userAuthdetailObj,

      {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Access-Control-Allow-Methods': 'PUT'

        })



      }
    ).pipe(

      retry(3),
      catchError(this.handleError)

    )

  }
  // to reset password
  updateUserEducationalDetails(educationobj:any, user_education_details_id:any){




    return this.http.put(this.url + '/user_education_details/' + user_education_details_id, educationobj,

      {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'from_user_id': user_education_details_id,
        })



      }
    ).pipe(

      retry(3),
      catchError(this.handleError)

    )







  }

  // getUsersforSuggestions

  getUsersforSuggestions(groupId, userId,fromsize, tosize) {
 
    return this.http.get(this.url +'/friend/suggestions?group_id='+groupId+'&user_id='+userId+'&from='+fromsize+'&size='+tosize,

    {
      headers: new HttpHeaders({

        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'from_user_id': userId,
      })



    }
  ).pipe(

    retry(3),
    catchError(this.handleError)

  )

  }

sendFriendRequest(requestObj: any, userId: string): Observable<any> {
    return this.http.post<any>(this.url + '/friend_request/', requestObj, {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'from_user_id': userId,

      })
    })
      .pipe(
        catchError(this.handleError)
      );
  }


acceptFriendRequest(AcceptrequestObj: any, userId: string): Observable<any> {
    return this.http.post<any>(this.url + '/friend/', AcceptrequestObj, {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'from_user_id':userId,

      })
    })
      .pipe(
        catchError(this.handleError)
      );
  }


deleteFriendRequest(DetailObj:any, userId:any) {
  console.log(" update detail in service");


  return this.http.put(this.url +'/friend_request/'+DetailObj.friend_req_id, DetailObj,

    {
      headers: new HttpHeaders({

        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'from_user_id': userId,

      })



    }
  ).pipe(

    retry(3),
    catchError(this.handleError)

  )

}

  getFriendListOfActiveUser(userId:any,groupid,fromSize, toSize):Observable<any> {
  
    return this.http.get<any>(this.url + '/friend/by_user/'+userId+'?group_id='+groupid+'&from='+fromSize+'&size='+toSize,

    {
      headers: new HttpHeaders({

        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'from_user_id': userId,
      })



    }
  ).pipe(

    retry(3),
    catchError(this.handleError)

  )

  }

  uploadUserDocument(docObj: any, userId: string): Observable<any> {
    return this.http.post<any>(this.url + '/user_doc_store/', docObj, {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'from_user_id':userId,

      })
    })
      .pipe(
        catchError(this.handleError)
      );
  }


  getUserDocumentDetails(userId) {
  
    return this.http.get(this.url +'/user_doc_store/by_user?user_id='+userId+'&from=1'+'&size=10',

    {
      headers: new HttpHeaders({

        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'from_user_id': userId,
      })



    }
  ).pipe(

    retry(3),
    catchError(this.handleError)

  )

  }


//   send friend request  
uploadUserdocument(documentobj: any, userId: string): Observable<any> {
  return this.http.post<any>(this.url + '/user_doc_store/', documentobj, {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'from_user_id': userId,

    })
  })
    .pipe(
      catchError(this.handleError)
    );
}

getUserDocumentDataById(docId): Observable<any> {
  return this.http.get<any>(this.url +'/user_doc_store/'+docId, {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'from_user_id': localStorage.getItem('user'),

    })
  })
    .pipe(
      catchError(this.handleError)
    );
}



  getDataforGroup() {
    return this.http.get(this.serverurl + '/group');

  }
  getDataforLevel() {
    return this.http.get(this.serverurl + '/level');

  }
  getDataforTag() {
    return this.http.get(this.serverurl + '/tag');

  }
  // searchForQuery(searchquery){
  //   console.log("in service search query");
  // var friends=this.http.get(this.usersSearchUrl+'/friends');
  // console.log(friends);
  // var groups =this.http.get(this.usersSearchUrl+'/groups');
  // console.log(groups);
  // var levels=this.http.get(this.usersSearchUrl+'/levels');
  // console.log(levels);
  // var tags=this.http.get(this.usersSearchUrl+'/tags');
  // console.log(tags);
  // }
  createGroup(group): Observable<User> {

    console.log("user data in service file");
    console.log(group);
    return this.http.post<User>(this.serverurl + '/group', group, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token'
      })

    })
      .pipe(
        catchError(this.handleError)
      );

  }

  getYoutubeVideoBasedOnQuery(searchquery): Observable<JSON> {


    var mainurl = this.youtubeurl + '?q=' + searchquery + '&part=snippet&videoDuration=medium&videoEmbeddable=true&type=video&maxResults=10&key=' + this.key;

    return this.http.get<JSON>(mainurl, {
      headers: new HttpHeaders({

        'Content-Type': 'application/json'

      })

    });


}

  getDurationOfVideos(videoId) {
    var mainurl = this.youtubevideourl + '?&part=contentDetails&id=' + videoId + '&key=' + this.key;

    return this.http.get<JSON>(mainurl, {
      headers: new HttpHeaders({

        'Content-Type': 'application/json'

      })

    });


  }

  getLocalVidoesLink(): Observable<JSON> {

    return this.http.get<JSON>(this.serverurl + '/localVideolinks', {
      headers: new HttpHeaders({

        'Content-Type': 'application/json'

      })

    });

}

  getFriendRequest(foruserId:any, groupid,from:number,size:number){
    console.log(foruserId);

    return this.http.get(this.url + '/friend_request/for_user/'+foruserId+'?group_id='+groupid+'&from='+from+'&size='+size,

    {
      headers: new HttpHeaders({

        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'from_user_id':foruserId,
      })



    }
  ).pipe(

    retry(3),
    catchError(this.handleError)

  )

  }


  getFriendRequestAtInterval(foruserId:any, groupid, status, starttime:any,endtime:any,from,to):Observable<any>{

  return this.http.get(this.url + '/friend_request/for_user/'+foruserId+'/request_status/'+status+'?group_id='+groupid+'&time_start='+starttime+'&time_end='+endtime+'&from='+from+'&size='+to,

    {
      headers: new HttpHeaders({

        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'from_user_id':foruserId,
      })

   }
  ).pipe(

    retry(3),
    catchError(this.handleError)

  )

  }

  deleteUserDocument(documentId,userid){
    console.log(documentId);

    return this.http.delete(this.url + '/user_doc_store/'+documentId,

    {
      headers: new HttpHeaders({

        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'from_user_id':userid,
      })



    }
  ).pipe(

    retry(3),
    catchError(this.handleError)

  )

  }

  updateFriendrequest(frID: string, body: any): Observable<any> {
    return this.http.put<any>(this.url + '/friend_request/' + frID, body, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'from_user_id': localStorage.getItem('user')

      })
    }).pipe(

      retry(3),
      catchError(this.handleError)
    );
  }
  updateForumrequest(forumrequestId: string, body: any): Observable<any> {
    return this.http.put<any>(this.url + '/forum_request/' + forumrequestId, body, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'from_user_id': localStorage.getItem('user')

      })
    }).pipe(

      retry(3),
      catchError(this.handleError)
    );
  }


  checkIfGroupExists(groupId:string,friend:string): Observable<any> {
    return this.http.get(this.url+'/chat/chat_for_users?group_id=public&chat_type=ONE_TO_ONE&user_list='+localStorage.getItem('user')+'%2C'+friend, 
                            {
                                headers: new HttpHeaders({

                                                          'Content-Type': 'application/json',
                                                          'Accept': 'application/json',
                                                          'from_user_id': localStorage.getItem('user'),
                                                        })

                            }
                       ).pipe(

                              retry(3),
                              catchError(this.handleError)
                            );
  }

  changePassword(obj,authid){

    return this.http.put(this.url+'/user_auth_details/'+authid,obj,
  
    {
      headers: new HttpHeaders({
  
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'from_user_id':localStorage.getItem('user')
      })
  
  
  
    }
  ).pipe(
  
    retry(3),
    catchError(this.handleError)
  
  )
  
  
  }

  addUserEducationalDetail(educationdata: any, userId: string): Observable<any> {
    console.log(" inside user service file");
    
    console.log(educationdata);
    console.log(userId);
    
    return this.http.post<any>(this.url + '/user_education_details/', educationdata, {
    headers: new HttpHeaders({
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'from_user_id': userId,
    
    })
    })
    .pipe(
    catchError(this.handleError)
    );
    }
    
    getEducationDetailsData(edu_details_id):Observable<any> {
    
    return this.http.get<any>(this.url +'/user_education_details/'+edu_details_id ,{
    headers: new HttpHeaders({
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'from_user_id': edu_details_id,
    
    })
    })
    .pipe(
    catchError(this.handleError)
    );
    }

    getEducationDetailsDataByUserId(userId):Observable<any> {
     return this.http.get<any>(this.url +'/user_education_details/by_user?user_id='+userId ,{
      headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'from_user_id': userId,
      })
      })
      .pipe(
      catchError(this.handleError)
      );
      }


      getuserDetailsDataByUserId(userId):Observable<any> {

        console.log(' method called'+userId)
        return this.http.get<any>(this.url +'/user/by_user_id?user_id='+userId ,{
         headers: new HttpHeaders({
         'Accept': 'application/json',
         'Content-Type': 'application/json',
         'from_user_id': userId,
         })
         })
         .pipe(
         catchError(this.handleError)
         );
         }
      getSentFriendRequestByActiveUser(userId,groupid):Observable<any> {
        return this.http.get<any>(this.url +'/friend_request/from_user/'+userId+'?group_id='+groupid+'&size=100' ,{
         headers: new HttpHeaders({
         'Accept': 'application/json',
         'Content-Type': 'application/json',
         'from_user_id': userId,
         })
         })
         .pipe(
         catchError(this.handleError)
         );
         }

         updateUserdocument(documentId: string, body: any): Observable<any> {
          return this.http.put<any>(this.url + '/user_doc_store/' + documentId, body, {
            headers: new HttpHeaders({
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'from_user_id': localStorage.getItem('user')
      
            })
          }).pipe(
      
            retry(3),
            catchError(this.handleError)
          );
        }

        getUserByUserID(user_id: string): Observable<any> {
          return this.http.get<any>(this.url + '/user/by_user_id?user_id=' + user_id, {
            headers: new HttpHeaders({
              'Accept': 'application/json',
              'from_user_id': localStorage.getItem('user'),
            })
          })
            .pipe(
              catchError(this.handleError)
            );
        }

        checkUserExistByEmail(email: string): Observable<any> {
          return this.http.get<any>(this.url + '/user_auth_details/by_email?email='+email, {
            headers: new HttpHeaders({
              'Accept': 'application/json',
              'from_user_id':email
            })
          })
            .pipe(
              catchError(this.handleError)
            );
        }

     getRoleByEntity(ID:string):Observable<any> {
      return this.http.get<any>(this.url + `/role_type/${ID}`, {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'from_user_id':localStorage.getItem('user')
        })
      })
        .pipe(
          catchError(this.handleError)
        );
     }  

     getFriendRequestByStatus(group:string, status:string , for_user:string,from:number,size:number):Observable<any> {
      return this.http.get<any>(this.url + `/friend_request/for_user/${for_user}/request_status/${status}?group_id=${group}&from=${from}&size=${size}`, {
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'from_user_id':localStorage.getItem('user')
        })
      })
        .pipe(
          catchError(this.handleError)
        );
     } 

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
    console.log(error);

       localStorage.setItem('errormsgfromServer',error.message);
    



    }
    // return an observable with a user-facing error message
    return throwError
      (error);
  };
}
