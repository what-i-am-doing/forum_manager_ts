import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ImageService } from 'src/app/services/image.service';
import { Router } from '@angular/router';
import { FriendService } from 'src/app/services/friend.service';
import { AddressService } from 'src/app/services/address.service';

@Component({
  selector: 'app-sentrequests',
  templateUrl: './sentrequests.component.html',
  styleUrls: ['./sentrequests.component.css'],
  providers:[UserService,ImageService,FriendService,AddressService]
})
export class SentrequestsComponent implements OnInit {
@Input() sentRequestData:any
public requestresult:any ;
public gender:any;
public profilePic:any
public activeUserPersonalData=[];
public activeUserEducationalData=[];
public deleteuserrequest:boolean=false;
public city;
@ViewChild("deleterequest", { read: ElementRef }) deleterequest: ElementRef;

constructor(public router:Router, private addressService:AddressService, public  friendService:FriendService, public service:UserService,public imageService:ImageService) { }

ngOnInit() {
  this.getUserFriendDetails(this.sentRequestData.to_user_id);
}
getUserFriendDetails(userId: any) {
  this.service.getUserDetails(userId).subscribe(receivedUserData => {

    this.requestresult = receivedUserData;
this.gender=receivedUserData.gender_type;

this.getProfile(this.requestresult.image_id);
try{

this.getUserEducationalDetails(this.sentRequestData.to_user_id);
this.getUserPersonalDetails(this.sentRequestData.to_user_id);


}catch(err){



}


})

}

getUserPersonalDetails(userId){

try{
  this.service.getUserPersonalDetails(userId).subscribe(data=>{

    console.log(data);
    this.activeUserPersonalData=data;
this.getAdress(data.address_id);
  })
}catch(err){

}
  
}


getUserEducationalDetails(userId){
  
  try{
    this.service.getEducationDetailsDataByUserId(userId).subscribe(data=>{

      console.log(data);
      this.activeUserEducationalData=data;

  })
}catch(err){


  }
}


seeFriendProfile(userId){
  this.router.navigate(['/home/userprofile'], { queryParams: { userId:userId} });
}

URLChange(img:any){
  return img.replace("https" , "http");
}

deleteFriendRequest(){

  this.friendService.deleteFriendRequest(this.sentRequestData.friend_req_id,this.sentRequestData.from_user_id).subscribe(data=>{
     console.log(data);
    console.log("friend request deleted");
    
  
    this.deleteuserrequest=true;
   
    this.deleterequest.nativeElement.style.display = 'none';
    setTimeout(() =>
 delete this.requestresult.user_full_text_name,
  2000)
  })
  
  
  }
  
  getAdress(adressId){
    this.addressService.getAdress(adressId).subscribe(data=>{
    console.log('adress data found');
    console.log( data);
   
    this.city=data.city;

    })
  }
  getProfile(image_id){
    console.log('thumbnail calling')
    console.log(image_id)
      this.imageService.getImageByType(image_id,'thumbnail').subscribe(data=>{
       console.log(data);
        if(data.length>0){
        console.log("thumbnail found");
        console.log(data[0].image_storage_url);
      this.profilePic=this.URLChange(data[0].image_storage_url);
        }
        else{
          this.getProfileImage(image_id);
        }
      })
    }
    getProfileImage(image_id){
      if(image_id!=''||image_id!='string'||image_id!=null){
  
  
      this.imageService.getImage(image_id).subscribe(data=>{
        console.log("image found");
        console.log(data.image_storage_url);
  this.profilePic=this.URLChange(data.image_storage_url);
  
      }
  
   ,err=>{
    console.log("image not found")
  switch(this.gender){
       case 'MALE':
       this.profilePic='assets/images/male.jpg'
       break;
       case 'FEMALE':
       this.profilePic='assets/images/female.jpg';
       break
  }
   }
   )}else{
    switch(this.gender){
      case 'MALE':
      this.profilePic='assets/images/male.jpg'
      break;
      case 'FEMALE':
      this.profilePic='assets/images/female.jpg';
      break
  }
   }
  
    }


}
