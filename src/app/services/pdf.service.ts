import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse ,HttpEventType} from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { map } from  'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

export class PdfService {

  private api_url = environment.api_url;

  constructor(private http: HttpClient) { }

  public uploadPdf(pdf: any,postData?:any) {
  
      var formData = new FormData();
      formData.append('pdfMultipartFile', pdf);
      return this.http.post(this.api_url+'/pdf/upload', formData,{headers: new HttpHeaders({

        'Accept':        'application/json',
        'user_id':  localStorage.getItem('user')
        
      }),
      reportProgress: true,
      observe: 'events'
    }).pipe(map((event) => {

      switch (event.type) {

        case HttpEventType.UploadProgress:
          const progress = Math.round(100 * event.loaded / event.total);
          return { status: 'progress', message: progress };

        case HttpEventType.Response:
          return event.body;
        default:
          return `Unhandled event: ${event.type}`;
      }
    })
    );
    }
  
    public uploadpdf(pdf: any) {
      console.log(' pdf in service');
      console.log('pdf') ;
      var formData = new FormData();
      formData.append('pdfMultipartFile', pdf);
        return this.http.post(this.api_url+'/pdf/upload', formData,{headers: new HttpHeaders({
  
          'Accept':        'application/json',
          'user_id':  localStorage.getItem('user')
          
        })
      }).pipe(
          catchError(this.handleError)
        )
      }
   getPdf(pdfID:string):Observable<any>{
             
    return this.http.get<any>(this.api_url+'/pdf/'+pdfID,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

}
