import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { OpengraphService } from "src/app/services/opengraph.service";

@Component({
  selector: 'app-share-forum_post',
  templateUrl: './share-forum_post.component.html',
  styleUrls: ['./share-forum_post.component.css']
})
export class ShareForumPostComponent implements OnInit {


  constructor(private opengraphService: OpengraphService) { }

  ngOnInit() {
  
    console.log(this);
    console.log(this.forum_postToShare);
    this.myModal.nativeElement.style.zIndex = "10000";
    this.modalback.nativeElement.style.display = "block";
    this.close.nativeElement.style.display = "block";
    this.appear.nativeElement.style.display = "block";
  }

  @ViewChild("upload", { read: ElementRef }) upload: ElementRef;
  @ViewChild("myModal", { read: ElementRef }) myModal: ElementRef;
  @ViewChild("modalback", { read: ElementRef }) modalback: ElementRef;
  @ViewChild("close", { read: ElementRef }) close: ElementRef;
  @ViewChild("appear", { read: ElementRef }) appear: ElementRef;

  @Input() forum_postToShare?: any;
  @Input() forum_postUser?: any;
  @Output() emitShare = new EventEmitter<any>();
  @Output() emitState = new EventEmitter<any>();
    
  
  lastLink: string;
  preview: boolean = false;
  showError: boolean = false;
  postTagFound: boolean = false;
  tagFriends: boolean = false;
  sharemessage: any = "";
  postMessage: string;
  heading: any = ""
  postTagFriends: Array<any> = [];
  firstName: string = localStorage.getItem("first_name");
  lastName: string = localStorage.getItem("last_name");
  userName: string = this.firstName + " " + this.lastName;
  curTime: number = +new Date;
  userPic: string = localStorage.getItem("activeProfilePic");

  closePostShare() {
    this.modalback.nativeElement.style.display = "none";
    this.close.nativeElement.style.display = "none";
    this.appear.nativeElement.style.display = "none";
    this.myModal.nativeElement.style.zIndex = "0";
    this.emitState.emit(false);
  }

  shareForumPost() {
    var shared = {
      "formatted_message": this.sharemessage,
      "heading": this.heading,
      "message": this.sharemessage.toString().replace(/<[^>]*>/g, "")
    };

    this.emitShare.emit(shared);
  }


  searchEvents(event:any) {
    event = event.event;
    if (event.keyCode === 32) {
      this.findOGLinks();
    }
  }

  findOGLinks() {

    this.postMessage = this.sharemessage
      .toString()
      .replace(/<[^>]*>/g, "");


    if (this.lastLink == undefined) {

      var filter = this.postMessage.replace(/&nbsp;/g, "");
      var links = this.findLinksInPost(filter);

      if (links.length) {
        this.lastLink = links[0];
        var reqURL = encodeURIComponent(this.lastLink);
        console.log(reqURL);
        this.opengraphService.getOgData(reqURL).subscribe(data => {
          console.log(data);
          data = data.hybridGraph;
          var main_container = document.createElement("div");
          main_container.style.border = "1px solid #c5c5c5";
          main_container.style.borderTop = "0px";
          main_container.style.padding = "2px";
          var footer_container = document.createElement("div");
          var heading = document.createElement("h4");
          if (data.title) heading.textContent = data.title;
          footer_container.appendChild(heading);
          var link = document.createElement("h5");
          var link_content = document.createElement("a");
          if (data.url) link_content.textContent = data.url;
          link.appendChild(link_content);
          footer_container.appendChild(link);
          var describe = document.createElement("div");
          if (data.description) describe.textContent = data.description.substring(0, 200) + "...";
          footer_container.appendChild(describe);
          if (data.image) {
            var img = document.createElement("img");
            img.src = data.image;
            img.width = 150;
            img.height = 70;
            main_container.appendChild(img);

          }
          var cross = document.createElement("span");
          cross.setAttribute("class", "pull-right glyphicon glyphicon-remove");
          cross.classList.add("cursor");

          cross.addEventListener("click", () => {
            this.lastLink = undefined;
            container.removeChild(container.childNodes[0]);
          });

          main_container.appendChild(cross);
          main_container.appendChild(footer_container);
          var container = document.getElementById("appendOG2");
          container.appendChild(main_container);
          console.log(container.childNodes[0]);
        },
          () => this.lastLink == undefined
        );
      }
    }

  }

  findLinksInPost(str) {
    var pattern = /(?:(?:https?|ftp):\/\/|\b(?:[a-z\d]+\.))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))?/gi;
    var result = str.match(pattern);
    console.log(result);
    return result;
  }
}
