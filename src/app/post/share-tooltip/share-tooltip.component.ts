import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-share-tooltip',
  templateUrl: './share-tooltip.component.html',
  styleUrls: ['./share-tooltip.component.css'],
  providers:[UserService]
})
export class ShareTooltipComponent implements OnInit {

  @Input() shareData: any;
  public userData:any;
  constructor(private userService:UserService,private router:Router) { }

  ngOnInit() {
    console.log('shareData tool tip')
    console.log(this.shareData);
    this.getUsersDetail();
  }

  getUsersDetail(){
    console.log(this.shareData.from_user_id);
    this.userService.getuserDetailsDataByUserId(this.shareData.from_user_id).subscribe(data => {
    console.log(' tooltip user data');
    console.log(data);
    console.log(data[0].user_id);
    console.log(data[0].user_full_text_name);
    this.userData=data[0];
    })
  }
  seeFriendProfile(){
    this.router.navigate(['/home/userprofile'], { queryParams: { userId:this.shareData.from_user_id} });
  }
}
