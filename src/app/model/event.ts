export class PostEvent{

constructor(){};


private comments: string;
private creation_timestamp: number;
private entity_id: string;
private event_id: string;
private event_type_id: string;
private from_user_id: string;
private group_id: string;
private last_updated_timestamp: number;
private level_id: string;
private on_entity_type: string;
private status: string;
private tag_id: string;
private to_group: string;
private to_level:string [];
private to_tag: string[];
private to_user: string[];

    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $creation_timestamp
     * @return {number}
     */
	public get $creation_timestamp(): number {
		return this.creation_timestamp;
	}

    /**
     * Getter $entity_id
     * @return {string}
     */
	public get $entity_id(): string {
		return this.entity_id;
	}

    /**
     * Getter $event_id
     * @return {string}
     */
	public get $event_id(): string {
		return this.event_id;
	}

    /**
     * Getter $event_type_id
     * @return {string}
     */
	public get $event_type_id(): string {
		return this.event_type_id;
	}

    /**
     * Getter $from_user_id
     * @return {string}
     */
	public get $from_user_id(): string {
		return this.from_user_id;
	}

    /**
     * Getter $group_id
     * @return {string}
     */
	public get $group_id(): string {
		return this.group_id;
	}

    /**
     * Getter $last_updated_timestamp
     * @return {number}
     */
	public get $last_updated_timestamp(): number {
		return this.last_updated_timestamp;
	}

    /**
     * Getter $level_id
     * @return {string}
     */
	public get $level_id(): string {
		return this.level_id;
	}

    /**
     * Getter $on_entity_type
     * @return {string}
     */
	public get $on_entity_type(): string {
		return this.on_entity_type;
	}

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Getter $tag_id
     * @return {string}
     */
	public get $tag_id(): string {
		return this.tag_id;
	}

    /**
     * Getter $to_group
     * @return {string}
     */
	public get $to_group(): string {
		return this.to_group;
	}

    /**
     * Getter $to_level
     * @return {string []}
     */
	public get $to_level(): string [] {
		return this.to_level;
	}

    /**
     * Getter $to_tag
     * @return {string[]}
     */
	public get $to_tag(): string[] {
		return this.to_tag;
	}

    /**
     * Getter $to_user
     * @return {string[]}
     */
	public get $to_user(): string[] {
		return this.to_user;
	}

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $creation_timestamp
     * @param {number} value
     */
	public set $creation_timestamp(value: number) {
		this.creation_timestamp = value;
	}

    /**
     * Setter $entity_id
     * @param {string} value
     */
	public set $entity_id(value: string) {
		this.entity_id = value;
	}

    /**
     * Setter $event_id
     * @param {string} value
     */
	public set $event_id(value: string) {
		this.event_id = value;
	}

    /**
     * Setter $event_type_id
     * @param {string} value
     */
	public set $event_type_id(value: string) {
		this.event_type_id = value;
	}

    /**
     * Setter $from_user_id
     * @param {string} value
     */
	public set $from_user_id(value: string) {
		this.from_user_id = value;
	}

    /**
     * Setter $group_id
     * @param {string} value
     */
	public set $group_id(value: string) {
		this.group_id = value;
	}

    /**
     * Setter $last_updated_timestamp
     * @param {number} value
     */
	public set $last_updated_timestamp(value: number) {
		this.last_updated_timestamp = value;
	}

    /**
     * Setter $level_id
     * @param {string} value
     */
	public set $level_id(value: string) {
		this.level_id = value;
	}

    /**
     * Setter $on_entity_type
     * @param {string} value
     */
	public set $on_entity_type(value: string) {
		this.on_entity_type = value;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}

    /**
     * Setter $tag_id
     * @param {string} value
     */
	public set $tag_id(value: string) {
		this.tag_id = value;
	}

    /**
     * Setter $to_group
     * @param {string} value
     */
	public set $to_group(value: string) {
		this.to_group = value;
	}

    /**
     * Setter $to_level
     * @param {string []} value
     */
	public set $to_level(value: string []) {
		this.to_level = value;
	}

    /**
     * Setter $to_tag
     * @param {string[]} value
     */
	public set $to_tag(value: string[]) {
		this.to_tag = value;
	}

    /**
     * Setter $to_user
     * @param {string[]} value
     */
	public set $to_user(value: string[]) {
		this.to_user = value;
	}

  
}