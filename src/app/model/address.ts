export class Address {

 
    private area: string;
    private city: string;
    private comments: string;
    private country: string;
    private landmark: string;
    private creation_timestamp: number;
    private plot_number: string;
    private last_updated_timestamp: number;
    private state: string;
    private status: string;
    private street_name: string;
    private zip: string;
    

    /**
     * Getter $area
     * @return {string}
     */
	public get $area(): string {
		return this.area;
	}

    /**
     * Setter $area
     * @param {string} value
     */
	public set $area(value: string) {
		this.area = value;
	}

    /**
     * Getter $city
     * @return {string}
     */
	public get $city(): string {
		return this.city;
	}

    /**
     * Setter $city
     * @param {string} value
     */
	public set $city(value: string) {
		this.city = value;
	}

    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Getter $country
     * @return {string}
     */
	public get $country(): string {
		return this.country;
	}

    /**
     * Setter $country
     * @param {string} value
     */
	public set $country(value: string) {
		this.country = value;
	}

    /**
     * Getter $landmark
     * @return {string}
     */
	public get $landmark(): string {
		return this.landmark;
	}

    /**
     * Setter $landmark
     * @param {string} value
     */
	public set $landmark(value: string) {
		this.landmark = value;
	}

    /**
     * Getter $creation_timestamp
     * @return {number}
     */
	public get $creation_timestamp(): number {
		return this.creation_timestamp;
	}

    /**
     * Setter $creation_timestamp
     * @param {number} value
     */
	public set $creation_timestamp(value: number) {
		this.creation_timestamp = value;
	}

    /**
     * Getter $plot_number
     * @return {string}
     */
	public get $plot_number(): string {
		return this.plot_number;
	}

    /**
     * Setter $plot_number
     * @param {string} value
     */
	public set $plot_number(value: string) {
		this.plot_number = value;
	}

    /**
     * Getter $last_updated_timestamp
     * @return {number}
     */
	public get $last_updated_timestamp(): number {
		return this.last_updated_timestamp;
	}

    /**
     * Setter $last_updated_timestamp
     * @param {number} value
     */
	public set $last_updated_timestamp(value: number) {
		this.last_updated_timestamp = value;
	}

    /**
     * Getter $state
     * @return {string}
     */
	public get $state(): string {
		return this.state;
	}

    /**
     * Setter $state
     * @param {string} value
     */
	public set $state(value: string) {
		this.state = value;
	}

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}

    /**
     * Getter $street_name
     * @return {string}
     */
	public get $street_name(): string {
		return this.street_name;
	}

    /**
     * Setter $street_name
     * @param {string} value
     */
	public set $street_name(value: string) {
		this.street_name = value;
	}

    /**
     * Getter $zip
     * @return {string}
     */
	public get $zip(): string {
		return this.zip;
	}

    /**
     * Setter $zip
     * @param {string} value
     */
	public set $zip(value: string) {
		this.zip = value;
	}
    
    }