import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SearchresultComponent } from './searchresult/searchresult.component';
import { ProfileComponent } from './profile/profile.component';
import { ViewprofileComponent } from './profile/viewprofile/viewprofile.component';
import { FriendlistComponent } from './friendlist/friendlist.component';
import { LoginComponent } from './accounts/login/login.component';
import { AboutusComponent } from './ExternalComponent/aboutus/aboutus.component';
import { AboutusmainComponent } from './ExternalComponent/aboutusmain/aboutusmain.component';
import { CompanyComponent } from './ExternalComponent/company/company.component';
import { ProductComponent } from './ExternalComponent/product/product.component';
import { PlatformComponent } from './ExternalComponent/platform/platform.component';
import { AuthGuard } from './auth/auth.guard';
import { ViewprofilepageComponent } from './viewprofilepage/viewprofilepage.component';
import { ChangeGroupComponent } from './change-group/change-group.component';
import { AllnotificationComponent } from './allnotification/allnotification.component';
import { ViewAllNotificationComponent } from './nav-bar/view-all-notification/view-all-notification.component';
import { WatchVideoComponent } from './searchresult/watch-video/watch-video.component';
import { AllFriendsComponent } from './profile/all-friends/all-friends.component';
import { CourseManagerComponent } from './course-manager/course-manager/course-manager.component';
import { ReviewManagerComponent } from './post-review/review-manager/review-manager.component';
import { ForumManagerComponent } from './forum-manager/forum-manager/forum-manager.component';
import { AdminComponent } from './admin/admin/admin.component';
import { ViewdashboardComponent } from './dashboard/viewdashboard/viewdashboard.component';
import { UserDocumentComponent } from './profile/user-document/user-document.component';
// import { CourseViewComponent } from './course-manager/course-view/course-view.component';

const appRoutes: Routes = [
  {
    path: "home",
    component: NavBarComponent,

    children: [

      { path: '', component: ViewdashboardComponent},
      { path: 'search-result/:term', component: SearchresultComponent ,
    
          canActivate:[AuthGuard],
          runGuardsAndResolvers: 'paramsChange'
      },
      { path: 'profile-setting', component: ProfileComponent },
      { path: 'profile', component: ViewprofileComponent},
      { path: 'document', component: UserDocumentComponent},
      { path: 'friendlist', component: FriendlistComponent },
      { path: 'userprofile', component: ViewprofilepageComponent },
      { path: 'notif', component: AllnotificationComponent },
      { path: 'notification', component: ViewAllNotificationComponent},
      { path: 'watch', component: WatchVideoComponent},
      { path: 'all-friends',component:AllFriendsComponent},
      { path: 'admin' ,component:AdminComponent},
      { path: 'course-manager', component: CourseManagerComponent},
      { path: 'forum-manager', component: ForumManagerComponent},
      { path: 'review-post', component:ReviewManagerComponent}
    ]
  },
  { path: "login", component: LoginComponent },
  { path: "main", component: AboutusComponent },
  { path: "about-us", component: AboutusmainComponent },
  { path: "company", component: CompanyComponent },
  { path: "product", component: ProductComponent },
  { path: "platform", component: PlatformComponent },
  { path: "change-group", component: ChangeGroupComponent },  
  // {path:'appCourseView',component:CourseViewComponent},
  { path: "", redirectTo: "/login", pathMatch: "full" },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes, { onSameUrlNavigation: "reload" })
  ],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule {}
