import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportTooltipComponent } from './report-tooltip.component';

describe('ReportTooltipComponent', () => {
  let component: ReportTooltipComponent;
  let fixture: ComponentFixture<ReportTooltipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportTooltipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportTooltipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
