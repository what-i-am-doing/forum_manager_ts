import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HoverListComponent } from './hover-list.component';

describe('HoverListComponent', () => {
  let component: HoverListComponent;
  let fixture: ComponentFixture<HoverListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HoverListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HoverListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
