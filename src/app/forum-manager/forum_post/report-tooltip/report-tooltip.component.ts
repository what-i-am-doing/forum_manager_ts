import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-report-tooltip',
  templateUrl: './report-tooltip.component.html',
  styleUrls: ['./report-tooltip.component.css']
})
export class ReportTooltipComponent implements OnInit {

  @Input() reportData: any;
  public userData:any;
  constructor(private userService:UserService, private router:Router) { }

  ngOnInit() {
    console.log('reportData tool tip')
    console.log(this.reportData);
   // this.getUsersDetail();
  }

  getUsersDetail(){
    console.log(this.reportData.from_user_id);
    this.userService.getuserDetailsDataByUserId(this.reportData.from_user_id).subscribe(data => {
    console.log(' tooltip user data');
    console.log(data);
    console.log(data[0].user_id);
    console.log(data[0].user_full_text_name);
    this.userData=data[0];
    })

  }

  seeFriendProfile(){
    this.router.navigate(['/home/userprofile'], { queryParams: { userId:this.reportData.from_user_id} });
  }
}
