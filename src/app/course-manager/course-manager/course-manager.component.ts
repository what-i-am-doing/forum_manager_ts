import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Content } from "../../model/content";
import { Course } from "../../model/course";
import { Module } from "../../model/module";
import { ExternalData } from "../../model/external-video";
import { ImageService } from "../../services/image.service";
import { PdfService } from "../../services/pdf.service";
import { VideoService } from "../../services/video.service";
import { CourseService } from "../../services/course.service";
import { UserService } from "../../services/user.service";
import {NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeNestedDataSource} from '@angular/material/tree';

interface CourseNode {
  id:string;
  name: string;
  course?:any;
  module?:any;
  children?: CourseNode[];
}

@Component({
  selector: 'app-course-manager',
  templateUrl: './course-manager.component.html',
  styleUrls: ['./course-manager.component.css']
})

export class CourseManagerComponent implements OnInit {
  @ViewChild("upload", { read: ElementRef }) upload: ElementRef;
  @ViewChild("upload2", { read: ElementRef }) upload2: ElementRef;

  TREE_DATA: CourseNode[] = [];
  treeControl = new NestedTreeControl<CourseNode>(node => node.children);
  dataSource = new MatTreeNestedDataSource<CourseNode>();
  isLinear = false;
  basicGroup: FormGroup;
  imgGroup: FormGroup;
  detailGroup: FormGroup;
  courseGroup: FormGroup;
  descGroup: FormGroup;
  extData: FormGroup;
  courseModules: any;
  modulemodules: any;
  public showJoinedCourseLoader = true;
  public fromSizeforjoinedCourses = 0;
  public toSizeforJoinedCourses = 4;
  public showMyCourseLoader = true;
  public fromSizeforMyCourses = 0;
  public toSizeforMyCourses = 4;
  public groupMyCourses:any;
  public groupJoinedCourses:any;
  activeLink:boolean = false;
  userId: string = localStorage.getItem("user");
  groupId: string = localStorage.getItem("groupId");
  courseAdded: boolean = false;
  moduleAdded: boolean = false;
  submoduleAdded:boolean = false;
  curCourse: any;
  curModule: any;
  curSubModule:any;
  saveCourse:any;
  newModule: boolean = false;
  newSubModule: boolean = false;
  newCourse: boolean = false;
  courseModuleImg: string;
  showCourse:any;
  showJoinedCourse:any;
  searchText:string;
  
  hasChild = (_: number, node: CourseNode) => !!node.children && node.children.length > 0;
  course: any;
  constructor(private _formBuilder: FormBuilder, private imageService: ImageService,
    private pdfService: PdfService, private videoService: VideoService,private userService:UserService, private courseService: CourseService
    ) {
      this.getCourses();
      this.getDataForJoinedCourses();
     }

  color: string = "accent";
  bgColor: string = "primary";
  showForm: boolean = false;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  uploadResponse = { status: '', message: 0, filePath: '' };
  imgResponse = { status: '', message: 0, filePath: '' };
  imageSRCS: Array<any> = [];
  pdfData: Array<any> = [];
  videoSRCS: Array<any> = [];
  userCourses: Array<any> = [];
  userJoinedCourses: Array<any> = [];
  coMoImageSRCS: any;
  coMoImageID: string;
  content: Content = new Content();
  searchedCourses:any;
  gotCourse:any;
  path:any = [{"id":1,"path_name":"courses"}];
  

 ngOnInit() {
  
  
    this.content.$content_images_list = [];
    this.content.$content_pdf_list = [];
    this.content.$content_video_list = [];

    this.dropdownList = [
      { item_id: 1, item_text: 'Group1' },
      { item_id: 2, item_text: 'Group2' },
      { item_id: 3, item_text: 'Group3' },
      { item_id: 4, item_text: 'Group4' },
      { item_id: 5, item_text: 'Group5' }
    ];

    this.selectedItems = [];

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.basicGroup = this._formBuilder.group({
      basicCtrl: ['', Validators.required],
      basicCtrl2: ['', Validators.required],
    });

    this.imgGroup = this._formBuilder.group({
      imgCtrl1: ['', Validators.required],
      imgCtrl2: ['', Validators.required],
    });

    this.detailGroup = this._formBuilder.group({
      detailCtrl1: ['', Validators.required],
      detailCtrl2: ['', Validators.required],
      detailCtrl3: ['', Validators.required]

    });

    this.courseGroup = this._formBuilder.group({
      courseCtrl1: ['', Validators.required],
      courseCtrl2: ['', Validators.required],
      courseCtrl3: ['', Validators.required],
      courseCtrl4: ['', Validators.required],
      courseCtrl5: ['', Validators.required],
      courseCtrl6: ['', Validators.required],
      courseCtrl7: ['', Validators.required]

    });

    this.descGroup = this._formBuilder.group({
      descCtrl1: ['']
    });

    this.extData = this._formBuilder.group({
      extDataCtrl1: ['']
    });

    
    
  }

 
    
      
     


 

  dataForJoinedCouses=[];
  courseModulesIdList=[];
  async getDataForJoinedCourses(){
    let a;
     a= localStorage.getItem('UserAllData');
    console.log('aaaaaaaaa',a);

    let b= await JSON.parse(a);
    let c =[];
    c  = b.courses_list;
    console.log('aaaaaaaaa2222',c,b.courses_list);
    c.forEach((element) => {
      // console.log('________====',this.course.course_id,element.course_id) 
        this.joinedCousesNameGetByUsingId(element);
    });

    if(this.dataForJoinedCouses.length > 0){
      console.log('OOOOOOOOOOOO',this.dataForJoinedCouses)
  //    this.callForModuleNameFromId();
  this.getCourseDetailCall(c);

    }
 
    
    // console.log('$$',this.courseSelected);
  }
  getCourseDetailCall(course_id){
    return new Promise((resolve,reject)=>{
    
      this.courseService.getCourseById(course_id).subscribe((res:any)=>{
        console.log('resssssssssssssssss',res);
        debugger;
        // this.course = {};
        this.course = res;
        console.log('reesssssssssssssssssssss2',this.course);
        resolve(this.course)
    //    localStorage.setItem('courseIdforjoin',this.idPassedFromParams);
    
        let moduleIdList= res.course_modules_list;
        console.log('hhhhhhhhhhhh',moduleIdList)
    
        moduleIdList.forEach((i:any)=>{
          this.getModulesById(i);
        })
       
    });
    })
    }

    getModulesById(module_id){
      console.log('Called....',module_id);
      this.courseService.getModulesByIds(module_id).subscribe((res:any)=>{
        console.log(res,'pppppppppppp')
        if(res.length > 0){
          this.arrOfModules.push(res);
          console.log('111111',this.arrOfModules)
        }
      });
    }
  
  
    


//  callForModuleNameFromId(){
//this.dataForJoinedCouses.forEach((obj:any)=>{
//      console.log('%%%%%%%%%',obj.data);
//        if(obj && obj.course_modules_list && obj.course_modules_list.length > 0){
//          // this.courseModulesIdList.push(obj.course_modules_list);
//         }
 //   })
 // }


joinedCousesNameGetByUsingId(course_id){
console.log('Called....',course_id)
this.courseService.getCourseById(course_id).subscribe((res:any)=>{
  console.log(res,'pppppppppppp')
  if(res){
    this.dataForJoinedCouses.push(res);
    
    res.forEach((obj:any)=>{
      console.log('%%%%%%%%%',obj.course_modules_list);
        if(obj && obj.course_modules_list && obj.course_modules_list.length > 0){
          this.courseModulesIdList.push(obj.course_modules_list);
         }
    })
  }
});
}


  createCourse() {

    var course = new Course;
    course.$course_name = this.basicGroup.value.basicCtrl;
    course.$course_type_id = this.basicGroup.value.basicCtrl2;
    course.$heading = this.imgGroup.value.imgCtrl1;
    course.$status = this.imgGroup.value.imgCtrl2;
    course.$category = this.detailGroup.value.detailCtrl1;
    course.$is_education_content = this.detailGroup.value.detailCtrl2;
    course.$language = this.detailGroup.value.detailCtrl3;
    course.$course = this.courseGroup.value.courseCtrl1;
    course.$stream = this.courseGroup.value.courseCtrl2;
    course.$class = this.courseGroup.value.courseCtrl3;
    course.$sub_class = this.courseGroup.value.courseCtrl4;
    course.$subject = this.courseGroup.value.courseCtrl5;
    course.$chapter = this.courseGroup.value.courseCtrl6;
    course.$concept = this.courseGroup.value.courseCtrl7;
    course.$course_description = this.descGroup.value.descCtrl1;
    course.$created_by = this.userId;
    course.$course_admin_list = [this.userId];
    if (this.coMoImageID != undefined) { course.$image_id = this.coMoImageID; }
    course.$enabled_for_groups = ["public"];

    if (this.basicGroup.valid && this.imgGroup.valid && this.detailGroup.valid && this.courseGroup.valid && this.descGroup.valid) {
      var ext_url = this.extData.value.extDataCtrl1;
      if (ext_url != '') {

        var ext_data = new ExternalData;
        ext_data.$video_url = this.extData.value.extDataCtrl1;
        var VID_REGEX = /(?:youtube(?:-nocookie)?\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|\S*?[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/;
        var vid_id = ext_url.match(VID_REGEX)[1];
        if (vid_id != undefined) ext_data.$video_id = ext_url.match(VID_REGEX)[1];
        this.videoService.addExtVideo(ext_data).subscribe(
          video => {
            this.content.$ext_link_data_list = [video.id];
            this.imageService.usercontent(this.content).subscribe(
              con => {
                course.$course_content_list = [con.id];
                this.addCourse(course);
              }
            );
          }
        );
      }

      else {
        if (this.content.$content_images_list.length || this.content.$content_pdf_list.length || this.content.$content_video_list.length) {
          this.imageService.usercontent(this.content).subscribe(
            con => {
              course.$course_content_list = [con.id];
              this.addCourse(course);
            }
          );
        }

        else this.addCourse(course);
      }


    }
  }

  addCourse(course: any) {
    this.courseService.addCourse(course).subscribe(
      data => {
        console.log(data);
        this.courseAdded = true;
        course.$course_id = data.id;
        this.userCourses.push(course);
        if(this.userCourses.length == 1) this.showCourse = course;
        this.basicGroup.reset();
        this.imgGroup.reset();
        this.detailGroup.reset();
        this.courseGroup.reset();
        this.descGroup.reset();
        this.extData.reset();
        this.content.$content_images_list = [];
        this.content.$content_pdf_list = [];
        this.content.$content_video_list = [];
        this.content.$ext_link_data_list = [];
        this.coMoImageSRCS = {};
      }
    );
  }



  clickUpload(): void {
    this.upload.nativeElement.click();
  }

  clickUpload2(): void {
    this.upload2.nativeElement.click();
  }

  onItemSelect(item: any) {
    console.log(item);
  }

  onSelectAll(items: any) {
    console.log(items);
  }


  processFile(imageInput: any) {
    var file = imageInput.files[0];
    var fileReader = new FileReader();

    if (file.type.match("image")) {

      fileReader.addEventListener("load", (event: any) => {
        console.log(event.target.result);
        this.imageService.uploadImage(file).subscribe(
          (res: any) => {
            console.log(res);
            this.uploadResponse = res;

            if (typeof res === 'object') {
              if ('id' in res) {

                this.content.$content_images_list.push(res.id);
                this.imageSRCS.push({
                  image_storage_url: fileReader.result,
                  content_type: "image/jpeg",
                  image_name: file.name,
                  image_size: file.size
                });
                console.log(this.imageSRCS);

              }
            }
          },
          err => {
            console.log(err);
          }
        );




      });

      fileReader.readAsDataURL(file);
    } else if (file.type.match("pdf")) {

      fileReader.addEventListener("load", (event: any) => {
        this.pdfService.uploadPdf(file).subscribe(
          (res: any) => {
            console.log(res);
            this.uploadResponse = res;
            if (typeof res === 'object') {
              if ('id' in res) {
                this.content.$content_pdf_list.push(res.id);

                this.pdfData.push({ pdf_storage_url: null, content_type: "application/pdf", pdf_name: file.name, pdf_size: file.size });

              }
            }

          },
          err => {
            console.log(err);
          }
        );
      });

      fileReader.readAsDataURL(file);
    } else {

      fileReader.addEventListener("load", (event: any) => {
        var blob = new Blob([fileReader.result], { type: file.type });
        var url = URL.createObjectURL(blob);

        var video = document.createElement("video");
        var timeupdate = function () {
          if (snapImage()) {
            video.removeEventListener("timeupdate", timeupdate);
            video.pause();
          }
        };
        video.addEventListener("loadeddata", function () {
          if (snapImage()) {
            video.removeEventListener("timeupdate", timeupdate);
          }
        });
        var snapImage = () => {
          var canvas = document.createElement("canvas");
          canvas.width = video.videoWidth;
          canvas.height = video.videoHeight;
          var img = document.createElement("img");
          canvas
            .getContext("2d")
            .drawImage(video, 0, 0, canvas.width, canvas.height);
          var image = canvas.toDataURL();
          var success = image.length > 100000;
          if (success) {

            this.videoSRCS.push({
              video_storage_url: image,
              content_type: "video/mp4",
              video_name: file.name,
              video_size: file.size

            });
            URL.revokeObjectURL(url);
          }
          return success;
        };
        video.addEventListener("timeupdate", timeupdate);
        video.preload = "metadata";
        video.src = url;
        // Load video in Safari / IE11
        video.muted = true;

        video.play();

        this.videoService.uploadVideo(file).subscribe(
          (res: any) => {
            console.log(res);
            this.uploadResponse = res;
            if (typeof res === 'object') {
              if ('id' in res) {
                this.content.$content_video_list.push(res.id);
              }
            }

          },
          err => {
            console.log(err);
          }
        );
      });

      fileReader.readAsArrayBuffer(file);
    }
  }

  coMoIMG(imageInput: any) {
    var file = imageInput.files[0];
    var fileReader = new FileReader();

    fileReader.addEventListener("load", (event: any) => {
      console.log(event.target.result);
      this.imageService.uploadImage(file).subscribe(
        (res: any) => {
          console.log(res);
          this.imgResponse = res;

          if (typeof res === 'object') {
            if ('id' in res) {

              this.coMoImageID = res.id;
              this.coMoImageSRCS = {
                image_storage_url: fileReader.result,
                content_type: "image/jpeg",
                image_name: file.name,
                image_size: file.size
              };
            }
          }
        },
        err => {
          console.log(err);
        }
      );
    });

    fileReader.readAsDataURL(file);
  }

  getCourses() {
    this.courseService.getCourseByUser(this.userId, this.groupId).subscribe(
      courses => {
        console.log(courses);
        this.userCourses = courses;
        courses.forEach((element) => {
          this.TREE_DATA.push({id:element.course_id,name:element.course_name,course:element});
          this.moduleListCall(element.course_id);
        });
         
        this.dataSource.data = this.TREE_DATA;
        this.showCourse = courses[0];
        console.log('##',this.arrOfModules);
      }
    );
  }
  arrOfModules=[];
  moduleListCall(courseId){
    this.courseService.ModuleByCourse(courseId).subscribe((res:any)=>{
      console.log('5555555555555555',res);
      this.arrOfModules.push(res);
    });
    // console.log('arrrrrrrrrrr5555555555',this.arrOfModules);
  }

  getJoinedCourses()
  {
    // this.userService.
  }

  createModule() {
    var module = new Module;
    module.$module_name = this.basicGroup.value.basicCtrl;
    module.$module_type = this.basicGroup.value.basicCtrl2;
    module.$module_heading = this.imgGroup.value.imgCtrl1;
    module.$status = this.imgGroup.value.imgCtrl2;
    module.$category = this.detailGroup.value.detailCtrl1;
    module.$is_education_content = this.detailGroup.value.detailCtrl2;
    module.$language = this.detailGroup.value.detailCtrl3;
    module.$course = this.courseGroup.value.courseCtrl1;
    module.$stream = this.courseGroup.value.courseCtrl2;
    module.$class = this.courseGroup.value.courseCtrl3;
    module.$sub_class = this.courseGroup.value.courseCtrl4;
    module.$subject = this.courseGroup.value.courseCtrl5;
    module.$chapter = this.courseGroup.value.courseCtrl6;
    module.$concept = this.courseGroup.value.courseCtrl7;
    module.$module_description = this.descGroup.value.descCtrl1;
    module.$created_by = this.userId;
    module.$enabled_group_ids = ["public"];
    console.log(module);
    console.log(this.curModule);
  // if(this.curModule)
   // module.$parent_module_id = this.curModule.module_id;
    // if (this.coMoImageID != undefined) module.$image_id = this.coMoImageID;
    // module.$course_id = this.showCourse.course_id;

    // if (this.basicGroup.valid && this.imgGroup.valid && this.detailGroup.valid && this.courseGroup.valid && this.descGroup.valid) {
    //   if (this.content.$content_images_list.length || this.content.$content_pdf_list.length || this.content.$content_video_list.length)
    //   this.imageService.usercontent(this.content).subscribe(
    //     con => {
    //       module.$module_content_list = [con.id];
    //       this.submitModule(module);
    //     }
    //   );

    //   else {
    //     console.log("else block");
    //     this.submitModule(module);
    //   }
    // }
  }

  submitModule(module:any) {
    this.courseService.createModule(module).subscribe(
      data => { 
        if(this.curModule) {
          console.log("module under module");
          if(this.curModule.sub_modules_list) this.curModule.sub_modules_list.push(data.id);
          else this.curModule.sub_modules_list = [data.id];
          this.courseService.updateModule(this.curModule.module_id,{"sub_modules_list":this.curModule.sub_modules_list}).subscribe();
          this.submoduleAdded = true;
        }

       else {
        console.log("module under course");
        if(this.showCourse.course_modules_list) this.showCourse.course_modules_list.push(data.id);
        else this.showCourse.course_modules_list = [data.id];
        this.courseService.updateCourse(this.showCourse.course_id , {"course_modules_list":this.showCourse.course_modules_list }).subscribe();

       }
        
        console.log(data);
        this.moduleAdded = true;
        
        module.$module_id = data.id;
        // this.userCourses.push(course);
        this.basicGroup.reset();
        this.imgGroup.reset();
        this.detailGroup.reset();
        this.courseGroup.reset();
        this.descGroup.reset();
        this.extData.reset();
        this.content.$content_images_list = [];
        this.content.$content_pdf_list = [];
        this.content.$content_video_list = [];
        this.content.$ext_link_data_list = [];
        this.coMoImageSRCS = {};
      }
    );
  }

//SEE MORE  OPTION //
seeMoreMyCourses(startIndex, endIndex) {
   console.log('iiiiiii',startIndex,endIndex,      this.fromSizeforMyCourses,this.toSizeforMyCourses,this.groupMyCourses.length)
   if (this.toSizeforMyCourses < this.userCourses.length)
     this.toSizeforMyCourses = this.toSizeforMyCourses + 3;
   else {
     this.fromSizeforMyCourses = 0;
     this.toSizeforMyCourses = 4;
  }
 }
 //seeMoreJoinedCourses(startIndex, endIndex) {
 //  console.log('iiiiiii',startIndex,endIndex,      this.fromSizeforMyCourses,this.toSizeforMyCourses,this.groupMyCourses.length)
 //  if (this.toSizeforJoinedCourses < this.dataForJoinedCouses.length)
//     this.toSizeforJoinedCourses = this.toSizeforJoinedCourses + 3;
 //  else {
 //    this.fromSizeforjoinedCourses = 0;
 //    this.toSizeforJoinedCourses = 4;
//   }
// }

  openModules(course: any) {
    this.saveCourse = course;
    this.newCourse = false;
    this.newModule = false;
    this.path.push({"id":course.course_id , "path_name":course.course_name});

    this.courseService.ModuleByCourse(course.course_id).subscribe(
      modules => {
        this.courseModules = modules;
      }
    );
  }

  openModulesofModules(module:any){
    this.newCourse = false;
    this.newModule = false;

    this.path.push({"id":module.module_id , "path_name":module.module_name});
       this.courseService.ModuleByParentModule(module.module_id,this.saveCourse.course_id).subscribe(
         modules=>{
          this.courseModules = modules;
         }
       );
  }

  backButton() {
    this.curCourse = undefined;
    this.curModule = undefined;
    this.newCourse = false;
    this.newModule = false;
     if(this.path.length > 1) this.path.pop();
      if(this.path.length == 1) {
         this.courseModules = undefined; 
        }
      else if(this.path.length == 2) {
        this.courseService.ModuleByCourse(this.path[this.path.length -1].id).subscribe(
          modules => {
            this.courseModules = modules;
          }
        );
      }
      else {
        this.courseService.ModuleByParentModule(this.path[this.path.length -1].id,this.saveCourse.course_id).subscribe(
          modules=>{
           this.courseModules = modules;
          }
        );
      }
  }

  isReadable:boolean =false;
  courseSelected(course:any) {
    // this.isReadable = false;
    this.showCourse = course;
    this.courseModules = undefined;
    this.curCourse = undefined;
    this.curModule = undefined;
    this.newModule = undefined;
    this.newSubModule = undefined;
    this.newCourse = undefined;
    this.path = [{"id":1,"path_name":"courses"}];
    // var node =  this.TREE_DATA.filter(cours => cours.course.course_id == course.course_id );
    // console.log(node);
    // if(node[0].course.course_modules_list){
    //    this.courseService.ModuleByCourse(course.course_id).subscribe(
    //       modules => {
    //         node[0].children = [];
    //         modules.forEach(element => {
    //           node[0].children.push({id:element.module_id,name:element.module_name,module:element});
    //           console.log(this.TREE_DATA);
    //         });

    //         this.dataSource= null;
            
    //         this.dataSource.data = this.TREE_DATA.slice(0);
    //       }
    //     );
     
    // }
  }

  getSearchResults() {
     this.courseService.searchCourseByText(this.searchText , this.groupId , 0,20).subscribe(
        courses => {
          console.log(courses);
          this.showCourse = undefined;
          this.searchedCourses = courses
        }
     );
  }
}
