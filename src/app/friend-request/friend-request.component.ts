import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { UserService } from '../services/user.service';
import { ImageService } from '../services/image.service';
import { Router } from '@angular/router';
import { AddressService } from '../services/address.service';

@Component({
  selector: 'app-friend-request',
  templateUrl: './friend-request.component.html',
  styleUrls: ['./friend-request.component.css'],
  providers: [UserService, ImageService, AddressService]
})
export class FriendRequestComponent implements OnInit {
  @ViewChild("acceptrequest", { read: ElementRef }) acceptrequest: ElementRef;
  @ViewChild("deleterequest", { read: ElementRef }) deleterequest: ElementRef;
  @ViewChild("blockrequest", { read: ElementRef }) blockrequest: ElementRef;
  @Input() friend: any;
  public requestresult: any;
  public userId: any;
  public userGroupId: any;

  public acceptuserrequest: boolean = false;
  public deleteuserrequest: boolean = false;
  public blockuserrequest: boolean = false;
  public profilePic: any;
  public activeUserPersonalData: any;
  public activeUserEducationalData: any;
  public gender: any;
  public city;
  constructor(private addressService: AddressService, private service: UserService, private router: Router, private imageService: ImageService) {
    var userData = JSON.parse(localStorage.getItem('LoggedInUserData'));

    this.userId = userData.user_id;
    this.userGroupId = localStorage.getItem('user_group');




  }

  ngOnInit() {
    try {
      this.getUserFriendDetails(this.friend.from_user_id);
      this.getUserEducationalDetails(this.friend.from_user_id);
      this.getUserPersonalDetails(this.friend.from_user_id)
    } catch (err) {


    }



  }

  getUserFriendDetails(userId: any) {
    this.service.getUserDetails(userId).subscribe(receivedUserData => {

      this.requestresult = receivedUserData;
      this.gender = receivedUserData.gender_type;

      this.getUsersFriendProfileImage(this.requestresult.image_id);


    })

  }



  getUsersFriendProfileImage(image_id) {
    console.log('thumbnail calling')
    console.log(image_id)
    this.imageService.getImageByType(image_id, 'thumbnail').subscribe(data => {
      console.log(data);
      if (data.length > 0) {
        console.log("thumbnail found");
        console.log(data[0].image_storage_url);
        this.profilePic = this.URLChange(data[0].image_storage_url);
      }
      else {
        this.getProfileImage(image_id);
      }
    })
  }
  getProfileImage(image_id) {
    if (image_id != '' || image_id != 'string' || image_id != null) {


      this.imageService.getImage(image_id).subscribe(data => {
        console.log("image found");
        console.log(data.image_storage_url);
        this.profilePic = this.URLChange(data.image_storage_url);

      }

        , err => {
          console.log("image not found")
          switch (this.gender) {
            case 'MALE':
              this.profilePic = 'assets/images/male.jpg'
              break;
            case 'FEMALE':
              this.profilePic = 'assets/images/female.jpg';
              break
          }
        }
      )
    } else {
      switch (this.gender) {
        case 'MALE':
          this.profilePic = 'assets/images/male.jpg'
          break;
        case 'FEMALE':
          this.profilePic = 'assets/images/female.jpg';
          break
      }
    }

  }


 accecptFriendRequest(friendrequestId, fromuserId) {

    this.service.updateFriendrequest(friendrequestId, { "req_status": "ACCEPTED" }).subscribe((data) => {
      console.log(data);
      this.acceptrequest.nativeElement.style.display = 'none';
      this.deleterequest.nativeElement.style.display = 'none';
      this.blockrequest.nativeElement.style.display = 'none';
      this.acceptuserrequest = true;


    });


  }

deleteFriendRequest(friend_req_id, from_user_id) {
   
   this.service.updateFriendrequest(friend_req_id, {"req_status": "DECLINED"}).subscribe(data => {
      this.acceptrequest.nativeElement.style.display = 'none';
      this.deleterequest.nativeElement.style.display = 'none';
      this.blockrequest.nativeElement.style.display = 'none';
      this.deleteuserrequest = true;
    })
  }

  blockFriendRequest(friend_req_id, from_user_id) {

    
    this.service.updateFriendrequest(friend_req_id, {"req_status": "BLOCKED"}).subscribe(data => {
      console.log(data);
      this.acceptrequest.nativeElement.style.display = 'none';
      this.deleterequest.nativeElement.style.display = 'none';
      this.blockrequest.nativeElement.style.display = 'none';
      this.blockuserrequest = true;
    })
    
  }

  getUserPersonalDetails(userId) {
    try {
      this.service.getUserPersonalDetails(userId).subscribe(data => {
        console.log(data);
        this.activeUserPersonalData = data;
        this.getAdress(data.address_id);
      })
    } catch (err) {

    }

  }

  getUserEducationalDetails(userId) {

    try {
      this.service.getEducationDetailsDataByUserId(userId).subscribe(data => {

        console.log(data);
        this.activeUserEducationalData = data;

      })
    } catch (err) {


    }
  }


  seeFriendProfile(userId) {
    this.router.navigate(['/home/userprofile'], { queryParams: { userId: userId } });

  }

  URLChange(img: any) {

    return img.replace("https", "http");
  }

  getAdress(adressId) {
    this.addressService.getAdress(adressId).subscribe(data => {
      console.log('adress data in friend request list found');
      console.log(data);

      this.city = data.city;

    })
  }


}