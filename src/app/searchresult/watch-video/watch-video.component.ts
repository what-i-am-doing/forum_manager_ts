import { Component, OnInit } from '@angular/core';
import { VideoService } from 'src/app/services/video.service';

@Component({
  selector: 'app-watch-video',
  templateUrl: './watch-video.component.html',
  styleUrls: ['./watch-video.component.css'],
  providers:[VideoService]
})
export class WatchVideoComponent implements OnInit {
public video:any;
  constructor(private videoService:VideoService) {

   }

  ngOnInit() {
   console.log(localStorage.getItem('extvideoId'));
    this.getextVideo(localStorage.getItem('extvideoId'))
  }
 

getextVideo(videoid){
this.videoService.getextVideobyId(videoid).subscribe(data=>{ 
  console.log(data);
this.video=data[0];
console.log(this.video.video_url)

}),err=>{
 
  if(err.message)
  alert(err.message);
  else  alert("video does not exist");
}
}
  playVideo(ev,thumbnailId, videoId){
    console.log('play video')

  
   
   console.log(document.getElementById(videoId) as HTMLImageElement)
   var result=document.getElementById(videoId) as HTMLImageElement;
   console.log( result.src)
    result.src += "&autoplay=1";
    
setTimeout(() =>this.toggleVideo(thumbnailId,videoId) , 1000)
}
toggleVideo(thumbnailId,videoId){

  document.getElementById(thumbnailId).style.display='none'
   document.getElementById(videoId).style.display='block';
}

}
