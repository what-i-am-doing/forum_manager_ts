import { Pipe,PipeTransform } from "@angular/core";


@Pipe({

    'name':'idTypes'
})

export class IdTypesPipe implements PipeTransform{

transform(value , search ){

   return value.filter(id => id.kyc_type_id === search)[0].kyc_id_type_name
}

}