import { TestBed } from '@angular/core/testing';

import { SessionEventsService } from './session-events.service';

describe('SessionEventsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SessionEventsService = TestBed.get(SessionEventsService);
    expect(service).toBeTruthy();
  });
});
