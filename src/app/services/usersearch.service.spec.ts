import { TestBed } from '@angular/core/testing';

import { UsersearchService } from './usersearch.service';

describe('UsersearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsersearchService = TestBed.get(UsersearchService);
    expect(service).toBeTruthy();
  });
});
