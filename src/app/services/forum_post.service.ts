import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Forum_Post } from '../model/Forum_Post';

@Injectable({
  providedIn: 'root'
})
export class ForumPostService {
  
  
  
  // isLikedTrue(arg0: string, forum_post_id: any) {
 //       throw new Error("Method not implemented.");
 //  }
   // updateForumPost(forum_post_id: any, arg1: { comments_counter: any; }) {
  //      throw new Error("Method not implemented.");
  //  }
  private serverurl:string='http://ec2-13-233-162-214.ap-south-1.compute.amazonaws.com:8500';
  private api_url = environment.api_url;
  constructor(private http: HttpClient) { }
  
   
   
   public addForum_Post(addForum_PostObj: any): Observable<any> {        //add new Forum Post using this function
                                                                //pass Forum Post obj here
    console.log(" Forum_Post will be created");
    return this.http.post<any>(this.api_url +'/forum_post/', addForum_PostObj, {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'from_user_id': addForum_PostObj.$user_id

      })
    })
    .pipe(
        catchError(this.handleError)
      );
      
      }
    
  
  forum_id(forum_id: any): string {
    throw new Error("Method not implemented.");
  }
  forum_post_id(forum_post_id: any): string {
    throw new Error("Method not implemented.");
  }
  img_id(img_id: any): string {
    throw new Error("Method not implemented.");
  }
  original_post_id(original_post_id: any): string {
    throw new Error("Method not implemented.");
  }
  original_user_id(original_user_id: any): string {
    throw new Error("Method not implemented.");
  }
  preview_content_id(preview_content_id: any): string {
    throw new Error("Method not implemented.");
  }
  handleError(handleError: any): any {
    throw new Error("Method not implemented.");
  }

  userfourmpost(forum_post:Forum_Post):Observable<any>{
    console.log('PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPs',forum_post);
    return this.http.post<any>(this.api_url+'/forum_post/',forum_post, {headers: new HttpHeaders({
        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user'),
      })
    })
      .pipe(
        catchError(this.handleError)
      );
}

      getForumPostContent(contentID:string):Observable<any>{
        return this.http.get<any>(this.api_url+'/content/'+contentID,{headers: new HttpHeaders({
          'Accept':        'application/json',
          'from_user_id':  localStorage.getItem('user')
        })
      } ).pipe(
         retry(1), // retry a failed request up to 1 time
         catchError(this.handleError) // then handle the error
        );
      }
      usercontent(content:any):Observable<any>{
        return this.http.post<any>(this.api_url+'/content/',content,{headers: new HttpHeaders({
         'Accept':        'application/json',
         'from_user_id':  localStorage.getItem('user')
       })
     }).pipe(
       retry(1), // retry a failed request up to 1 time
       catchError(this.handleError) // then handle the error
      );
 }
 //delete forum_post using this function
      deleteforum_post(forum_id:string):Observable<any>{
         return this.http.delete<any>(this.api_url+'/forum_post/'+forum_id, {
          headers: new HttpHeaders({
            'Accept': 'application/json',
            'from_user_id': localStorage.getItem('user')
    
          })
        }).pipe(

          catchError(this.handleError)
        );
  }
  updateforum_post(forum_post_id:string,body:any):Observable<any>{            //update forum_post_message
    return this.http.put<any>(this.api_url+'/forum_post/'+forum_post_id,body, {headers: new HttpHeaders({

      'Content-Type':  'application/json',
      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem("user")
      
    })
  })
    .pipe(
      catchError(this.handleError)
    );
  }
  getPostsFromUser(forum_id:string,user_id:string):Observable<any>{
    return this.http.put<any>(this.api_url+'/forum_post/from_user?forum_id='+forum_id+'&user_id='+user_id , {headers: new HttpHeaders({

      'Content-Type':  'application/json',
      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem("user")
      
    })
  })
    .pipe(
      catchError(this.handleError)
    );
  }
  


  //search Forum Posts by Search Text here
   

    searchPostsForGroupBySearchText(searchQuery,groupId,from,to):Observable<any>{
        
    return this.http.get<any>(this.api_url+'/forum_post/by_group/search_by_text?group_id='+groupId+'&search_text='+searchQuery+'&from='+from+'&size='+to,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }

  //search  Forum Posts by Heading Text here
   

  searchPostsForGroupBySearchTextInHeading(searchQuery,groupId,from,to):Observable<any>{
        
    return this.http.get<any>(this.api_url+'/forum_post/by_group/search_text/headings?group_id='+groupId+'&search_text='+searchQuery+'&from='+from+'&size='+to,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }
   //search Posts by User_Name here
   

  getForumPostsFromUser(group_id:string,forum_id:string,user_id:string):Observable<any>{
        
    return this.http.get<any>(this.api_url+'/forum_post/from_user?group_id='+group_id+'&forum_id='+forum_id+'&user_id='+user_id,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }
   getForumPostById(forum_post_id:string):Observable<any>{
     return this.http.get<any>(this.api_url+'/forum_post/='+forum_post_id,{headers: new HttpHeaders({
       'Accept':       'application/json',
       'from_user_id': localStorage.getItem('user')
     })
  } ).pipe(
    retry(1), // retry a failed request up to 1 time
    catchError(this.handleError) // then handle the error
  )
   } 
   getPostsForForum(groupID:string,forum_id:string):Observable<any>{
    return this.http.get<any>(this.api_url+'/forum_post/for_forum?group_id='+groupID+'&forum_id='+forum_id,{headers: new HttpHeaders({
        'Accept' :' application/json',
        'from_user_id': localStorage.getItem('user')
    })
  } ).pipe(
    retry(1), // retry a failed request up to 1 time
    catchError(this.handleError) // then handle the error
   );
  }

  

isLikedTrue(user:string,forumpostId:string):Observable<any>{

           return this.http.get<any>(this.api_url+'/event/search_by_user/by_entity/by_event?group_id=public&user_id='+user+'&entity_type=FORUM_POST&entity_id='+forumpostId+'&event_type=like&size=3',{headers: new HttpHeaders({

            'Accept':        'application/json',
            'from_user_id':  localStorage.getItem('user')
            
          })}).pipe(

            retry(1), // retry a failed request up to 1 time
            catchError(this.handleError) // then handle the error
           );

      }

isSharedTrue(user:string,forumpostId:string):Observable<any>{

return this.http.get<any>(this.api_url+'/event/search_by_user/by_entity/by_event?group_id=public&user_id='+user+'&entity_type=FORUM_POST&entity_id='+forumpostId+'&event_type=share&size=3',{headers: new HttpHeaders({

'Accept':        'application/json',
'from_user_id':  localStorage.getItem('user')

})}).pipe(

retry(1), // retry a failed request up to 1 time
catchError(this.handleError) // then handle the error
);

}

//check this out later 
deleteLikedData(dataId:string){

return this.http.delete(this.serverurl+'/forum_post_events/'+dataId);
}       

createLikedData(forum_post_id:string){

return this.http.post(this.serverurl+'/forum_post_events/',{"forum_post_id":forum_post_id,"event_type_id":1,"user_id":localStorage.getItem("user")})
}

getAllEventonForumPost(groupID:string,forumpostId:string):Observable<any>{

  return this.http.get<any>(this.api_url+'/event/search_by_entity?group_id='+groupID+'&entity_type=FORUM_POST&entity_id='+forumpostId+'&from=0&size=100',{headers: new HttpHeaders({

    'Accept':        'application/json',
    'from_user_id':  localStorage.getItem('user')
    
  })}).pipe(

    retry(1), // retry a failed request up to 1 time
    catchError(this.handleError) // then handle the error
  );

}
getAllEventonForumPostatInterval(groupID:string,forumpostId:string,starttime, endtime):Observable<any>{

return this.http.get<any>(this.api_url+'/event/search_by_entity/async?group_id='+groupID+'&entity_type=FORUM_POST&entity_id='+forumpostId+'&time_start='+starttime+'&time_end='+endtime,{headers: new HttpHeaders({

  'Accept':        'application/json',
  'from_user_id':  localStorage.getItem('user')
  
})}).pipe(

  retry(1), // retry a failed request up to 1 time
  catchError(this.handleError) // then handle the error
);

}
getPostsForForumByStatus(forumId:string,groupID:string,status:string,fromforumpost:any,size:any):Observable<any>{
  return this.http.get<any>(this.api_url+'/forum_post/for_forum/by_status?group_id='+groupID+'&forum_id='+forumId+'&status='+status+'&from='+fromforumpost+'&size='+size,{headers: new HttpHeaders({
    'Accept':        'application/json',
    'from_user_id':  localStorage.getItem('user'),
    'Content-Type':  'application/json',
  })}).pipe(
  
    retry(1), // retry a failed request up to 1 time
    catchError(this.handleError) // then handle the error
  );
  
  }
  
}