export class ChatGroup {

constructor() { };

private chat_group_name: string;
private chat_id: string;
private chat_type_id: string;
private chat_users_list: string[];
private comments: string;
private created_by_user_id: string;
private creation_timestamp: number;
private group_id: string;
private last_updated_timestamp: number;
private level_id: string;
private status: string;
private tag_id: string;


    /**
     * Getter $chat_group_name
     * @return {string}
     */
	public get $chat_group_name(): string {
		return this.chat_group_name;
	}

    /**
     * Getter $chat_id
     * @return {string}
     */
	public get $chat_id(): string {
		return this.chat_id;
	}

    /**
     * Getter $chat_type_id
     * @return {string}
     */
	public get $chat_type_id(): string {
		return this.chat_type_id;
	}

    /**
     * Getter $chat_users_list
     * @return {string[]}
     */
	public get $chat_users_list(): string[] {
		return this.chat_users_list;
	}

    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $created_by_user_id
     * @return {string}
     */
	public get $created_by_user_id(): string {
		return this.created_by_user_id;
	}

    /**
     * Getter $creation_timestamp
     * @return {number}
     */
	public get $creation_timestamp(): number {
		return this.creation_timestamp;
	}

    /**
     * Getter $group_id
     * @return {string}
     */
	public get $group_id(): string {
		return this.group_id;
	}

    /**
     * Getter $last_updated_timestamp
     * @return {number}
     */
	public get $last_updated_timestamp(): number {
		return this.last_updated_timestamp;
	}

    /**
     * Getter $level_id
     * @return {string}
     */
	public get $level_id(): string {
		return this.level_id;
	}

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Getter $tag_id
     * @return {string}
     */
	public get $tag_id(): string {
		return this.tag_id;
	}

    /**
     * Setter $chat_group_name
     * @param {string} value
     */
	public set $chat_group_name(value: string) {
		this.chat_group_name = value;
	}

    /**
     * Setter $chat_id
     * @param {string} value
     */
	public set $chat_id(value: string) {
		this.chat_id = value;
	}

    /**
     * Setter $chat_type_id
     * @param {string} value
     */
	public set $chat_type_id(value: string) {
		this.chat_type_id = value;
	}

    /**
     * Setter $chat_users_list
     * @param {string[]} value
     */
	public set $chat_users_list(value: string[]) {
		this.chat_users_list = value;
	}

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $created_by_user_id
     * @param {string} value
     */
	public set $created_by_user_id(value: string) {
		this.created_by_user_id = value;
	}

    /**
     * Setter $creation_timestamp
     * @param {number} value
     */
	public set $creation_timestamp(value: number) {
		this.creation_timestamp = value;
	}

    /**
     * Setter $group_id
     * @param {string} value
     */
	public set $group_id(value: string) {
		this.group_id = value;
	}

    /**
     * Setter $last_updated_timestamp
     * @param {number} value
     */
	public set $last_updated_timestamp(value: number) {
		this.last_updated_timestamp = value;
	}

    /**
     * Setter $level_id
     * @param {string} value
     */
	public set $level_id(value: string) {
		this.level_id = value;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}

    /**
     * Setter $tag_id
     * @param {string} value
     */
	public set $tag_id(value: string) {
		this.tag_id = value;
	}


}