import { Component, OnInit, Input } from "@angular/core";
import { UserService } from "../../services/user.service";
import { ImageService } from "../../services/image.service";
import { Router } from "@angular/router";
import { AddressService } from "src/app/services/address.service";

@Component({
  selector: "app-userfriend",
  templateUrl: "./userfriend.component.html",
  styleUrls: ["./userfriend.component.css"],
  providers: [UserService, ImageService, AddressService]
})
export class UserfriendComponent implements OnInit {
  @Input() friendlist: any;

  public receivedUserFriendData: any;
  public friendProfilePicUrl: any;
  public activeUserPersonalData: any;
  public activeUserEducationalData: any;
  public city: any;
  firstName:string;
  lastName:string;

  constructor(
    public userService: UserService,
    private imageService: ImageService,
    private router: Router,
    private addressService: AddressService
  ) {}

  ngOnInit() {
    this.getUsersFriendDetails(this.friendlist.friend_user_id);
    this.getUserPersonalDetails(this.friendlist.friend_user_id);
    this.getUserEducationalDetails(this.friendlist.friend_user_id);
  }

  // get  the friend details of current users
  getUsersFriendDetails(frienduserId: any) {
    this.userService
      .getUserDetails(frienduserId)
      .subscribe(receivedUserFriendData => {
        this.receivedUserFriendData = receivedUserFriendData;
        this.firstName = receivedUserFriendData.first_name;
        this.lastName = receivedUserFriendData.last_name;
        if(receivedUserFriendData.image_id && receivedUserFriendData.image_id != 'string')
        this.getUsersProfileImage(this.receivedUserFriendData.image_id);
      });
  }

  getUsersProfileImage(imageId) {
    this.imageService.getImage(imageId).subscribe(imagedata => {
      if (imagedata.image_storage_url != null) {
        this.friendProfilePicUrl = this.URLChange(imagedata.image_storage_url);
      }
    });
  }

  URLChange(img: any) {
    return img.replace("https", "http");
  }

  seeFriendProfile(userId) {
    this.router.navigate(["/home/userprofile"], {
      queryParams: { userId: userId }
    });
  }

  getUserPersonalDetails(userId) {
    try {
      this.userService.getUserPersonalDetails(userId).subscribe(data => {
        this.activeUserPersonalData = data;
        this.getAdress(data.address_id);
      });
    } catch (err) {}
  }

  getUserEducationalDetails(userId) {
    try {
      this.userService
        .getEducationDetailsDataByUserId(userId)
        .subscribe(data => {
          this.activeUserEducationalData = data;
        });
    } catch (err) {}
  }

  getAdress(adressId) {
    this.addressService.getAdress(adressId).subscribe(data => {
      this.city = data.city;
    });
  }
}
