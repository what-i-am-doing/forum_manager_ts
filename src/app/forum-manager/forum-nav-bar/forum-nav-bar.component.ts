import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ChatService } from '../../services/chat.service';
import { SessionService } from '../../services/session.service';
import { FilterPipe } from '../../filter/filter.pipe';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { MatDialog } from '@angular/material';
import { GroupmodelComponent } from '../../groupmodel/groupmodel.component';
import { Router } from '@angular/router';
import { ChatBoxComponent } from '../../chat/chat-box/chat-box.component';
import { Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { ExtraChatBoxContainerComponent } from '../../chat/extra-chat-box-container/extra-chat-box-container.component';
import { TREE_ACTIONS, KEYS, ITreeOptions } from 'angular-tree-component';
import { timer, Subscription } from 'rxjs';
import { ImageService } from '../../services/image.service';
import { FriendService } from '../../services/friend.service';
import { GroupService } from '../../services/group.service';
import { PostService } from '../../services/post.service';
import { EventService } from '../../services/event.service';
import { Renderer2 } from '@angular/core';
import { UserIdleService } from 'angular-user-idle';
import { SessionEventsService } from '../../services/session-events.service';
import { sessionEvents } from '../../model/session-events';
import { EventSourcePolyfill } from 'ng-event-source';

@Component({
  selector: 'app-forum-nav-bar',
  host: {
    '(document:click)': 'functionClick($event)'
  },
  templateUrl: './forum-nav-bar.component.html',
  styleUrls: ['./forum-nav-bar.component.css'],
  providers: [UserService, EventService, FilterPipe, PostService, ImageService, FriendService, GroupmodelComponent, GroupService],
  entryComponents: [ChatBoxComponent, ExtraChatBoxContainerComponent]
})

export class ForumNavBarComponent implements OnInit {

  constructor(
    private userIdle: UserIdleService,
    private chatService: ChatService,
    private service: UserService,
    private groupService: GroupService,
    public modal: MatDialog,
    public router: Router,
    public imageService: ImageService,
    public friendService: FriendService,
    private postService: PostService,
    private eventService: EventService,
    private sessionService: SessionService,
    private renderer: Renderer2,
    @Inject(DOCUMENT) document
  ) {

    this.initializeSession();
    this.datePickerConfig = Object.assign({}, { containerClass: "theme-dark-blue" });
    this.maxDate.setDate(this.maxDate.getDate() - 7);
    this.bsRangeValue = [this.maxDate, this.bsValue];
    this.show_online_friends_on_rightside = true;
    var userData = JSON.parse(localStorage.getItem("LoggedInUserData"));
    this.userName = userData.user_name;
    this.userId = userData.user_id;
    this.user_auth_id = userData.user_auth_id;
    this.userGroupId = userData.group_id[0];
    this.currrentGroup = userData.group_id[0];
    console.log(this.userGroupId);


    this.renderer.listen('window', 'click', (e: Event) => {

      if (this.toggleFriendrequest)
        if (e.target !== this.toggleFriendrequest.nativeElement && this.friendrequest.nativeElement != undefined && e.target !== this.friendrequest.nativeElement && this.isRequestClicked == true) {
          this.hideRequest = true;
          console.log('hide request tab');
        }
      if (this.toggleNotifi)
        if (e.target !== this.toggleNotifi.nativeElement && this.notification.nativeElement != undefined && e.target !== this.notification.nativeElement && this.isNotificationClicked == true) {
          this.hideNotif = true;
          console.log('hide notif tab');
        }

    });

  }
  zone: NgZone;
  color = 'primary';
  checked = false;
  disabled = false;
  slideme: any;
  counter: number = 0;
  groupDetails: Array<any> = [];
  sessionAlert: boolean = false;
  count: number = 0;
  startSub: Subscription;
  outSub: Subscription;
  currrentGroup: string;
  allEvents: Array<any> = [];
  userChatGroups: Array<any> = [];
  private entities = "";
  firstName: string = localStorage.getItem("first_name");
  lastName: string = localStorage.getItem("last_name");
  stream_sub: Subscription;
  showManager: boolean = false;
  userRoles: Array<any> = [];
  eventSource: any;

  sub: Subscription;
  sub1: Subscription;
  sub2: Subscription;
  chatBoxes: Array<any> = [];
  datePickerConfig: Partial<BsDatepickerConfig>;
  bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();
  public users: any;
  public fromSize = 0;
  public toSize: number = 8;
  public activeUserDetails: any;
  public screenHeight: any;
  public screenWidth: any;
  public searchText: string;
  public Found_friendlist: any[];
  public friendName = "";
  public results: any[];
  public friends: Array<any> = [];
  public friendData: Array<any> = [];
  public values = "";
  public showfriendlist: boolean = true;
  public flag: boolean = false;
  public mouseover: boolean = false;
  public show_online_friends_on_rightside: boolean = false;
  public toggle: boolean = false;
  public userName: string;
  public isAdmin: string = localStorage.getItem("isAdmin");
  public allGroups: any = JSON.parse(localStorage.getItem("all_groups"));
  public userSession: any = JSON.parse(localStorage.getItem("user_session"));
  public active: boolean = false;
  public toggleRequestWindow: boolean = false;
  public toggleMessagesWindow: boolean = false;
  public toggleFilter: boolean = false;
  public showList: boolean = false;
  public morethan4child: boolean = false;
  public selected_date: string = "7";
  public userId: string;
  public user_auth_id: string;
  public userGroupId: string;
  public friendRequestList: any;
  public receivedFriendRequestSenderData = [];
  public requestresult;
  public receivedFriendList = [];
  public user_friendlist;
  public gotFriendRequestData: Array<any> = [];
  isPost: boolean = false;
  isPeople: boolean = false;
  isPdf: boolean = false;
  isVideos: boolean = false;
  toggleNotification: boolean = false;
  chatGroups: Array<any> = [];
  friendsForSuggestoion: any;
  public profilePic: any;
  public friendsForAutosuggest: Array<any> = [];
  public gender: any;
  public isSearchFilterClicked: boolean = false;
  public isRequestClicked: boolean = false;
  public isNotificationClicked: boolean = false;
  public ismsgClicked: boolean = false;
  public showNumber: boolean = true;
  public initialLength: number = 0;
  public acceptedRequestData = [];
  public acceptRequestlength: number;
  public allEventsForUser: Array<any> = [];
  public usersAllPost = [];
  public showNotif: boolean = true;
  public anotherPage: boolean = false;
  public hideToggle: boolean = false;
  public hideNotif: boolean = false;
  public hideRequest: boolean = false;
  ONE_TO_ONE_GROUPS: Array<any> = [];

  @ViewChild('friendrequest') friendrequest: ElementRef;
  @ViewChild('notification') notification: ElementRef;
  @ViewChild('userSetting') userSetting: ElementRef;
  @ViewChild('toggleFriendrequest') toggleFriendrequest: ElementRef;
  @ViewChild('toggleNotifi') toggleNotifi: ElementRef;

  nodes = [
    {
      id: 1,
      name: "Institutes",
      type: 1,
      children: [
        {
          id: 2,
          name: "DPS",
          type: 2,
          children: [
            {
              id: 3,
              name: "Branches",
              type: 1,
              children: [
                {
                  id: 4,
                  name: "DPS Noida",
                  type: 2,
                  children: [
                    {
                      id: 5,
                      name: "Course Type",
                      type: 1,
                      children: [
                        {
                          id: 6,
                          name: "Engineering",
                          type: 2,
                          children: [
                            {
                              id: 8,
                              name: "Stream Type",
                              type: 1,
                              children: [
                                {
                                  id: 9,
                                  name: "CS",
                                  type: 2
                                },

                                {
                                  id: 10,
                                  name: "ME",
                                  type: 2
                                }
                              ]
                            }
                          ]
                        },

                        {
                          id: 7,
                          name: "MBA",
                          type: 2,
                          children: [
                            {
                              id: 8,
                              name: "Stream Type",
                              type: 1,
                              children: [
                                {
                                  id: 9,
                                  name: "HR",
                                  type: 2
                                },

                                {
                                  id: 10,
                                  name: "FINANCE",
                                  type: 2
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },

                {
                  id: 11,
                  name: "DPS Kanpur",
                  type: 2,
                  children: [
                    {
                      id: 12,
                      name: "Course Type",
                      type: 1,
                      children: [
                        {
                          id: 13,
                          name: "Engineering",
                          type: 2,
                          children: [
                            {
                              id: 8,
                              name: "Stream Type",
                              type: 1,
                              children: [
                                {
                                  id: 9,
                                  name: "CS",
                                  type: 2
                                },

                                {
                                  id: 10,
                                  name: "ME",
                                  type: 2
                                }
                              ]
                            }
                          ]
                        },

                        {
                          id: 14,
                          name: "MBA",
                          type: 2
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    }
  ];

  options: ITreeOptions = {
    isExpandedField: "expanded",
    idField: "id",
    hasChildrenField: "nodes",
    useCheckbox: true,
    actionMapping: {
      mouse: {
        dblClick: (tree, node, $event) => {
          if (node.hasChildren)
            TREE_ACTIONS.TOGGLE_EXPANDED(tree, node, $event);
        }
      },
      keys: {
        [KEYS.ENTER]: (tree, node, $event) => {
          node.expandAll();
        }
      }
    }
  };

  public arrowkeyLocation = 0;

  showNavbar: boolean = false;

  ngOnInit() {

    this.allGroups.forEach(element => {
      this.groupService.getGroupDetails(element).subscribe(data => {
        console.log(data);
        this.groupDetails.unshift(data);
      })
    });

    // this.stream_sub = this.eventService.getEventsByStream("eVD3L2oBo3oYpNg-T6ml").subscribe(
    //   data =>{
    //      alert("message recieved");
    //   }
    // );

    // this.stream_sub = this.eventService.streamEvents('http://ec2-13-233-162-214.ap-south-1.compute.amazonaws.com:8085/scholarsbook/streams/user_monitor_stream/public/'+this.userId).subscribe();

    //var source = new EventSource('http://ec2-13-233-162-214.ap-south-1.compute.amazonaws.com:8085/scholarsbook/streams/user_monitor_stream/public/GFCLaGoBo3oYpNg-4K7R');
    //  this.eventSource = new EventSourcePolyfill('http://ec2-13-233-162-214.ap-south-1.compute.amazonaws.com:8085/scholarsbook/streams/user_monitor_stream/public/'+this.userId, 
    //                       {
    //                          headers: { "Accept": 'application/stream+json' ,
    //                                     "from_user_id":"vijay"
    //                                    }
    //                       });

    // this.eventSource.onopen = (a) => {
    //   console.log("connection opened");
    // };                      

    // this.eventSource.onmessage = (message) => {
    //   this.zone.run(() => {
    //     console.log("msg recieved");
    //     console.log(message.data);
    // });
    //  };


    // this.eventSource.onerror = (e) => {
    //   console.log("error");

    // }

    this.getActiveUserDetails(this.userId);
    this.getActiveUserPersonalDetails(this.userId);
    this.getFriendListOfActiveUser(this.userId, this.userGroupId, 0, 8);
    this.getChatGroupsOfUser(this.userId)
    this.getUsersforSuggestions(this.userGroupId, this.userId, 0, 100);
    this.getFriendRequest(this.userId, this.userGroupId, 0, 10);
   // this.getAllPostOfUser(this.userGroupId, this.userId);
    this.checkFriendRequestForAccepted(this.userId, 'ACCEPTED', this.userGroupId);
    this.allChatGroups();
    this.checkUserRoles();
  }



  ngOnDestroy() {
    if (this.sub)
      this.sub.unsubscribe();
    if (this.sub1)
      this.sub1.unsubscribe();
    if (this.sub2)
      this.sub2.unsubscribe();
    // if (this.sub3)
    //   this.sub3.unsubscribe();
    // if (this.sub4)
    //   this.sub4.unsubscribe();

    if (this.stream_sub) this.stream_sub.unsubscribe();
    if (this.startSub) this.startSub.unsubscribe();
    if (this.outSub) this.outSub.unsubscribe();
    console.log("NAV BAR DESTROYED");

  }

  checkUserRoles() {
    var user = JSON.parse(localStorage.getItem("UserAllData"));
    console.log(user);
    user.roles_list.forEach(element => {
      this.service.getRoleByEntity(element).subscribe(

        role => {
          console.log(role);
          this.userRoles.push(role.role_name);
        }
      )
    });
  }

  initializeSession() {
    console.log("NAV BAR NG-ONINIT STARTED");
    //Start watching for user inactivity.
    console.log(this.userSession);
    setInterval(() => {
      this.sessionService.updateSession(this.userSession.user_session_details_id, { "session_end_time": this.userSession.session_end_time + 600000 }).subscribe(data => {
        console.log(data);
        this.userSession.session_end_time = this.userSession.session_end_time + 600000;
        localStorage.setItem("user_session", JSON.stringify(this.userSession));
      });
    }, 60 * 9 * 1000);

    this.userIdle.startWatching();

    // Start watching when user idle is starting.
    this.startSub = this.userIdle.onTimerStart().subscribe(count => {
      this.sessionAlert = true;
      this.count = count;
      //  console.log("Dear user, you are inactive from 5 minutes please click anywhere on page to keep loged in within 60 seconds:"+ count);
    });

    // Start watch when time is up.
    this.outSub = this.userIdle.onTimeout().subscribe(() => {
      this.userIdle.stopWatching();

      this.sessionService.updateSession(this.userSession.user_session_details_id, { "session_end_time": +new Date }).subscribe(data => {
        console.log(data);
        localStorage.clear();
        sessionStorage.clear();
        this.router.navigateByUrl("/session-out");
      });

    });
  }

  allChatGroups() {
    this.chatService.chatByUser(this.userId, +new Date, +new Date + 600000).subscribe(
      chat_msgs => {
        if (Object.keys(chat_msgs).length) {
          Object.keys(chat_msgs).forEach(element => {

            this.chatService.getChatGroupByID(element).subscribe(
              grp => {
                if (grp.chat_type_id == 'ONE_TO_ONE') {
                  this.ONE_TO_ONE_GROUPS.push(grp);
                }
              }
            );
          });
        }

      }
    );
  }


  addBox(event: any) {
    console.log(event);
    var already_open = this.chatBoxes.filter(box => box == event);
    if (!(already_open.length)) this.chatBoxes.push(event);
  }


  searchFriend(term) {
    var word = term;
    if ((term = !null && term == "")) this.flag = false;
    else this.flag = true;

    this.searchTerm(word);
  }

  onselectFriend(name) {
    this.friendName = name;
    this.flag = false;
  }


  functionClick() { this.flag = false; }

  pressKeyboard(event: KeyboardEvent, name) {
    if (event.key === "Enter") this.friendName = name;

  }
  keyDown(event: KeyboardEvent) {
    if (event.keyCode === 40 && this.arrowkeyLocation < this.results.length - 1) this.arrowkeyLocation++;
    else if (event.keyCode === 38 && this.arrowkeyLocation > 0) this.arrowkeyLocation--;
    else this.arrowkeyLocation = 0;

  }

  toggle_settings() {
    console.log("toggle clicked");
    this.toggle = !this.toggle;
  }


  logout() {
    localStorage.clear();
    sessionStorage.clear();
    this.sessionService.updateSession(this.userSession.user_session_details_id, { "session_end_time": + new Date }).subscribe(data => {
      console.log(data);

    });
    this.router.navigateByUrl("/login");
  }


  openDialog() { this.modal.open(GroupmodelComponent); }
  showFriendList() { this.showfriendlist == true; }

  getFriendRequest(userId: string, groupid: string, from: number, size: number) {


    this.service.getFriendRequestByStatus(groupid, 'OPEN', userId, from, size).subscribe((data: any) => {

      this.gotFriendRequestData = data;
      this.initialLength = this.gotFriendRequestData.length;

      // this.getFriendRequestAtInterVal(this.userId,groupid,+new Date(), +new Date() + Number(60000));

    }), () => console.log('error in get friend request ')


  }


  // sub3: Subscription = timer(300000, 300001).subscribe(() => this.sub3 = this.service.getFriendRequestAtInterval(this.userId, this.userGroupId,"OPEN", +new Date - 300000, +new Date, 0, 100).subscribe((data) => {

  //   console.log(data);

  //   if (data.length) {

  //     data.forEach(element => {
  //       this.gotFriendRequestData.push(element);
  //     })

  //     this.gotFriendRequestData.sort(
  //       (post1, post2) => post2.creation_timestamp - post1.creation_timestamp
  //     );

  //     this.initialLength += data.length;
  //     this.showNumber = true;

  //   }


  // }, () => console.log('error in get friend request while interval calling ')

  // ));


  showRequests() {
    this.showNumber = false;
    this.hideRequest = false;
    this.initialLength = 0;
  }

  //get active user details
  getActiveUserDetails(userId: any) {
    this.service.getUserDetails(userId).subscribe(receivedUserData => {

      this.activeUserDetails = receivedUserData;

      if (receivedUserData.image_id && receivedUserData.image_id != 'string') {
        this.getProfile(receivedUserData.image_id)

      }

    });
  }

  getActiveUserPersonalDetails(userId) {
    this.service.getUserPersonalDetails(userId).subscribe(data => {
      console.log("getActiveUserPersonalDetails")
      console.log(data)

      this.gender = data.gender_type;
      console.log(this.gender);
    })

  }

  //get friend request sender details using friend request sender Id
  getUserDetails(userId: any) {
    this.service.getUserDetails(userId).subscribe(receivedUserData => {
      this.requestresult = receivedUserData;
      console.log(receivedUserData);
      this.receivedFriendRequestSenderData.push(this.requestresult);
    });
  }

  onChange(event) {
    console.log(event);
  }

  close_Popup() {
    this.toggle = false;
  }
  // when menu button is clicked , it diplays left  menu bar
  menuClicked(showNavbar: boolean) {
    localStorage.setItem("leftmenubar", "showNavbar");

    if (showNavbar == false) {
      this.showNavbar = true;
    } else if (showNavbar == true) {
      this.showNavbar = false;
    }
  }


  // this function hided the left menu bar and navigate to  the pages
  //after clicking on menu bar options
  hideLeftMenubar(link: string) {
    console.log(link);
    if (link == "home") {
      this.showNavbar = false;
      this.router.navigateByUrl("/home");
    } else if (link == "profile") {
      this.showNavbar = false;
      this.router.navigateByUrl("home/profile");
    } else if (link == "friendlist") {
      this.showNavbar = false;
      this.router.navigateByUrl("home/friendlist");
    } else if (link == "message") {
      this.showNavbar = false;
      this.router.navigateByUrl("/home");
    }
  }

  navigateToPages(link: string, searchquery: string) {
    if (link == "home") {

      this.router.navigateByUrl("/home");
      this.show_online_friends_on_rightside = true;
      this.toggleRequestWindow = false;
      this.toggleMessagesWindow = false;
      this.toggle = false;
      this.toggleFilter = false;
    } else if (link == "profile") {
      this.router.navigateByUrl("home/profile");
      this.show_online_friends_on_rightside = true;
      this.toggleRequestWindow = false;
      this.toggleMessagesWindow = false;
      this.toggle = false;
      this.toggleFilter = false;
    } else if (link == "friendlist") {
      console.log("friend list clicked")
      this.show_online_friends_on_rightside = false;
      this.toggleMessagesWindow = false;
      this.toggleRequestWindow = false;
      this.toggle = false;
      this.toggleFilter = false;
      this.hideRequest = true;
      console.log('hide request' + this.hideRequest);
      this.router.navigateByUrl("home/friendlist");
      this.anotherPage = true;
      this.hideToggle = true;

    }

    else if (link == "message") {
      this.router.navigateByUrl("/home");
      this.show_online_friends_on_rightside = true;
      this.toggleRequestWindow = false;
      this.toggleMessagesWindow = false;
      this.toggle = false;
      this.toggleFilter = false;
    } else if ((link = "search-result")) {
      if (searchquery != undefined && searchquery.length > 0) {
        localStorage.setItem("searchquery", searchquery);
        var filter = {
          isPost: this.isPost,
          isImages: this.isPeople,
          isPdf: this.isPdf,
          isVideos: this.isVideos,
          dateRange: this.bsRangeValue
        };
        localStorage.setItem("filter", JSON.stringify(filter));
        this.router.navigateByUrl("home/search-result/" + searchquery);
        this.anotherPage = true;
        this.hideToggle = true;
        this.show_online_friends_on_rightside = false;
        this.toggleRequestWindow = false;
      } else {
        this.router.navigateByUrl("home");
        this.show_online_friends_on_rightside = true;
        this.toggleRequestWindow = false;
      }
    }
  }

  viewMore(fromSize, toSize) {
    this.fromSize = this.fromSize + fromSize
    this.toSize = this.toSize + toSize;
    this.getFriendListOfActiveUser(this.userId, this.userGroupId, this.fromSize, this.toSize);
  }

  changeDate() {
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate() - Number(this.selected_date));
    this.bsRangeValue = [this.maxDate, this.bsValue];
  }

  // Accept friend request

  getFriendListOfActiveUser(userId: any, group_id, fromsize, toSize) {
    this.service.getFriendListOfActiveUser(userId, group_id, fromsize, toSize).subscribe(data => {
      var my_friends = [];
      data.forEach(element => {
        element.friendsMsges = [];
        element.messages = [];
        my_friends.push(element.friend_user_id);
      });
      this.friends = data;
      localStorage.setItem("my_friends", JSON.stringify(my_friends));
      for (var i = 0; i < this.friends.length; i++) {

        this.service.getUserDetails(this.friends[i].friend_user_id).subscribe((data: any) => {
          console.log(data);
          this.friendData.push(data);
          this.friendsForAutosuggest.push(data.user_full_text_name);
        });

        this.checkIfGroupExists(i, group_id);
      }

    });
  }

  checkIfGroupExists(i, group_id) {
    this.service.checkIfGroupExists(group_id, this.friends[i].friend_user_id).subscribe(
      res => {
        if (res.length) this.friends[i].chat_id = res[0].chat_id;
        if (i == this.friends.length - 1) {
          this.getRecentChat();

        }

      }
    );
  }

  getRecentChat() {
    this.chatService.chatByUser(this.userId, +new Date - 172800000, +new Date).subscribe(
      chats => {
        console.log(chats);
        if (Object.keys(chats).length) {

          Object.keys(chats).forEach(elem => {

            var cur_chat: any = this.friends.filter(frnd => frnd.chat_id == elem);
            if (cur_chat.length) {
              cur_chat[0].messages = chats[elem];
              chats[elem].forEach(element => {
                if (element.from_user_id != this.userId) cur_chat[0].friendsMsges.push(element);
              });
            }
          });
        }

       // this.getChatAsync(this.userId, +new Date, +new Date + 600000);

      }
    );
  }

  getChatAsync(user: string, start: number, end: number) {
    this.chatService.chatByUserAsync(user, start, end).subscribe(
      chats => {
        console.log(chats);
        if (Object.keys(chats).length) {
          var max = 0;

          Object.keys(chats).forEach(elem => {

            var cur_chat: any = this.friends.filter(frnd => frnd.chat_id == elem);
            if (cur_chat.length) {
              chats[elem].forEach(element => {
                if (element.creation_timestamp > max) max = element.creation_timestamp;
                if (element.from_user_id != user) {
                  cur_chat[0].friendsMsges.push(element);
                  cur_chat[0].messages.push(element);
                }
              });
            }
          });

          this.getChatAsync(user, max + 1, max + 1 + 600000);
        }

        else this.getChatAsync(user, +new Date, +new Date + 600000);
      }
    );
  }

  removeUser(event: any) {

    this.chatBoxes = this.chatBoxes.filter(
      chatBox => chatBox.user_id != event[0].user_id
    );
  }

  getChatGroupsOfUser(userID: string) {
    this.chatService.getGroupsForUser(userID, this.userGroupId).subscribe(group => {

      group.forEach(element => {
        this.chatGroups.push(element);
      });

      //   this.getChatGroupAtInterval();
    });
  }

  getChatGroupAtInterval() {
    this.sub = timer(0, 20000).subscribe(() =>
      this.chatService
        .getGroupsByGroup(
          this.userGroupId,
          +new Date() - Number(20000),
          +new Date()
        )
        .subscribe(groups => {

          var req_groups = groups.filter(group =>
            group.chat_users_list.includes(this.userId)
          );
          req_groups.forEach(element => {
            this.chatGroups.push(element);
          });
        })
    );
  }

  getUsersforSuggestions(userId, group_id, fromsize: Number, tosize: Number) {

    this.service.getUsersforSuggestions(userId, group_id, fromsize, tosize).subscribe(data => {
      this.friendsForSuggestoion = data;

      this.friendsForSuggestoion.forEach(element => {
        this.friendsForAutosuggest.push(element.user_full_text_name);
      });
    })
  }

  searchTerm(data: string) {

    data = data.trim().toLowerCase();
    if (data != '') {
      this.results = this.friendsForAutosuggest.filter(word =>
        word.startsWith(data)).slice(0, 11);
    }

  }


  URLChange(img: any) { return img.replace("https", "http"); }

  getProfile(image_id: string) {


    this.imageService.getImageByType(image_id, 'thumbnail').subscribe(data => {

      if (data.length) this.profilePic = this.URLChange(data[0].image_storage_url);
      else this.getProfileImage(image_id);

    })
  }

  getProfileImage(image_id: string) {
    this.imageService.getImage(image_id).subscribe(data => this.profilePic = this.URLChange(data.image_storage_url))
  }



  notificationClicked() {

    this.isNotificationClicked = true;
    this.showNotif = false;
    this.hideNotif = true;
    this.hideRequest = true
    this.counter = 0;
    this.anotherPage = true;
    this.hideToggle = true;


  }

  closeModels(event: any) {

    if (!event.target.className.includes("popup-windows")) {
      if (this.isSearchFilterClicked == true) {
        this.toggleFilter = true;
        this.toggle = false;
        this.toggleRequestWindow = false;
        this.toggleMessagesWindow = false;
        this.toggleNotification = false;
        this.isSearchFilterClicked = false;


      }
      else if (this.isRequestClicked == true) {
        this.toggleRequestWindow = true;
        this.toggle = false;
        this.toggleMessagesWindow = false;
        this.toggleNotification = false;
        this.toggleFilter = false;
        this.isRequestClicked = false;
      }
      else if (this.isNotificationClicked == true) {
        this.toggle = false;
        this.toggleRequestWindow = false;
        this.toggleMessagesWindow = false;
        this.toggleNotification = true;
        this.toggleFilter = false;
        this.isNotificationClicked = false;
      }
      else {
        this.toggle = false;
        this.toggleRequestWindow = false;
        this.toggleMessagesWindow = false;
        this.toggleNotification = false;
        this.toggleFilter = false;
      }

    }
  }



  //check friend request status is accepted
  checkFriendRequestForAccepted(userId: string, status: string, groupid: string) {
    this.friendService.getSentFriendRequestByStatus(userId, groupid, status).subscribe(data => {

      this.acceptedRequestData = data;
      this.initialLength = this.initialLength + data.length;
      this.acceptedRequestData.sort((req1, req2) => req2.creation_timestamp - req1.creation_timestamp);
      // this.checkFriendRequestForAcceptedAtInterval(this.userId, groupid, 'ACCEPTED', +new Date(), +new Date() + Number(600000));
    })
  }

  reset() {
    this.isPeople = this.isPdf = this.isPost = this.isVideos = false;
    this.friendName = "";
    this.selected_date = "7";
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate() - 7);
    this.bsRangeValue = [this.maxDate, this.bsValue];
  }

  userSwitchGroup() {

   console.log(this.currrentGroup);
    var switchGroupObj = {
      group_id: [this.currrentGroup],
      user_id: this.userId
    };

    this.groupService
      .userSwitchGroup(switchGroupObj, this.currrentGroup, this.userId)
      .subscribe(data => {
        console.log(data);

        if (data == true) {
          console.log("user swithced successfully");
          var cur_ls = JSON.parse(localStorage.getItem("LoggedInUserData"));
          cur_ls.group_id = [this.currrentGroup];
          console.log(cur_ls);
          localStorage.setItem("LoggedInUserData", JSON.stringify(cur_ls));
          localStorage.setItem("user_group", JSON.stringify([this.currrentGroup]));
          localStorage.setItem("groupId", this.currrentGroup);
          console.log(this.currrentGroup);
          this.router.navigateByUrl("/change-group");
        }
      });
  }


  // sub4: Subscription = timer(600000, 600001).subscribe(() => this.friendService.getSentFriendRequestByStatusAtInterval(this.userId, this.userGroupId, "ACCEPTED", +new Date - 600000, +new Date, 0, 100).subscribe(data => {
  //   console.log(data);

  //   if (data.length) {

  //     data.forEach(element => this.acceptedRequestData.push(element));
  //     this.acceptedRequestData.sort((post1, post2) => post2.creation_timestamp - post1.creation_timestamp);
  //     this.initialLength = this.initialLength + data.length;
  //     this.showNumber = true;

  //   }

  // }))

  getAllPostOfUser(groupId, userId) {
    this.postService.getPostByUser(groupId, userId).subscribe(data => {

      this.usersAllPost = data;
      var entities = "";
      data.forEach(element => {
        entities += "&entity_id=" + element.post_id;
      });

      this.entities = entities;
      this.evtSearchSync(groupId, entities, +new Date - 3 * 259200000, +new Date);
    })
  }

  evtSearchSync(groupId, entities: any, start: number, end: number) {
    this.eventService.eventSearchByEntitySync(this.userGroupId, entities, start, end).subscribe(
      data => {
        console.log(data);
        if (Object.keys(data).length) {

          if (data.like) this.pusher(this.allEvents, data.like);
          if (data.share) this.pusher(this.allEvents, data.share);
          if (data.comment) this.pusher(this.allEvents, data.comment);
          if (data.report) this.pusher(this.allEvents, data.report);

          this.allEvents.sort(
            (evt1, evt2) => evt2.creation_timestamp - evt1.creation_timestamp
          );

        }

        this.evtSearchAsync(groupId, entities, +new Date, +new Date + 600000);
      }
    );
  }

  evtSearchAsync(groupId, entities: any, start: number, end: number) {
    this.eventService.eventSearchByEntityAsync(this.userGroupId, entities, start, end).subscribe(
      data => {
        console.log(data);
        if (Object.keys(data).length) {

          if (data.like) this.pusher(this.allEvents, data.like);
          if (data.share) this.pusher(this.allEvents, data.share);
          if (data.comment) this.pusher(this.allEvents, data.comment);
          if (data.report) this.pusher(this.allEvents, data.report);

          this.allEvents.sort(
            (evt1, evt2) => evt2.creation_timestamp - evt1.creation_timestamp
          );

          var max = this.allEvents[0].creation_timestamp + 1;
          this.evtSearchAsync(groupId, entities, max, max + Number(600000));

        }
        else this.evtSearchAsync(groupId, entities, +new Date(), +new Date() + Number(600000))

      }
    );
  }

  pusher(origin: any, data: any) {
    data.forEach(element => {
      origin.push(element);
      this.counter = this.counter + 1;
    });
  }

  displayCounter(count) {
    console.log("notification received");
    console.log(count);
    console.log(this.toggleNotification);
    if (this.toggleNotification) {
      console.log(' when notification box  is open');
      this.showNotif = true;
      this.counter = count
      setTimeout(() => this.showNotif = false, 2000)
    }
    else if (!this.toggleNotification) {
      console.log(' when notification box  is close');
      this.showNotif = true;
      this.counter = this.counter + count
    }
  }

  closeNotifyModel(value) {
    console.log('close  the model on notificatoin clicked');
    this.toggleNotification = !this.toggleNotification;

  }

  notifTabClicked() {
    this.hideNotif = false;
    this.showNotif = false;
    this.counter = 0;

  }

  settingClicked() {
    console.log("setttng clikced");
    this.hideToggle = false;

  }

  viewAllNotif() {
    var test = JSON.parse(localStorage.getItem('notificationdata'));

    console.log('notification  data');
    console.log(test);
    this.router.navigate(['/home/notification']);

  }

  stopTimer() {

    this.userIdle.stopTimer();
    //Start watching for user inactivity.
    this.sessionAlert = false;
    this.userIdle.startWatching();

  }


  removeDuplicates(myArr, prop) {
    console.log('duplicates in friend request called')
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }

  fixSlider() {

  }
  onChangeSlide(value) {
    if (value.checked === true) {
      var element = document.getElementById('slide');

      element.classList.remove("sidenavbar");
      element.classList.remove("chat_side_issue");
      element.classList.add("slidebar");
    } else {
      var element = document.getElementById('slide');
      element.classList.remove("slidebar");
      element.classList.add("sidenavbar");
      element.classList.add("chat_side_issue");

    }
  }

  onClickOutside(event: any) {

    if (event && event['value'] === true) {
      this.showManager = false;
    }
  }
}


