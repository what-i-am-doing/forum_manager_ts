import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { AddressService } from 'src/app/services/address.service';

@Component({
  selector: 'app-personal-detail',
  templateUrl: './personal-detail.component.html',
  styleUrls: ['./personal-detail.component.css'],
  providers:[AddressService,UserService]
})
export class PersonalDetailComponent implements OnInit {

  @Input() uesrId;
  public  activeUserPersonalData:any;
  public city:any;
    constructor(private  service:UserService,private addressService:AddressService) { }
  
    ngOnInit() {
  this.getUserPersonalDetails(this.uesrId)
    }
  
    getUserPersonalDetails(userId){
  
    
        this.service.getUserPersonalDetails(userId).subscribe(data=>{
      
          console.log(data);
          this.activeUserPersonalData=data;
          this.getAdress(data.address_id);
        },err=>{
        console.log('no personal data');

        })
     
    
      }
      
      getAdress(adressId){
        this.addressService.getAdress(adressId).subscribe(data=>{
        console.log('adress data found');
        console.log( data);
       
        this.city=data.city;
     
        })
        
        }
    
}
