import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { UserService } from "../../services/user.service";
import { PostService } from "../../services/post.service";
import { ImageService } from "../../services/image.service";
import { Lightbox, LightboxConfig, LightboxEvent, LIGHTBOX_EVENT, IEvent } from "ngx-lightbox";
import { PdfService } from "../../services/pdf.service";
import { VideoService } from "../../services/video.service";
import { Subscription } from "rxjs";


@Component({
  selector: 'app-post-review',
  templateUrl: './post-review.component.html',
  styleUrls: ['./post-review.component.css'],
  providers: [UserService, PostService]
})

export class PostReviewComponent implements OnInit, OnChanges {
  @Input() singlePost: any;
  userDetail: any;
  firstName: string;
  lastName: string;
  originalPost: any;
  profilePic: any;
  public hidden: boolean = true;
  public ifMax: boolean = false;
  private _subscription: Subscription;
  public mobileFriendlyZoomPercent = false;
  
  constructor(private userService: UserService, private postService: PostService,
    private pdfService:PdfService, private videoService:VideoService, 
    private imageService: ImageService,private _lightbox: Lightbox,
    private _lightboxEvent: LightboxEvent,
    private _lighboxConfig: LightboxConfig) {
         this._lighboxConfig.fadeDuration = 1;

     }

  ngOnInit() {

    this.userService.getUserDetails(this.singlePost.user_id).subscribe(user => {
      console.log(user);
      this.singlePost.user_name = user.user_full_text_name;
      this.userDetail = user;
      this.firstName = user.first_name;
      this.lastName = user.last_name;
      if (this.singlePost.post_type == "SHARED") {

        this.userService.getUserDetails(this.singlePost.original_user_id).subscribe(
          data => this.singlePost.originalUser = data
        );

        this.postService.getPostById(this.singlePost.original_post_id).subscribe(
          post => this.singlePost.originalPost = post
        );

      }
      if (user.image_id && user.image_id != 'string') this.getProfile(user.image_id);

    });

    if (this.singlePost.post_content) {
      this.postService
        .getPostContent(this.singlePost.post_content)
        .subscribe(datalist => {
          console.log(datalist);

          if (datalist.content_images_list) {
            this.singlePost._albums = [];
            this.singlePost._real_albums = [];
            datalist.content_images_list.forEach(element => {
              this.imageService.getImage(element).subscribe(imgData => {
                console.log(imgData);
                const src = imgData.image_storage_url.replace("https", "http");
                const caption = imgData.image_name;
                const thumb = imgData.image_storage_url.replace(
                  "https",
                  "http"
                );
                if (imgData.image_id == this.singlePost.img_id) var is_preview = true;
                else is_preview = false;
                const album = {
                  src: src,
                  caption: caption,
                  thumb: thumb,
                  is_preview: is_preview
                };
                console.log(album);
                (album.is_preview) ? this.singlePost._albums.unshift(album) : this.singlePost._albums.push(album);
                this.singlePost._real_albums.push(imgData);

              });
            });
          }

          if (datalist.content_pdf_list) {
            
            this.singlePost._pdfs = [];
            datalist.content_pdf_list.forEach(element => {
              this.pdfService.getPdf(element).subscribe(pdfData => {
                console.log(pdfData);
                this.singlePost._pdfs.push(pdfData);
              });
            });
          }

          if (datalist.content_video_list) {
           
            this.singlePost._videos = [];
            datalist.content_video_list.forEach(element => {
              this.videoService.getVideo(element).subscribe(videoData => {
                console.log(videoData);
                this.singlePost._videos.push(videoData);
              });
            });
          }
        });
    }
  }
  
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    this.ngOnInit();
  }
  
  public get mobileFriendlyZoom(): string | undefined {
    if (this.mobileFriendlyZoomPercent) {
      return "200%";
    }
    return undefined;
  }

  getProfile(image_id: string) {

    this.imageService.getImage(image_id).subscribe(data =>
      this.profilePic = this.URLChange(data.image_storage_url)

    )

  }
  open(index: number, album): void {
    this._subscription = this._lightboxEvent.lightboxEvent$.subscribe(
      (event: IEvent) => this._onReceivedEvent(event)
    );

    // override the default config
    this._lightbox.open(album, index, {
      wrapAround: true,
      showImageNumberLabel: true
    });
  }

  close(): void {
    // close lightbox programmatically
    this._lightbox.close();
  }

  private _onReceivedEvent(event: IEvent): void {
    if (event.id === LIGHTBOX_EVENT.CLOSE) {
      this._subscription.unsubscribe();
    }
  }
  URLChange(pdf: any) {
    return pdf.replace("https", "http");
  }

}
