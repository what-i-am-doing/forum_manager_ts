export class UserDocument {

    constructor(){}
    
    private comments:string;
    private creation_timestamp:string;
    private doc_description:string;
    private doc_name_by_user:string;
    private doc_type_id:string;
    private document_content:string;
    private last_updated_timestamp:string;
    private status:string;
    // private user_document_id:string;
    
    /**
    * Getter $user_id
    * @return {string}
    */
    public get $user_id(): string {
    return this.user_id;
    }
    
    /**
    * Setter $user_id
    * @param {string} value
    */
    public set $user_id(value: string) {
    this.user_id = value;
    }
    private user_id:string;
    
    /**
    * Getter $comments
    * @return {string}
    */
    public get $comments(): string {
    return this.comments;
    }
    
    /**
    * Setter $comments
    * @param {string} value
    */
    public set $comments(value: string) {
    this.comments = value;
    }
    
    /**
    * Getter $creation_timestamp
    * @return {string}
    */
    public get $creation_timestamp(): string {
    return this.creation_timestamp;
    }
    
    /**
    * Setter $creation_timestamp
    * @param {string} value
    */
    public set $creation_timestamp(value: string) {
    this.creation_timestamp = value;
    }
    
    /**
    * Getter $doc_description
    * @return {string}
    */
    public get $doc_description(): string {
    return this.doc_description;
    }
    
    /**
    * Setter $doc_description
    * @param {string} value
    */
    public set $doc_description(value: string) {
    this.doc_description = value;
    }
    
    /**
    * Getter $doc_name_by_user
    * @return {string}
    */
    public get $doc_name_by_user(): string {
    return this.doc_name_by_user;
    }
    
    /**
    * Setter $doc_name_by_user
    * @param {string} value
    */
    public set $doc_name_by_user(value: string) {
    this.doc_name_by_user = value;
    }
    
    /**
    * Getter $doc_type_id
    * @return {string}
    */
    public get $doc_type_id(): string {
    return this.doc_type_id;
    }
    
    /**
    * Setter $doc_type_id
    * @param {string} value
    */
    public set $doc_type_id(value: string) {
    this.doc_type_id = value;
    }
    
    /**
    * Getter $document_content
    * @return {string}
    */
    public get $document_content(): string {
    return this.document_content;
    }
    
    /**
    * Setter $document_content
    * @param {string} value
    */
    public set $document_content(value: string) {
    this.document_content = value;
    }
    
    /**
    * Getter $last_updated_timestamp
    * @return {string}
    */
    public get $last_updated_timestamp(): string {
    return this.last_updated_timestamp;
    }
    
    /**
    * Setter $last_updated_timestamp
    * @param {string} value
    */
    public set $last_updated_timestamp(value: string) {
    this.last_updated_timestamp = value;
    }
    
    /**
    * Getter $status
    * @return {string}
    */
    public get $status(): string {
    return this.status;
    }
    
    /**
    * Setter $status
    * @param {string} value
    */
    public set $status(value: string) {
    this.status = value;
    }
    
    /**
    * Getter $user_document_id
    * @return {string}
    */
    // public get $user_document_id(): string {
    // return this.user_document_id;
    // }
    
    /**
    * Setter $user_document_id
    * @param {string} value
    */
    // public set $user_document_id(value: string) {
    // this.user_document_id = value;
    // }
    
    
    
    
    }