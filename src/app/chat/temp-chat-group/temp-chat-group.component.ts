
import { Component, OnInit, ElementRef, ComponentFactoryResolver, ViewContainerRef, ViewChild, ComponentRef, ComponentFactory } from '@angular/core';
import { ChatMessageComponent } from '../chat-message/chat-message.component';
import {MatMenuTrigger} from '@angular/material/menu';
import {MatAutocomplete} from '@angular/material';

@Component({
  selector: 'app-temp-chat-group',
  templateUrl: './temp-chat-group.component.html',
  styleUrls: ['./temp-chat-group.component.css']
})
export class TempChatGroupComponent implements OnInit {

  
  createChatGroup:boolean=false;
  @ViewChild('fruitInput') fruitInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;


  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild("chatBody", { read: ViewContainerRef }) container:ViewContainerRef;
  componentRef: ComponentRef<any>;
  tempchatGroupcomponentRef: ComponentRef<any>;

  currentMsg:string = "Start Conversation";
  friends:any;
  constructor() { }

  ngOnInit() {
  }
  _ref: any;

  remove = function () {
  
    this._ref.destroy();
  
  }
}
