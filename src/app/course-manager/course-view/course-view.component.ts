import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ImageService } from "../../services/image.service";
import { VideoService } from "../../services/video.service";
import { Lightbox, LightboxConfig, LightboxEvent, LIGHTBOX_EVENT, IEvent } from "ngx-lightbox";
import { Subscription } from 'rxjs';
import { Course } from "../../model/course";
import { CourseService } from "../../services/course.service";
import { Route, Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-course-view',
  templateUrl: './course-view.component.html',
  styleUrls: ['./course-view.component.css']
})

export class CourseViewComponent implements OnInit, OnChanges {

  constructor(private _formBuilder: FormBuilder, private imageService: ImageService,
  private videoService: VideoService,
  private courseService: CourseService,
  private _lightbox: Lightbox,
  private _lightboxEvent: LightboxEvent,
  private _lighboxConfig: LightboxConfig,private router:Router,private route:ActivatedRoute) { }
  basicGroup: FormGroup;
  imgGroup: FormGroup;
  detailGroup: FormGroup;
  courseGroup: FormGroup;
  descGroup: FormGroup;
  extData: FormGroup;
  private _subscription: Subscription;
  coUpdated: boolean = false;
  edit: boolean = false;
  coursePic: string;
  video: any;
  showExt: boolean = false;
  courseModules: any;
  activeLink: boolean = false;
  curModule: any;

  @Input() course: any;
  @Input() readable?: boolean;
  
  path:any = [];

  ngOnInit() {
    console.log('recieved',this.course);
    this.courseModules = undefined;
    this.curModule = undefined;
    this.video = undefined;
    this.coursePic = undefined;

    //     this.route.queryParams.subscribe((obj:any)=>{
    //   // console.log('@@@courses',obj);
    //   this.idPassedFromParams  = obj.courses;
    // });

    // this.courseService.getCourseById(this.idPassedFromParams).subscribe((res:any)=>{
    //     console.log('resssssssssssssssss',res,this.course);
    //     this.course = {};
    //     this.course = res;
    //     console.log('reesssssssssssssssssssss2',this.course);
    // });


    this.basicGroup = this._formBuilder.group({
      basicCtrl: [{ value: this.course.course_name, disabled: true }, Validators.required],
      basicCtrl2: [{ value: this.course.heading, disabled: true }, Validators.required], 
    });

    this.imgGroup = this._formBuilder.group({
      imgCtrl1: [{ value: this.course.course_type_id, disabled: true }, Validators.required],
      imgCtrl2: [{ value: this.course.status, disabled: true }, Validators.required],

    });

    this.detailGroup = this._formBuilder.group({
      detailCtrl1: [{ value: this.course.category, disabled: true }, Validators.required],
      detailCtrl2: [{ value: this.course.is_education_content, disabled: true }, Validators.required],
      detailCtrl3: [{ value: this.course.language, disabled: true }, Validators.required]

    });

    this.courseGroup = this._formBuilder.group({
      courseCtrl1: [{ value: this.course.course, disabled: true }, Validators.required],
      courseCtrl2: [{ value: this.course.stream, disabled: true }, Validators.required],
      courseCtrl3: [{ value: this.course.class, disabled: true }, Validators.required],
      courseCtrl4: [{ value: this.course.sub_class, disabled: true }, Validators.required],
      courseCtrl5: [{ value: this.course.subject, disabled: true }, Validators.required],
      courseCtrl6: [{ value: this.course.chapter, disabled: true }, Validators.required],
      courseCtrl7: [{ value: this.course.concept, disabled: true }, Validators.required]

    });

    this.descGroup = this._formBuilder.group({
      descCtrl1: [{ value: this.course.course_description, disabled: true }]
    });

    this.extData = this._formBuilder.group({
      extDataCtrl1: ['']
    });

    if (this.course.image_id) {
      this.imageService.getImage(this.course.image_id).subscribe(
        img => this.coursePic = this.URLChange(img.image_storage_url)
      );
    }

    if (this.course.course_content_list) {
      this.imageService.getContent(this.course.course_content_list[0]).subscribe(

        content => {
          if (content.ext_link_data_list) {
            this.videoService.getExtVideo(content.ext_link_data_list[0]).subscribe(
              video_data => {
                this.videoService.getYouTubeVideoData(video_data.video_id).subscribe(
                  video => this.video = video
                )
              }
            );
          }

          if (content.content_images_list) {
            this.course._albums = [];
            content.content_images_list.forEach(element => {
              this.imageService.getImage(element).subscribe(imgData => {
                console.log(imgData);
                const src = imgData.image_storage_url.replace("https", "http");
                const caption = imgData.image_name;
                const thumb = imgData.image_storage_url.replace(
                  "https",
                  "http"
                );

                const album = {
                  src: src,
                  caption: caption,
                  thumb: thumb

                };

                this.course._albums.push(album);


              });
            });
          }
        }
      );
    }
    this.getCourses();
  }

  
  allCoursesCreatedByUser:any;
 // edits:boolean=false;
  getCourses() {
    // console.log('pkpk@',this.course);
    // this.courseService.getCourseByUser(localStorage.getItem('user'),localStorage.getItem('groupId')).subscribe(
    //   courses => {
    //     let a= courses;
    //     this.allCoursesCreatedByUser = a;
    //     console.log(courses,        this.allCoursesCreatedByUser,a,'0000');
    //     // this.userCourses = courses;
    //     courses.forEach((element) => {
    //       // this.TREE_DATA.push({id:element.course_id,name:element.course_name,course:element});
    //       console.log('________====',this.course.course_id,element.course_id)

          if(this.course.course_id ){
            this.moduleListCall(this.course.course_id);
          }
        //});
         
        // this.dataSource.data = this.TREE_DATA;
        // this.showCourse = courses[0];
     // }
    
    console.log('&&&&arrrrrrrrrrr',this.arrOfModules);
      localStorage.setItem('courseidformodule',this.course.course_id);

      // logic for showing edit button
    //  this.showEdit();
      }



//showEdit(){
  //console.log('TT',this.allCoursesCreatedByUser,this.allCoursesCreatedByUser);
//   for(let i=0 ;i<this.allCoursesCreatedByUser.length;i++){
//     console.log('TTTT1',this.allCoursesCreatedByUser[i].course_id,this.course.course_id);
//     if(this.allCoursesCreatedByUser[i].course_id === this.course.course_id){
//       this.edits = true;
//       break;
//     }
//     else{
//       this.edits = false;
//     }
// }
// console.log('TTTTTT',this.edit);
// }
  arrOfModules=[];
  
  moduleListCall(courseId){
    console.log('Called....',courseId);

    this.courseService.getModulesByCourse(courseId).subscribe((res:any)=>{
        console.log('kpl',res);
      if(res.length > 0){
        this.arrOfModules.push(res);
      }
    });
  }
  

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    if (!changes.course.firstChange) this.ngOnInit();
  }

  editCourse() {
    this.edit = true;
    this.basicGroup.get('basicCtrl').enable();
    this.basicGroup.get('basicCtrl2').enable();
    this.imgGroup.get('imgCtrl1').enable();
    this.imgGroup.get('imgCtrl2').enable();
    this.detailGroup.get('detailCtrl1').enable();
    this.detailGroup.get('detailCtrl2').enable();
    this.detailGroup.get('detailCtrl3').enable();
    this.courseGroup.get('courseCtrl1').enable();
    this.courseGroup.get('courseCtrl2').enable();
    this.courseGroup.get('courseCtrl3').enable();
    this.courseGroup.get('courseCtrl4').enable();
    this.courseGroup.get('courseCtrl5').enable();
    this.courseGroup.get('courseCtrl6').enable();
    this.courseGroup.get('courseCtrl7').enable();
    this.descGroup.get('descCtrl1').enable();

    console.log(this.basicGroup);

  }

  updateCourse() {


    var course = new Course;
    course.$course_name = this.basicGroup.value.basicCtrl;
    course.$heading = this.basicGroup.value.basicCtrl2;
    course.$course_type_id = this.imgGroup.value.imgCtrl1;
    course.$status = this.imgGroup.value.imgCtrl2;
    course.$category = this.detailGroup.value.detailCtrl1;
    course.$is_education_content = this.detailGroup.value.detailCtrl2;
    course.$language = this.detailGroup.value.detailCtrl3;
    course.$course = this.courseGroup.value.courseCtrl1;
    course.$stream = this.courseGroup.value.courseCtrl2;
    course.$class = this.courseGroup.value.courseCtrl3;
    course.$sub_class = this.courseGroup.value.courseCtrl4;
    course.$subject = this.courseGroup.value.courseCtrl5;
    course.$chapter = this.courseGroup.value.courseCtrl6;
    course.$concept = this.courseGroup.value.courseCtrl7;
    course.$course_description = this.descGroup.value.descCtrl1;

    this.courseService.updateCourse(this.course.course_id, course).subscribe(
      () => {
        this.coUpdated = true;
        this.updateView();
        this.removeEdit();
      }
    );
  }

  updateView() {
    this.course.course_name = this.basicGroup.value.basicCtrl;
    this.course.course_type_id = this.imgGroup.value.imgCtrl1;
    this.course.heading = this.basicGroup.value.basicCtrl2;
    this.course.status = this.imgGroup.value.imgCtrl2;
    this.course.category = this.detailGroup.value.detailCtrl1;
    this.course.is_education_content = this.detailGroup.value.detailCtrl2;
    this.course.language = this.detailGroup.value.detailCtrl3;
    this.course.course = this.courseGroup.value.courseCtrl1;
    this.course.stream = this.courseGroup.value.courseCtrl2;
    this.course.class = this.courseGroup.value.courseCtrl3;
    this.course.sub_class = this.courseGroup.value.courseCtrl4;
    this.course.subject = this.courseGroup.value.courseCtrl5;
    this.course.chapter = this.courseGroup.value.courseCtrl6;
    this.course.concept = this.courseGroup.value.courseCtrl7;
    this.course.course_description = this.descGroup.value.descCtrl1;
  }

  removeEdit() {
    this.edit = false;
   // this.basicGroup.get('basicCtrl').disable();
    this.basicGroup.get('basicCtrl2').disable();
    this.imgGroup.get('imgCtrl1').disable();
    this.imgGroup.get('imgCtrl2').disable();
    this.detailGroup.get('detailCtrl1').disable();
    this.detailGroup.get('detailCtrl2').disable();
    this.detailGroup.get('detailCtrl3').disable();
    this.courseGroup.get('courseCtrl1').disable();
    this.courseGroup.get('courseCtrl2').disable();
    this.courseGroup.get('courseCtrl3').disable();
    this.courseGroup.get('courseCtrl4').disable();
    this.courseGroup.get('courseCtrl5').disable();
    this.courseGroup.get('courseCtrl6').disable();
    this.courseGroup.get('courseCtrl7').disable();
    this.descGroup.get('descCtrl1').disable();
  }


  URLChange(url: any) {
    return url.replace("https", "http");
  }


  open(index: number, album): void {
    this._subscription = this._lightboxEvent.lightboxEvent$.subscribe(
      (event: IEvent) => this._onReceivedEvent(event)
    );

    // override the default config
    this._lightbox.open(album, index, {
      wrapAround: true,
      showImageNumberLabel: true
    });
  }

  close(): void {
    // close lightbox programmatically
    this._lightbox.close();
  }

  private _onReceivedEvent(event: IEvent): void {
    if (event.id === LIGHTBOX_EVENT.CLOSE) {
      this._subscription.unsubscribe();
    }
  }

  modulesOfCourse() {
    
      this.courseService.ModuleByCourse(this.course.course_id).subscribe(
        modules => {
          this.courseModules = modules;
          this.path.push({"id":this.course.course_id,"path_name":this.course.course_name});
        }
      );
    

    // else {
    //   this.courseService.ModuleByParentModule(this.curModule.module_id,this.course.course_id).subscribe(
    //     modules=>{
    //      this.courseModules = modules;
    //     }
    //   );
    // }
  }

  openModulesofModules(module:any) {
    this.courseService.ModuleByParentModule(module.module_id,this.course.course_id).subscribe(
      modules=>{
       this.courseModules = modules;
      }
    );
  }

  showChildModules(event:any) {
     this.courseModules = event[0];
     this.path.push(event[1]);
     this.curModule = undefined;
     
  }

  backButton() {
    this.curModule = undefined;
    this.path.pop();
    if(!this.path.length) 
       this.courseModules = undefined;
    else if (this.path.length == 1) 
      this.courseService.ModuleByCourse(this.course.course_id).subscribe(
        modules => this.courseModules = modules
       );
    else
      this.courseService.ModuleByParentModule(this.path[this.path.length -1].id,this.course.course_id).subscribe(
        modules=>{
        this.courseModules = modules;
        }
      );
  }


  moduleShow:boolean = false;
  currentModule:string;
  moduleView(moduleInfo){
    this.curModule = moduleInfo;
    this.moduleShow =true;
    this.currentModule  =  moduleInfo.module_name;
    console.log('moduleInfo',moduleInfo,moduleInfo.module_id,this.curModule);

    this.fromIdFetchSubmoduleName(moduleInfo.module_id); 
  }

  arrOfSubModules=[];
  fromIdFetchSubmoduleName(moduleId){
    this.arrOfSubModules = []; //A.splice(0,A.length)
    console.log('fffffffff',moduleId)
    var course_id=localStorage.getItem('courseIdforjoin');
    console.log('ggggggggggg',course_id);
    this.courseService.ModuleByParentModule(moduleId,course_id).subscribe((b:any)=>
    { 
      console.log('jjjjjjjjjjjjjjj',b);
       if(b.length > 0){
      this.arrOfSubModules.push(b);
    }
        console.log('111111111111111',this.arrOfSubModules)
    
  });
}
SubmoduleShow:boolean = false;
SubmoduleView(SubmoduleInfo){
  this.curModule = SubmoduleInfo;
    this.SubmoduleShow =true;
    console.log('SubmoduleInfo',SubmoduleInfo);

}
}
