import { Injectable } from '@angular/core';
import { Post } from '../model/post';
import { HttpClient , HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry ,delay } from 'rxjs/operators';
import { post } from 'selenium-webdriver/http';
import { environment } from '../../environments/environment';


@Injectable()
export class PostService{
  private serverurl:string='http://ec2-13-127-215-157.ap-south-1.compute.amazonaws.com:3000';
  private api_url:string = environment.api_url;
  //serverurl:"http://localhost:3000"
  
    constructor(private http: HttpClient) { }

    userpost(post:Post):Observable<any>{

            return this.http.post<any>(this.api_url+'/post/', post, {headers: new HttpHeaders({

                'Content-Type':  'application/json',
                'Accept':        'application/json',
                'from_user_id':  post.$user_id
                
              })
            })
              .pipe(
                catchError(this.handleError)
              );
    }

    usercontent(content:any):Observable<any>{

           return this.http.post<any>(this.api_url+'/content/',content,{headers: new HttpHeaders({

            'Content-Type':  'application/json',
            'Accept':        'application/json',
            'from_user_id':  localStorage.getItem('user')
            
          })
        }).pipe(
               
          retry(1), // retry a failed request up to 1 time
          catchError(this.handleError) // then handle the error
         );;
    }

    getPostContent(contentID:string):Observable<any>{
          
      return this.http.get<any>(this.api_url+'/content/'+contentID,{headers: new HttpHeaders({

        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })
    } ).pipe(
       
       retry(1), // retry a failed request up to 1 time
       catchError(this.handleError) // then handle the error
      );


    }

    getPostById(postId):Observable<any> {
      return this.http.get<any>(this.api_url+'/post/'+postId,{headers: new HttpHeaders({

        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })
    } ).pipe(
       
       retry(1), // retry a failed request up to 1 time
       catchError(this.handleError) // then handle the error
      );
    }

    getPostByUser(groupID:string , userID:string):Observable<any> {
             
      return this.http.get<any>(this.api_url+'/post/from_user?group_id='+groupID+'&user_id='+userID+'&from=0&size=10',{headers: new HttpHeaders({

        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })
    } ).pipe(
       
       retry(1), // retry a failed request up to 1 time
       catchError(this.handleError) // then handle the error
      );return 
    }
    getPostByUserAtInterval(groupID:string , userID:string,time_start, time_end,from,size):Observable<any> {
            
      return this.http.get<any>(this.api_url+'/post/from_user?group_id='+groupID+'&user_id='+userID+'&time_start='+time_start+'&time_end='+time_end+'&from='+from+'&size='+size,{headers: new HttpHeaders({

        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })
    } ).pipe(
       
       retry(1), // retry a failed request up to 1 time
       catchError(this.handleError) // then handle the error
      );return 
    }

    getAllPosts(groupId:string,from:number,size:number):Observable<Post[]>{
     
              return this.http.get<Post[]>(this.api_url+'/post/by_group/?group_id='+groupId+'&group_id_list=public&from='+from+'&size='+size,{headers: new HttpHeaders({

                'Accept':        'application/json',
                'from_user_id':  localStorage.getItem('user')
                
              })
            } ).pipe(
               
               retry(1), // retry a failed request up to 1 time
               catchError(this.handleError) // then handle the error
              );

      }
    
    getAllPostsByStatus(groupID:string, status:string, from:number, size:number):Observable<Post[]> {
      return this.http.get<Post[]>(this.api_url+`/post/by_group/by_status?group_id=${groupID}&status=${status}&from=${from}&size=${size}`,{headers: new HttpHeaders({

        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })
    } ).pipe(
       
       retry(1), // retry a failed request up to 1 time
       catchError(this.handleError) // then handle the error
      );
    }

    getPostByGroupsearchtext(groupId:string,searchText:string,from:number,size:number):Observable<any>{

      return this.http.get<any>(this.api_url+'/post/by_group/search_text?group_id='+groupId+'&search_text='+searchText+'&from='+from+'&size='+size,{headers: new HttpHeaders({

        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })
    } ).pipe(
       
       retry(1), // retry a failed request up to 1 time
       catchError(this.handleError) // then handle the error
      );

            }

      changeLikes(postId:string,post:any):Observable<any>{

              let body = JSON.stringify({"id":postId,"number_of_likes": post});
             return this.http.patch<any>(this.serverurl+'/posts/'+postId,{"number_of_likes": post},{headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': 'my-auth-token'
            })
          }).pipe(

              catchError(this.handleError) 
             );
    }  


    isLikedTrue(user:string,postId:string):Observable<any>{

                     return this.http.get<any>(this.api_url+'/event/search_by_user/by_entity/by_event?group_id=public&user_id='+user+'&entity_type=POST&entity_id='+postId+'&event_type=like&size=3',{headers: new HttpHeaders({

                      'Accept':        'application/json',
                      'from_user_id':  localStorage.getItem('user')
                      
                    })}).pipe(

                      retry(1), // retry a failed request up to 1 time
                      catchError(this.handleError) // then handle the error
                     );

                }

    isSharedTrue(user:string,postId:string):Observable<any>{

      return this.http.get<any>(this.api_url+'/event/search_by_user/by_entity/by_event?group_id=public&user_id='+user+'&entity_type=POST&entity_id='+postId+'&event_type=share&size=3',{headers: new HttpHeaders({

        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })}).pipe(

        retry(1), // retry a failed request up to 1 time
        catchError(this.handleError) // then handle the error
      );

  }


    deleteLikedData(dataId:string){

          return this.http.delete(this.serverurl+'/post_events/'+dataId);
    }       

    createLikedData(post_id:string){

          return this.http.post(this.serverurl+'/post_events/',{"post_id":post_id,"event_type_id":1,"user_id":localStorage.getItem("user")})
    }

    updatePost(postId:string, body:any):Observable<any>{
       
      return this.http.put<any>(this.api_url+'/post/'+postId,body,{headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })
    } ).pipe(
       
       retry(1), // retry a failed request up to 1 time
       catchError(this.handleError) // then handle the error
      );

    }

    updateusercontent(contentId:string, body:any):Observable<any>{
       
      return this.http.put<any>(this.api_url+'/content/'+contentId,body,{headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })
    } ).pipe(
       
       retry(1), // retry a failed request up to 1 time
       catchError(this.handleError) // then handle the error
      );

    }

    getAllEventonPost(groupID:string,postId:string):Observable<any>{

      return this.http.get<any>(this.api_url+'/event/search_by_entity?group_id='+groupID+'&entity_type=POST&entity_id='+postId+'&from=0&size=100',{headers: new HttpHeaders({

        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })}).pipe(

        retry(1), // retry a failed request up to 1 time
        catchError(this.handleError) // then handle the error
      );

  }
  getAllEventonPostatInterval(groupID:string,postId:string,starttime, endtime):Observable<any>{

    return this.http.get<any>(this.api_url+'/event/search_by_entity/async?group_id='+groupID+'&entity_type=POST&entity_id='+postId+'&time_start='+starttime+'&time_end='+endtime,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })}).pipe(

      retry(1), // retry a failed request up to 1 time
      catchError(this.handleError) // then handle the error
    );

}
 

 findPostData(post:any){
                     console.log(post);
             }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
          // A client-side or network error occurred. Handle it accordingly.
          console.error('An error occurred:', error.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
          'Something bad happened; please try again later.');
      };

}