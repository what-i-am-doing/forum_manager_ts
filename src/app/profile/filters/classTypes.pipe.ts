import { Pipe,PipeTransform } from "@angular/core";


@Pipe({

    'name':'classTypes'
})

export class ClassTypesPipe implements PipeTransform{

transform(value , search ){

   return value.filter(clas => clas.class_id === search)[0].class_name
}

}