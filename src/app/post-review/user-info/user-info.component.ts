import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ImageService } from 'src/app/services/image.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css'],
  providers: [UserService, ImageService]
})
export class UserInfoComponent implements OnInit {

  @Input() userId: any;
  public imageId: any;
  public username: any;
  profilePic: any;
  gender: any;
  constructor(private userService: UserService, private imageService: ImageService, private router: Router) { }

  ngOnInit() {
    this.getUserDetails(this.userId);
    
  }

  getUserDetails(userId) {

    this.userService.getUserDetails(userId).subscribe(data => {

      this.username = data.user_full_text_name;
      this.getProfile(data.image_id);

    })


  }


  getProfile(image_id) {
    this.imageService.getImage(image_id).subscribe(data => {

      this.profilePic = this.URLChange(data.image_storage_url);

    })

  }


  seeFriendProfile() {
    this.router.navigate(['/home/userprofile'], { queryParams: { userId: this.userId } });
  }


  URLChange(img: any) { return img.replace("https", "http"); }


  

}
