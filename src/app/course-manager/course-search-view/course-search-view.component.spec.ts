import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseSearchViewComponent } from './course-search-view.component';

describe('CourseSearchViewComponent', () => {
  let component: CourseSearchViewComponent;
  let fixture: ComponentFixture<CourseSearchViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseSearchViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseSearchViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
