export class Group {

constructor( ){};

private client_id: string;
private comments: string;
private creation_timestamp: number;
private custom_tag_line: string;
private group_id: string;
private group_name: string;
private image_id: string;
private last_updated_timestamp:number ;
private status: string;


    /**
     * Getter $client_id
     * @return {string}
     */
	public get $client_id(): string {
		return this.client_id;
	}

    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $creation_timestamp
     * @return {number}
     */
	public get $creation_timestamp(): number {
		return this.creation_timestamp;
	}

    /**
     * Getter $custom_tag_line
     * @return {string}
     */
	public get $custom_tag_line(): string {
		return this.custom_tag_line;
	}

    /**
     * Getter $group_id
     * @return {string}
     */
	public get $group_id(): string {
		return this.group_id;
	}

    /**
     * Getter $group_name
     * @return {string}
     */
	public get $group_name(): string {
		return this.group_name;
	}

    /**
     * Getter $image_id
     * @return {string}
     */
	public get $image_id(): string {
		return this.image_id;
	}

    /**
     * Getter $last_updated_timestamp
     * @return {number }
     */
	public get $last_updated_timestamp(): number  {
		return this.last_updated_timestamp;
	}

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Setter $client_id
     * @param {string} value
     */
	public set $client_id(value: string) {
		this.client_id = value;
	}

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $creation_timestamp
     * @param {number} value
     */
	public set $creation_timestamp(value: number) {
		this.creation_timestamp = value;
	}

    /**
     * Setter $custom_tag_line
     * @param {string} value
     */
	public set $custom_tag_line(value: string) {
		this.custom_tag_line = value;
	}

    /**
     * Setter $group_id
     * @param {string} value
     */
	public set $group_id(value: string) {
		this.group_id = value;
	}

    /**
     * Setter $group_name
     * @param {string} value
     */
	public set $group_name(value: string) {
		this.group_name = value;
	}

    /**
     * Setter $image_id
     * @param {string} value
     */
	public set $image_id(value: string) {
		this.image_id = value;
	}

    /**
     * Setter $last_updated_timestamp
     * @param {number } value
     */
	public set $last_updated_timestamp(value: number ) {
		this.last_updated_timestamp = value;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}

 

}