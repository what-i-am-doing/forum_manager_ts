import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ImageService } from "../../services/image.service";
import { VideoService } from "../../services/video.service";
import { Lightbox, LightboxConfig, LightboxEvent, LIGHTBOX_EVENT, IEvent} from "ngx-lightbox";
import { Subscription } from 'rxjs';
import { User } from "../../model/user";
import { UserService } from  "../../services/user.service";
import { CourseService } from "../../services/course.service";
import { Module } from "../../model/module";
import { publish } from 'rxjs/operators';
import {  Router, Route, ActivatedRoute } from '@angular/router';

interface ModuleNode {
  id:string;
  name: string;
  course?:any;
  module?:any;
  submodule?:any;
  children?: ModuleNode[];
}


@Component({
  selector: 'app-module-search-view',
  templateUrl: './module-search-view.component.html',
  styleUrls: ['./module-search-view.component.css']
})

export class ModuleSearchViewComponent implements OnInit {
 
  // @Input() module: any;
  // @Input() readable?:boolean = false;
//  @Output() childModules = new EventEmitter<any>();
  modulePic:string;
  showExt:boolean = false;
  edit:boolean = false;
  video: any;
  basicGroup: FormGroup;
  basicGroup1:FormGroup;
  imgGroup: FormGroup;
  detailGroup: FormGroup;
  courseGroup: FormGroup;
  descGroup: FormGroup;
  extData:FormGroup;
  // modUpdated:boolean = false;
  moduleModules:any;
  private _subscription: Subscription;
  idPassedFromParams: any;
  curSubModule: any;
  curModule:any;
  activeLink:any;
  groupId: string = localStorage.getItem("groupId");
  searchQuery:string;
  module_list:any=[];
  
  path:any = [];
  userId:any;

  constructor(private router:Router,private _formBuilder: FormBuilder, private imageService:ImageService, private route:ActivatedRoute,
    private videoService:VideoService,
    private courseService:CourseService,
    private userService: UserService,
    private _lightbox: Lightbox,
    private _lightboxEvent: LightboxEvent,
    private _lighboxConfig: LightboxConfig,
    ) {


      this.basicGroup = this._formBuilder.group({
        basicCtrl: [{ value: '', disabled: true }, Validators.required],
        basicCtrl2: [{ value: '', disabled: true }, Validators.required],
      });
      this.imgGroup = this._formBuilder.group({
        imgCtrl1: [{ value: '', disabled: true }, Validators.required],
        imgCtrl2: [{ value: '', disabled: true }, Validators.required],
  
      });
  
      this.detailGroup = this._formBuilder.group({
        detailCtrl1: [{ value: '', disabled: true }, Validators.required],
        detailCtrl2: [{ value: '', disabled: true }, Validators.required],
        detailCtrl3: [{ value: '', disabled: true }, Validators.required]
  
      });
  
      this.courseGroup = this._formBuilder.group({
        courseCtrl1: [{ value: '', disabled: true }, Validators.required],
        courseCtrl2: [{ value: '', disabled: true }, Validators.required],
        courseCtrl3: [{ value: '', disabled: true }, Validators.required],
        courseCtrl4: [{ value: '', disabled: true }, Validators.required],
        courseCtrl5: [{ value: '', disabled: true }, Validators.required],
        courseCtrl6: [{ value: '', disabled: true }, Validators.required],
        courseCtrl7: [{ value: '', disabled: true }, Validators.required]
  
      });
  
      this.descGroup = this._formBuilder.group({
        descCtrl1: [{ value: '', disabled: true }]
      });
  
      this.extData = this._formBuilder.group({
        extDataCtrl1:['']
      });
  
  
      
    }
  ngOnInit() {
    this.video = undefined;
     this.modulePic = undefined;
     this.moduleModules = undefined;
     this.curSubModule = undefined;
  // this.getModules();
     this.route.queryParams.subscribe((obj:any)=>{
      console.log('@@@modules',obj);
      this.idPassedFromParams  = obj.modules;
     });

    this.getModuleDetailCall().then((obj:any)=>{
      this.fillFormFieldNow();
      //this.getUserDetailsForJoinedCourses(localStorage.getItem('user'));
      console.log(this.descGroup,'!!!!!!!!!');
      }
      );
    }
  
//   getModules(){
//   this.courseService. getModulesBySearchText( this.module.module_name,this.groupId,0,20).subscribe(
//     modules => {
//     console.log(modules,'JJJJJJJJJJJJJJJJJJJ', this.module,this.groupId); 
//       // this.userCourses
//       modules.forEach((element) => {
//         // this.TREE_DATA.push({id:element.course_id,name:element.course_name,course:element});
//         console.log('________====',this.module.module_id,element.module_id)
//           this.subModuleListCall(element.module_id);
//       });       
//       // this.dataSource.data = this.TREE_DATA;
//       // this.showCourse = courses[0];
//     }
//   );
//   console.log('&&&&arrrrrrrrrrr',this.arrOfSubModules);
// }
arrOfSubModules=[];
 
// subModuleListCall(module_id){
//   console.log('Called....',module_id);
//   //edit later

//   var course_id=localStorage.getItem('courseIdforjoin');
//   console.log('nnnnnnnnnnnooooo',localStorage.getItem('courseIdforjoin'));
//  console.log('heyyyyyyyyyyyyyyyyyyyyyyy',this.module.module_id);
// this.courseService.ModuleByParentModule(this.module.module_id,course_id).subscribe((res:any)=>
// {  if(res.length > 0){
//  this.arrOfSubModules.push(res);
// }
//     console.log('9999999999999',res)
  
// })

// }
module:any;
getModuleDetailCall(){
  return new Promise((resolve,reject)=>{

    this.courseService.getModulesByIds(this.idPassedFromParams).subscribe((res:any)=>{
     console.log('resssssssssssssssss',res);
      this.module = res;
      console.log('2',this.module);
      resolve(this.module)
      localStorage.setItem('courseIdforjoin',this.idPassedFromParams);
      let subModuleIdList= res.sub_modules_list;
      console.log('aaaaaaaaaaaaa',subModuleIdList)
      if(subModuleIdList != undefined){

        subModuleIdList.forEach((i:any)=>{
          console.log('qwas',subModuleIdList[i]);
          this.getSubModulesById(subModuleIdList[i]);
        })
      }
      console.log('111111',this.arrOfSubModules)
  });
})
} 


getSubModulesById(moduleId){
  console.log(moduleId);
  var course_id=localStorage.getItem('courseIdforjoin');
  console.log('nnnnnnnnnnnooooo',localStorage.getItem('courseIdforjoin'));
 console.log('heyyyyyyyyyyyyyyyyyyyyyyy',this.module.module_id);
this.courseService.ModuleByParentModule(moduleId,course_id).subscribe((res:any)=>
{  if(res.length > 0){
 this.arrOfSubModules.push(res);
}
    console.log('888888888888888',res)
  
})
}


fillFormFieldNow(){


     this.basicGroup = this._formBuilder.group({
      basicCtrl: [{ value: this.module[0].module_name, disabled: true }, Validators.required],
      basicCtrl2: [{ value: this.module[0].module_type, disabled: true }, Validators.required],
    });
    this.basicGroup1 = this._formBuilder.group({
      basicCtrl1: [{ value: this.module[0].module_name, disabled: true }, Validators.required],
      basicCtrl21: [{ value: this.module[0].module_type, disabled: true }, Validators.required],
    });
    this.imgGroup = this._formBuilder.group({
      imgCtrl1: [{ value: this.module[0].heading, disabled: true }, Validators.required],
      imgCtrl2: [{ value: this.module[0].language, disabled: true }, Validators.required],

    });

    this.detailGroup = this._formBuilder.group({
      detailCtrl1: [{ value: this.module[0].category, disabled: true }, Validators.required],
      detailCtrl2: [{ value: this.module[0].is_education_content, disabled: true }, Validators.required],
      detailCtrl3: [{ value: this.module[0].status, disabled: true }, Validators.required]

    });

    this.courseGroup = this._formBuilder.group({
      courseCtrl1: [{ value: this.module[0].course, disabled: true }, Validators.required],
      courseCtrl2: [{ value: this.module[0].stream, disabled: true }, Validators.required],
      courseCtrl3: [{ value: this.module[0].class, disabled: true }, Validators.required],
      courseCtrl4: [{ value: this.module[0].sub_class, disabled: true }, Validators.required],
      courseCtrl5: [{ value: this.module[0].subject, disabled: true }, Validators.required],
      courseCtrl6: [{ value: this.module[0].chapter, disabled: true }, Validators.required],
      courseCtrl7: [{ value: this.module[0].concept, disabled: true }, Validators.required]

    });

    this.descGroup = this._formBuilder.group({
      descCtrl1: [{ value: this.module[0].module_description, disabled: true }]
    });

    this.extData = this._formBuilder.group({
      extDataCtrl1:['']
    });





    if(this.module.image_id){
      this.imageService.getImage(this.module[0].image_id).subscribe(
        img => this.modulePic = this.URLChange(img.image_storage_url)
      );
   }

   if(this.module.module_content_list){
      this.imageService.getContent(this.module[0].module_content_list[0]).subscribe(

        content=> {
          if(content.ext_link_data_list) {
            this.videoService.getExtVideo(content.ext_link_data_list[0]).subscribe(
              video_data=> {
                this.videoService.getYouTubeVideoData(video_data.video_id).subscribe(
                  video => this.video = video
                )
              }
            );
          }

          if (content.content_images_list) {
            this.module._albums = [];
            content.content_images_list.forEach(element => {
              this.imageService.getImage(element).subscribe(imgData => {
                console.log(imgData);
                const src = imgData.image_storage_url.replace("https", "http");
                const caption = imgData.image_name;
                const thumb = imgData.image_storage_url.replace(
                  "https",
                  "http"
                );
                
                const album = {
                  src: src,
                  caption: caption,
                  thumb: thumb
                  
                };
                
                this.module._albums.push(album);
                

              });
            });
          }  
        }
      );
   }
  }


 //  this.getSubModulesOfCourseModules();
 //  this.getChildModules();
 // }


//ngOnChanges(changes: SimpleChanges) {
 //   console.log(changes);
//    if (!changes.module.firstChange) this.ngOnInit();
//  }
//  arrOfSubModules=[];
  // getSubModulesOfCourseModules(){
 
//}


   

  URLChange(url: any) {
    return url.replace("https", "http");
  }


  open(index: number, album): void {
    this._subscription = this._lightboxEvent.lightboxEvent$.subscribe(
      (event: IEvent) => this._onReceivedEvent(event)
    );

    // override the default config
    this._lightbox.open(album, index, {
      wrapAround: true,
      showImageNumberLabel: true
    });
  }

  close(): void {
    // close lightbox programmatically
    this._lightbox.close();
  }

  private _onReceivedEvent(event: IEvent): void {
    if (event.id === LIGHTBOX_EVENT.CLOSE) {
      this._subscription.unsubscribe();
    }
  }

   getChildModules() {
    this.courseService.ModuleByParentModule(this.module[0].module_id,this.module[0].course_id).subscribe(
      modules=>{
      if(modules.length>0) 
         this.moduleModules = modules;
      else 
         this.moduleModules = undefined;
      }
    );
  }
  SubmoduleShow:boolean = false;
SubmoduleView(SubmoduleInfo){
    this.curSubModule = SubmoduleInfo;
    this.SubmoduleShow =true;
    console.log('SubmoduleInfo',SubmoduleInfo);
}

  
}

