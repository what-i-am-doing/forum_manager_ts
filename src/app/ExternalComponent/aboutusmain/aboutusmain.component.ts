import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-aboutusmain',
  templateUrl: './aboutusmain.component.html',
  styleUrls: ['./aboutusmain.component.css']
})
export class AboutusmainComponent implements OnInit {

  showContactUs:boolean=false;
  showContactSidebar:boolean=false;

  constructor( public router:Router) { }

  ngOnInit() {
    var herosec="assets/img/backdrop_our-story.jpg"
    var heart= "assets/img/simple-heart.png";
    var aboutus="assets/img/video_about-us_full.jpg";
    document.getElementById('herosec').style.backgroundImage = 'url('+herosec+')';
    document.getElementById('herosec').style.height = '660px';
    
    document.getElementById('video_about-us').style.backgroundImage = 'url('+aboutus+')';
    //document.getElementById('heart').style.backgroundImage = 'url('+heart+')';
    console.log(window.innerHeight);
  }


  
  navigateToPages(link:string) {
    console.log(link);
  
    if(link=='about-us'){
    
    
     this.router.navigateByUrl('/about-us');
    
    }
    else if(link=="login"){
    
    
      this.router.navigateByUrl('/login');
     
    }
    else if(link=="main"){
  
  
      this.router.navigateByUrl('/main');
     
    }
    else if(link=="product"){
    
      this.router.navigateByUrl('/product');
   
    
    }
    else if(link=="platform"){
    
    
      this.router.navigateByUrl('/platform')
      
    
    }
    else if(link=="company"){
    
    
      this.router.navigateByUrl('/company')
      
    
    }
    
  
    
    }
    showNavbar: boolean = false;

    // when menu button is clicked , it diplays left  menu bar
      menuClicked(showNavbar: boolean) {
    
        localStorage.setItem('leftmenubar', 'showNavbar');
    
        if (showNavbar == false) {
    
          this.showNavbar = true;
        }
        else if (showNavbar == true) {
          this.showNavbar = false;
        }
    
      }
    


    openNav() {
      this.showContactUs=false;
   
  
  
      document.getElementById("mySidebar").style.display="block";
      document.getElementById("mySidebar").style.width = "500px";
  
      document.getElementById("main").style.marginLeft = "0px";
     
    }
  
    closeNav() {
      this.showContactUs=true;
      
      document.getElementById("mySidebar").style.display= "none";
      document.getElementById("mySidebar").style.width = "0";
      document.getElementById("main").style.marginLeft= "0";
      console.log(" close nav  clicked");
    }
  

    myEventHandler(){

      document.getElementById("mySidebar").style.display= "none";
      this.showContactUs=true;
    }

    closemenuoptions(){

      this.showNavbar=false;
    }
  }