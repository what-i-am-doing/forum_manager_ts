import { Component, OnInit } from '@angular/core';
import { ViewdashboardComponent } from './viewdashboard/viewdashboard.component';
import { AdminComponent } from '../admin/admin/admin.component';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  entryComponents: [ViewdashboardComponent, AdminComponent]

})
export class DashboardComponent implements OnInit {


  constructor() {}
  public dynamicTabs: Array<Object>;
  

  ngOnInit() {
    console.log("DASHBOAR INIT");
    var user = JSON.parse(localStorage.getItem("UserAllData"));
    console.log(user);

    if (user.admin_mapping_list && user.admin_mapping_list[0] != "string") {

      this.dynamicTabs = [{
        label: 'dashboard',
        component: ViewdashboardComponent
      },
      {
        label: 'admin',
        component: AdminComponent
      }]
    }

    else
      this.dynamicTabs = [{
        label: 'dashboard',
        component: ViewdashboardComponent
      }]

  }


  ngOnDestroy(){
    console.log("DASHBOARD DESTROYED");
  }

}
