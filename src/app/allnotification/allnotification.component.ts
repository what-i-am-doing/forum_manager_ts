
import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { ImageService } from '../services/image.service';
import { UserService } from '../services/user.service';
import { NavBarComponent } from '../nav-bar/nav-bar.component';
import { MatMenuTrigger } from '@angular/material/menu';
import { ActivatedRoute, Router } from '@angular/router';
import { PostService } from '../services/post.service';
@Component({
  selector: 'app-allnotification',
  templateUrl: './allnotification.component.html',
  styleUrls: ['./allnotification.component.css'],
  providers:[PostService,ImageService,UserService]
})
export class AllnotificationComponent implements OnInit {
public sub:any;
public tempUserId:any;
public displayName:any;
public gender:any;
public profilePic:any;
public userGroupId:any;
public allPosts:any;
public Message:string
public postId:any;

  constructor(private route: ActivatedRoute,private userService:UserService,private postService:PostService, private imageService:ImageService) {


   }

  ngOnInit() {
    var test= JSON.parse(localStorage.getItem('notifdata'));
    console.log(" test local data");
    console.log(test);
    this.sub = this.route.queryParams.subscribe(params => {
     console.log(" notification page routed");
      console.log(params);
      this.tempUserId = test.userId;
      this.postId=test.postId
      this.getUserDetails(test.userId);
      this.getActiveUserPersonalDetails(test.userId);
    });


  }

  getUserDetails(userID) {
    this.userService.getUserDetails(userID).subscribe(userdata => {
      console.log(userdata);

      this.displayName = userdata.user_full_text_name;
      this.userGroupId = userdata.group_id[0];
      
      this.getUsersProfileImage(userdata.image_id);

      this.postService.getPostById(this.postId).subscribe(
        data => {
          console.log(data);
          this.allPosts = data;
          this.Message = data.length ? "data got" : "No data found";

        }
      );


    })


  }

 
  getUsersProfileImage(image_id){

    this.imageService.getImageByType(image_id,'thumbnail').subscribe(data=>{
      if(data.length>0){
      console.log("thumbnail found");
      console.log(data.image_storage_url);
    this.profilePic=this.URLChange(data.image_storage_url);
      }
      else{
        this.getProfileImage(image_id);
      }
    })
  }
  getProfileImage(image_id){
    this.imageService.getImage(image_id).subscribe(data=>{
      console.log("image found");
      console.log(data.image_storage_url);
this.profilePic=this.URLChange(data.image_storage_url);

    },err=>{
      console.log("image not found")
    switch(this.gender){
         case 'MALE':
         this.profilePic='assets/images/male.jpg'
         break;
         case 'FEMALE':
         this.profilePic='assets/images/female.jpg';
         break
 }
 
 })

  }
  
  
  URLChange(img: any) {
    console.log("convert image to http");
    console.log(img);
    return img.replace("https", "http");
  }






  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getActiveUserPersonalDetails(userId) {
    this.userService.getUserPersonalDetails(userId).subscribe(data => {
      this.gender = data.gender_type;

      console.log(data.gender_type);

      console.log("gender");
      console.log(this.gender);

    })

  }
}
