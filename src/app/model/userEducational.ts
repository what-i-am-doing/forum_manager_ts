export class UserEducational {

    constructor(){}
    
    // "class_id": "string",
    // "comments": "string",
    // "course_id": "string",
    // "creation_timestamp": 0,
    // "education_type_id": "string",
    // "institution_id": "string",
    // "last_updated_timestamp": 0,
    // "section_id": "string",
    // "status": "ACTIVE",
    // "stream_id": "string",
    
    // "user_id": "4xLrtGkBd-kimDmnyK1z"
    
    
    private user_id:string;
    private stream_id:string;
    private education_type_id:string;
    private institution_id:string;
    private course_id:string;
    
    private class_id:string;
    private section_id:string;
    private status:string;
    private comments:string; 
    private creation_timestamp:number; 
    
    /**
    * Getter $user_id
    * @return {string}
    */
    public get $user_id(): string {
    return this.user_id;
    }
    
    /**
    * Setter $user_id
    * @param {string} value
    */
    public set $user_id(value: string) {
    this.user_id = value;
    }
    
    /**
    * Getter $stream_id
    * @return {string}
    */
    public get $stream_id(): string {
    return this.stream_id;
    }
    
    /**
    * Setter $stream_id
    * @param {string} value
    */
    public set $stream_id(value: string) {
    this.stream_id = value;
    }
    
    /**
    * Getter $education_type_id
    * @return {string}
    */
    public get $education_type_id(): string {
    return this.education_type_id;
    }
    
    /**
    * Getter $institution_id
    * @return {string}
    */
    public get $institution_id(): string {
    return this.institution_id;
    }
    
    /**
    * Setter $institution_id
    * @param {string} value
    */
    public set $institution_id(value: string) {
    this.institution_id = value;
    }
    
    /**
    * Getter $course_id
    * @return {string}
    */
    public get $course_id(): string {
    return this.course_id;
    }
    
    /**
    * Setter $course_id
    * @param {string} value
    */
    public set $course_id(value: string) {
    this.course_id = value;
    }
    
    /**
    * Getter $class_id
    * @return {string}
    */
    public get $class_id(): string {
    return this.class_id;
    }
    
    /**
    * Setter $class_id
    * @param {string} value
    */
    public set $class_id(value: string) {
    this.class_id = value;
    }
    
    /**
    * Getter $section_id
    * @return {string}
    */
    public get $section_id(): string {
    return this.section_id;
    }
    
    /**
    * Setter $section_id
    * @param {string} value
    */
    public set $section_id(value: string) {
    this.section_id = value;
    }
    
    /**
    * Getter $stateId
    * @return {string}
    */
    public get $status(): string {
    return this.status;
    }
    
    /**
    * Setter $stateId
    * @param {string} value
    */
    public set $status(value: string) {
    this.status = value;
    }
    
    /**
    * Getter $comments
    * @return {string}
    */
    public get $comments(): string {
    return this.comments;
    }
    
    /**
    * Setter $comments
    * @param {string} value
    */
    public set $comments(value: string) {
    this.comments = value;
    }
    
    /**
    * Getter $creation_timestamp
    * @return {number}
    */
    public get $creation_timestamp(): number {
    return this.creation_timestamp;
    }
    
    /**
    * Setter $creation_timestamp
    * @param {number} value
    */
    public set $creation_timestamp(value: number) {
    this.creation_timestamp = value;
    }
    
    /**
    * Getter $last_updated_timestamp
    * @return {number}
    */
    public get $last_updated_timestamp(): number {
    return this.last_updated_timestamp;
    }
    
    /**
    * Setter $last_updated_timestamp
    * @param {number} value
    */
    public set $last_updated_timestamp(value: number) {
    this.last_updated_timestamp = value;
    }
    
    /**
    * Setter $education_type_id
    * @param {string} value
    */
    public set $education_type_id(value: string) {
    this.education_type_id = value;
    }
    private last_updated_timestamp:number;
    
    
    }