import {
  Component,
  OnInit,
  ElementRef,
  ViewContainerRef,
  ViewChild,
  ComponentRef,
  Input,
  Output,
  EventEmitter,
  AfterViewInit,
  OnChanges,
  ChangeDetectorRef,
  SimpleChanges
} from "@angular/core";
import { MatMenuTrigger } from "@angular/material/menu";
import { MatAutocomplete } from "@angular/material";
import { ChatService } from "../../services/chat.service";
import { ImageService } from "../../services/image.service";
import { ChatMessage } from "../../model/chat-message";
import { ChatGroup } from "../../model/chat-group";
import { timer, Subscription } from "rxjs";
import { Router } from "@angular/router";

@Component({
  selector: "chat-box",
  templateUrl: "./chat-box.component.html",
  styleUrls: ["./chat-box.component.css"],
  providers: []
})
export class ChatBoxComponent implements OnInit,AfterViewInit,OnChanges {
  @Input() user: Array<any> = []; // If already chatted it has chat_id field
  allUsers: Array<any> = [];
  @Input() friends: Array<any>;
  filteredFriends: Array<any> = [];
  myID: string = localStorage.getItem("user");
  @Output() removeUser = new EventEmitter<any>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild("chatBody", { read: ElementRef }) chatBody: ElementRef; 
  container: ViewContainerRef;
  @ViewChild("chatBox", { read: ElementRef }) chatBox: ElementRef;
  sub: Subscription;
  tagFound: boolean = false;
  beforePrevious: number = 0;
  previousKey: number = 0;
  currentTag: string;
  taggedUsers: Array<any> = [];
  total_users: Array<string> = [];
  messages: Array<any> = [];
  friendsMsges: Array<any> = [];
  chatID: any;
  chat_users_list: Array<string>;
  chatMessage: ChatMessage = new ChatMessage();
  start: number;
  end: number;
  userImage: string;
  ngOnInit() {
    console.log(this.friends);
    console.log(this.user);
    if(this.user[0].messages) 
    this.user[0].messages.sort((msg1,msg2) => msg1.creation_timestamp - msg2.creation_timestamp);
    this.allUsers = Array.isArray(this.user[0]) ? this.user[0] : this.user;
    if (Array.isArray(this.user[0])) {
      this.chatID = "chat_id" in this.user[0][0] ? this.user[0][0].chat_id : null;
    } else
      this.chatID = "chat_id" in this.user[0] ? this.user[0].chat_id : null;
    console.log(this.chatID);
    this.total_users = [this.userID, this.user[0].user_id];
    console.log(this.user[0]);
    console.log(this.user);
    console.log("chat_users_list" in this.user[0]);

    if (Array.isArray(this.user[0])) {
      this.chat_users_list =
        "chat_users_list" in this.user[0][0]
          ? this.user[0][0].chat_users_list
          : null;
    } else
      this.chat_users_list =
        "chat_users_list" in this.user[0] ? this.user[0].chat_users_list : null;

    console.log(this.chat_users_list);
   // if (this.chatID != null) this.getChatforBox();
    if (this.user[0].image_id) {
      this.imageService.getImage(this.user[0].image_id).subscribe(
        img => this.userImage = this.URLChange(img.image_storage_url)
      );
    }
    
  }

  ngAfterViewInit(){
    this.chatBody.nativeElement.scrollTop = this.chatBody.nativeElement.scrollHeight;
      
  }
  
  ngOnChanges(changes: SimpleChanges){
       console.log(changes);
      // alert("change detected");
  }

  ngOnDestroy() {
    if(this.sub) this.sub.unsubscribe();
  }

  createChatGroup: boolean = false;
  componentRef: ComponentRef<any>;
  tempchatGroupcomponentRef: ComponentRef<any>;
  userGroup: Array<string> = JSON.parse(localStorage.getItem("user_group"));
  groupID: string = localStorage.getItem("groupId");
  userID: string = localStorage.getItem("user");
  currentMsg: string = "";

  constructor(private chatService: ChatService, private imageService: ImageService, private router: Router) { }

  getSelectedTempfriendsforChatGroup(event) {
    console.log(event);
    event = event.filter(
      e => e.user_id != this.userID && e.user_id != this.user[0].user_id
    );
    event.forEach(element => {
      this.user.push(element);
      this.total_users.push(element.user_id);
    });

    console.log(this.user);
    this.messages = [];
    this.createChatGroup = !this.createChatGroup;
    var chatGroup = new ChatGroup();
    chatGroup.$chat_group_name = "temp-group";
    chatGroup.$chat_type_id = "GROUP_CHAT";
    chatGroup.$group_id = this.userGroup[0];
    chatGroup.$level_id = "1";
    chatGroup.$tag_id = "1";
    chatGroup.$chat_users_list = this.total_users;
    chatGroup.$created_by_user_id = this.userID;
    chatGroup.$creation_timestamp = +new Date();
    chatGroup.$status = "ACTIVE";

    this.chatService.createChatGroup(chatGroup).subscribe(data => {
      console.log(data);
      this.chatID = data.id;
      this.getChatAtInterval(data.id, +new Date(), +new Date() + Number(600000));
    });
  }

  submitChat() {
    if (!this.user[0].chat_id) return this.createGroup();
    this.preparechat();
    //this.currentMsg = "";

  }

  createGroup() {
    var chatGroup = new ChatGroup();
    chatGroup.$chat_group_name = this.user[0].user_id;
    chatGroup.$chat_type_id = "ONE_TO_ONE";
    chatGroup.$group_id = this.userGroup[0];
    chatGroup.$level_id = "1";
    chatGroup.$tag_id = "1";
    chatGroup.$chat_users_list = this.total_users;
    chatGroup.$created_by_user_id = this.userID;
    chatGroup.$creation_timestamp = +new Date();
    chatGroup.$status = "ACTIVE";
    this.chatService.createChatGroup(chatGroup).subscribe(data => {
      this.chatID = data.id;
      this.user[0].chat_id = data.id;
      this.preparechat();
      this.getChatAtInterval(data.id, +new Date(), +new Date() + Number(600000));
    });
  }

  preparechat() {
    var chatMessage: any = {};
    chatMessage.group_id = this.userGroup[0];
    chatMessage.chat_message = this.chatBox.nativeElement.innerHTML;
    if ((chatMessage.chat_message == "")) return;
    console.log(this.taggedUsers);
    if (this.taggedUsers.length) {
      chatMessage.tagged_user_id = [];
      var temp_tags = this.taggedUsers;
      this.taggedUsers = [];
      temp_tags.forEach(element => {
        if (
          chatMessage.chat_message.includes(
            Object.values(element)[0].toString()
          )
        ) {
          this.taggedUsers.push(Object.values(element)[1]);
        }
      });

      chatMessage.tagged_user_id = this.taggedUsers;
    }

    chatMessage.chat_users_list = this.chat_users_list
      ? this.chat_users_list
      : this.total_users;
    console.log(chatMessage.chat_users_list);
    // chatMessage.$chat_id = (this.chatID == null)?this.createGroup():this.chatID;
    chatMessage.chat_id = this.user[0].chat_id;
    chatMessage.creation_timestamp = +new Date();
    // this.user.forEach(element => {
    //   chatMessage.$chat_users_list.push(element.user_id);
    // });

    chatMessage.from_user_id = this.userID;
    chatMessage.status = "ACTIVE";
    console.log(chatMessage);
    this.chatService.submit121Chat(chatMessage).subscribe(data => {
      console.log(data);

      chatMessage.chat_message_id = data.id;
      var msg = Object.assign({}, chatMessage);
      this.user[0].messages.push(msg);
      console.log(this.chatBody.nativeElement.scrollHeight);
      setTimeout(()=>{ this.chatBody.nativeElement.scrollTop = this.chatBody.nativeElement.scrollHeight }, 1000);
      this.chatBox.nativeElement.innerHTML = "";
      console.log(chatMessage);
    });
  }
  getChatforBox() {
    this.chatService.get121Chat(this.groupID, this.chatID, 0, 10).subscribe(msg => {
      if (Object.keys(msg).length) {
        console.log(msg);
        var key = Object.keys(msg)[0];
        msg = msg[key];
        msg.sort(
          (msg1, msg2) => msg1.creation_timestamp - msg2.creation_timestamp
        );
        this.messages = msg;
        this.friendsMsges = msg.filter(element => element.from_user_id != this.myID);

      }

      this.getChatAtInterval(this.chatID, +new Date(), +new Date() + Number(600000));
    });
  }

  getChatAtInterval(chatID: string, start: number, end: number) {
    this.sub =
      this.chatService
        .get121ChatByTime(
          this.groupID,
          chatID,
          start,
          end
        )
        .subscribe(data => {
          console.log(data);
          if (Object.keys(data).length) {
            console.log(data);
            var key = Object.keys(data)[0];
            
            data[key].sort(
              (msg1, msg2) => msg2.creation_timestamp - msg1.creation_timestamp
            );
            
            var max = data[key][0].creation_timestamp + 1;
            this.getChatAtInterval(chatID, max , max + Number(600000));
            data[key].reverse().forEach(element => {
              console.log(element);
              if (element.from_user_id != this.myID) this.messages.push(element);
            });

          }

          else {
            this.getChatAtInterval(chatID, +new Date, +new Date() + Number(600000));
          }

        })

  }

  remove() {
    this.removeUser.emit(this.user);
  }

  triggerMenu() {
    this.trigger.openMenu();
  }

  searchFriend(event: any) {
    console.log(this.chatBox.nativeElement.innerHTML);
    if (event.keyCode == 13) {
      console.log("previous:"+ this.previousKey);
      if(this.previousKey == 16) return;

      else {
        event.preventDefault();
        this.chatBox.nativeElement.innerHTML = this.chatBox.nativeElement.innerHTML.replace(
          "<div><br></div>",
          ""
        );
        return this.submitChat();
      }
      
    }
    this.chatMessage.$chat_message = this.chatBox.nativeElement.innerHTML;
    var str = this.chatMessage.$chat_message;

    if (this.tagFound === true) {
      console.log("found");
      if (str.includes("@")) {
        var str = str.substring(str.lastIndexOf("@") + 1);
        this.currentTag = str;
        console.log(this.filteredFriends);
        this.filteredFriends = this.friends.filter(word =>
          word.user_full_text_name.startsWith(str)
        );
        console.log(this.filteredFriends);
      } else this.tagFound = false;
    } else if (
      (this.chatMessage.$chat_message == "@" ||
        this.beforePrevious == 32 ||
        this.beforePrevious == 8) &&
      (event.key == 2 && this.previousKey == 16)
    ) {
      this.tagFound = true;
      this.start = str.length - 1;
      if (this.friends.length > 6) this.filteredFriends = this.friends.slice(0, 6);
      else this.filteredFriends = this.friends;
      this.currentTag = str.substring(str.lastIndexOf("@") + 1);
    }

    this.beforePrevious = this.previousKey;
    this.previousKey = event.keyCode;
  }

  // *this method need to be fixed
  selectFriend(friend: any) {
    var comment = this.chatBox.nativeElement.innerHTML;
    this.end = comment.length;
    this.chatBox.nativeElement.innerHTML = this.replaceBetween(this.start, this.end, "<a contenteditable='false'>@" + friend.user_full_text_name + "</a> &nbsp;", comment);

    // this.chatBox.nativeElement.innerHTML = comment.replace(
    //   "@" + this.currentTag,
    //   "<a contenteditable='false'>@" + friend.user_full_text_name + "</a>"
    // );
    this.tagFound = false;
    this.taggedUsers.push({
      content:
        '<a contenteditable="false">@' + friend.user_full_text_name + "</a>",
      id: friend.user_id
    });
    console.log(this.chatBox.nativeElement.innerHTML);
    console.log(this.taggedUsers[0].content);

    console.log(this.chatBox.nativeElement.innerHTML.includes(this.taggedUsers[0].content));
    this.tagFound = false;
  }
  seeFriendProfile(userId) {
    this.router.navigate(['/home/userprofile'], { queryParams: { userId: userId } });
  }

  getMsg(event: any) {
    console.log(event);
    this.chatBox.nativeElement.innerHTML = "<div style='border-radius:15px;width:80%;padding:3px;color:#fff;background-color:#5299ca'>" + "\"" + event.chat_message + "\"" + "</div><div></div>"
  }

  replaceBetween = function (start, end, what, string) {
    return string.substring(0, start) + what + string.substring(end);
  };

  URLChange(img: any) {
    return img.replace("https", "http");
  }
}
