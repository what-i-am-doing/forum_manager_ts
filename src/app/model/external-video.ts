export class ExternalData {
  constructor(){}

 private caption: string;
 private category: string;
 private category_id: string;
 private category_list:string[];
 private category_name: string;
 private chapter: string;
 private class: string;
 private comments: string;
 private comments_counter: number;
 private concept: string;
 private content_upload_date: string;
 private content_uploaded_by: string;
 private course: string;
 private creation_timestamp: number;
 private data_source: string;
 private data_type: string;
 private default_audio_language: string;
 private default_thumbnail_url: string;
 private definition: string;
 private description: string;
 private details: string;
 private duration: string;
 private education_level: string;
 private enabled_for_groups: string[];
 private ext_video_data_id: string;
 private highRes_thumbnail_url: string;
 private high_thumbnail_url: string;
 private img_id: string;
 private is_education_content: true;
 private language: string;
 private last_updated_timestamp: number;
 private license: string;
 private licensed_content: true;
 private likes_counter: number;
 private medium_thumbnail_url: string;
 private privacy_status: string;
 private relevance: number;
 private reports_counter: number;
 private shared_by_user_id: string;
 private shares_counter: number;
 private standard_thumbnail_url: string;
 private status: string;
 private stream: string;
 private sub_class: string;
 private subject: string;
 private syllabus_entry_sequence: number;
 private tags: string[];
 private title: string;
 private video_id: string;
 private video_url: string;
 private views_counter: number;


    /**
     * Getter $caption
     * @return {string}
     */
	public get $caption(): string {
		return this.caption;
	}

    /**
     * Getter $category
     * @return {string}
     */
	public get $category(): string {
		return this.category;
	}

    /**
     * Getter $category_id
     * @return {string}
     */
	public get $category_id(): string {
		return this.category_id;
	}

    /**
     * Getter $category_list
     * @return {string[]}
     */
	public get $category_list(): string[] {
		return this.category_list;
	}

    /**
     * Getter $category_name
     * @return {string}
     */
	public get $category_name(): string {
		return this.category_name;
	}

    /**
     * Getter $chapter
     * @return {string}
     */
	public get $chapter(): string {
		return this.chapter;
	}

    /**
     * Getter $class
     * @return {string}
     */
	public get $class(): string {
		return this.class;
	}

    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $comments_counter
     * @return {number}
     */
	public get $comments_counter(): number {
		return this.comments_counter;
	}

    /**
     * Getter $concept
     * @return {string}
     */
	public get $concept(): string {
		return this.concept;
	}

    /**
     * Getter $content_upload_date
     * @return {string}
     */
	public get $content_upload_date(): string {
		return this.content_upload_date;
	}

    /**
     * Getter $content_uploaded_by
     * @return {string}
     */
	public get $content_uploaded_by(): string {
		return this.content_uploaded_by;
	}

    /**
     * Getter $course
     * @return {string}
     */
	public get $course(): string {
		return this.course;
	}

    /**
     * Getter $creation_timestamp
     * @return {number}
     */
	public get $creation_timestamp(): number {
		return this.creation_timestamp;
	}

    /**
     * Getter $data_source
     * @return {string}
     */
	public get $data_source(): string {
		return this.data_source;
	}

    /**
     * Getter $data_type
     * @return {string}
     */
	public get $data_type(): string {
		return this.data_type;
	}

    /**
     * Getter $default_audio_language
     * @return {string}
     */
	public get $default_audio_language(): string {
		return this.default_audio_language;
	}

    /**
     * Getter $default_thumbnail_url
     * @return {string}
     */
	public get $default_thumbnail_url(): string {
		return this.default_thumbnail_url;
	}

    /**
     * Getter $definition
     * @return {string}
     */
	public get $definition(): string {
		return this.definition;
	}

    /**
     * Getter $description
     * @return {string}
     */
	public get $description(): string {
		return this.description;
	}

    /**
     * Getter $details
     * @return {string}
     */
	public get $details(): string {
		return this.details;
	}

    /**
     * Getter $duration
     * @return {string}
     */
	public get $duration(): string {
		return this.duration;
	}

    /**
     * Getter $education_level
     * @return {string}
     */
	public get $education_level(): string {
		return this.education_level;
	}

    /**
     * Getter $enabled_for_groups
     * @return {string[]}
     */
	public get $enabled_for_groups(): string[] {
		return this.enabled_for_groups;
	}

    /**
     * Getter $ext_video_data_id
     * @return {string}
     */
	public get $ext_video_data_id(): string {
		return this.ext_video_data_id;
	}

    /**
     * Getter $highRes_thumbnail_url
     * @return {string}
     */
	public get $highRes_thumbnail_url(): string {
		return this.highRes_thumbnail_url;
	}

    /**
     * Getter $high_thumbnail_url
     * @return {string}
     */
	public get $high_thumbnail_url(): string {
		return this.high_thumbnail_url;
	}

    /**
     * Getter $img_id
     * @return {string}
     */
	public get $img_id(): string {
		return this.img_id;
	}

    /**
     * Getter $is_education_content
     * @return {true}
     */
	public get $is_education_content(): true {
		return this.is_education_content;
	}

    /**
     * Getter $language
     * @return {string}
     */
	public get $language(): string {
		return this.language;
	}

    /**
     * Getter $last_updated_timestamp
     * @return {number}
     */
	public get $last_updated_timestamp(): number {
		return this.last_updated_timestamp;
	}

    /**
     * Getter $license
     * @return {string}
     */
	public get $license(): string {
		return this.license;
	}

    /**
     * Getter $licensed_content
     * @return {true}
     */
	public get $licensed_content(): true {
		return this.licensed_content;
	}

    /**
     * Getter $likes_counter
     * @return {number}
     */
	public get $likes_counter(): number {
		return this.likes_counter;
	}

    /**
     * Getter $medium_thumbnail_url
     * @return {string}
     */
	public get $medium_thumbnail_url(): string {
		return this.medium_thumbnail_url;
	}

    /**
     * Getter $privacy_status
     * @return {string}
     */
	public get $privacy_status(): string {
		return this.privacy_status;
	}

    /**
     * Getter $relevance
     * @return {number}
     */
	public get $relevance(): number {
		return this.relevance;
	}

    /**
     * Getter $reports_counter
     * @return {number}
     */
	public get $reports_counter(): number {
		return this.reports_counter;
	}

    /**
     * Getter $shared_by_user_id
     * @return {string}
     */
	public get $shared_by_user_id(): string {
		return this.shared_by_user_id;
	}

    /**
     * Getter $shares_counter
     * @return {number}
     */
	public get $shares_counter(): number {
		return this.shares_counter;
	}

    /**
     * Getter $standard_thumbnail_url
     * @return {string}
     */
	public get $standard_thumbnail_url(): string {
		return this.standard_thumbnail_url;
	}

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Getter $stream
     * @return {string}
     */
	public get $stream(): string {
		return this.stream;
	}

    /**
     * Getter $sub_class
     * @return {string}
     */
	public get $sub_class(): string {
		return this.sub_class;
	}

    /**
     * Getter $subject
     * @return {string}
     */
	public get $subject(): string {
		return this.subject;
	}

    /**
     * Getter $syllabus_entry_sequence
     * @return {number}
     */
	public get $syllabus_entry_sequence(): number {
		return this.syllabus_entry_sequence;
	}

    /**
     * Getter $tags
     * @return {string[]}
     */
	public get $tags(): string[] {
		return this.tags;
	}

    /**
     * Getter $title
     * @return {string}
     */
	public get $title(): string {
		return this.title;
	}

    /**
     * Getter $video_id
     * @return {string}
     */
	public get $video_id(): string {
		return this.video_id;
	}

    /**
     * Getter $video_url
     * @return {string}
     */
	public get $video_url(): string {
		return this.video_url;
	}

    /**
     * Getter $views_counter
     * @return {number}
     */
	public get $views_counter(): number {
		return this.views_counter;
	}

    /**
     * Setter $caption
     * @param {string} value
     */
	public set $caption(value: string) {
		this.caption = value;
	}

    /**
     * Setter $category
     * @param {string} value
     */
	public set $category(value: string) {
		this.category = value;
	}

    /**
     * Setter $category_id
     * @param {string} value
     */
	public set $category_id(value: string) {
		this.category_id = value;
	}

    /**
     * Setter $category_list
     * @param {string[]} value
     */
	public set $category_list(value: string[]) {
		this.category_list = value;
	}

    /**
     * Setter $category_name
     * @param {string} value
     */
	public set $category_name(value: string) {
		this.category_name = value;
	}

    /**
     * Setter $chapter
     * @param {string} value
     */
	public set $chapter(value: string) {
		this.chapter = value;
	}

    /**
     * Setter $class
     * @param {string} value
     */
	public set $class(value: string) {
		this.class = value;
	}

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $comments_counter
     * @param {number} value
     */
	public set $comments_counter(value: number) {
		this.comments_counter = value;
	}

    /**
     * Setter $concept
     * @param {string} value
     */
	public set $concept(value: string) {
		this.concept = value;
	}

    /**
     * Setter $content_upload_date
     * @param {string} value
     */
	public set $content_upload_date(value: string) {
		this.content_upload_date = value;
	}

    /**
     * Setter $content_uploaded_by
     * @param {string} value
     */
	public set $content_uploaded_by(value: string) {
		this.content_uploaded_by = value;
	}

    /**
     * Setter $course
     * @param {string} value
     */
	public set $course(value: string) {
		this.course = value;
	}

    /**
     * Setter $creation_timestamp
     * @param {number} value
     */
	public set $creation_timestamp(value: number) {
		this.creation_timestamp = value;
	}

    /**
     * Setter $data_source
     * @param {string} value
     */
	public set $data_source(value: string) {
		this.data_source = value;
	}

    /**
     * Setter $data_type
     * @param {string} value
     */
	public set $data_type(value: string) {
		this.data_type = value;
	}

    /**
     * Setter $default_audio_language
     * @param {string} value
     */
	public set $default_audio_language(value: string) {
		this.default_audio_language = value;
	}

    /**
     * Setter $default_thumbnail_url
     * @param {string} value
     */
	public set $default_thumbnail_url(value: string) {
		this.default_thumbnail_url = value;
	}

    /**
     * Setter $definition
     * @param {string} value
     */
	public set $definition(value: string) {
		this.definition = value;
	}

    /**
     * Setter $description
     * @param {string} value
     */
	public set $description(value: string) {
		this.description = value;
	}

    /**
     * Setter $details
     * @param {string} value
     */
	public set $details(value: string) {
		this.details = value;
	}

    /**
     * Setter $duration
     * @param {string} value
     */
	public set $duration(value: string) {
		this.duration = value;
	}

    /**
     * Setter $education_level
     * @param {string} value
     */
	public set $education_level(value: string) {
		this.education_level = value;
	}

    /**
     * Setter $enabled_for_groups
     * @param {string[]} value
     */
	public set $enabled_for_groups(value: string[]) {
		this.enabled_for_groups = value;
	}

    /**
     * Setter $ext_video_data_id
     * @param {string} value
     */
	public set $ext_video_data_id(value: string) {
		this.ext_video_data_id = value;
	}

    /**
     * Setter $highRes_thumbnail_url
     * @param {string} value
     */
	public set $highRes_thumbnail_url(value: string) {
		this.highRes_thumbnail_url = value;
	}

    /**
     * Setter $high_thumbnail_url
     * @param {string} value
     */
	public set $high_thumbnail_url(value: string) {
		this.high_thumbnail_url = value;
	}

    /**
     * Setter $img_id
     * @param {string} value
     */
	public set $img_id(value: string) {
		this.img_id = value;
	}

    /**
     * Setter $is_education_content
     * @param {true} value
     */
	public set $is_education_content(value: true) {
		this.is_education_content = value;
	}

    /**
     * Setter $language
     * @param {string} value
     */
	public set $language(value: string) {
		this.language = value;
	}

    /**
     * Setter $last_updated_timestamp
     * @param {number} value
     */
	public set $last_updated_timestamp(value: number) {
		this.last_updated_timestamp = value;
	}

    /**
     * Setter $license
     * @param {string} value
     */
	public set $license(value: string) {
		this.license = value;
	}

    /**
     * Setter $licensed_content
     * @param {true} value
     */
	public set $licensed_content(value: true) {
		this.licensed_content = value;
	}

    /**
     * Setter $likes_counter
     * @param {number} value
     */
	public set $likes_counter(value: number) {
		this.likes_counter = value;
	}

    /**
     * Setter $medium_thumbnail_url
     * @param {string} value
     */
	public set $medium_thumbnail_url(value: string) {
		this.medium_thumbnail_url = value;
	}

    /**
     * Setter $privacy_status
     * @param {string} value
     */
	public set $privacy_status(value: string) {
		this.privacy_status = value;
	}

    /**
     * Setter $relevance
     * @param {number} value
     */
	public set $relevance(value: number) {
		this.relevance = value;
	}

    /**
     * Setter $reports_counter
     * @param {number} value
     */
	public set $reports_counter(value: number) {
		this.reports_counter = value;
	}

    /**
     * Setter $shared_by_user_id
     * @param {string} value
     */
	public set $shared_by_user_id(value: string) {
		this.shared_by_user_id = value;
	}

    /**
     * Setter $shares_counter
     * @param {number} value
     */
	public set $shares_counter(value: number) {
		this.shares_counter = value;
	}

    /**
     * Setter $standard_thumbnail_url
     * @param {string} value
     */
	public set $standard_thumbnail_url(value: string) {
		this.standard_thumbnail_url = value;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}

    /**
     * Setter $stream
     * @param {string} value
     */
	public set $stream(value: string) {
		this.stream = value;
	}

    /**
     * Setter $sub_class
     * @param {string} value
     */
	public set $sub_class(value: string) {
		this.sub_class = value;
	}

    /**
     * Setter $subject
     * @param {string} value
     */
	public set $subject(value: string) {
		this.subject = value;
	}

    /**
     * Setter $syllabus_entry_sequence
     * @param {number} value
     */
	public set $syllabus_entry_sequence(value: number) {
		this.syllabus_entry_sequence = value;
	}

    /**
     * Setter $tags
     * @param {string[]} value
     */
	public set $tags(value: string[]) {
		this.tags = value;
	}

    /**
     * Setter $title
     * @param {string} value
     */
	public set $title(value: string) {
		this.title = value;
	}

    /**
     * Setter $video_id
     * @param {string} value
     */
	public set $video_id(value: string) {
		this.video_id = value;
	}

    /**
     * Setter $video_url
     * @param {string} value
     */
	public set $video_url(value: string) {
		this.video_url = value;
	}

    /**
     * Setter $views_counter
     * @param {number} value
     */
	public set $views_counter(value: number) {
		this.views_counter = value;
	}

    
}