import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ChatService } from '../../services/chat.service';
import { UserService } from '../../services/user.service';
import { timer, Subscription } from 'rxjs';

@Component({
  selector: 'app-chat-group-list',
  templateUrl: './chat-group-list.component.html',
  styleUrls: ['./chat-group-list.component.css']
})
export class ChatGroupListComponent implements OnInit {

  @Input() chatGroup:any;
  @Output() openBox = new EventEmitter<any>();
  messages:Array<any> = [];
  sub:Subscription;
  friendDetail:Array<any> = [];
  groupID:string = localStorage.getItem("groupId");

  constructor(private chatService:ChatService, private userService:UserService) { }

  ngOnInit() {
    console.log(this.chatGroup);
    this.getChatforBox();
    this.chatGroup.chat_users_list.forEach(element => {
        this.userService.getUserDetails(element).subscribe(
          user=>{
            console.log(user);
            user.chat_id = this.chatGroup.chat_id;
            user.chat_users_list = this.chatGroup.chat_users_list;
            this.friendDetail.push(user);
          }
        );    

    });
  }

  ngOnDestroy() {
   if(this.sub) this.sub.unsubscribe()
  }
  
  getChatforBox() {
    this.chatService.get121Chat(this.groupID,this.chatGroup.chat_id, 0, 10).subscribe(msg => {

      if (Object.keys(msg).length) {
        console.log(msg);
        var key = Object.keys(msg)[0];
        this.messages = msg[key];
      }

      //this.getChatAtInterval(this.chatGroup.chat_id);

    });
  }

  getChatAtInterval(chatID:string){
   this.sub = timer(0, 3000).subscribe(() => this.chatService.get121ChatByTime(this.groupID,chatID, +new Date()- Number(3000), + new Date()).subscribe(
      data => {
        
        console.log(data)
        if (Object.keys(data).length) {
          console.log(data);
          var key = Object.keys(data)[0];
          data[key].forEach(element => {
            console.log(element);
            this.messages.push(element);
          });

        }
    }))
  }
  
  createBox() {
    this.openBox.emit(this.friendDetail);
  }

}
