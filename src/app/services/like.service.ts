import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry ,delay } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LikeService {

  

  constructor(private http: HttpClient) { }
  api_url = environment.api_url;

  increaseLike(postID:string,body:any):Observable<any> {
          return this.http.put<any>(this.api_url+'/post/'+postID,body,{headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept':        'application/json',
            'from_user_id':  localStorage.getItem('user')
            
          })
        }).pipe(

            retry(1),
            catchError(this.handleError)
        );
        }
        increaseForumLike(forumpostID:string,body:any):Observable<any> {
          return this.http.put<any>(this.api_url+'/forum_post/'+forumpostID,body,{headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept':        'application/json',
            'from_user_id':  localStorage.getItem('user')
            
          })
        }).pipe(

            retry(1),
            catchError(this.handleError)
        );
        }

  likeComment(commentId:string , body):Observable<any> {
    return this.http.put<any>(this.api_url+'/comment/'+commentId,body,{headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  }).pipe(

      retry(1),
      catchError(this.handleError)
  );
  }

  isLikedTrue(user:string,commentId:string):Observable<any>{

    return this.http.get<any>(this.api_url+'/event/search_by_user/by_entity/by_event?group_id=public&user_id='+user+'&entity_type=COMMENT&entity_id='+commentId+'&event_type=like&size=3',{headers: new HttpHeaders({

     'Accept':        'application/json',
     'from_user_id':  localStorage.getItem('user')
     
   })}).pipe(

     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );

}
  

  checkIfAlreadyLiked(fourmpostID:string, user_id:string){
                 
         }      

        private handleError(error: HttpErrorResponse) {
          if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
          } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
              `Backend returned code ${error.status}, ` +
              `body was: ${error.error}`);
          }
          // return an observable with a user-facing error message
          return throwError(
            'Something bad happened; please try again later.');
        }      
}
