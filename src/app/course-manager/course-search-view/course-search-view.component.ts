import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ImageService } from "../../services/image.service";
import { VideoService } from "../../services/video.service";
import { Lightbox, LightboxConfig, LightboxEvent, LIGHTBOX_EVENT, IEvent } from "ngx-lightbox";
import { Subscription } from 'rxjs';

import { User } from "../../model/user";
import { CourseService } from "../../services/course.service";
import { UserService } from  "../../services/user.service";
import { Route, Router, ActivatedRoute } from '@angular/router';

import {NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import { getMatIconFailedToSanitizeLiteralError } from '@angular/material';


interface CourseNode {
  id:string;
  name: string;
  course?:any;
  module?:any;
  children?: CourseNode[];
}


@Component({
  selector: 'app-course-search-view',
  templateUrl: './course-search-view.component.html',
  styleUrls: ['./course-search-view.component.css']
})
export class CourseSearchViewComponent implements OnInit {

  TREE_DATA: CourseNode[] = [];
  treeControl = new NestedTreeControl<CourseNode>(node => node.children);
  dataSource = new MatTreeNestedDataSource<CourseNode>();

  constructor(private _formBuilder: FormBuilder, private imageService: ImageService,
    private videoService: VideoService,
    private courseService: CourseService,
    private userService: UserService,
    private _lightbox: Lightbox,
    private _lightboxEvent: LightboxEvent,
    private _lighboxConfig: LightboxConfig,private router:Router,private route:ActivatedRoute) { }
    basicGroup: FormGroup;
    imgGroup: FormGroup;
    detailGroup: FormGroup;
    courseGroup: FormGroup;
    descGroup: FormGroup;
    extData: FormGroup;
    private _subscription: Subscription;
    coJoined: boolean = false;
    join: boolean = false;
    coursePic: string;
    video: any;
    showExt: boolean = false;
    courseModules: any;
    activeLink: boolean = false;
    curModule: any;
    curSubModule: any;
    groupId: string = localStorage.getItem("groupId");
    searchQuery:string;
    course_list:any=[];
  course: any;
  idPassedFromParams;
  path:any = [];
  userId:any;

  ngOnInit() {
    this.courseModules = undefined;
    this.curModule = undefined;
    this.video = undefined;
    this.coursePic = undefined;

        this.route.queryParams.subscribe((obj:any)=>{
      console.log('@@@courses',obj);
      this.idPassedFromParams  = obj.courses;
    });

    this.getCourseDetailCall().then((obj:any)=>{
    this.fillFormFieldNow();
    this.getUserDetailsForJoinedCourses(localStorage.getItem('user'));
    console.log(this.descGroup,'!!!!!!!!!');
    }
    );
    // this.getCourses();
    
  }



  userAlreadyHavejoinedCurses;
  getUserDetailsForJoinedCourses(userId){
    console.log('0000001')
    this.userService.getUserDetails(userId).subscribe((res:any)=>{
      console.log('000000',res)
      this.userAlreadyHavejoinedCurses = res.courses_list;
    })
  }

  getCourses() {
    this.courseService.searchCourseByText( this.course.course_name,this.groupId,0,20).subscribe(
      courses => {
        console.log(courses,'JJJJJJJJJJJJJJJJJJJ', this.course.course_name,this.groupId);
       
        // this.userCourses = courses;
        courses.forEach((element) => {
          // this.TREE_DATA.push({id:element.course_id,name:element.course_name,course:element});
          console.log('________====',this.course.course_id,element.course_id)

          
            this.moduleListCall(element.course_id);
          
        });
         
        // this.dataSource.data = this.TREE_DATA;
        // this.showCourse = courses[0];
      }
    );
    console.log('&&&&arrrrrrrrrrr',this.arrOfModules);
  }
  arrOfModules=[];
  
  moduleListCall(course_id){
    console.log('Called....',course_id);
    this.courseService.ModuleByCourse(course_id).subscribe((res:any)=>{
      console.log(res,'pppppppppppp')
      if(res.length > 0){
        this.arrOfModules.push(res);
  
      }
    });
  }
 

  
  getCourseDetailCall(){
return new Promise((resolve,reject)=>{

  this.courseService.getCourseById(this.idPassedFromParams).subscribe((res:any)=>{
    console.log('resssssssssssssssss',res);
  
    // this.course = {};
    this.course = res;
    console.log('reesssssssssssssssssssss2',this.course);
    resolve(this.course)
    localStorage.setItem('courseIdforjoin',this.idPassedFromParams);

    let moduleIdList= res.course_modules_list;
    console.log('aaaaaaaaaaaaa',moduleIdList)

    moduleIdList.forEach((i:any)=>{
      this.getModulesById(i);
    })
    console.log('111111',this.arrOfModules)
    
});
})
}


  
  getModulesById(module_id){
    console.log('Called....',module_id);
    this.courseService.getModulesByIds(module_id).subscribe((res:any)=>{
      console.log(res,'pppppppppppp')
      if(res.length > 0){
        this.arrOfModules.push(res);
      }
    });
  }


  joinCourse(){
    var alreadyJoin:boolean= false;
    var currentCourseId = localStorage.getItem('courseIdforjoin');
    console.log('JoinedCourseNow',this.course.course_id,currentCourseId)
    if(this.course && this.course.course_id){
      for(let i=0 ; i < this.userAlreadyHavejoinedCurses.length;i++){
        if(this.course.course_id == this.userAlreadyHavejoinedCurses[i]){
          alreadyJoin = true;
        } 
        else{
          alreadyJoin =false;
        }
      }

    if(alreadyJoin == false){
             //api call
             this.userAlreadyHavejoinedCurses.push(localStorage.getItem('courseIdforjoin'));
             var user_detail = {
              "courses_list":this.userAlreadyHavejoinedCurses,
            }
    this.userId=localStorage.getItem('user');
    this.userService.updateUsercourseDetail(user_detail,this.userId).subscribe((res:any)=>{
      console.log('@@@@@@',res);
      // this.coJoined = true;
      alert('User Successfully Joined that course')
      // this.course = {};
      });
      }
    else{
             alert('User Already Join that course')
      }
    }


    // this.course_list= localStorage.getItem('courseIdforjoin');

     
  }


  fillFormFieldNow(){


    this.basicGroup = this._formBuilder.group({
      basicCtrl: [{ value: this.course.course_name, disabled: true }, Validators.required],
      basicCtrl2: [{ value: this.course.course_type_id, disabled: true }, Validators.required],
    });

    this.imgGroup = this._formBuilder.group({
      imgCtrl1: [{ value: this.course.heading, disabled: true }, Validators.required],
      imgCtrl2: [{ value: this.course.status, disabled: true }, Validators.required],

    });

    this.detailGroup = this._formBuilder.group({
      detailCtrl1: [{ value: this.course.category, disabled: true }, Validators.required],
      detailCtrl2: [{ value: this.course.is_education_content, disabled: true }, Validators.required],
      detailCtrl3: [{ value: this.course.language, disabled: true }, Validators.required]

    });

    this.courseGroup = this._formBuilder.group({
      courseCtrl1: [{ value: this.course.course, disabled: true }, Validators.required],
      courseCtrl2: [{ value: this.course.stream, disabled: true }, Validators.required],
      courseCtrl3: [{ value: this.course.class, disabled: true }, Validators.required],
      courseCtrl4: [{ value: this.course.sub_class, disabled: true }, Validators.required],
      courseCtrl5: [{ value: this.course.subject, disabled: true }, Validators.required],
      courseCtrl6: [{ value: this.course.chapter, disabled: true }, Validators.required],
      courseCtrl7: [{ value: this.course.concept, disabled: true }, Validators.required]

    });

    this.descGroup = this._formBuilder.group({
      descCtrl1: [{ value: this.course.course_description, disabled: true }]
      
    });

    this.extData = this._formBuilder.group({
      extDataCtrl1: ['']
    });

    if (this.course.image_id) {
      this.imageService.getImage(this.course.image_id).subscribe(
        img => this.coursePic = this.URLChange(img.image_storage_url)
      );
    }
    if (this.course.course_content_list) {
      this.imageService.getContent(this.course.course_content_list[0]).subscribe(

        content => {
          if (content.ext_link_data_list) {
            this.videoService.getExtVideo(content.ext_link_data_list[0]).subscribe(
              video_data => {
                this.videoService.getYouTubeVideoData(video_data.video_id).subscribe(
                  video => this.video = video
                )
              }
            );
          }

          if (content.content_images_list) {
            this.course._albums = [];
            content.content_images_list.forEach(element => {
              this.imageService.getImage(element).subscribe(imgData => {
                console.log(imgData);
                const src = imgData.image_storage_url.replace("https", "http");
                const caption = imgData.image_name;
                const thumb = imgData.image_storage_url.replace(
                  "https",
                  "http"
                );

                const album = {
                  src: src,
                  caption: caption,
                  thumb: thumb

                };

                this.course._albums.push(album);


              });
            });
          }
        }
      );
    }
  }
  


  // ngOnChanges(changes: SimpleChanges) {
  //   console.log(changes);
  //   if (!changes.course.firstChange) this.ngOnInit();
  // }


  URLChange(url: any) {
    return url.replace("https", "http");
  }

  modulesOfCourse() {
    console.log('aaaaaaaaaaaa',this.course.course_id);
    this.courseService.ModuleByCourse(this.course.course_id).subscribe(
      modules => {
        console.log('aaaaaaaaaaaa',modules);
        this.courseModules = modules;
        this.path.push({"id":this.course.course_id,"path_name":this.course.course_name});
      }
    
    );
    

  // else {
  //   this.courseService.ModuleByParentModule(this.curModule.module_id,this.course.course_id).subscribe(
  //     modules=>{
  //      this.courseModules = modules;
  //     }
  //   );
  // }
}

openModulesofModules(module:any) {
  this.courseService.ModuleByParentModule(module.module_id,this.course.course_id).subscribe(
    modules=>{
     this.courseModules = modules;
    }
  );
}

showChildModules(event:any) {
   this.courseModules = event[0];
   this.path.push(event[1]);
   this.curModule = undefined;
   
}

moduleShow:boolean = false;
currentModule:string;
  moduleView(moduleInfo){
    this.curModule = moduleInfo;
    this.moduleShow =true;
    this.currentModule  =  moduleInfo.module_name;
    console.log('moduleInfo',moduleInfo);

    this.fromIdFetchSubmoduleName(moduleInfo.module_id); 
  }

  arrOfSubModules=[];
  fromIdFetchSubmoduleName(moduleId){
    this.arrOfSubModules = []; //A.splice(0,A.length)
    console.log('fffffffff',moduleId)
    var course_id=localStorage.getItem('courseIdforjoin');
    console.log('ggggggggggg',course_id);
    this.courseService.ModuleByParentModule(moduleId,course_id).subscribe((b:any)=>
    { 
      console.log('jjjjjjjjjjjjjjj',b);
       if(b.length > 0){
      this.arrOfSubModules.push(b);
    }
        console.log('111111111111111',this.arrOfSubModules)
    
  });
}
SubmoduleShow:boolean = false;
SubmoduleView(SubmoduleInfo){
  this.curModule = SubmoduleInfo;
    this.SubmoduleShow =true;
    console.log('SubmoduleInfo',SubmoduleInfo);
}

}

