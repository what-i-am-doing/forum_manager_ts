import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse ,HttpEventType } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { map } from  'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class VideoService {

  private api_url = environment.api_url;

  constructor(private http: HttpClient) { }

  public uploadVideo(video: any) {
    console.log(video); 
      var formData = new FormData();
      formData.append('videoMultipartFile', video);
      return this.http.post(this.api_url+'/video/upload', formData,{headers: new HttpHeaders({

        'Accept':        'application/json',
        'user_id':  localStorage.getItem('user')
        
      }),
      reportProgress: true,
      observe: 'events'
    }
    
    ).pipe(map((event) => {

      switch (event.type) {

        case HttpEventType.UploadProgress:
          const progress = Math.round(100 * event.loaded / event.total);
          return { status: 'progress', message: progress };

        case HttpEventType.Response:
          return event.body;
        default:
          return `Unhandled event: ${event.type}`;
      }
    })
    );
    }
  
   getVideo(videoID:string):Observable<any>{
             
    return this.http.get<any>(this.api_url+'/video/'+videoID,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }


   getVideobySearchText(searchQuery,groupId,time_start,time_end,from,to):Observable<any>{
        
    return this.http.get<any>(this.api_url+'/ext_video_data/search?group_id='+groupId+'&search_text='+searchQuery+'&time_start='+time_start+'&time_end='+time_end+'&from='+from+'&size='+to,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }
   
   getextVideobyId(extVideoId):Observable<any>{
        
    return this.http.get<any>(this.api_url+'/ext_video_data/search_by_id?ext_link_data_id='+extVideoId,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }

   addExtVideo(video_data:any):Observable<any> {
    return this.http.post<any>(this.api_url+'/ext_video_data/',video_data , {headers: new HttpHeaders({

      'Content-Type':  'application/json',
      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem("user")
      
    })
  })
    .pipe(
      catchError(this.handleError)
    );
   }
   
   getExtVideo(dataID:string){
    return this.http.get<any>(this.api_url+`/ext_video_data/${dataID}`,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }

   getYouTubeVideoData(videoID:string):Observable<any> {
    return this.http.get<any>(this.api_url+`/ext_video_data/search_on_youtube?group_id=public&video_id=${videoID}`,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }
   
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

}
