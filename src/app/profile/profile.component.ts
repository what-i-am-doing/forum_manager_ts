
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserService } from '../services/user.service';
import { UserInterests } from "../model/user_interests";
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker'
import { PdfService } from '../services/pdf.service';
import { UserDocument } from '../model/UserDocument';
import { PostService } from '../services/post.service';
import { FormControl } from '@angular/forms';
import { Address } from '../model/address';
import { AddressService } from '../services/address.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [UserService, PdfService, UserDocument, PostService, AddressService]
})

export class ProfileComponent implements OnInit {

  public doc_src: any;
  public documentUpdate: boolean = false;
  public showEdit: boolean = false;
  interests = new FormControl();
  interestsList: string[] = ['reading', 'songs', 'cricket ', 'movie', 'foods', 'entertainment'];
  @ViewChild("upload", { read: ElementRef }) upload: ElementRef;
  public activeMenu: string = '1';
  public edittogle: boolean = false;
  public userInterests: UserInterests[];
  public edu_types;
  public institute_types;
  public course_types;
  public stream_types;
  public class_types;
  public section_types;
  public kyc_types: any;
  public user_Personal_details_Id: any;
  public datePickerConfig: Partial<BsDatepickerConfig>;
  public showaddress: boolean = false;
  public update_message: boolean = false;
  public user_detail: any;
  public userId: string;
  public user_auth_id: string;
  public user_personal_id: string;
  public receivedUserData: any;
  public receivedUserAuthData: any;
  public receivedUserPersonalData: any;
  public receivedUserEducationalData: any;
  public src: any;
  public currentPass: any;
  public oldPassword: any;
  public newPassword: any;
  public repeatPassword: any;
  public passchange: boolean = false;
  public passnotmatch: boolean = false;
  public edu_details_id: any;
  public street: any;
  public area: any;
  public city: any;
  public country: any;
  public landmark: any;
  public plotno: any;
  public state: any;
  public zip: any;
  public showOtpSection: boolean = false;
  public showPassSection: boolean = true;
  public otpnotmatch: boolean = false;
  public mobile_number: any;
  curImgID: string;
  sure: boolean = false;
  docID: string;
  public pdfSrc: any;
  constructor(private userService: UserService, private addressService: AddressService, private pdfService: PdfService, private postService: PostService) {
    var userData = JSON.parse(localStorage.getItem('LoggedInUserData'));
    //this.userName=userData.user_name;
    this.userId = userData.user_id;
    this.user_auth_id = userData.user_auth_id;
    this.currentPass = localStorage.getItem('oldPass');
    this.getUserDetails(this.userId);
    // get user auth detais
    this.getUserAuthDetails(this.user_auth_id);
    // get user personal details
    this.getUserPersonalDetails(this.userId);
    //get user Educational details
    this.getUserEducationDetails(this.userId);
    this.get_education_types();
    this.datePickerConfig = Object.assign({}, { containerClass: 'theme-dark-blue' });
    this.user_id = Number(localStorage.getItem("user"));
    
  }
  
  public user_id: number;
  ngOnInit() {

    
    var userData = JSON.parse(localStorage.getItem('LoggedInUserData'));
    this.userId = userData.user_id;
    this.user_auth_id = userData.user_auth_id;
    this.get_education_types();
    this.datePickerConfig = Object.assign({}, { containerClass: 'theme-dark-blue' });
    this.user_id = Number(localStorage.getItem("user"));
    

  }

  
 
  getUserDetails(userIdwhilecreation: any) {
    this.userService.getUserDetails(userIdwhilecreation).subscribe(receivedUserData => {
      this.receivedUserData = receivedUserData;
      console.log('receivedUserData');
      console.log(receivedUserData);
    })
  }

  getUserAuthDetails(user_auth_id: any) {
    this.userService.getUserAuthDetails(user_auth_id).subscribe(receivedUserAuthData => {
      this.receivedUserAuthData = receivedUserAuthData;

    })
  }

  getUserPersonalDetails(userId: string): void {
    this.userService.getUserPersonalDetails(userId).subscribe(receivedUserPersonalData => {
      this.receivedUserPersonalData = receivedUserPersonalData;

      // this.mobile_number=receivedUserPersonalData.mobile_number;
      var todate = new Date(receivedUserPersonalData.date_of_birth).getDate();
      var tomonth = new Date(receivedUserPersonalData.date_of_birth).getMonth() + 1;
      var toyear = new Date(receivedUserPersonalData.date_of_birth).getFullYear();
      var original_date = tomonth + '/' + todate + '/' + toyear;
      receivedUserPersonalData.date_of_birth = original_date;
      this.user_Personal_details_Id = this.receivedUserPersonalData.user_details_id;

      this.getAdress(this.receivedUserPersonalData.address_id)
    })
  }

  get_kyc_types(): void {
    this.userService.getKycType()
      .subscribe(data => {
        console.log("kyc details ")
        this.kyc_types = data
        console.log(this.kyc_types);
      });
  }

  getUserEducationDetails(userId: string): void {

    this.userService.getUserEducationDetails(userId).subscribe(educationdata => {
      this.receivedUserEducationalData = [];
      console.log("education data  founnd");
      console.log(educationdata);
      this.receivedUserEducationalData = educationdata;
      this.edu_details_id = educationdata.user_education_details_id;
    }, err => {
      console.log("education data not founnd");
    })
  }

  toggleme(): void {
    this.edittogle = !this.edittogle;
  }

  get_all_interests(): void {
    this.userService.userInterests()
      .subscribe(success => { this.userInterests = success });
  }

  get_education_types(): void {

    this.userService.getEducationType().subscribe(data => this.edu_types = data);
    this.userService.getInstituteType().subscribe(data => this.institute_types = data);
    this.userService.getCourseType().subscribe(data => this.course_types = data);
    this.userService.getStreamType().subscribe(data => this.stream_types = data);
    this.userService.getClassType().subscribe(data => this.class_types = data);
    this.userService.getSectionType().subscribe(data => this.section_types = data);
  }


  // updates users personal details
  updateuserDetails(): void {
    var user_detail = {
      "first_name": this.receivedUserData.first_name,
      "last_name": this.receivedUserData.last_name,
      "user_full_text_name": this.receivedUserData.first_name + ' ' + this.receivedUserData.last_name
    }

    var user_auth_detail = {
      "email": this.receivedUserAuthData.email,
      "last_updated_timestamp": Date.now(),

    }

    // convert date of birth to timestamp
    var dateofbirth = this.receivedUserPersonalData.date_of_birth.toString();
    var myDate = []
    myDate = dateofbirth.split("(");
    var tempdob = new Date(myDate[0]);
    var dob = tempdob.getTime()
    var mobileno = this.mobile_number;
    var mobile = mobileno.internationalNumber;
    this.receivedUserPersonalData.mobile_number = mobile;
    var user_personal_detail = {
      "date_of_birth": dob,
      "gender_type": this.receivedUserPersonalData.gender_type,
      "last_updated_timestamp": Date.now(),
      "mobile_number": this.receivedUserPersonalData.mobile_number,
      "address_id": this.receivedUserPersonalData.address_id,
      'interests_list': this.interests.value
    }

    this.userService.updateUserDetail(user_detail, this.userId)
      .subscribe(data => {
        this.receivedUserPersonalData.interests_list = this.interests.value;
        console.log(data);
        // this.mobile_number = null;

      });
    this.userService.updateUserAuthDetail(user_auth_detail, this.user_auth_id)
      .subscribe(user_auth_detail => {
        console.log(user_auth_detail);


      });




    this.userService.updateUserPersonalDetail(user_personal_detail, this.receivedUserPersonalData.user_details_id)
      .subscribe(user_personal_detail => {
        console.log(user_personal_detail);

        this.updateAddressDetails();
      });

  }
  // users educational details
  updateUserEducationDetails() {
    var UserEducationDetailsObj = {
      "class_id": this.receivedUserEducationalData.class_id,
      "comments": "change user education data",
      "course_id": this.receivedUserEducationalData.course_id,
      "education_type_id": this.receivedUserEducationalData.education_type_id,
      "institution_id": this.receivedUserEducationalData.institution_id,
      "last_updated_timestamp": 0,
      "section_id": this.receivedUserEducationalData.section_id,
      "status": "ACTIVE",
      "stream_id": this.receivedUserEducationalData.stream_id,
      "user_id": this.receivedUserEducationalData.user_id
    }

    this.userService.updateUserEducationalDetails(UserEducationDetailsObj, this.edu_details_id).subscribe(educationaldata => {

      this.update_message = true
      setTimeout(() => this.update_message = false, 2000);
    }, () => console.log("educational data not updated")


    )

  }

  filter_edu() {

    return (this.edu_types.filter(edu => edu.education_type_id === this.user_detail.user_educational.education_type_id)).education_type_name;

  }

  showMoreOptions() {
    console.log(" calldeddfadf");
    this.showaddress = true;
  }

  hideMoreOptions() {
    this.showaddress = false;
  }

  clickUpload(): void {
    this.upload.nativeElement.click();
  }

 
  changePassword(newpassword, repeatpassword) {


    console.log(newpassword);

    if ((newpassword != repeatpassword && newpassword != '') || newpassword == null || repeatpassword == null) {

      console.log("pass dont match");
      this.passnotmatch = true;
      setTimeout(() => this.passnotmatch = false, 2500)

    }



    else {

      this.showOtpSection = true;
      this.showPassSection = false;
      this.newPassword = newpassword;

    }

  }

  changePasswordMain(newpassword, repeatpassword) {
    var obj = {


      "group_id": [
        localStorage.getItem("groupId")
      ],
      "password": this.newPassword,
      "user_auth_id": this.user_auth_id,
      "user_id": this.userId

    }

    this.userService.changePassword(obj, this.user_auth_id).subscribe(data => {
      console.log(data);
      console.log("passowrd changed");
      this.passchange = true;
      this.newPassword = null;
      this.repeatPassword = null;
      this.showOtpSection = false;
      this.showPassSection = true;
      setTimeout(() => this.passchange = false, 2500)

    })


  }

  verifyOtp(otp) {

    if (otp == '123456') {
      this.changePasswordMain(this.newPassword, this.oldPassword)

    }

    else {
      this.otpnotmatch = true;
      setTimeout(() => this.otpnotmatch = false, 2500)
    }
  }

  cancelUser() {

    this.showOtpSection = false;
    this.showPassSection = true;
    this.newPassword = null;
    this.repeatPassword = null;
  }

  


  addAdress() {
    var adressobj: Address = new Address();
    adressobj.$area = this.area;
    adressobj.$city = this.city;
    adressobj.$country = this.country;
    adressobj.$landmark = this.landmark;
    adressobj.$plot_number = this.plotno;
    adressobj.$state = this.state;
    adressobj.$street_name = this.street;
    adressobj.$zip = this.zip;


    this.addressService.addAdress(adressobj).subscribe(data => {
      console.log('address Id')
      console.log(data);


    })
  }
  getAdress(adressId) {
    this.addressService.getAdress(adressId).subscribe(data => {
      console.log('adress data found');
      console.log(data);
      this.area = data.area;
      this.city = data.city;
      this.country = data.country;
      this.landmark = data.landmark;
      this.plotno = data.plot_number;
      this.state = data.state;
      this.street = data.street_name;
      this.zip = data.zip;




    })

  }

  updateAddressDetails() {

    var adressobj: Address = new Address();
    adressobj.$area = this.area;
    adressobj.$city = this.city;
    adressobj.$country = this.country;
    adressobj.$landmark = this.landmark;
    adressobj.$plot_number = this.plotno;
    adressobj.$state = this.state;
    adressobj.$street_name = this.street;
    adressobj.$zip = this.zip;
    console.log('object to be updated');
    console.log(adressobj);
    this.addressService.updateAdressDetails(this.receivedUserPersonalData.address_id, adressobj).subscribe(data => {
      console.log('address updated');
      this.update_message = true;

      setTimeout(() => this.update_message = false, 2000);
      this.edittogle = false;

    })
  }


}
