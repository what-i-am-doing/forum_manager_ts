import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { SessionEventsService } from '../../services/session-events.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AddressService } from 'src/app/services/address.service';
import { SessionService } from '../../services/session.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AuthService, SocialUser } from "angularx-social-login";
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker'
import { UserEducational } from '../../model/userEducational';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { UserService } from '../../services/user.service';
import { sessionEvents } from '../../model/session-events';
import { UserPersonal } from '../../model/userPersonal';
import { Address } from 'src/app/model/address';
import { UserAuth } from '../../model/userAuth';
import { UserKYC } from '../../model/userKYC';
import { TemplateRef } from '@angular/core';
import { Router } from "@angular/router";
import { User } from '../../model/user';
import * as Cookies from 'es-cookie';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService, BsModalService,
     UserPersonal, UserEducational, 
     AddressService, NgxIntlTelInputModule]
})

export class LoginComponent implements OnInit {


  @ViewChild("Email", { read: ElementRef }) Email: ElementRef;
  @ViewChild("Password", { read: ElementRef }) Password: ElementRef;
  @ViewChild("googleBtn", { read: ElementRef }) googleBtn: ElementRef;
  
  googleclientId = '139417300527-o68k9jka1bu8sji6je4b8jof79n0vh87.apps.googleusercontent.com';
  userPersonal: UserPersonal = new UserPersonal();
  userEducational: UserEducational = new UserEducational();
  public datePickerConfig: Partial<BsDatepickerConfig>;
 public userEducationData = new UserEducational();
  public userPersonalData = new UserPersonal();
  userAuth: UserAuth = new UserAuth();
  userKYC: UserKYC = new UserKYC();
  genders: any = ['MALE', 'FEMALE'];
  rememberme: boolean = false;
  storecookies: boolean = false;
  private socialuser: SocialUser;
  private loggedIn: boolean;
  public userAdded: boolean = false;
  modalSignup: boolean = false;
  showLoader: boolean = false;
  public errorMessage: any;
  modalRef: BsModalRef;
  bsValue: Date = null;
  modalbsValue: Date = null;
  formGroup: FormGroup;
  public myemail: any = '';
  public mypassword: any = '';
  wrongpassword = false;
  error: boolean = false;
  public errormessage: any;
  public userId: any;
  x = (new Date()).getTime();
  user: User = new User();
  success: boolean = false;
  count_user: number = 0;
  public date_of_birth: any;
  public gender_type: any;
  selectedGender: string = '';
  public addressId: any;
  public cCode: any;
  public mobile_number: any;
  public mobile: any;
  modalselectedGender;
  modalconfirmpassword;
  modal_mobile_number;
  modal_first_name;
  modal_last_name;
  modalemail;
  modalpassword;
  modalstatus;
  items: any[];
  date: Date;
  
  constructor(private sessionService: SessionService, private sessionEventsService: SessionEventsService,
    private authService: AuthService, private modalService: BsModalService, private userService: UserService,
    private router: Router, private addressService: AddressService) {

    this.datePickerConfig = Object.assign({}, { containerClass: 'theme-dark-blue' });

    if (Cookies.get('email') != undefined && Cookies.get('password') != undefined) {

      this.storecookies = true;
      this.myemail = Cookies.get('email');
      this.mypassword = Cookies.get('password');

    }

    this.items = Array(2).fill(0);

  }

  ngOnInit() {

    this.authService.authState.subscribe((data) => {
      this.socialuser = data;
      this.loggedIn = (data != null);

    });

    var img = "assets/images/fulllogin.jpg";
    document.getElementById('bgimg').style.backgroundImage = 'url(' + img + ')';
    document.getElementById('bgimg').style.backgroundSize = 'cover';

    if (sessionStorage.getItem("login")) this.router.navigateByUrl("home");

  }

  myForm = new FormGroup({ myDateYMD: new FormControl(new Date()), });
  phoneForm = new FormGroup({ phone: new FormControl(undefined, [Validators.required]) });
  onValueChange(value: Date): void { this.date = value; }
  onValueChangemodal(value: Date): void { console.log(value); }
  radioChangeHandler(event: any) { this.selectedGender = event.target.value; }
  radioChangeHandlerForModal(event: any) { this.modalselectedGender = event.target.value; }
  openModal(template: TemplateRef<any>) { this.modalRef = this.modalService.show(template); }
  getErrorMessage(err, template) { this.openModal(template); }
  values(result, template) { this.openModal(template) }
  changeImageUrltoByte(url) { }
  closeModal() { this.modalRef.hide(); }
  
  signInWithFB(template): void {

    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(value =>
      this.values(value, template)
    )

  }

 toggleEditable(event) {

    if (event.target.checked) this.rememberme = true;
    else this.rememberme = false;

  }

  showwerroralert() {

    this.wrongpassword = true;
    setTimeout(() => this.wrongpassword = false, 2500)

  }

  signInWithGoogle(template): void {

    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(value => {
      this.googleResult(value, template)
    });


  }

  googleResult(result, template) {

    this.modal_first_name = result.firstName;
    this.modal_last_name = result.lastName;
    this.modalemail = result.email;
    var picUrl = result.photoUrl;
    this.changeImageUrltoByte(picUrl);
    this.checkUserExistByEmail(result.email, template)

  }


  findAge(timestamp1, timestamp2) {
    var difference = timestamp2 - timestamp1;
    var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
    var year = Math.trunc(daysDifference / (365 * 4 + 1 / 4));
    return year;

  }


  addUserEducationDetails() {

    this.userEducationData.$class_id = '';
    this.userEducationData.$course_id = '';
    this.userEducationData.$stream_id = '';
    this.userEducationData.$education_type_id = '';
    this.userEducationData.$institution_id = '';
    this.userEducationData.$user_id = this.userId;
    this.userEducationData.$section_id = '';
    this.userEducationData.$status = 'ACTIVE';
    this.userEducationData.$creation_timestamp = Date.now();

    this.userService.addUserEducationalDetail(this.userEducationData, this.userId).subscribe(data => this.addAdress())
  }

  addAdress() {
    var adressobj: Address = new Address();
    adressobj.$area = '';
    adressobj.$city = '';
    adressobj.$country = '';
    adressobj.$landmark = '';
    adressobj.$plot_number = '';
    adressobj.$state = '';
    adressobj.$street_name = '';
    adressobj.$zip = '';
    adressobj.$comments = 'adding adress'
    adressobj.$creation_timestamp = Date.now()
    adressobj.$last_updated_timestamp = Date.now()
    adressobj.$status = 'ACTIVE'


    this.addressService.addAdress(adressobj).subscribe(data => {

      this.addressId = data.id;
      this.addUserPersonalDetails();

    })


  }

 addUser(form:any): void {

    var mobileno = this.mobile_number;
    this.mobile = mobileno.internationalNumber;

    var userRegisterObj =
    {

      "user": {

        "comments": "string",
        "creation_timestamp": 0,
        "current_status_type_id": "string",
        "custom_user_id": this.user.$first_name + ' ' + this.user.$last_name,
        "first_name": this.user.$first_name,
        "group_id": ["public"],
        "image_id": "string",
        "last_name": this.user.$last_name,
        "last_updated_timestamp": 0,
        "roll_number": "string",
        "status": "ACTIVE",
        "status_message": "string",
        "user_full_text_name": this.user.$first_name + ' ' + this.user.$last_name,
        "user_name": this.user.$first_name + ' ' + this.user.$last_name
      },
      "user_auth_details": {
        "comments": "string",
        "cookie_id": "string",
        "creation_timestamp": 0,
        "email": this.userAuth.$email,
        "group_id": [
          "public"
        ],
        "last_updated_timestamp": 0,
        "password": this.userAuth.$password,
        "status": "ACTIVE",
        "user_name": this.user.$first_name + ' ' + this.user.$last_name
      }

    }

    try {

      this.userService.addUser(userRegisterObj, this.user.$first_name).subscribe(data => {

        this.userId = data.id;
        this.addUserEducationDetails();

      }, () => {

        setTimeout(() => this.error = false, 3000)
        this.error = true;
        setTimeout(() => this.error = false, 3000)

      })


    } catch (err) {
      this.error = true;
      setTimeout(() => this.error = false, 3000)

    }

    form.reset();

  }


 addUserPersonalDetails() {

    if (this.modalbsValue && this.modalSignup) var dateofbirth = this.modalbsValue.toString();
    if (this.bsValue) var dateofbirth = this.bsValue.toString();
    var myDate = [];

    try {
      myDate = dateofbirth.split("(");
      var tempdob = new Date(myDate[0]);
      var dob = tempdob.getTime()
      var age = this.findAge(dob, Date.now());

    }
    catch (err) { }


    this.userPersonalData.$address_id = this.addressId;
    this.userPersonalData.$age = age;
    this.userPersonalData.$report_card_entry_list = [''];
    this.userPersonalData.$date_of_birth = dob;

    if (this.selectedGender) this.userPersonalData.$gender_type = this.selectedGender;
    if (this.modalselectedGender) this.userPersonalData.$gender_type = this.modalselectedGender;

    this.userPersonalData.$interests_list = [''];
    this.userPersonalData.$mobile_number = this.mobile;
    this.userPersonalData.$status = 'ACTIVE';
    this.userPersonalData.$user_documents_list = [''];
    this.userPersonalData.$user_id = this.userId;

    this.userService.addUserPersonalDetails(this.userPersonalData, this.userId).subscribe(data => {


      if (this.bsValue) {
        this.userAdded = true;
        this.bsValue = null
        this.mobile_number = null;
        this.mobile_number = '';
        this.mobile_number = [];

        setTimeout(() => this.userAdded = false, 3000)

      }

      if (this.modalbsValue) this.loginmodalUser();

    })
  }

  
  

  loginmain(email, password) {


    var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
    if (!email.match(reEmail)) this.showwerroralert();
    var loginObj = { "email": email, "password": password };

    this.userService.loginUserviaEmail(loginObj).subscribe((data: any) => {

      if (data.email == email) {

        localStorage.setItem("LoggedInUserData", JSON.stringify(data));
        localStorage.setItem("user_level", "1");
        localStorage.setItem("user_tag", "e978f067-23ea-4a1d-ae19-c442313b9ce9");
        sessionStorage.setItem("login", "true");
        var userData = JSON.parse(localStorage.getItem("LoggedInUserData"));
        localStorage.setItem("user", userData.user_id);
        localStorage.setItem("all_groups", JSON.stringify(userData.group_id));
        localStorage.setItem("user_group", JSON.stringify(userData.group_id));
        localStorage.setItem("groupId", userData.group_id[0]);

        Cookies.set('myemail', email);
        Cookies.set('mypassword', password);
        if (this.rememberme == true) {
          Cookies.set('email', email);
          Cookies.set('password', password);

        }

        this.userService.getUserDetails(userData.user_id).subscribe(
          detail => {
            console.log(detail);
            localStorage.setItem("UserAllData", JSON.stringify(detail));
            localStorage.setItem("first_name", detail.first_name);
            localStorage.setItem("last_name", detail.last_name);

            if (detail.admin_mapping_list && detail.admin_mapping_list[0] != "string") localStorage.setItem("isAdmin", "true");
            else localStorage.setItem("isAdmin", "false");


            this.sessionService.getSessionByUser(data.user_id).subscribe(
              session => {
                console.log(session);
                localStorage.setItem("user_session", JSON.stringify(session));
                var logevt = new sessionEvents;
                logevt.$entity = "user_auth_detail";
                logevt.$group_id = userData.group_id[0];
                logevt.$entity_id = data.user_auth_id;
                logevt.$session_id = session.session_id;
                logevt.$locality = 'group';
                logevt.$locality_id = userData.group_id[0];
                logevt.$status = "ACTIVE";
                logevt.$user_id = data.user_id;

                this.sessionEventsService.createSessionEvent(logevt).subscribe(
                  () => {
                    this.router.navigateByUrl("home");
                  }, (err) => console.log(err)
                );
              },
              err => console.log(err)
            );
          }
        );



      }

    }
      , error => {

        if (error.status == 400) {
          this.errorMessage = error.message;
        }
        if (error.status == 500) {
          this.errorMessage = 'Something went wrong, please try again'
        }

        this.showwerroralert();

      }

    )
  }


 checkUserExistByEmail(email, template) {
    this.userService.checkUserExistByEmail(email).subscribe(data => {
      console.log(data);

      localStorage.setItem("LoggedInUserData", JSON.stringify(data));
      localStorage.setItem("user_level", "1");
      localStorage.setItem("user_tag", "e978f067-23ea-4a1d-ae19-c442313b9ce9");
      sessionStorage.setItem("login", "true");
      var userData = JSON.parse(localStorage.getItem("LoggedInUserData"));
      localStorage.setItem("user", userData.user_id);

      localStorage.setItem("all_groups", JSON.stringify(userData.group_id));
      localStorage.setItem("user_group", JSON.stringify(userData.group_id));
      localStorage.setItem("groupId", userData.group_id[0]);
      Cookies.set('myemail', email);
      Cookies.set('mypassword', 'true');
      this.userService.getUserDetails(userData.user_id).subscribe(
        detail => {
          console.log(detail);
          localStorage.setItem("UserAllData", JSON.stringify(detail));
          this.router.navigateByUrl("/home");
        }
      );


    }, err =>  this.getErrorMessage(err, template)

    )}

  addUserOnFirstGoogleLogin() {

    var userRegisterObj =
    {

      "user": {
        "custom_user_id": this.modal_first_name + ' ' + this.modal_last_name,
        "first_name": this.modal_first_name,
        "group_id": ["public"],
        "image_id": "string",
        "last_name": this.modal_last_name,
        "status": "ACTIVE",
        "user_full_text_name": this.modal_first_name + ' ' + this.modal_last_name,
        "user_name": this.modal_first_name + ' ' + this.modal_last_name,
      },
      "user_auth_details": {
        "email": this.modalemail,
        "group_id": [
          "public"
        ],
        "password": this.modalpassword,
        "status": "ACTIVE",
        "user_name": this.modal_first_name + ' ' + this.modal_last_name
      }
    }

    this.userService.addUser(userRegisterObj, this.modal_first_name).subscribe(data => {

      this.showLoader = true;
      this.userId = data.id;
      this.modalSignup = true
      this.addUserEducationDetails();


    }, err => console.log(err.message)

)}


loginmodalUser() {

    var loginObj = { "email": this.modalemail, "password": this.modalpassword };

    this.userService.loginUserviaEmail(loginObj).subscribe((data: any) => {

      console.log(data);
      localStorage.setItem("LoggedInUserData", JSON.stringify(data));
      localStorage.setItem("user_level", "1");
      localStorage.setItem("user_tag", "e978f067-23ea-4a1d-ae19-c442313b9ce9");
      sessionStorage.setItem("login", "true");
      var userData = JSON.parse(localStorage.getItem("LoggedInUserData"));
      localStorage.setItem("user", userData.user_id);

      localStorage.setItem("all_groups", JSON.stringify(userData.group_id));
      localStorage.setItem("user_group", JSON.stringify(userData.group_id));
      localStorage.setItem("groupId", userData.group_id[0]);

      this.userService.getUserDetails(userData.user_id).subscribe(
        detail => {
          this.showLoader = false;
          this.modalRef.hide();
          console.log(detail);
          localStorage.setItem("UserAllData", JSON.stringify(detail));
          this.router.navigateByUrl("/home");
        }
      );

    }

    )

  }


}


