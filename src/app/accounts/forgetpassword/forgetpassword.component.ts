import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router } from "@angular/router";
import { User } from '../../model/user';


@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css'],
  providers: [UserService]
})
export class ForgetpasswordComponent implements OnInit {
  showsearch: boolean = true;
  showpasssection: boolean = false;
  userData: any;
  userUniqueId: any;
  userdetail: User;
  random_number: any;
  emailId: string;
  wrongpassword:boolean=false;
 
  page_Url: string = "http://localhost:4200/resetPassword"
  constructor(public userservice: UserService, public router: Router) {

  }
  // forget password section only with email
  checkUser(input:any) {

console.log(input);
var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
    if (input == "") {
      // alert("  Please enter your Email");
     this.showwerroralert();
    ;

    }
    else if (!input.match(reEmail)) {
      this.showwerroralert();
     
 
    }
    else {

      try{

        var emailobj={

          "email":input
        }

        console.log("forget password");
        this.userservice.checkUserEmail(emailobj)
        .subscribe(data => {
         
          console.log(data);

           if (data.type=="OTP-Generator") {
  
            localStorage.setItem('UserAuthData',JSON.stringify(data));
            localStorage.setItem('UserAuthEmail',input);
            this.router.navigate(['/resetPassword']);
          }
          else { 
            this.showwerroralert(); 
          }
        }

        )
      }catch(err){
console.log(err);


      }
    

      
    }
    

  }

  

  // when user clicks on cancel button
  cancelUser() {
    this.router.navigate(['/login']);
  }
  ngOnInit() {


  }


 





  // generate 6 digit random number to send OTP
  generateOtp() {
    this.random_number = Math.floor(100000 + Math.random() * 900000);
    console.log(this.random_number);
  }








  showwerroralert(){

    this.wrongpassword=true;
    setTimeout(()=> this.wrongpassword = false,5000)
   
  }
}
