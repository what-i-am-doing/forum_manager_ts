import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-forum-search',
  templateUrl: './forum-search.component.html',
  styleUrls: ['./forum-search.component.css']
})
export class ForumSearchComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  @Input() forum:any;
  @Output() emitForum = new EventEmitter<any>();

  sendForum() {
       this.emitForum.emit(this.forum);
  }
}
