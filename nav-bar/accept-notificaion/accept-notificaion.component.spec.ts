import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptNotificaionComponent } from './accept-notificaion.component';

describe('AcceptNotificaionComponent', () => {
  let component: AcceptNotificaionComponent;
  let fixture: ComponentFixture<AcceptNotificaionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptNotificaionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptNotificaionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
