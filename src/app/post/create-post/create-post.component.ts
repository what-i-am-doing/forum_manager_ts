import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter,
  Input
} from "@angular/core";
import { Post } from "../../model/post";
import { Content } from "../../model/content";
import { ImageService } from "../../services/image.service";
import { PdfService } from "../../services/pdf.service";
import { VideoService } from "../../services/video.service";
import { PostService } from "../../services/post.service";
import { OpengraphService } from "src/app/services/opengraph.service";
import { EmojiEvent, EmojiData } from '../../lib/emoji/public_api';
import { Renderer2 } from '@angular/core';
import { UserService } from "src/app/services/user.service";

const CUSTOM_EMOJIS = [
  {
    name: "Party Parrot",
    shortNames: ["parrot"],
    keywords: ["party"],
    imageUrl: "./assets/images/parrot.gif"
  },
  {
    name: "Octocat",
    shortNames: ["octocat"],
    keywords: ["github"],
    imageUrl: "https://assets-cdn.github.com/images/icons/emoji/octocat.png?v7"
  },
  {
    name: "Squirrel",
    shortNames: ["shipit", "squirrel"],
    keywords: ["github"],
    imageUrl: "https://assets-cdn.github.com/images/icons/emoji/shipit.png?v7"
  },
  {
    name: "Test Flag",
    shortNames: ["test"],
    keywords: ["test", "flag"],
    spriteUrl:
      "https://unpkg.com/emoji-datasource-twitter@4.0.4/img/twitter/sheets-256/64.png",
    sheet_x: 1,
    sheet_y: 1,
    size: 64,
    sheetColumns: 52,
    sheetRows: 52
  }
];

@Component({
  selector: "app-create-post",
  templateUrl: "./create-post.component.html",
  styleUrls: ["./create-post.component.css"],
  providers: [
    PostService,
    PdfService,
    VideoService,
    ImageService,
    OpengraphService
  ]
})

export class CreatePostComponent implements OnInit {
  public gender: any;
  constructor(
    private postService: PostService,
    private imageService: ImageService,
    private pdfService: PdfService,
    private videoService: VideoService,
    private opengraphService: OpengraphService,
    private renderer: Renderer2,
    private userService: UserService
  ) {



    this.renderer.listen('window', 'click', (e: Event) => {

      if (e.target !== this.toggleButton.nativeElement && this.menu.nativeElement != undefined && e.target !== this.menu.nativeElement && this.emojiclicked == false) {
        this.isMenuOpen = false;

      }
    });


    this.isMenuOpen = false;
  }

  public profilePic: any;
  public isMenuOpen = false;
  public Activegender: any;
  firstName: string = localStorage.getItem("first_name");
  lastName: string = localStorage.getItem("last_name");
  userName: string = this.firstName + " " + this.lastName;
  uploadResponse = { status: '', message: 0, filePath: '' };
  toggleMenu() {
    this.isMenuOpen = !this.isMenuOpen;
  }

  ngOnInit() {

    this.getUsersDetails(this.userId);
    this.getActiveUserPersonalDetails(this.userId);
    console.log('eeeee',this.postToEdit);
    if (this.edit) {
      this.postBoxActive = !this.postBoxActive;
      this.myModal.nativeElement.style.zIndex = "10000";
      this.modalback.nativeElement.style.display = "block";
      this.close.nativeElement.style.display = "block";
      this.appear.nativeElement.style.display = "block";
      this.findOGLinks();

    }

    this.content.$content_images_list = [];
    this.content.$content_pdf_list = [];
    this.content.$content_video_list = [];
  }

  @Input() friends: any;
  @Input() edit: any;
  @Input() postToEdit?: any;
  @Output() emitUpdate = new EventEmitter<any>();
  @Output() emitPost = new EventEmitter<any>();
  @Output() emitState = new EventEmitter<any>();
  @ViewChild("upload", { read: ElementRef }) upload: ElementRef;
  @ViewChild("myModal", { read: ElementRef }) myModal: ElementRef;
  @ViewChild("modalback", { read: ElementRef }) modalback: ElementRef;
  @ViewChild("close", { read: ElementRef }) close: ElementRef;
  @ViewChild("appear", { read: ElementRef }) appear: ElementRef;
  @ViewChild('toggleButton') toggleButton: ElementRef;
  @ViewChild('menu') menu: ElementRef;
  @ViewChild('emoji') emoji: ElementRef
  preview: boolean = false;
  postTagFound: boolean = false;
  postBoxActive: boolean = false;
  tagFriends: boolean = false;
  showemojis: boolean = false;
  beforePrevious: number = 0;
  previousKey: number = 0;
  currentTag: string;
  formatted_message: any;
  heading: any;
  postMessage: string;
  lastLink: string;
  showError: true;
  postTagFriends: Array<any> = [];
  imageSRCS: Array<any> = [];
  pdfData: Array<any> = [];
  videoSRCS: Array<any> = [];
  filteredFriends: Array<string> = null;
  userId: string = localStorage.getItem("user");
  userGroupId: Array<string> = JSON.parse(localStorage.getItem("user_group"));
  userLevel: string = localStorage.getItem("user_level");
  userTag: string = localStorage.getItem("user_tag");
  post: Post = new Post();
  content: Content = new Content();
  mediaUpload: boolean = false;
  taggedUsers: Array<any> = [];
  Message: boolean = false;
  start: number;
  end: number;
  previousThumbnail: number = 0;
  previousType: string;
  curTime: number = +new Date;
  public emojiclicked: boolean = false;
  submitPost(): void {
    if (this.formatted_message.length > 10000) {
      this.showError = true;
      return;
    }
    var post: Post = new Post;
    post.$message = this.postMessage;
    post.$user_id = this.userId;
    post.$original_user_id = this.userId;
    post.$group_id = this.userGroupId;
    post.$heading = this.heading;
    post.$formatted_message = this.formatted_message;
    post.$level_id = this.userLevel;
    post.$creation_timestamp = +new Date();
    post.$tag_id = this.userTag;
    post.$post_type = "CREATED";
    post.$post_has_data = false;
    post.$status = "UNDER_REVIEW";
    if (
      this.content.$content_images_list.length ||
      this.content.$content_pdf_list.length ||
      this.content.$content_video_list.length
    ) {
      if (this.previousType == 'image') {
        post.$img_id = this.content.$content_images_list[this.previousThumbnail];
        post.$preview_content_id = this.content.$content_images_list[this.previousThumbnail];
        post.$preview_content_type = "images";
      }

      if (this.previousType == 'pdf') {
        post.$img_id = this.content.$content_pdf_list[this.previousThumbnail];
        post.$preview_content_id = this.content.$content_pdf_list[this.previousThumbnail];
        post.$preview_content_type = "pdf";
      }

      if (this.previousType == 'video') {
        post.$img_id = this.content.$content_video_list[this.previousThumbnail];
        post.$preview_content_id = this.content.$content_video_list[this.previousThumbnail];
        post.$preview_content_type = "videos";
      }

      post.$post_has_data = true;
      this.postService.usercontent(this.content).subscribe(content => {
        console.log(content);
        post.$post_content = content.id;
        this.postService.userpost(post).subscribe(data => {
          console.log(data);
          this.imageSRCS = [];
          this.pdfData = [];
          this.videoSRCS = [];
          this.content.$content_images_list = [];
          this.content.$content_pdf_list = [];
          this.content.$content_video_list = [];
          this.mediaUpload = false;
          this.Message = false;
          this.formatted_message = "";
          this.heading = "";
          console.log(data);
          this.getCreatedPost(data.id);
        });
      });
    } else {
      this.postService.userpost(post).subscribe(data => {

        this.imageSRCS = [];
        this.pdfData = [];
        this.videoSRCS = [];
        this.mediaUpload = false;
        this.Message = false;
        this.formatted_message = "";
        this.heading = "";
        console.log(data);
        this.getCreatedPost(data.id);
      });
    }
    this.postBoxActive = !this.postBoxActive;
    this.preview = false;
    this.closePostcreate();
  }

  getCreatedPost(postID: string) {
    // this.postService.getPostById(postID).subscribe(post => {
    //   console.log(post);
    //   var emitPost = Object.assign({}, post);
    //   this.emitPost.emit(emitPost);
    // });
    this.emitPost.emit(true);
  }

  findLinksInPost(str) {
    var pattern = /(?:(?:https?|ftp):\/\/|\b(?:[a-z\d]+\.))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))?/gi;
    var result = str.match(pattern);
    console.log(result);
    return result;
  }

  closePostcreate() {
    this.modalback.nativeElement.style.display = "none";
    this.close.nativeElement.style.display = "none";
    this.appear.nativeElement.style.display = "none";
    this.myModal.nativeElement.style.zIndex = "0";
    this.imageSRCS = [];
    this.pdfData = [];
    this.videoSRCS = [];
    this.content.$content_images_list = [];
    this.content.$content_pdf_list = [];
    this.content.$content_video_list = [];
    this.mediaUpload = false;
    this.Message = false;

    if (this.edit) {
      this.emitState.emit(false);
    }
  }

  postFocus(event: Event) {
    console.log(this.imageSRCS);
    console.log(this.content.$content_images_list);
    console.log(this.postToEdit);
    event.preventDefault();
    this.myModal.nativeElement.style.zIndex = "10000";
    this.modalback.nativeElement.style.display = "block";
    this.close.nativeElement.style.display = "block";
    this.appear.nativeElement.style.display = "block";
  }


  tagInPost(event: any) {
    console.log(this.postTagFound);
    if (!this.edit) this.postMessage = this.formatted_message
      .toString()
      .replace(/<[^>]*>/g, "");

    else this.postMessage = this.postToEdit.formatted_message
      .toString()
      .replace(/<[^>]*>/g, "");

    if (this.postMessage.length) this.Message = true;
    else this.Message = false;
    console.log(this.Message);
    event = event.event;
    console.log(event.keyCode);
    if (event.keyCode === 32) {
      this.findOGLinks();
    }

    var str = this.postMessage;
    if (this.postTagFound === true) {
      console.log(event.keyCode);
      console.log(str);

      if (str.includes("@")) {
        var str = str.substring(str.lastIndexOf("@") + 1);
        this.currentTag = str;
        console.log(str);
        this.filteredFriends = this.friends.filter(word =>
          word.user_full_text_name.startsWith(str)
        );
        console.log(this.filteredFriends);
      } else this.postTagFound = false;

    } else {
      if (
        (this.postMessage == "@" || this.beforePrevious == 32 || this.beforePrevious == 8) &&
        (event.key == 2 && this.previousKey == 16)
      ) {

        this.postTagFound = true;
        this.start = this.formatted_message.length - 1;
        if (this.friends.length > 6) this.filteredFriends = this.friends.slice(0, 6);
        else this.filteredFriends = this.friends;
        this.currentTag = str.substring(str.lastIndexOf("@") + 1);
      }
    }

    this.beforePrevious = this.previousKey;
    this.previousKey = event.keyCode;
    console.log(this.postTagFound);
  }

  findOGLinks() {

    if (!this.postToEdit) {
      this.postMessage = this.formatted_message
        .toString()
        .replace(/<[^>]*>/g, "");
    }

    else this.postMessage = this.postToEdit.formatted_message
      .toString()
      .replace(/<[^>]*>/g, "");

    if (this.postMessage.length) this.Message = true;
    else this.Message = false;

    if (this.lastLink == undefined) {
      console.log(this.postMessage);
      var filter = this.postMessage.replace(/&nbsp;/g, "");
      var links = this.findLinksInPost(filter);

      if (links.length) {
        this.lastLink = links[0];
        var reqURL = encodeURIComponent(this.lastLink);
        console.log(reqURL);
        this.opengraphService.getOgData(reqURL).subscribe(data => {
          console.log(data);
          data = data.hybridGraph;
          var main_container = document.createElement("div");
          main_container.style.border = "1px solid #c5c5c5";
          main_container.style.borderTop = "0px";
          main_container.style.padding = "2px";
          var footer_container = document.createElement("div");
          var heading = document.createElement("h4");
          if (data.title) heading.textContent = data.title;
          footer_container.appendChild(heading);
          var link = document.createElement("h5");
          var link_content = document.createElement("a");
          if (data.url) link_content.textContent = data.url;
          link.appendChild(link_content);
          footer_container.appendChild(link);
          var describe = document.createElement("div");
          if (data.description) describe.textContent = data.description.substring(0, 200) + "...";
          footer_container.appendChild(describe);
          if (data.image) {
            var img = document.createElement("img");
            img.src = data.image;
            img.width = 150;
            img.height = 70;
            main_container.appendChild(img);

          }
          var cross = document.createElement("span");
          cross.setAttribute("class", "pull-right glyphicon glyphicon-remove");
          cross.classList.add("cursor");

          cross.addEventListener("click", () => {
            this.lastLink = undefined;
            container.removeChild(container.childNodes[0]);
          });

          main_container.appendChild(cross);
          main_container.appendChild(footer_container);
          if (this.edit) var container = document.getElementById("appendOGedit");
          else var container = document.getElementById("appendOG");
          container.appendChild(main_container);
          console.log(container.childNodes[0]);
        },
          () => this.lastLink == undefined
        );
      }
    }

  }

  selectPostfriend(friend) {
    this.end = this.formatted_message.length;
    this.formatted_message = this.replaceBetween(this.start, this.end, "<a contenteditable='false'>@" + friend.user_full_text_name + "</a> &nbsp;", this.formatted_message);

    this.taggedUsers.push({
      content:
        '<a contenteditable="false">@' + friend.user_full_text_name + "</a>",
      id: friend.user_id
    });
    this.postTagFound = false;
  }
  clickUpload(): void {
    this.upload.nativeElement.click();
  }

  processFile(imageInput: any) {
    var file = imageInput.files[0];
    var fileName = file.name;
    var fileSize = file.size;
    var fileReader = new FileReader();

    if (file.type.match("image")) {

      fileReader.addEventListener("load", (event: any) => {
        console.log(event.target.result);
        this.imageService.uploadImage(file).subscribe(
          (res: any) => {
            console.log(res);
            this.uploadResponse = res;

            if (typeof res === 'object') {
              if ('id' in res) {
                this.mediaUpload = true;
                this.content.$content_images_list.push(res.id);
                if (this.edit) {
                  console.log("if block executed");
                  this.postToEdit._real_albums = [];
                  this.postToEdit._real_albums.push(
                    {
                      image_storage_url: fileReader.result,
                      content_type: "image/jpeg",
                      image_name: file.name,
                      image_size: file.size
                    }

                  );
                  console.log(this.imageSRCS);

                }
                else {
                  console.log("else block executed");
                  if (!this.imageSRCS.length && !this.pdfData.length && !this.videoSRCS.length) {
                    var isPreview = true;
                    this.previousType = "image";
                  }
                  else var isPreview = false;

                  this.imageSRCS.push({
                    image_storage_url: fileReader.result,
                    content_type: "image/jpeg",
                    image_name: file.name,
                    image_size: file.size,
                    is_preview: isPreview
                  });
                  console.log(this.imageSRCS);
                }
              }
            }
          },
          err => {
            console.log(err);
          }
        );




      });

      fileReader.readAsDataURL(file);
    } else if (file.type.match("pdf")) {

      fileReader.addEventListener("load", (event: any) => {
        this.pdfService.uploadPdf(file).subscribe(
          (res: any) => {
            console.log(res);
            this.uploadResponse = res;
            if (typeof res === 'object') {
              if ('id' in res) {
                this.mediaUpload = true;
                this.content.$content_pdf_list.push(res.id);
                if (!this.imageSRCS.length && !this.pdfData.length && !this.videoSRCS.length) {
                  var isPreview = true;
                  this.previousType = "pdf";

                }
                else var isPreview = false;
                this.pdfData.push({ pdf_storage_url: null, content_type: "application/pdf", pdf_name: file.name, pdf_size: file.size, is_preview: isPreview });



              }
            }

          },
          err => {
            console.log(err);
          }
        );
      });

      fileReader.readAsDataURL(file);
    } else {

      fileReader.addEventListener("load", (event: any) => {
        var blob = new Blob([fileReader.result], { type: file.type });
        var url = URL.createObjectURL(blob);

        var video = document.createElement("video");
        var timeupdate = function () {
          if (snapImage()) {
            video.removeEventListener("timeupdate", timeupdate);
            video.pause();
          }
        };
        video.addEventListener("loadeddata", function () {
          if (snapImage()) {
            video.removeEventListener("timeupdate", timeupdate);
          }
        });
        var snapImage = () => {
          var canvas = document.createElement("canvas");
          canvas.width = video.videoWidth;
          canvas.height = video.videoHeight;
          var img = document.createElement("img");
          canvas
            .getContext("2d")
            .drawImage(video, 0, 0, canvas.width, canvas.height);
          var image = canvas.toDataURL();
          var success = image.length > 100000;
          if (success) {
            if (!this.imageSRCS.length && !this.pdfData.length && !this.videoSRCS.length) {
              var isPreview = true;
              this.previousType = "video";
            }
            else var isPreview = false;
            this.videoSRCS.push({
              video_storage_url: image,
              content_type: "video/mp4",
              video_name: file.name,
              video_size: file.size,
              is_preview: isPreview

            });
            URL.revokeObjectURL(url);
          }
          return success;
        };
        video.addEventListener("timeupdate", timeupdate);
        video.preload = "metadata";
        video.src = url;
        // Load video in Safari / IE11
        video.muted = true;

        video.play();

        this.videoService.uploadVideo(file).subscribe(
          (res: any) => {
            console.log(res);
            this.uploadResponse = res;
            if (typeof res === 'object') {
              if ('id' in res) {
                this.mediaUpload = true;
                this.content.$content_video_list.push(res.id);
              }
            }

          },
          err => {
            console.log(err);
          }
        );
      });

      fileReader.readAsArrayBuffer(file);
    }
  }

  showandhideEmojis(showemojis) {
    if (showemojis == false) {
      this.showemojis = true;
    } else if (showemojis == true) {
      this.showemojis = false;
    }
    this.set = this.themes[0];
  }

  themes = ['google'];
  set = 'google';
  native = true;
  CUSTOM_EMOJIS = CUSTOM_EMOJIS;

  setTheme(set: string) {
    this.native = set === 'native';
    this.set = set;
  }

  handleClick(event: EmojiEvent) {
    var emoji = event.$event.srcElement;
    var cloneEmoji: HTMLElement = <HTMLElement>emoji.cloneNode(true);

    this.formatted_message += cloneEmoji.innerHTML;

    // document.getElementById("editor").appendChild(cloneEmoji);
  }

  emojiClick($event) {
    console.log(" emoji  clicked");
  }

  keyDown(event: KeyboardEvent) {
    if (event.keyCode > 1) {
      this.showemojis = false;
    }
  }

  getSelectedFriends(event: any) {
    console.log(event);
    this.postTagFriends = event;
  }

  cancelORSubmit() { this.emitState.emit(false); }

  updatePost() {
    var x = {
      "formatted_message": this.postToEdit.formatted_message,
      "heading": this.postToEdit.heading,
      "message": this.postToEdit.formatted_message.toString().replace(/<[^>]*>/g, "")
    };
    console.log(x);
    this.postService.updatePost(this.postToEdit.post_id, x).subscribe(data => {
      console.log(data);
      this.emitUpdate.emit(x);

    }
    );

  }

  public addEmoji(event: { $event: MouseEvent, emoji: EmojiData }) {

    console.log("emoji added");
    let emoticonElement = <HTMLElement>event.$event.target;
    if (!emoticonElement.style.backgroundImage || emoticonElement.style.backgroundImage === '') {
      emoticonElement = <HTMLElement>emoticonElement.firstChild;
    }


    console.log(emoticonElement.outerHTML);
    console.log(event.emoji.unified);
    console.log(this.URLChange(emoticonElement.outerHTML))

    if (this.formatted_message == undefined) {
      this.formatted_message = "<span style='position: relative;top: 5px' contenteditable='false'>" + this.URLChange(emoticonElement.outerHTML) + "</span";

    }
    else {
      this.formatted_message += "<span style='position:relative;top:5px' contenteditable='false'>" + this.URLChange(emoticonElement.outerHTML) + "</span>";

    }

  }

  replaceBetween = function (start, end, what, string) {
    return string.substring(0, start) + what + string.substring(end);
  };
  onSearchChange(value) {
    console.log(" value changes")
    this.formatted_message = value;
  }

  URLChange(element) {
    return element.replace("https", "http");
  }

  reCreateThumbnail(event: any) {
    console.log(event);
    if (this.previousType == 'image') this.imageSRCS[this.previousThumbnail].is_preview = false;
    if (this.previousType == 'pdf') this.pdfData[this.previousThumbnail].is_preview = false;
    if (this.previousType == 'video') this.videoSRCS[this.previousThumbnail].is_preview = false;

    if (event.type == 'image') this.imageSRCS[event.index].is_preview = true;
    else if (event.type == 'pdf') this.pdfData[event.index].is_preview = true;
    else this.videoSRCS[event.index].is_preview = true;
    this.previousThumbnail = event.index;
    this.previousType = event.type;

  }

  emojiCicked() {
    console.log(' emoji clicked true');
    this.emojiclicked = true;
    setTimeout(() => this.emojiclicked = false, 500)

  }

  onChange(event) {

    console.log('value changing');
    this.isMenuOpen = false;

  }

  getUsersDetails(userId) {
    this.userService.getUserDetails(userId).subscribe(data => {

      this.getProfile(data.image_id);

    })
  }

  getProfile(image_id: string) {

    this.imageService.getImageByType(image_id, 'thumbnail').subscribe(data => {
      if (data.length) this.profilePic = this.URLChange(data[0].image_storage_url);
      else this.getProfileImage(image_id);
    })
  }

  getProfileImage(image_id: string) {

    this.imageService.getImage(image_id).subscribe(data => {

      this.profilePic = this.URLChange(data.image_storage_url);

    })
  }

  getActiveUserPersonalDetails(userId) {
    this.userService.getUserPersonalDetails(userId).subscribe(data => {
      this.Activegender = data.gender_type;
      console.log(data.gender_type);


    })
  }
}
