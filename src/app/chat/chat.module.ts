import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatBoxComponent } from './chat-box/chat-box.component';
import { ChatMessageComponent } from './chat-message/chat-message.component';
import { ChatGroupComponent } from './chat-group/chat-group.component';
import {MatMenuModule} from '@angular/material/menu';
import { ExtraChatBoxContainerComponent } from './extra-chat-box-container/extra-chat-box-container.component';
import { MatIconModule, MatInputModule, MatFormFieldModule, MatAutocompleteModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatChipsModule} from '@angular/material/chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material';
import { AutoCompleteComponent } from './auto-complete/auto-complete.component';
import { TempChatGroupComponent } from './temp-chat-group/temp-chat-group.component';
import { ChatFriendComponent } from './chat-friend/chat-friend.component';
import { ChatGroupListComponent } from './chat-group-list/chat-group-list.component';
import { ChatGroupFriendListComponent } from './chat-group-friend-list/chat-group-friend-list.component';

@NgModule({
  declarations: [ChatBoxComponent, ChatMessageComponent, ChatGroupComponent, ExtraChatBoxContainerComponent,AutoCompleteComponent, TempChatGroupComponent, ChatFriendComponent, ChatGroupListComponent, ChatGroupFriendListComponent],
  imports: [
    CommonModule,
    MatMenuModule,
    MatIconModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatChipsModule,MatAutocompleteModule, MatFormFieldModule ,MatInputModule,MatButtonModule



  ],
  exports:[ChatBoxComponent,ChatMessageComponent,ChatGroupComponent,ChatFriendComponent,ChatGroupListComponent,ChatGroupFriendListComponent],
  entryComponents:[ChatMessageComponent]
})
export class ChatModule { }
