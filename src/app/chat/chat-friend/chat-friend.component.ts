import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ImageService } from 'src/app/services/image.service';
import { ChatService } from "../../services/chat.service";
import { Router } from "@angular/router";
import {  Subscription } from 'rxjs';
import { AddressService } from 'src/app/services/address.service';

@Component({
  selector: 'app-chat-friend',
  templateUrl: './chat-friend.component.html',
  styleUrls: ['./chat-friend.component.css'],
  providers: [UserService, ImageService, AddressService]
})

export class ChatFriendComponent implements OnInit {

  @Input() friend: any;
  @Output() openBox = new EventEmitter<any>();

  myID: string = localStorage.getItem("user");
  public active: boolean = false;
  freindDetail: any;
  firstName:string;
  lastName:string;
  chatID: string;
  gender: string;
  public userId: any;
  public userGroupId: any;
  public activeUserPersonalData: any;
  public profilePic: any;
  public activeUserEducationalData: any;
  sub: Subscription;
  messages: Array<any> = [];
  friendsMsges: Array<any> = [];
  public city: any;
  groupID: string = localStorage.getItem("groupId");

  constructor(private addressService: AddressService, private userService: UserService, private imageService: ImageService, private chatService: ChatService, private router: Router) { }

  ngOnInit() {
    this.userService.getUserDetails(this.friend.friend_user_id).subscribe(detail => {
      
      this.freindDetail = detail;
      this.firstName = detail.first_name;
      this.lastName = detail.last_name;
      if(this.friend.chat_id) this.freindDetail.chat_id = this.friend.chat_id;
      this.userId = this.friend.friend_user_id;
      this.userGroupId = this.friend.group_id;
      this.getUserEducationalDetails(this.userId);
      this.getUserPersonalDetails(this.userId);
      if(this.freindDetail.image_id && this.freindDetail.image_id != 'string')
      this.getProfile(this.freindDetail.image_id);

     });

  }

  ngOnDestroy() {
    if (this.sub) this.sub.unsubscribe();
  }

  getChatforBox() {
    this.chatService.get121Chat(this.groupID, this.chatID, 0, 10).subscribe(msg => {
      if (Object.keys(msg).length) {
        console.log(msg);
        var key = Object.keys(msg)[0];
        msg = msg[key];
        msg.sort(
          (msg1, msg2) => msg1.creation_timestamp - msg2.creation_timestamp
        );
        this.messages = msg;
        this.friendsMsges = msg.filter(element => element.from_user_id != this.myID);
        this.friendsMsges.forEach(element => {
             this.friend.friendsMsges.push(element);
        });
      }

     // this.getChatAtInterval(this.chatID, +new Date(), +new Date() + Number(600000));
    });
  }

  getChatAtInterval(chatID: string, start: number, end: number) {
    this.sub =
      this.chatService
        .get121ChatByTime(
          this.groupID,
          chatID,
          start,
          end
        )
        .subscribe(data => {
          console.log(data);
          if (Object.keys(data).length) {
            console.log(data);
            var key = Object.keys(data)[0];
            
            data[key].sort(
              (msg1, msg2) => msg2.creation_timestamp - msg1.creation_timestamp
            );

            var max = data[key][0].creation_timestamp + 1;
            this.getChatAtInterval(chatID, max.creation_timestamp + 1, max.creation_timestamp + 1 + Number(600000));
            data[key].forEach(element => {
              console.log(element);
              if (element.from_user_id != this.myID) this.friendsMsges.push(element);
            });

          }

          else {
            this.getChatAtInterval(chatID, +new Date, +new Date() + Number(600000));
          }

        })

  }

  createBox() {
    if(this.friend.chat_id) this.freindDetail.chat_id = this.friend.chat_id;
    this.freindDetail.messages = this.friend.messages;
    this.openBox.emit(this.freindDetail);
  }

  getUserPersonalDetails(userId) {

    try {
      this.userService.getUserPersonalDetails(userId).subscribe(data => {

        console.log(data);
        this.activeUserPersonalData = data;
        this.getAdress(data.address_id);

      })
    } catch (err) {

    }

  }


  getUserEducationalDetails(userId) {

    try {
      this.userService.getEducationDetailsDataByUserId(userId).subscribe(data => {

        console.log(data);
        this.activeUserEducationalData = data;

      })
    } catch (err) {


    }
  }

  URLChange(img: any) {
    if(img) return img.replace("https", "http");
  }


  getProfile(image_id:string) {

    this.imageService.getImageByType(image_id, 'thumbnail').subscribe(data => {
      if (data.length) this.profilePic = this.URLChange(data.image_storage_url);
      else this.getProfileImage(image_id);
      
    })
  }

  getProfileImage(image_id:string) {
    this.imageService.getImage(image_id).subscribe(data => this.profilePic = this.URLChange(data.image_storage_url))

  }


  seeFriendProfile(userId) {
      this.router.navigate(['/home/userprofile'], { queryParams: { userId: userId } });
  }
  
  getAdress(adressId) {
    this.addressService.getAdress(adressId).subscribe(data => this.city = data.city )
  }


}
