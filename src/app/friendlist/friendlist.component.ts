import { Component, OnInit, Input, HostListener, ViewChild } from '@angular/core';
import { UserService } from '../services/user.service';
import { MatCardModule } from '@angular/material/card';
import {MatMenuTrigger} from '@angular/material/menu';
import { FriendService } from '../services/friend.service';

@Component({
  selector: 'app-friendlist',
  templateUrl: './friendlist.component.html',
  styleUrls: ['./friendlist.component.css'],
  providers: [UserService,FriendService]
})

export class FriendlistComponent implements OnInit {
  public friends: any;
  public total_friend: any;
  listORgrid:boolean = false;
  public receivedFriendRequestSenderData = []
  public requestresult;
  public userId:any;
public userGroupId:any;
 public   gotFriendRequestData:Array<any>= []
 public receivedFriendList = []
 public sentFriendRequestList:Array<any>= []
 public blockedRequestList:Array<any>= []
 public deletedRequestList:Array<any>= []
  public user_friendlist;
  public openRequestList:Array<any>= []
  constructor(public service: UserService,public friendService:FriendService) {
   

  }


 

  ngOnInit() {
    console.log('friendlist page called');
    var userData = JSON.parse(localStorage.getItem('LoggedInUserData'));

    this.userId = userData.user_id;
   this.userGroupId = localStorage.getItem("groupId");
this.getFriendRequest(this.userId,this.userGroupId, 0,100);
// this.getFriendRequest_Of_Open_Status(this.userId,"OPEN");
this.getSentFriendRequestByActiveUser(this.userId,this.userGroupId);
this.getBlockedRequestList(this.userId,"BLOCKED");
this.getDeletedRequestList(this.userId,"DECLINED");
    
  }
  

  getFriendRequest(userId: any,groupid, starttime: any, endtime: any) {
    this.service.getFriendRequest(userId, groupid, starttime, endtime).subscribe((data: any) => {
   
      console.log('friend request recieved')
      console.log(data)
       var result;
       result = data.filter(req => req.req_status == 'OPEN');
       console.log(result)
       if(result.length){
         const uniqueArray=this.removeDuplicates(result,'from_user_id')
         console.log('unique');
       console.log(uniqueArray);
       uniqueArray.forEach(element => {
         this.gotFriendRequestData.push(element);
       })
       }
       this.gotFriendRequestData.sort(
         (post1, post2) => post2.creation_timestamp - post1.creation_timestamp
       );
 
       console.log(this.gotFriendRequestData);
      
    
     
      
     // this.getFriendRequestAtInterVal(this.userId,groupid,+new Date(), +new Date() + Number(50000));
 
     }), err => {
     
     
    }

    
  



  }

  getSentFriendRequestByActiveUser(userId,groupid){
    console.log("friend request sent by user");
    this.service.getSentFriendRequestByActiveUser(userId,groupid).subscribe(data=>{

      console.log("friend request sent by user");
      console.log(data);
      
      if(data!=null || data!=undefined)

        var result=[];
         data.forEach(element => {
        if(element.req_status=='OPEN'){
          result.push(element);
        }
        
      });
      this.openRequestList=this.removeDuplicates(result,'to_user_id')
   console.log(this.openRequestList)
    })
  
  
  }

  URLChange(img:any){
     return img.replace("https" , "http");
     
  }



  getBlockedRequestList(userid, status){
    this.friendService.getSentFriendRequestByStatus(userid,this.userGroupId,status).subscribe(data=>{
    console.log("blocked friend requests")
    console.log(data);
    if(data.length)
    this.blockedRequestList=this.removeDuplicates(data,'from_user_id')
    console.log(this.blockedRequestList)
})

  }
  getDeletedRequestList(userid,status){
   
    this.friendService.getSentFriendRequestByStatus(userid,this.userGroupId,status).subscribe(data=>{
      console.log("deleted friend requests")
      console.log(data);
      if(data.length)
      this.deletedRequestList=this.removeDuplicates(data,'from_user_id')
      console.log(this.blockedRequestList)
 
    })
    
    
  }

  removeDuplicates(myArr, prop) {
    console.log('duplicates in friend request called')
    return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
}

}
