import { Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css'],
  providers: []
})


export class NotificationComponent implements OnInit {
  @Input() evnt: any;
  @Output() notifClik = new EventEmitter();
  userId:string = localStorage.getItem("user");
  constructor(private router: Router){}

  ngOnInit(){}
  
  notifClicked() {
    
    var notify = 'true'
    this.notifClik.emit(notify);


  }

  seeEventNotif(userId, postId) {

    this.notifClicked();
    var notifdata = {"userId": userId, "postId": postId };
    localStorage.setItem('notifdata', JSON.stringify(notifdata));
    this.router.navigate(['/home/notif'], { queryParams: { notifId: postId } });

  }
}
