export class Tag {
constructor(){}

private comments: string;
private creation_timestamp: number;
private custom_tag_line: string;
private group_id: string;
private image_id: string;
private last_updated_timestamp: number;
private level_id: string;
private path_from_group_root: string;
private status: string;
private tag_admin_user_id: string;
private tag_id: string;
private tag_name: string;

    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $creation_timestamp
     * @return {number}
     */
	public get $creation_timestamp(): number {
		return this.creation_timestamp;
	}

    /**
     * Getter $custom_tag_line
     * @return {string}
     */
	public get $custom_tag_line(): string {
		return this.custom_tag_line;
	}

    /**
     * Getter $group_id
     * @return {string}
     */
	public get $group_id(): string {
		return this.group_id;
	}

    /**
     * Getter $image_id
     * @return {string}
     */
	public get $image_id(): string {
		return this.image_id;
	}

    /**
     * Getter $last_updated_timestamp
     * @return {number}
     */
	public get $last_updated_timestamp(): number {
		return this.last_updated_timestamp;
	}

    /**
     * Getter $level_id
     * @return {string}
     */
	public get $level_id(): string {
		return this.level_id;
	}

    /**
     * Getter $path_from_group_root
     * @return {string}
     */
	public get $path_from_group_root(): string {
		return this.path_from_group_root;
	}

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Getter $tag_admin_user_id
     * @return {string}
     */
	public get $tag_admin_user_id(): string {
		return this.tag_admin_user_id;
	}

    /**
     * Getter $tag_id
     * @return {string}
     */
	public get $tag_id(): string {
		return this.tag_id;
	}

    /**
     * Getter $tag_name
     * @return {string}
     */
	public get $tag_name(): string {
		return this.tag_name;
	}

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $creation_timestamp
     * @param {number} value
     */
	public set $creation_timestamp(value: number) {
		this.creation_timestamp = value;
	}

    /**
     * Setter $custom_tag_line
     * @param {string} value
     */
	public set $custom_tag_line(value: string) {
		this.custom_tag_line = value;
	}

    /**
     * Setter $group_id
     * @param {string} value
     */
	public set $group_id(value: string) {
		this.group_id = value;
	}

    /**
     * Setter $image_id
     * @param {string} value
     */
	public set $image_id(value: string) {
		this.image_id = value;
	}

    /**
     * Setter $last_updated_timestamp
     * @param {number} value
     */
	public set $last_updated_timestamp(value: number) {
		this.last_updated_timestamp = value;
	}

    /**
     * Setter $level_id
     * @param {string} value
     */
	public set $level_id(value: string) {
		this.level_id = value;
	}

    /**
     * Setter $path_from_group_root
     * @param {string} value
     */
	public set $path_from_group_root(value: string) {
		this.path_from_group_root = value;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}

    /**
     * Setter $tag_admin_user_id
     * @param {string} value
     */
	public set $tag_admin_user_id(value: string) {
		this.tag_admin_user_id = value;
	}

    /**
     * Setter $tag_id
     * @param {string} value
     */
	public set $tag_id(value: string) {
		this.tag_id = value;
	}

    /**
     * Setter $tag_name
     * @param {string} value
     */
	public set $tag_name(value: string) {
		this.tag_name = value;
	}


}