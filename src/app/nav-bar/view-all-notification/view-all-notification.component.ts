import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-view-all-notification',
  templateUrl: './view-all-notification.component.html',
  styleUrls: ['./view-all-notification.component.css']
})
export class ViewAllNotificationComponent implements OnInit {
  public allEventsForUser:Array<any> = [];
  constructor(private router:Router) { }
  public activeUserId=localStorage.getItem("user");
  ngOnInit() {

 this.allEventsForUser=JSON.parse(localStorage.getItem('notificationdata'));
  }

  @Output() notifClik = new EventEmitter();
  notifClicked(){
    console.log('notification click  event output')
  var notify='true'
    this.notifClik.emit(notify);


  }
  seeEventNotif(userId,postId){

    this.notifClicked();
    console.log(postId)
    console.log('post id and user id')
    console.log(userId)
   var notifdata={
      "userId":userId,
      "postId":postId

  }
  localStorage.setItem('notifdata',JSON.stringify(notifdata));
  var test= JSON.parse(localStorage.getItem('notifdata'));
  console.log(" test local data");
  console.log(test);

    this.router.navigate(['/home/notif'], { queryParams: {notifId:postId} });

  }

}
