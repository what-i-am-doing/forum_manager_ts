import { Component, OnInit, Input } from '@angular/core';
import { UserService } from "src/app/services/user.service";
import { ImageService } from "src/app/services/image.service";
import { PostService} from "src/app/services/post.service";
import { PdfService  } from "src/app/services/pdf.service";
import { VideoService  } from "src/app/services/video.service";

@Component({
  selector: 'app-post-snapshot',
  templateUrl: './post-snapshot.component.html',
  styleUrls: ['./post-snapshot.component.css'],
  providers: [UserService]
})

export class PostSnapshotComponent implements OnInit {

  user: any;
  userPIC: string;
  firstName:string;
  lastName:string;

  constructor(private userService: UserService, private imageService: ImageService,private postService:PostService,
    private pdfService:PdfService,private videoService:VideoService
    
    ) { }

  ngOnInit() {
    console.log(this.postToShare);

    this.userService.getUserDetails(this.postToShare.original_user_id).subscribe(
      user => {
        this.user = user;
        this.firstName = user.first_name;
        this.lastName = user.last_name;
        if (user.image_id && user.image != '') this.getuserThumb(user.image_id);
      }
    );

    if (this.postToShare.post_content) this.getPostContent();

  }

  @Input() postToShare: any;
  @Input() postUser: any;

  getuserThumb(ID: string) {
    this.imageService.getImageByType(ID, 'thumbnail').subscribe(data => {

      if (data.length) this.userPIC = this.URLChange(data[0].image_storage_url);
      else this.getUserImage(ID);

    })
  }


getUserImage(image_id: string) {
    this.imageService.getImage(image_id).subscribe(data => this.userPIC = this.URLChange(data.image_storage_url))
  }


  URLChange(img: any) {

    return img.replace("https", "http");

  }

  getPostContent(){
    
      this.postService
        .getPostContent(this.postToShare.post_content)
        .subscribe(datalist => {
          console.log(datalist);

          if (datalist.content_images_list) {
            this.postToShare._albums = [];
            this.postToShare._real_albums = [];
            datalist.content_images_list.forEach(element => {
              this.imageService.getImage(element).subscribe(imgData => {
                console.log(imgData);
                const src = imgData.image_storage_url.replace("https", "http");
                const caption = imgData.image_name;
                const thumb = imgData.image_storage_url.replace(
                  "https",
                  "http"
                );
                if (imgData.image_id == this.postToShare.img_id) var is_preview = true;
                else is_preview = false;
                const album = {
                  src: src,
                  caption: caption,
                  thumb: thumb,
                  is_preview: is_preview
                };
                console.log(album);
                (album.is_preview) ? this.postToShare._albums.unshift(album) : this.postToShare._albums.push(album);
                this.postToShare._real_albums.push(imgData);

              });
            });
          }

          if (datalist.content_pdf_list) {
            
            this.postToShare._pdfs = [];
            datalist.content_pdf_list.forEach(element => {
              this.pdfService.getPdf(element).subscribe(pdfData => {
                console.log(pdfData);
                this.postToShare._pdfs.push(pdfData);
              });
            });
          }

          if (datalist.content_video_list) {
            
            this.postToShare._videos = [];
            datalist.content_video_list.forEach(element => {
              this.videoService.getVideo(element).subscribe(videoData => {
                console.log(videoData);
                this.postToShare._videos.push(videoData);
              });
            });
          }
        });
    }
  
}
