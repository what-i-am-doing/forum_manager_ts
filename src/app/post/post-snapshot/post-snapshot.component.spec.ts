import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostSnapshotComponent } from './post-snapshot.component';

describe('PostSnapshotComponent', () => {
  let component: PostSnapshotComponent;
  let fixture: ComponentFixture<PostSnapshotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostSnapshotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostSnapshotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
