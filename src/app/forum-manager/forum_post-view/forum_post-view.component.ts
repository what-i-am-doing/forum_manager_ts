import { Component, OnInit, QueryList, Input } from '@angular/core';
import { UserService } from '../../services/user.service';
import { GroupService } from '../../services/group.service';
import { ForumPostService } from '../../services/forum_post.service';
import { ImageService } from '../../services/image.service';
import { CommonService } from 'src/app/services/common.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LevelService } from 'src/app/services/level.service';
import { TagService } from 'src/app/services/tag.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { AddressService } from 'src/app/services/address.service';
import {MatCardModule} from '@angular/material/card';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
// tslint:disable-next-line: component-selector
  selector: 'app-forum_post-view',
  templateUrl: './forum_post-view.component.html',
  styleUrls: ['./forum_post-view.component.css'] ,
  providers: [UserService, AddressService, GroupService, ForumPostService, ImageService,   CommonService, LevelService, TagService]


})
export class ForumPostViewComponent implements OnInit {
   @Input()forumId: string;
  friends: Array<any> = [];
  friendData: Array<any> = [];
  userName: string;
  forumAllPosts: Array<any> = [];
  forumPostImages: Array<string>;
  userId: string;
  userGroupId: Array<string>;
  userTag: string = localStorage.getItem('user_tag');
  active = false;
  currentThing: any;
  currentProfile: any;
  addFriendsec = false;
  users: any;
  public user_auth_id: string;

  userMainDetails: any;
  forumpostTagFriends: Array<any> = [];
  fromSize = 1;
  toSize = 5;
  public profilePic: any;
  public userGroupList: any = JSON.parse(localStorage.getItem('all_groups'));
  public activeUserAllData: any;
  public allUserData: Array<any> = [];
  public activeUserPersonalData: any;
  public activeUserEducationalData: any;
//  public realfriendsForSuggestoionList: Array<any> = [];
  public groupId: any;
//  public allLevels: Array<any> = [];
  public allTags: Array<any> = [];
 // public curLevel: string = "all";
  public curTag = 'all';
  datePickerConfig: Partial<BsDatepickerConfig>;
  bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();
  public selected_date = '7';
  totalUnchangedForumPosts: Array<any> = [];
  fromDate: number;
  toDate: number;
  isOnlyFriend = false;
  friendIDS: Array<string> = [];
  fromPost = 0;
  noMorePosts = false;
  public city: any;
  public profilePercentage = 0;
  isLikes = false;
  isComments = false;
  isShares = false;
  likeArrow = false;
  comArrow = false;
  shareArrow = false;
  newForumPostCreated = false;

    sByName = '';
  forumPic: any;
 
  idpassed: any;

  status = 'ACTIVE';
  allLevels: any;

  constructor(
    private imageService: ImageService,
    private forumPostService: ForumPostService,
    private groupService: GroupService,
    private service: UserService,

    private commonService: CommonService,
    private levelService: LevelService,
    private tagService: TagService,
    private addressService: AddressService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.datePickerConfig = Object.assign({}, { containerClass: 'theme-dark-blue' });
    this.maxDate.setDate(this.maxDate.getDate() - 7);
    this.bsRangeValue = [this.maxDate, this.bsValue];
    const userData = JSON.parse(localStorage.getItem('LoggedInUserData'));
    this.userId = userData.user_id;
    this.user_auth_id = userData.user_auth_id;

    this.userGroupId = userData.group_id;
    this.groupId = localStorage.getItem('groupId');
    this.forumId = localStorage.getItem('forum_id');
    const ForumId: string = this.forumId;
    this.getforumId();
  }
    src: string;
  public gender: any;
  public hidden = true;
  public mobileFriendlyZoomPercent = false;

  getforumId() {
  
    this.allPostsForForum(this.forumId);
  }

  allPostsForForum(forumIdPassed) {
    console.log('iddddddd', forumIdPassed);
    this.forumPostService.getPostsForForumByStatus(forumIdPassed, this.groupId, this.status, this.fromPost, '20').subscribe((posts: any) => {
      console.log('rrrrannnnnn', posts);
      posts.forEach(element => {
        if (!element.comments_counter) {
          element.comments_counter = 0;
        }

        if (!element.likes_counter) {
          element.likes_counter = 0;
        }

        if (!element.shares_counter) {
          element.shares_counter = 0;
        }
      });

      this.forumAllPosts = posts;
    //  localStorage.setItem('user_forum_posts' , JSON.stringify(posts));
      this.totalUnchangedForumPosts = posts.slice(0);
      this.fromPost = this.fromPost + 5;
      console.log(this.forumAllPosts);
  
    });
  }

   getcreatedForumPost(event: any) {
  //  console.log(event);
  // this.forumAllPosts.unshift(event);
  // this.totalUnchangedForumPosts.unshift(event);
  this.newForumPostCreated = true;
 // this.allPostsForForum(this.idPassedFromParams);

}

 /**
  * name
  */
 public get mobileFriendlyZoom(): string | undefined {
    if (this.mobileFriendlyZoomPercent) {
      return '200%';
    }
    return undefined;
  }

  ngOnInit() {
     this.groupService.getUserlistByGroup(this.groupId, 0, 100).subscribe(data => this.allUserData = data);
    // this.getFriendListOfActiveUser(this.userId, this.userGroupId[0], 0, 100);
    // let x = +new Date() / 1000;
    // console.log(new Date(x * 1000));
    // need this   API to remove, from here
    // this.activeUserAllData = this.commonService.getUserAllDetails(this.userId, this.userGroupId[0]);
    // console.log(this.activeUserAllData);
    this.getUsersDetails(this.userId);
    // this.getUserEducationalDetails(this.userId);
    // this.getUserPersonalDetails(this.userId);
    // this.levelService.searchLevelByGroup(this.groupId).subscribe(data => this.allLevels = data);
    // this.tagService.searchTagByGroup(this.groupId).subscribe(data => this.allTags = data);
  }
  getFriendListOfActiveUser(userId: any, group_id, from, to) {
    this.service.getFriendListOfActiveUser(userId, group_id, 0, 100).subscribe(data => {
      console.log('Real friend List of Logged in users');
      console.log(data);
      data.forEach(element => {
        this.friends.push(element);
      });

      this.friends.forEach(element => {
        this.service.getUserDetails(element.friend_user_id).subscribe(data => {
          console.log(data);
          this.friendData.push(data);
          this.friendIDS.push(data.user_id);
        });
      });
      console.log(this.friendData);
    });
  }

  getUsersDetails(userId) {
      this.service.getUserDetails(userId).subscribe(data => {
      this.userMainDetails = data;
      //  console.log(Object(data).values);
      if (this.userMainDetails.first_name != null && this.userMainDetails.first_name != '' && this.userMainDetails.first_name != 'string') {
        this.profilePercentage = this.profilePercentage + 10;
      }
      if (this.userMainDetails.last_name != null && this.userMainDetails.last_name != '' && this.userMainDetails.last_name != 'string') {
        this.profilePercentage = this.profilePercentage + 10;
      }
      if (this.userMainDetails.image_id != null && this.userMainDetails.image_id != '' && this.userMainDetails.image_id != 'string') {
        this.profilePercentage = this.profilePercentage + 10;
      }
      if (this.userMainDetails.roll_number != null && this.userMainDetails.roll_number != '' && this.userMainDetails.roll_number != 'string') {
        this.profilePercentage = this.profilePercentage + 5;
      }
      if (this.userMainDetails.user_full_text_name != null && this.userMainDetails.user_full_text_name != '' && this.userMainDetails.user_full_text_name != 'string') {
        this.profilePercentage = this.profilePercentage + 5;
      }

      if (data.image_id && data.image_id != 'string') { 
        this.getProfile(this.userMainDetails.image_id); }
      this.userName = this.userMainDetails.user_full_text_name;
      localStorage.setItem(
        'userMainDetails',
        JSON.stringify(this.userMainDetails)
      );
      this.userMainDetails = JSON.parse(
        localStorage.getItem('userMainDetails')
      );
    });
  }

  getProfile(image_id) {
    this.imageService.getImageByType(image_id, 'thumbnail').subscribe(data => {
      if (data.length) {
        this.profilePic = this.URLChange(data[0].image_storage_url);
        localStorage.setItem('activeProfilePic', this.URLChange(data[0].image_storage_url));
      }
      else {
        this.getProfileImage(image_id);
      }
    })
  }

  getProfileImage(image_id) {
      this.imageService.getImage(image_id).subscribe(data => {
        this.profilePic = this.URLChange(data.image_storage_url);
        localStorage.setItem('activeProfilePic', this.profilePic);
      })
    }

  getUserEducationalDetails(userId) {


    this.service.getEducationDetailsDataByUserId(userId).subscribe(data => {

      console.log('education data');
      this.activeUserEducationalData = data;

      if (data.class_id != null && data.class_id != '' && data.class_id != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.course_id != null && data.course_id != '' && data.course_id != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.education_type_id != null && data.education_type_id != '' && data.education_type_id != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.institution_id != null && data.institution_id != '' && data.institution_id != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.section_id != null && data.section_id != '' && data.section_id != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.stream_id != null && data.stream_id != '' && data.stream_id != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }


    });
  }

  getUserPersonalDetails(userId) {


    this.service.getUserPersonalDetails(userId).subscribe(data => {
      console.log('getActiveUserPersonalDetails');
      console.log(data);
      this.activeUserPersonalData = data;
      this.gender = data.gender_type;
      console.log(this.gender);
      localStorage.setItem('activeGender', this.gender);
      this.getAdress(data.address_id);
      if (data.address_id != null && data.address_id != '' && data.address_id != 'string') {

        this.profilePercentage = 5;
      }
      if (data.age != null && data.age != '' && data.age > 0) {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.date_of_birth != null && data.date_of_birth != '' && data.date_of_birth > 0) {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.mobile_number != null && data.mobile_number != '' && data.mobile_number != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      // if(data.address_id!=null && data.address_id!=''&&data.address_id!='string'){

      //   this.profilePercentage= this.profilePercentage+5
      // }


    });



  }

  getAdress(adressId) {
    this.addressService.getAdress(adressId).subscribe(data => {
      console.log('adress data found');
      console.log(data);

      this.city = data.city;

      if (data.area != null && data.area != '' && data.area != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.city != null && data.city != '' && data.city != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.country != null && data.country != '' && data.country != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.landmark != null && data.landmark != '' && data.landmark != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.plot_number != null && data.plot_number != '' && data.plot_number != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.state != null && data.state != '' && data.state != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.street_name != null && data.street_name != '' && data.street_name != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }
      if (data.zip != null && data.zip != '' && data.zip != 'string') {

        this.profilePercentage = this.profilePercentage + 5;
      }

    })
    if (this.profilePercentage > 100) {
      this.profilePercentage = 100;
    }
  }

    viewMore(pagenumber, pagesize) {
      console.log(' view more clicked');
      this.toSize = this.toSize + pagesize;
    }
      postMessageFormat(message: string) {
        return message.substring(0, 2000) + '...<a class=\'see-more\'>See More</a>';
      }
     userSwitchGroup(groupId) {
        const switchGroupObj = {
          group_id: [groupId],
          user_id: this.userId
        };
        this.groupService
          .userSwitchGroup(switchGroupObj, groupId, this.userId)
          .subscribe(data => {
            console.log(data);
            if (data === true) {
              localStorage.setItem('user_group', JSON.stringify([groupId]));
            }
          });
      }

      seeFriendProfile(userId) {
        this.router.navigate(['/home/userprofile'], { queryParams: { userId: userId } });
      }

      URLChange(img: any) {
        return img.replace('https', 'http');
      }
      // includeShared(event: any) {
      //   this.forumAllPosts.unshift(event);
      //   this.totalUnchangedForumPosts.unshift(event);
      // }

      getForumImage(image_id) {
        if (image_id != '' || image_id != 'string' || image_id != null) {


          this.imageService.getImage(image_id).subscribe(data => {
            this.forumPic = this.URLChange(data.image_storage_url);
            localStorage.setItem('activeProfilePic', this.forumPic);
          }


          );
        }
      }


      // onScroll(event: any) {
      //   console.log(event);
      //   this.forumPostService.getPostsForForumByStatus(this.groupId, this.forumId, this.status, this.fromPost, 5).subscribe((forum_posts: any) => {
      //     console.log(forum_posts);

      //     if (forum_posts.length) {
      //       forum_posts.forEach(element => {
      //         if (!element.comments_counter) {
      //           element.comments_counter = 0;
      //         }

      //         if (!element.likes_counter) {
      //           element.likes_counter = 0;
      //         }

      //         if (!element.shares_counter) {
      //           element.shares_counter = 0;
      //         }
      //       });

      //       this.forumAllPosts = this.forumAllPosts.concat(forum_posts).slice(0);
      //       this.totalUnchangedForumPosts = this.forumAllPosts.slice(0);
      //       this.fromPost = this.fromPost + 5;
      //       console.log(this.forumAllPosts);
      //     } else { this.noMorePosts = true; }

      //   });

      // }


    // End brackets -->
  }



