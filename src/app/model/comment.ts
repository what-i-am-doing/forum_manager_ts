export class Comment {

 constructor() { };

private comment_id:string;
private comment_on_entity: string;
private comments: string;
private creation_timestamp:number;
private entity_id:string;
private from_user_id: string;
private group_id: string;
private last_updated_timestamp: number;
private like_counter: string;
private message: string;
private parent_comment_id: string;
private report_counter: number;
private status: string;
private tagged_mapping_list:string [];


    /**
     * Getter $comment_id
     * @return {string}
     */
	public get $comment_id(): string {
		return this.comment_id;
	}

    /**
     * Getter $comment_on_entity
     * @return {string}
     */
	public get $comment_on_entity(): string {
		return this.comment_on_entity;
	}

    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $creation_timestamp
     * @return {number}
     */
	public get $creation_timestamp(): number {
		return this.creation_timestamp;
	}

    /**
     * Getter $entity_id
     * @return {string}
     */
	public get $entity_id(): string {
		return this.entity_id;
	}

    /**
     * Getter $from_user_id
     * @return {string}
     */
	public get $from_user_id(): string {
		return this.from_user_id;
	}

    /**
     * Getter $group_id
     * @return {string}
     */
	public get $group_id(): string {
		return this.group_id;
	}

    /**
     * Getter $last_updated_timestamp
     * @return {number}
     */
	public get $last_updated_timestamp(): number {
		return this.last_updated_timestamp;
	}

    /**
     * Getter $like_counter
     * @return {string}
     */
	public get $like_counter(): string {
		return this.like_counter;
	}

    /**
     * Getter $message
     * @return {string}
     */
	public get $message(): string {
		return this.message;
	}

    /**
     * Getter $parent_comment_id
     * @return {string}
     */
	public get $parent_comment_id(): string {
		return this.parent_comment_id;
	}

    /**
     * Getter $report_counter
     * @return {number}
     */
	public get $report_counter(): number {
		return this.report_counter;
	}

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Getter $tagged_mapping_list
     * @return {string []}
     */
	public get $tagged_mapping_list(): string [] {
		return this.tagged_mapping_list;
	}

    /**
     * Setter $comment_id
     * @param {string} value
     */
	public set $comment_id(value: string) {
		this.comment_id = value;
	}

    /**
     * Setter $comment_on_entity
     * @param {string} value
     */
	public set $comment_on_entity(value: string) {
		this.comment_on_entity = value;
	}

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $creation_timestamp
     * @param {number} value
     */
	public set $creation_timestamp(value: number) {
		this.creation_timestamp = value;
	}

    /**
     * Setter $entity_id
     * @param {string} value
     */
	public set $entity_id(value: string) {
		this.entity_id = value;
	}

    /**
     * Setter $from_user_id
     * @param {string} value
     */
	public set $from_user_id(value: string) {
		this.from_user_id = value;
	}

    /**
     * Setter $group_id
     * @param {string} value
     */
	public set $group_id(value: string) {
		this.group_id = value;
	}

    /**
     * Setter $last_updated_timestamp
     * @param {number} value
     */
	public set $last_updated_timestamp(value: number) {
		this.last_updated_timestamp = value;
	}

    /**
     * Setter $like_counter
     * @param {string} value
     */
	public set $like_counter(value: string) {
		this.like_counter = value;
	}

    /**
     * Setter $message
     * @param {string} value
     */
	public set $message(value: string) {
		this.message = value;
	}

    /**
     * Setter $parent_comment_id
     * @param {string} value
     */
	public set $parent_comment_id(value: string) {
		this.parent_comment_id = value;
	}

    /**
     * Setter $report_counter
     * @param {number} value
     */
	public set $report_counter(value: number) {
		this.report_counter = value;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}

    /**
     * Setter $tagged_mapping_list
     * @param {string []} value
     */
	public set $tagged_mapping_list(value: string []) {
		this.tagged_mapping_list = value;
	}

}