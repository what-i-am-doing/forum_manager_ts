import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule  } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { ReviewManagerComponent } from './review-manager/review-manager.component';
import { MatIconModule} from "@angular/material";
import { MatButtonModule} from '@angular/material/button';
import { PostModalComponent } from './post-modal/post-modal.component';
import { LightboxModule } from "ngx-lightbox";
import { NgxExtendedPdfViewerModule } from "ngx-extended-pdf-viewer";
import { UserInfoComponent } from './user-info/user-info.component';
import { MatRadioModule} from '@angular/material/radio';

@NgModule({
  declarations: [ReviewManagerComponent, PostModalComponent, UserInfoComponent],
  imports: [
    CommonModule,
    FormsModule,
    DataTablesModule,
    MatIconModule,
    MatButtonModule,
    MatRadioModule,
    LightboxModule,
    NgxExtendedPdfViewerModule
  ]
})

export class PostReviewModule { }
