export class Forum{

    constructor(){};
    private comments: string;
    private created_by_user:string;
    private creation_timestamp:number;
    private custom_tag_line:string;
    private enabled_for_groups:string[];
    private formatted_course_description:string;
    private forum_admin_user_id:string[];
    private forum_description:string;
    private forum_id:string;
    private forum_name:string;
    private forum_type:string;
    private img_id:string;
    private last_updated_timestamp:number;
    private status:string;


    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $created_by_user
     * @return {string}
     */
	public get $created_by_user(): string {
		return this.created_by_user;
	}

    /**
     * Getter $creation_timestamp
     * @return {number}
     */
	public get $creation_timestamp(): number {
		return this.creation_timestamp;
	}

    /**
     * Getter $custom_tag_line
     * @return {string}
     */
	public get $custom_tag_line(): string {
		return this.custom_tag_line;
	}

    /**
     * Getter $enabled_for_groups
     * @return {string[]}
     */
	public get $enabled_for_groups(): string[] {
		return this.enabled_for_groups;
	}

    /**
     * Getter $formatted_course_description
     * @return {string}
     */
	public get $formatted_course_description(): string {
		return this.formatted_course_description;
	}

    /**
     * Getter $forum_admin_user_id
     * @return {string[]}
     */
	public get $forum_admin_user_id(): string[] {
		return this.forum_admin_user_id;
	}

    /**
     * Getter $forum_description
     * @return {string}
     */
	public get $forum_description(): string {
		return this.forum_description;
	}

    /**
     * Getter $forum_id
     * @return {string}
     */
	public get $forum_id(): string {
		return this.forum_id;
	}

    /**
     * Getter $forum_name
     * @return {string}
     */
	public get $forum_name(): string {
		return this.forum_name;
	}

    /**
     * Getter $forum_type
     * @return {string}
     */
	public get $forum_type(): string {
		return this.forum_type;
	}

    /**
     * Getter $img_id
     * @return {string}
     */
	public get $img_id(): string {
		return this.img_id;
	}

    /**
     * Getter $last_updated_timestamp
     * @return {number}
     */
	public get $last_updated_timestamp(): number {
		return this.last_updated_timestamp;
	}

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $created_by_user
     * @param {string} value
     */
	public set $created_by_user(value: string) {
		this.created_by_user = value;
	}

    /**
     * Setter $creation_timestamp
     * @param {number} value
     */
	public set $creation_timestamp(value: number) {
		this.creation_timestamp = value;
	}

    /**
     * Setter $custom_tag_line
     * @param {string} value
     */
	public set $custom_tag_line(value: string) {
		this.custom_tag_line = value;
	}

    /**
     * Setter $enabled_for_groups
     * @param {string[]} value
     */
	public set $enabled_for_groups(value: string[]) {
		this.enabled_for_groups = value;
	}

    /**
     * Setter $formatted_course_description
     * @param {string} value
     */
	public set $formatted_course_description(value: string) {
		this.formatted_course_description = value;
	}

    /**
     * Setter $forum_admin_user_id
     * @param {string[]} value
     */
	public set $forum_admin_user_id(value: string[]) {
		this.forum_admin_user_id = value;
	}

    /**
     * Setter $forum_description
     * @param {string} value
     */
	public set $forum_description(value: string) {
		this.forum_description = value;
	}

    /**
     * Setter $forum_id
     * @param {string} value
     */
	public set $forum_id(value: string) {
		this.forum_id = value;
	}

    /**
     * Setter $forum_name
     * @param {string} value
     */
	public set $forum_name(value: string) {
		this.forum_name = value;
	}

    /**
     * Setter $forum_type
     * @param {string} value
     */
	public set $forum_type(value: string) {
		this.forum_type = value;
	}

    /**
     * Setter $img_id
     * @param {string} value
     */
	public set $img_id(value: string) {
		this.img_id = value;
	}

    /**
     * Setter $last_updated_timestamp
     * @param {number} value
     */
	public set $last_updated_timestamp(value: number) {
		this.last_updated_timestamp = value;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}
    
}
