import { UserEducational } from '../../model/userEducational';
import { UserPersonal } from '../../model/userPersonal';
import { Level } from '../../model/level';
import { Role } from '../../model/role';
import { userGroupMapping } from '../../model/user-group-mapping';
import { Component, ViewChild, AfterViewInit, OnInit, ElementRef, OnChanges } from '@angular/core';
import { Group } from '../../model/group';
import { TREE_ACTIONS, KEYS, ITreeOptions, TreeComponent } from 'angular-tree-component';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import * as XLSX from 'xlsx';
import { GroupService } from '../../services/group.service';
import { UserService } from 'src/app/services/user.service';
import { PostService } from 'src/app/services/post.service';
import { LevelService } from 'src/app/services/level.service';
import { TagService } from 'src/app/services/tag.service';
import { AdminService } from '../../services/admin.service';
import { RoleService } from '../../services/role.service';
import { UserGroupMappingService } from '../../services/user-group-mapping.service';
import { FormControl } from '@angular/forms';
import { Tag } from 'src/app/model/tag';
import { AddressService } from 'src/app/services/address.service';
import { Address } from 'src/app/model/address';
import { ImageService } from 'src/app/services/image.service';
import { Admin } from 'src/app/model/admin';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker'
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}



@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
  providers: [GroupService, UserService, ImageService, UserPersonal, UserEducational, TagService, AddressService, PostService, RoleService]
})

export class AdminComponent implements AfterViewInit, OnChanges, OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings[] = [];
  dtTrigger: Subject<any>[] = [];
  public date: any;
  public bsValue: Date = null;
  modalbsValue: Date = null;
  public selectedGender: any;
  postReview: any;
  userName: string;
  onValueChange(value: Date): void {
    this.date = value;
  }

  genders: any = ['MALE', 'FEMALE'];
  radioChangeHandler(event: any) {
    console.log('gender selected');
    this.selectedGender = event.target.value;
    console.log(this.selectedGender)


  }
  public datePickerConfig: Partial<BsDatepickerConfig>;
  expanded: boolean = false;
  public group = new Group();
  public group_name: string;
  public custom_tag_line: string;
  public comments: string;
  public success: boolean = false;
  public formvisible: boolean = false;
  public addUserformvisible: boolean = false;
  public addLevelformvisible: boolean = false;
  public userAdded: boolean = false;
  public first_name: string;
  public last_name: string;
  public hide = true;
  public password: string;
  public error: boolean = false;
  public userId: any;
  public email: any;
  public groupName: any;
  public groupId: any;
  public uesrGroupList: Array<any> = []
  public userEducationData = new UserEducational();
  public userPersonalData = new UserPersonal();
  public myvarexpand = true;
  public levelAdded: boolean = false;
  public levelName: string = "";
  public levelCustomTag: string = "";
  public userGroupId = localStorage.getItem("groupId");
  public allLevels: Array<any> = [];
  public addressId: any;
  userPosts: Array<any> = [];
  tagForm: boolean = false;
  curNode: any;
  nodeDelete: boolean = false;
  tagName: string = "";
  public tagCustomTag: string = "";
  totalNodes: number = 0;
  tagPath: string;
  file: any;
  curImage: any = null;
  curNodeId: number = 0;
  ELEMENT_DATA = [];
  public mobile_number: any;
  dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
  selectedLevel: any = "0";
  selectedTag: any = "0";
  gotTags: Array<any> = [];
  roleAdded:boolean = false;
  user = JSON.parse(localStorage.getItem("UserAllData"));
  CLIENT_ADMIN: boolean = false;
  GROUP_ADMIN: boolean = false;
  LEVEL_ADMIN: boolean = false;
  TAG_ADMIN: boolean = false;
  CLIENT_ADMIN_LIST: Array<any> = [];
  GROUP_ADMIN_LIST: Array<any> = [];
  LEVEL_ADMIN_LIST: Array<any> = [];
  TAG_ADMIN_LIST: Array<any> = [];
  Activated = {
    type: null,
    id: null
  };

  adminCreated: boolean = false;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  constructor(private imageService: ImageService, private groupService: GroupService, private addressService: AddressService, private userService: UserService, private levelService: LevelService, private tagService: TagService,
    private adminService: AdminService, private userGroupMappingService: UserGroupMappingService, private postService: PostService,
    private roleService: RoleService

  ) {
    this.datePickerConfig = Object.assign({}, { containerClass: 'theme-dark-blue' });
  }
  
  ngOnInit() {

    console.log(this.tree);
    this.dropdownList = [
      { item_id: 1, item_text: 'View' },
      { item_id: 2, item_text: 'Edit' },
      { item_id: 3, item_text: 'Delete' },
      { item_id: 4, item_text: 'Move' },
      { item_id: 5, item_text: 'Download' }
    ];
    this.selectedItems = [];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };
    this.dtTrigger["new"] = new Subject<any>();
    this.dtOptions["new"] = {
      pagingType: 'full_numbers',
      pageLength: 10,
      
    };

    // console.log(JSON.parse(localStorage.getItem("user_posts")));
    //this.userPosts = JSON.parse(localStorage.getItem("user_posts"));
    this.postService.getAllPosts("public", 0, 5).subscribe(
      data => {
        console.log(data);
        this.userPosts = data;
        setTimeout(() => {
          this.dtTrigger['new'].next();
        });

      }
    );

    this.user.admin_mapping_list.forEach(element => {
      this.adminService.getAdmin(element).subscribe(
        admin => {
          console.log(admin);
          if (admin.admin_type == 'CLIENT_ADMIN') {
            this.CLIENT_ADMIN = true;
            this.getClientGroupList(admin.for_entity_id[0]);
          }

          if (admin.admin_type == 'GROUP_ADMIN') {
            this.GROUP_ADMIN = true;
            this.groupService.getGroupDetails(admin.for_entity_id[0]).subscribe(

              grp => {
                console.log(grp);
                this.GROUP_ADMIN_LIST.push(grp);
              }
            );
          }

          if (admin.admin_type == 'LEVEL_ADMIN') {
            this.LEVEL_ADMIN = true;
          }

          if (admin.admin_type == 'TAG_ADMIN') {
            this.TAG_ADMIN = true;
          }

        }
      );
    });

  }

  ngOnDestroy() {
    this.dtTrigger["new"].unsubscribe();
  }

  ngOnChanges(): void { }

  ngAfterViewInit() {

    console.log("ngAfterViewInit");
    this.dataSource.filterPredicate = this.createFilter();

    this.nameFilter.valueChanges
      .subscribe(
        name => {
          this.filterValues.name = name;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )

    // this.snoFilter.valueChanges
    //   .subscribe(
    //     colour => {
    //       this.filterValues.position = colour;
    //       this.dataSource.filter = JSON.stringify(this.filterValues);
    //     }
    //   )
    this.imageFilter.valueChanges
      .subscribe(
        pet => {
          this.filterValues.image = pet;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )

    this.classFilter.valueChanges
      .subscribe(
        pet => {
          this.filterValues.weight = pet;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )

    this.rollnoFilter.valueChanges
      .subscribe(
        pet => {
          this.filterValues.symbol = pet;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
  }

  @ViewChild('tree') private tree: TreeComponent;


  showOptions: boolean = false;
  active: boolean = false;
  showDropdown: boolean = false;
  currentRow: any;
  allRowsSelected: boolean = false;
  private paginator: MatPaginator;
  private sort: MatSort;
  // data table
  displayedColumns: string[] = ['select', 'image', 'name', 'weight', 'symbol', 'star'];


  @ViewChild('TABLE') table: ElementRef;

  filterValues = {

    image: '',
    name: '',
    weight: '',
    symbol: ''
  };

 
  onItemSelect(item: any) {
    console.log(item);
  }

  onSelectAll(items: any) {
    console.log(items);
  }


  ExportTOExcel() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'Student_Data.xlsx');

  }

  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  @ViewChild(MatSort) set matSort(ms: MatSort) {
    this.sort = ms;
    this.setDataSourceAttributes();
  }

  setDataSourceAttributes() {

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;


  }
  nameFilter = new FormControl('');
  // snoFilter = new FormControl('');
  imageFilter = new FormControl('');
  classFilter = new FormControl('');
  rollnoFilter = new FormControl('');
  selection = new SelectionModel<PeriodicElement>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => { this.selection.select(row) });
  }

  applyFilter(filterValue: string) {
    console.log("applyFilter() called");
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  //data table ends




  user_id: string = localStorage.getItem("user");
  group_details: Group[];
  notInGroup: boolean = true;
  nodes: Array<any> = [];


  options: ITreeOptions = {

    isExpandedField: 'expanded',
    idField: 'id',
    hasChildrenField: 'nodes',
    useVirtualScroll: true,
    actionMapping: {
      mouse: {
        dblClick: (tree, node, $event) => {
          if (node.hasChildren) TREE_ACTIONS.TOGGLE_EXPANDED(tree, node, $event);
        }
      },
      keys: {
        [KEYS.ENTER]: (tree, node, $event) => {
          node.expandAll();
        }
      }
    },

    allowDrag: (node) => {
      return false;
    },
    allowDrop: (node) => {
      return false;
    },

    allowDragoverStyling: true,
    animateExpand: true,
    scrollOnActivate: true,
    animateSpeed: 30,
    animateAcceleration: 1.2,
    scrollContainer: document.documentElement // HTML
  }



  onActivated(event) {

    var element: any = document.getElementsByClassName('tree-node-level-' + event.node.level + ' tree-node-active tree-node-focused')[0];
    console.log(element);
    element.style.border = "2px solid red";

  }


  toggleTree() {

    (this.expanded = !this.expanded) ? this.tree.treeModel.expandAll() : this.tree.treeModel.collapseAll();
    this.myvarexpand = !this.myvarexpand;
  }

  createFilter(): (data: any, filter: string) => boolean {

    console.log("createFilter() called");
    let filterFunction = function (data, filter): boolean {
      let searchTerms = JSON.parse(filter);
      return (
        data.name.toLowerCase().indexOf(searchTerms.name) !== -1

        && data.symbol.toLowerCase().indexOf(searchTerms.symbol) !== -1)
    }
    return filterFunction;
  }

  getClientGroupList(clinet_id: string) {

    this.groupService.getAllGroupList(clinet_id).subscribe(data => {

      this.CLIENT_ADMIN_LIST = data;

    })


  }

  addToGroupList(data) {

    console.log('received group');
    console.log(data);
    this.group_details.push(data);
  }


  createGroup() {

    var group = new Group;
    group.$client_id = "Y696JmoBT5ChMQxrB5ba";
    group.$custom_tag_line = this.custom_tag_line;
    group.$group_name = this.group_name;
    group.$status = "ACTIVE";

    if (this.file != undefined && this.file != null) {
      this.imageService.uploadImage(this.file).subscribe(img => {
        console.log(img);
        group.$image_id = img.id;
        console.log(group);
        this.file = null;
        this.submitGroup(group);
      }
      );
    }

    else this.submitGroup(group);



  }

  submitGroup(group: any) {

    this.groupService.createUserGroup(group, localStorage.getItem('user')).subscribe(data => {

      console.log(data);
      this.getGroupDetails(data.id)

    })
  }

  getGroupDetails(groupId) {

    this.groupService.getGroupDetails(groupId).subscribe(data => {

      console.log(data);
      this.CLIENT_ADMIN_LIST.push(data);

    }
    )
  }


  addUser(form: any): void {

    var userRegisterObj =
    {

      "user": {

        "comments": "string",
        "creation_timestamp": 0,
        "current_status_type_id": "string",
        "custom_user_id": this.first_name + " " + this.last_name,
        "first_name": this.first_name,
        "group_id": ["public"],
        "image_id": "string",
        "last_name": this.last_name,
        "last_updated_timestamp": 0,
        "roll_number": "string",
        "status": "ACTIVE",
        "status_message": "string",
        "user_full_text_name": this.first_name + " " + this.last_name,
        "user_name": this.first_name + " " + this.last_name
      },
      "user_auth_details": {
        "comments": "string",
        "cookie_id": "string",
        "creation_timestamp": 0,
        "email": this.email,
        "group_id": [
          "public"
        ],
        "last_updated_timestamp": 0,
        "password": this.password,
        "status": "ACTIVE",
        "user_name": this.first_name + " " + this.last_name
      }

    }


    console.log(userRegisterObj);
    try {

      this.userService.addUser(userRegisterObj, this.first_name)
        .subscribe(data => {
          console.log(data);
          this.userId = data.id;
          this.addUserEducationDetails(form);

        })


    } catch (err) {

      console.log(" if  called inside user register");
      this.error = true;
      setTimeout(() => this.error = false, 3000)

    }
  }

  addUserEducationDetails(form: any) {

    this.userEducationData.$class_id = '';
    this.userEducationData.$course_id = '';
    this.userEducationData.$stream_id = '';
    this.userEducationData.$education_type_id = '';
    this.userEducationData.$institution_id = '';
    this.userEducationData.$user_id = this.userId;
    this.userEducationData.$section_id = '';
    this.userEducationData.$status = 'ACTIVE';
    this.userEducationData.$creation_timestamp = Date.now();
    this.userService.addUserEducationalDetail(this.userEducationData, this.userId).subscribe(data => {
      console.log("user educational details added");

      this.addAdress(form);

    })

  }


  addUserPersonalDetails(form: any) {
    if (this.bsValue) {
      var dateofbirth = this.bsValue.toString();

    }
    var myDate = []
    try {
      myDate = dateofbirth.split("(");
      var tempdob = new Date(myDate[0]);
      var dob = tempdob.getTime()

      console.log(Date.now());
      console.log(dob);


    } catch (err) {


    }
    var mobileno = this.mobile_number;
    console.log(mobileno);
    console.log(mobileno.internationalNumber);
    var mobile = mobileno.internationalNumber;

    this.userPersonalData.$address_id = '';
    this.userPersonalData.$age = 0;
    this.userPersonalData.$report_card_entry_list = [''];
    this.userPersonalData.$date_of_birth = dob;
    this.userPersonalData.$gender_type = this.selectedGender;
    this.userPersonalData.$interests_list = [''];
    this.userPersonalData.$mobile_number = mobile;
    this.userPersonalData.$status = 'ACTIVE';
    this.userPersonalData.$user_documents_list = [''];
    this.userPersonalData.$user_id = this.userId;

    this.userService.addUserPersonalDetails(this.userPersonalData, this.userId).subscribe(data => {

      console.log("personal details added");
      console.log(data);
      this.userAdded = true;
      setTimeout(() => this.userAdded = false, 3000);
      this.userService.getUserByUserID(this.userId).subscribe(
        user => {
          console.log(user[0]);
          if (this.selectedLevel != '0' && this.selectedTag != '0') {
            var userGroup = new userGroupMapping;
            userGroup.$creation_timestamp = +new Date;
            userGroup.$group_id = this.groupId;
            userGroup.$level_id = this.selectedLevel;
            userGroup.$status = "ACTIVE";
            userGroup.$tag_id = this.selectedTag;
            userGroup.$user_id = this.userId;

            this.userGroupMappingService.createUserGroupMapping(userGroup).subscribe(
              grp => {
                form.reset();
                this.bsValue = null
                this.mobile_number = null;
                this.mobile_number = '';
                this.mobile_number = [];
                this.userService.updateUserDetail({ "user_group_mapping_details": [grp.id] }, this.userId).subscribe(
                  data => console.log(data)
                );
              },
              (err) => {
                console.log(err)
              }
            );
          }
          this.ELEMENT_DATA.push(user[0]);
          this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
        }
      );

    })

  }

  switchGroup(event: any) {
    //alert(groupId);
    console.log(event);
    this.groupName = event.group_name;
    this.groupId = event.group_id;
    this.Activated.type = "GROUP";
    this.Activated.id = event.group_id;
    this.nodes = [];
    this.curNodeId = 0;
    this.levelService.searchLevelByGroup(this.groupId).subscribe(
      data => {
        console.log(data);
        this.allLevels = data;

        var zeroSequence = data.filter(seq => seq.sequence_number == 0);
        if (zeroSequence.length) {
          if (zeroSequence[0].image_id) {
            this.imageService.getImage(zeroSequence[0].image_id).subscribe(
              img => {
                this.curImage = this.URLChange(img.image_storage_url);
                this.initializeNode(zeroSequence[0]);
              }
            );
          }

          else this.initializeNode(zeroSequence[0]);
        }



        console.log(this.nodes);
      }
    );

    this.getAllGroupUsersList(this.groupId, 0, 100);
    document.getElementById(this.groupName).classList.add('cur_poi_selected');

  }

  initializeNode(node: any) {
    this.curNodeId = this.curNodeId + 1;
    this.nodes = [{
      id: this.curNodeId,
      real_id: node.level_id,
      name: node.level_name,
      sequence: 0,
      type: 1,
      image: this.curImage
    }];

    this.curImage = null;
    this.addLevelChildren(this.nodes[0], "root");
  }


  addLevelChildren(level: any, ID: string) {
    this.tagService.searchTagByLevel(level.real_id).subscribe(
      data => {
        console.log(data);
        level.children = [];
        var nextLevel = this.allLevels.filter(seq => seq.sequence_number == level.sequence + 1);
        console.log(nextLevel);
        data.forEach(element => {
          console.log(element.path_from_group_root.substring(element.path_from_group_root.lastIndexOf("/") + 1));
          if (ID == element.path_from_group_root.substring(element.path_from_group_root.lastIndexOf("/") + 1)) {
            this.curNodeId = this.curNodeId + 1;
            if (element.image_id) {
              this.imageService.getImage(element.image_id).subscribe(
                img => {
                  this.curImage = this.URLChange(img.image_storage_url);
                  level.children.push({
                    id: this.curNodeId,
                    real_id: element.tag_id,
                    level_id: element.level_id,
                    name: element.tag_name,
                    type: 2,
                    path: element.path_from_group_root,
                    image: this.curImage

                  });

                  this.curImage = null;
                  console.log("pushed");
                  this.tree.treeModel.update();

                }
              );
            }
            else {
              level.children.push({
                id: this.curNodeId,
                real_id: element.tag_id,
                level_id: element.level_id,
                name: element.tag_name,
                type: 2,
                path: element.path_from_group_root,
                image: this.curImage

              });

              this.curImage = null;
              console.log("pushed");
              this.tree.treeModel.update();
            }
          }




        });

        if (nextLevel.length) {

          if (nextLevel[0].image_id) {
            this.imageService.getImage(nextLevel[0].image_id).subscribe(
              img => {
                this.curImage = this.URLChange(img.image_storage_url);
                level.children.forEach(element => {
                  console.log(element);
                  this.curNodeId = this.curNodeId + 1;
                  element.children = [{
                    id: this.curNodeId,
                    real_id: nextLevel[0].level_id,
                    name: nextLevel[0].level_name,
                    sequence: nextLevel[0].sequence_number,
                    type: 1,
                    image: this.curImage
                    //is level ke wo children(tag) jinka parent ye tag hai
                  }];

                  this.tree.treeModel.update();
                  console.log("pushed children");
                  console.log(this.nodes);
                  this.addLevelChildren(element.children[0], element.real_id);
                });
              }
            );
          }
          else
            level.children.forEach(element => {
              console.log(element);
              this.curNodeId = this.curNodeId + 1;
              element.children = [{
                id: this.curNodeId,
                real_id: nextLevel[0].level_id,
                name: nextLevel[0].level_name,
                sequence: nextLevel[0].sequence_number,
                type: 1,
                image: this.curImage
                //is level ke wo children(tag) jinka parent ye tag hai
              }];

              this.tree.treeModel.update();
              console.log("pushed children");
              console.log(this.nodes);
              this.addLevelChildren(element.children[0], element.real_id);
            });

          this.curImage = null;

        }


      });
  }



  getAllGroupUsersList(groupId, fromSize, toSize) {
    this.groupService.getUserlistByGroup(groupId, fromSize, toSize).subscribe(data => {
      console.log('user group list');
      console.log(data);
      this.ELEMENT_DATA = data;
      this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);

    })
  }

  addLevel(tree: any) {
    console.log(tree);
    var level = new Level();
    level.$creation_timestamp = +new Date;
    level.$custom_tag_line = this.levelCustomTag;
    level.$level_name = this.levelName;
    level.$group_id = this.groupId;
    level.$level_type_id = this.levelName;
    if (!this.allLevels.length) level.$sequence_number = 0;
    else level.$sequence_number = this.allLevels.length;
    if (this.file != undefined && this.file != null) {
      this.imageService.uploadImage(this.file).subscribe(img => {
        console.log(img);
        level.$image_id = img.id;
        console.log(level);
        this.file = null;
        this.createLevel(level);
      }
      );
    }

    else this.createLevel(level);

  }

  createLevel(level: any) {
    console.log(level);
    this.levelService.createLevel(level).subscribe(
      data => {


        console.log(data);
        this.curNodeId = this.curNodeId + 1;
        this.allLevels.push({
          id: this.curNodeId,
          level_id: data.id,
          real_id: data.id,
          level_name: level.$level_name,
          sequence: level.$sequence_number,
          type: 1
        });
        this.levelAdded = true;
        console.log(this.nodes);
        console.log(level.$sequence_number);

        if (level.$image_id) {
          this.imageService.getImage(level.$image_id).subscribe(
            Image => {
              console.log(Image);
              console.log(data);
              this.curImage = this.URLChange(Image.image_storage_url);
              this.setLevelInTree(level, data);
            }

          );
        }

        else this.setLevelInTree(level, data);

      }
    );
  }

  setLevelInTree(level, data) {
    console.log(level);
    console.log(data);
    if (level.$sequence_number == 0) {
      this.nodes = [{
        id: this.curNodeId,
        real_id: data.id,
        name: level.$level_name,
        sequence: level.$sequence_number,
        type: 1,
        image: this.curImage
      }];


      //this.tree.treeModel.update();
    }
    else {

      if (this.nodes[0].children) this.filterChildrenForSequence(this.nodes, level.$sequence_number - 1, level, data);

    }
  }

  submitTag(level: any) {
    // var parent = Object.assign({},level);
    console.log(level.parent);

    var tag = new Tag();
    tag.$tag_name = this.tagName;
    tag.$custom_tag_line = this.tagCustomTag;
    tag.$creation_timestamp = +new Date;
    tag.$group_id = this.groupId;
    tag.$level_id = level.data.real_id;
    tag.$status = "ACTIVE";
    if (level.data.sequence == 0) tag.$path_from_group_root = "/root";
    else tag.$path_from_group_root = level.parent.data.path + "/" + level.parent.data.real_id;
    // this.getTagPath(this.nodes[0].children , level);

    console.log(tag);


    if (this.file != undefined && this.file != null) {
      this.imageService.uploadImage(this.file).subscribe(img => {
        console.log(img);
        tag.$image_id = img.id;
        console.log(tag);
        this.file = null;
        this.createTag(tag, level);
      }
      );
    }

    else this.createTag(tag, level);

  }

  createTag(tag: any, level: any) {
    this.tagService.createTag(tag).subscribe(
      data => {
        console.log(data);


        if (tag.$image_id) {
          this.imageService.getImage(tag.$image_id).subscribe(
            Image => {
              console.log(Image);
              console.log(data);
              this.curImage = this.URLChange(Image.image_storage_url);
              this.setTagInTree(tag, level, data);
            }

          );
        }

        else { this.setTagInTree(tag, level, data); }

        this.tagForm = false;
        console.log(this.nodes);
      }
    );
  }

  setTagInTree(tag: any, level: any, data: any) {

    var node: any = this.nodes.filter(
      node => node.id == level.data.id
    );

    if (node.length) {
      console.log(node[0]);
      console.log(!node[0].children);
      if (!node[0].children) node[0].children = [];
      this.curNodeId = this.curNodeId + 1;
      node[0].children.push({
        id: this.curNodeId,
        real_id: data.id,
        level_id: tag.$level_id,
        name: tag.$tag_name,
        type: 2,
        path: tag.$path_from_group_root,
        image: this.curImage
      });

      this.tree.treeModel.update();

    }

    else {
      if (this.nodes[0].children)
        this.filterChildren(this.nodes[0].children, tag, level.data.id, data.id);
    }
  }



  filterChildren(child: any, tag: any, idtosearch: any, idtoset: any) {
    var node = child.filter(node => node.id == idtosearch);
    if (node.length) {
      console.log(node[0]);
      console.log(!node[0].children);
      if (!node[0].children) node[0].children = [];
      this.curNodeId = this.curNodeId + 1;
      node[0].children.push({
        id: this.curNodeId,
        real_id: idtoset,
        name: tag.$tag_name,
        level_id: tag.$level_id,
        type: 2,
        path: tag.$path_from_group_root,
        image: this.curImage
      });


      this.tree.treeModel.update();

    }

    else {
      child.forEach(element => {
        if (element.children)
          this.filterChildren(element.children, tag, idtosearch, idtoset);
      });

    }

  }

  filterChildrenForSequence(child: any, sequence: any, level: any, data: any) {
    var node = child.filter(node => node.sequence == sequence);

    if (node.length) {
      console.log("found");
      node[0].children.forEach(element => {
        element.children = [];
        this.curNodeId = this.curNodeId + 1;
        element.children.push({
          id: this.curNodeId,
          real_id: data.id,
          name: level.$level_name,
          sequence: level.$sequence_number,
          type: 1,
          image: this.curImage
        });

        this.tree.treeModel.update();

        console.log("pushed");
      });


    }

    else {
      child.forEach(element => {
        if (element.children) this.filterChildrenForSequence(element.children, sequence, level, data);
      });
    }
  }

  getTagPath(rootChild: any, level: any) {

    for (var i = 0; i < rootChild.length; i++) {
      var node = rootChild[i].filter(element => element.id == level.id);
      if (node.length) { break; }
      if (rootChild[i].children) this.getTagPath(rootChild[i].children, level);
    }

  }



  addAdress(form: any) {
    var adressobj: Address = new Address();
    adressobj.$area = '';
    adressobj.$city = '';
    adressobj.$country = '';
    adressobj.$landmark = '';
    adressobj.$plot_number = '';
    adressobj.$state = '';
    adressobj.$street_name = '';
    adressobj.$zip = '';


    this.addressService.addAdress(adressobj).subscribe(data => {
      console.log('address Id')
      console.log(data);
      this.addressId = data.id;
      this.addUserPersonalDetails(form);

    })

  }

  URLChange(url: any) {
    return url.replace("https", "http");
  }

  createGLTAdmin(user: any) {
    console.log(user);
    if (this.Activated.type == "GROUP") this.createAdmin('GROUP_ADMIN', [this.Activated.id], this.groupId, user);
    else if (this.Activated.type == "LEVEL") this.createAdmin('LEVEL_ADMIN', [this.Activated.id], this.groupId, user);
    else if (this.Activated.type == "TAG") this.createAdmin('TAG_ADMIN', [this.Activated.id], this.groupId, user);

  }


  createAdmin(admin_type: string, for_entity_id: string[], group_id: string, user: any) {
    var admin = new Admin;
    admin.$admin_type = admin_type;
    admin.$creation_timestamp = +new Date;
    admin.$for_entity_id = for_entity_id;
    admin.$group_id = group_id;
    admin.$status = "ACTIVE";

    this.adminService.createAdmin(admin).subscribe(
      admin => {
        if (user.admin_mapping_list)
          var ad_map = user.admin_mapping_list ? user.admin_mapping_list : [];
        if (ad_map[0] == "string") ad_map = [];
        ad_map.push(admin.id);
        this.userService.updateUserDetail({ "admin_mapping_list": ad_map }, user.user_id).subscribe(
          data => {
            this.adminCreated = true;
            console.log(data);
          }
        );

      }
    );
  }

  getTagsByLevel() {

    this.tagService.searchTagByLevel(this.selectedLevel).subscribe(
      tags => this.gotTags = tags
    );
  }

  getUsersList(event: any) {
    /*get  users at this level*/
    if (event.node.data.type == 1) {
      this.Activated.type = "LEVEL"
      this.Activated.id = event.node.data.real_id;
      this.levelService.userByLevel(this.groupId, event.node.data.real_id, 0, 100).subscribe(
        users => {
          this.ELEMENT_DATA = users;
          this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
        }
      );
    }
    /*get  users at this tag*/

    else {
      this.Activated.type = "TAG"
      this.Activated.id = event.node.data.real_id;
      this.tagService.userBytag(event.node.data.real_id, event.node.data.level_id, this.groupId, 0, 100).subscribe(
        users => {
          this.ELEMENT_DATA = users;
          this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
        }
      );
    }
  }

  getUserName(user: string) {
    this.userService.getUserDetails(user).subscribe(
      user => {
        this.userName = user.user_full_text_name;

      }
    );
  }

  roleName: string;
  addRole() {
    var role = new Role;
    role.$role_name = this.roleName;
    role.$group_id = this.groupId;
    role.$creation_timestamp = +new Date;

    this.roleService.createRole(role).subscribe(
      role => {
        console.log(role);
        this.roleAdded = true;
      }

    )
  }
}


