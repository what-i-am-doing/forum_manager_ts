export  class  Forum_Post  {

    constructor(){};


          private  comments:string;
          private  comments_counter:number;
          private  creation_timestamp:number;
          private  formatted_message: string;
          private  enabled_for_groups: string[];
          private  heading:string;
          private  img_id:string;
          private  last_updated_timestamp:number;
          private  is_tagged:boolean;
          private  likes_counter:number;
          private  message:string;
          private  original_post_id: string;
          private  original_user_id:string;
          private  post_content:string;
          private  post_has_data:boolean;
          private  post_type:string;
          private  preview_content_id: string;
          private  preview_content_type: string;
          private  reports_counter:number;
          private  shares_counter:number;
          private  status:string;
          private  tag_id:string;
          private  tagged_mapping_list:string[];
          private  user_id:string;
          private forum_id:string;
          private forum_post_content:string;
          private  with_tagged_mapping_list:string[];

    /**
     * Getter $original_post_id
     * @return {string}
     */
	public get $original_post_id(): string {
		return this.original_post_id;
	}

    /**
     * Getter $preview_content_id
     * @return {string}
     */
	public get $preview_content_id(): string {
		return this.preview_content_id;
	}

    /**
     * Getter $preview_content_type
     * @return {string}
     */
	public get $preview_content_type(): string {
		return this.preview_content_type;
	}

    /**
     * Getter $tagged_mapping_list
     * @return {string[]}
     */
	public get $tagged_mapping_list(): string[] {
		return this.tagged_mapping_list;
	}

    /**
     * Getter $with_tagged_mapping_list
     * @return {string[]}
     */
	public get $with_tagged_mapping_list(): string[] {
		return this.with_tagged_mapping_list;
	}

    /**
     * Setter $original_post_id
     * @param {string} value
     */
	public set $original_post_id(value: string) {
		this.original_post_id = value;
	}

    /**
     * Setter $preview_content_id
     * @param {string} value
     */
	public set $preview_content_id(value: string) {
		this.preview_content_id = value;
	}

    /**
     * Setter $preview_content_type
     * @param {string} value
     */
	public set $preview_content_type(value: string) {
		this.preview_content_type = value;
	}

    /**
     * Setter $tagged_mapping_list
     * @param {string[]} value
     */
	public set $tagged_mapping_list(value: string[]) {
		this.tagged_mapping_list = value;
	}

    /**
     * Setter $with_tagged_mapping_list
     * @param {string[]} value
     */
	public set $with_tagged_mapping_list(value: string[]) {
		this.with_tagged_mapping_list = value;
	}
      

    /**
     * Getter $original_user_id
     * @return {string}
     */
	public get $original_user_id(): string {
		return this.original_user_id;
	}

    /**
     * Setter $original_user_id
     * @param {string} value
     */
	public set $original_user_id(value: string) {
		this.original_user_id = value;
	}
          
    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $comments_counter
     * @return {number}
     */
	public get $comments_counter(): number {
		return this.comments_counter;
	}

    /**
     * Getter $creation_timestamp
     * @return {number}
     */
	public get $creation_timestamp(): number {
		return this.creation_timestamp;
	}
    /**
     * Getter $formatted_message
     * @return {string}
     */
	public get $formatted_message(): string {
		return this.formatted_message;
	}

    /**
     * Setter $formatted_message
     * @param {string} value
     */
	public set $formatted_message(value: string) {
		this.formatted_message = value;
	}
    

    /**
     * Getter $heading
     * @return {string}
     */
	public get $heading(): string {
		return this.heading;
	}

    /**
     * Getter $img_id
     * @return {string}
     */
	public get $img_id(): string {
		return this.img_id;
	}

    /**
     * Getter $last_updated_timestamp
     * @return {number}
     */
	public get $last_updated_timestamp(): number {
		return this.last_updated_timestamp;
	}

    /**
     * Getter $level_id
     * @return {string}
     */
	public get $enabled_for_groups(): string[] {
		return this.enabled_for_groups;
	}

    /**
     * Getter $likes_counter
     * @return {number}
     */
	public get $likes_counter(): number {
		return this.likes_counter;
	}

    /**
     * Getter $message
     * @return {string}
     */
	public get $message(): string {
		return this.message;
	}

    /**
     * Getter $post_content
     * @return {string}
     */
	public get $post_content(): string {
		return this.post_content;
    }

    /**
     * Getter $Forum_post_content
     * @return {string}
     */
	public get $forum_post_content(): string {
		return this.forum_post_content;
	}

    /**
     * Getter $post_has_data
     * @return {boolean}
     */
	public get $post_has_data(): boolean {
		return this.post_has_data;
    }
    
     /**
     * Getter $is_tagged
     * @return {boolean}
     */
	public get $is_tagged(): boolean {
		return this.is_tagged;
	}


    /**
     * Getter $post_type
     * @return {string}
     */
	public get $post_type(): string {
		return this.post_type;
	}

    /**
     * Getter $reports_counter
     * @return {number}
     */
	public get $reports_counter(): number {
		return this.reports_counter;
	}

    /**
     * Getter $shares_counter
     * @return {number}
     */
	public get $shares_counter(): number {
		return this.shares_counter;
	}

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Getter $tag_id
     * @return {string}
     */
	public get $tag_id(): string {
		return this.tag_id;
	}

    /**
     * Getter $user_id
     * @return {string}
     */
	public get $user_id(): string {
		return this.user_id;
    }

    /**
     * Getter $forum_id
     * @return {string}
     */
	public get $forum_id(): string {
		return this.forum_id;
    }
    

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $comments_counter
     * @param {number} value
     */
	public set $comments_counter(value: number) {
		this.comments_counter = value;
    }
    
    

    /**
     * Setter $creation_timestamp
     * @param {number} value
     */
	public set $creation_timestamp(value: number) {
		this.creation_timestamp = value;
	}

   

    /**
     * Setter $heading
     * @param {string} value
     */
	public set $heading(value: string) {
		this.heading = value;
    }
    
    
    /**
     * Setter $heading
     * @param {string} value
     */
	public set $forum_post_content(value: string) {
		this.forum_post_content = value;
	}

    /**
     * Setter $img_id
     * @param {string} value
     */
	public set $img_id(value: string) {
		this.img_id = value;
	}

    /**
     * Setter $last_updated_timestamp
     * @param {number} value
     */
	public set $last_updated_timestamp(value: number) {
		this.last_updated_timestamp = value;
	}

    /**
     * Setter $level_id
     * @param {string} value
     */
	public set $enabled_for_groups(value: string[]) {
		this.enabled_for_groups= value;
	}

    /**
     * Setter $likes_counter
     * @param {number} value
     */
	public set $likes_counter(value: number) {
		this.likes_counter = value;
	}

    /**
     * Setter $message
     * @param {string} value
     */
	public set $message(value: string) {
		this.message = value;
	}

    /**
     * Setter $post_content
     * @param {string} value
     */
	public set $post_content(value: string) {
		this.post_content = value;
	}

    /**
     * Setter $post_has_data
     * @param {boolean} value
     */
	public set $post_has_data(value: boolean) {
		this.post_has_data = value;
    }

    /**
     * Setter $is_tagged
     * @param {boolean} value
     */
	public set $is_tagged(value: boolean) {
		this.is_tagged= value;
    }
    


    /**
     * Setter $post_type
     * @param {string} value
     */
	public set $post_type(value: string) {
		this.post_type = value;
	}

    /**
     * Setter $reports_counter
     * @param {number} value
     */
	public set $reports_counter(value: number) {
		this.reports_counter = value;
	}

    /**
     * Setter $shares_counter
     * @param {number} value
     */
	public set $shares_counter(value: number) {
		this.shares_counter = value;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}

    /**
     * Setter $tag_id
     * @param {string} value
     */
	public set $tag_id(value: string) {
		this.tag_id = value;
	}

    /**
     * Setter $user_id
     * @param {string} value
     */
	public set $user_id(value: string) {
		this.user_id = value;
    }
    /**
     * Setter $user_id
     * @param {string} value
     */
	public set $forum_id(value: string) {
		this.forum_id = value;
	}
         
             
}