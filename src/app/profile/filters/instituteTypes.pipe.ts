import { Pipe,PipeTransform } from "@angular/core";


@Pipe({

    'name':'instituteTypes'
})

export class InstituteTypesPipe implements PipeTransform{

transform(value , search ){

   return value.filter(inst => inst.institution_type_id === search)[0].institution_type_name
}

}