import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Component, ElementRef, ViewChild, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete} from '@angular/material';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-auto-complete',
  templateUrl: './auto-complete.component.html',
  styleUrls: ['./auto-complete.component.css']
})
export class AutoCompleteComponent implements OnInit{

 

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  friendCtrl = new FormControl();
  filteredFriends: Observable<string[]>;
  friends: Array<any> =[];

  @Input() allFriends: Array<any>;

  @ViewChild('friendInput') friendInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  @Output() valueChange = new EventEmitter(); 
  
  constructor(private userService:UserService) {
    this.filteredFriends = this.friendCtrl.valueChanges.pipe(
        startWith(null),
        map((friend: string | null) => friend ? this._filter(friend) : this.allFriends.slice()));
  }

  ngOnInit(){
    console.log(this.allFriends);
  }



sendSelectedData(){
            console.log(this.friends);
            this.valueChange.emit(this.friends);
  }

  add(event: MatChipInputEvent): void {
    // Add friend only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    console.log(event);
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;
      
      // Add our friend
      if ((value || '').trim()) {

        this.friends.push( {
          id:Math.random(),
          name:value.trim()
        }
          );
       

      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.friendCtrl.setValue(null);
    }
  }

  remove(friend: any , index): void {
    this.friends.splice(index, 1);

  }

  selected(event: MatAutocompleteSelectedEvent): void {
    console.log(event);
    this.friends.push(event.option.value);
    this.friendInput.nativeElement.value = '';
    this.friendCtrl.setValue(null);
    console.log(this.friends);
  }

  private _filter(value: string): string[] {

    return this.allFriends.filter(friend => friend.first_name.toLowerCase().startsWith(value));

  }

  
}
