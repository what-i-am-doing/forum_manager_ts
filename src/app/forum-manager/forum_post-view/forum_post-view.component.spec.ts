/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Forum_postViewComponent } from './forum_post-view.component';

describe('Forum_postViewComponent', () => {
  let component: Forum_postViewComponent;
  let fixture: ComponentFixture<Forum_postViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Forum_postViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Forum_postViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
