export class Admin {

 constructor() { };
 
private admin_mapping_id: string;
private admin_type: string;
private comments: string;
private creation_timestamp: number;
private for_entity_id: string[];
private group_id: string;
private last_updated_timestamp: number;
private status: string;


    /**
     * Getter $admin_mapping_id
     * @return {string}
     */
	public get $admin_mapping_id(): string {
		return this.admin_mapping_id;
	}

    /**
     * Getter $admin_type
     * @return {string}
     */
	public get $admin_type(): string {
		return this.admin_type;
	}

    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $creation_timestamp
     * @return {number}
     */
	public get $creation_timestamp(): number {
		return this.creation_timestamp;
	}

    /**
     * Getter $for_entity_id
     * @return {string[]}
     */
	public get $for_entity_id(): string[] {
		return this.for_entity_id;
	}

    /**
     * Getter $group_id
     * @return {string}
     */
	public get $group_id(): string {
		return this.group_id;
	}

    /**
     * Getter $last_updated_timestamp
     * @return {number}
     */
	public get $last_updated_timestamp(): number {
		return this.last_updated_timestamp;
	}

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Setter $admin_mapping_id
     * @param {string} value
     */
	public set $admin_mapping_id(value: string) {
		this.admin_mapping_id = value;
	}

    /**
     * Setter $admin_type
     * @param {string} value
     */
	public set $admin_type(value: string) {
		this.admin_type = value;
	}

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $creation_timestamp
     * @param {number} value
     */
	public set $creation_timestamp(value: number) {
		this.creation_timestamp = value;
	}

    /**
     * Setter $for_entity_id
     * @param {string[]} value
     */
	public set $for_entity_id(value: string[]) {
		this.for_entity_id = value;
	}

    /**
     * Setter $group_id
     * @param {string} value
     */
	public set $group_id(value: string) {
		this.group_id = value;
	}

    /**
     * Setter $last_updated_timestamp
     * @param {number} value
     */
	public set $last_updated_timestamp(value: number) {
		this.last_updated_timestamp = value;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}

 
}