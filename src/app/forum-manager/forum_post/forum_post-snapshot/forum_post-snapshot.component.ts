import { Component, OnInit, Input } from '@angular/core';
import { UserService } from "src/app/services/user.service";
import { ImageService } from "src/app/services/image.service";
import { ForumPostService} from "src/app/services/forum_post.service";
import { PdfService  } from "src/app/services/pdf.service";
import { VideoService  } from "src/app/services/video.service";

@Component({
  selector: 'app-forum_post-snapshot',
  templateUrl: './forum_post-snapshot.component.html',
  styleUrls: ['./forum_post-snapshot.component.css'],
  providers: [UserService]
})

export class ForumPostSnapshotComponent implements OnInit {

  user: any;
  userPIC: string;
  firstName:string;
  lastName:string;

  constructor(private userService: UserService, private imageService: ImageService,private ForumPostService:ForumPostService,

    
    ) { }

  ngOnInit() {
    console.log(this.forum_postToShare);

    this.userService.getUserDetails(this.forum_postToShare.original_user_id).subscribe(
      user => {
        this.user = user;
        this.firstName = user.first_name;
        this.lastName = user.last_name;
        if (user.image_id && user.image != '') this.getuserThumb(user.image_id);
      }
    );

    if (this.forum_postToShare.forum_post_content) this.getForumPostContent();

  }

  @Input() forum_postToShare: any;
  @Input() forum_postUser: any;

  getuserThumb(ID: string) {
    this.imageService.getImageByType(ID, 'thumbnail').subscribe(data => {

      if (data.length) this.userPIC = this.URLChange(data[0].image_storage_url);
      else this.getUserImage(ID);

    })
  }


getUserImage(image_id: string) {
    this.imageService.getImage(image_id).subscribe(data => this.userPIC = this.URLChange(data.image_storage_url))
  }


  URLChange(img: any) {

    return img.replace("https", "http");

  }

  getForumPostContent(){
    
      this.ForumPostService
        .getForumPostContent(this.forum_postToShare.post_content)
        .subscribe(datalist => {
          console.log(datalist);

          if (datalist.content_images_list) {
            this.forum_postToShare._albums = [];
            this.forum_postToShare._real_albums = [];
            datalist.forum_content_images_list.forEach(element => {
              this.imageService.getImage(element).subscribe(imgData => {
                console.log(imgData);
                const src = imgData.image_storage_url.replace("https", "http");
                const caption = imgData.image_name;
                const thumb = imgData.image_storage_url.replace(
                  "https",
                  "http"
                );
                if (imgData.image_id == this.forum_postToShare.img_id) var is_preview = true;
                else is_preview = false;
                const album = {
                  src: src,
                  caption: caption,
                  thumb: thumb,
                  is_preview: is_preview
                };
                console.log(album);
                (album.is_preview) ? this.forum_postToShare._albums.unshift(album) : this.forum_postToShare._albums.push(album);
                this.forum_postToShare._real_albums.push(imgData);

              });
            });
          }

        
        });
    }
  
}
