export class tagMapping {
  constructor(){}

private comments: string;
private creation_timestamp: number;
private groupId: string;
private last_updated_timestamp: number;
private on_entity_id: string;
private on_entity_type: string;
private status: string;
private tag_description: string;
private tag_end_index: string;
private tag_index_position: number;
private tag_link: string;
private tag_mapping_id: string;
private tag_name: string;
private tag_start_index: string;
private tagged_entity_id: string;
private tagged_entity_type: string;


    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $creation_timestamp
     * @return {number}
     */
	public get $creation_timestamp(): number {
		return this.creation_timestamp;
	}

    /**
     * Getter $groupId
     * @return {string}
     */
	public get $groupId(): string {
		return this.groupId;
	}

    /**
     * Getter $last_updated_timestamp
     * @return {number}
     */
	public get $last_updated_timestamp(): number {
		return this.last_updated_timestamp;
	}

    /**
     * Getter $on_entity_id
     * @return {string}
     */
	public get $on_entity_id(): string {
		return this.on_entity_id;
	}

    /**
     * Getter $on_entity_type
     * @return {string}
     */
	public get $on_entity_type(): string {
		return this.on_entity_type;
	}

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Getter $tag_description
     * @return {string}
     */
	public get $tag_description(): string {
		return this.tag_description;
	}

    /**
     * Getter $tag_end_index
     * @return {string}
     */
	public get $tag_end_index(): string {
		return this.tag_end_index;
	}

    /**
     * Getter $tag_index_position
     * @return {number}
     */
	public get $tag_index_position(): number {
		return this.tag_index_position;
	}

    /**
     * Getter $tag_link
     * @return {string}
     */
	public get $tag_link(): string {
		return this.tag_link;
	}

    /**
     * Getter $tag_mapping_id
     * @return {string}
     */
	public get $tag_mapping_id(): string {
		return this.tag_mapping_id;
	}

    /**
     * Getter $tag_name
     * @return {string}
     */
	public get $tag_name(): string {
		return this.tag_name;
	}

    /**
     * Getter $tag_start_index
     * @return {string}
     */
	public get $tag_start_index(): string {
		return this.tag_start_index;
	}

    /**
     * Getter $tagged_entity_id
     * @return {string}
     */
	public get $tagged_entity_id(): string {
		return this.tagged_entity_id;
	}

    /**
     * Getter $tagged_entity_type
     * @return {string}
     */
	public get $tagged_entity_type(): string {
		return this.tagged_entity_type;
	}

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $creation_timestamp
     * @param {number} value
     */
	public set $creation_timestamp(value: number) {
		this.creation_timestamp = value;
	}

    /**
     * Setter $groupId
     * @param {string} value
     */
	public set $groupId(value: string) {
		this.groupId = value;
	}

    /**
     * Setter $last_updated_timestamp
     * @param {number} value
     */
	public set $last_updated_timestamp(value: number) {
		this.last_updated_timestamp = value;
	}

    /**
     * Setter $on_entity_id
     * @param {string} value
     */
	public set $on_entity_id(value: string) {
		this.on_entity_id = value;
	}

    /**
     * Setter $on_entity_type
     * @param {string} value
     */
	public set $on_entity_type(value: string) {
		this.on_entity_type = value;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}

    /**
     * Setter $tag_description
     * @param {string} value
     */
	public set $tag_description(value: string) {
		this.tag_description = value;
	}

    /**
     * Setter $tag_end_index
     * @param {string} value
     */
	public set $tag_end_index(value: string) {
		this.tag_end_index = value;
	}

    /**
     * Setter $tag_index_position
     * @param {number} value
     */
	public set $tag_index_position(value: number) {
		this.tag_index_position = value;
	}

    /**
     * Setter $tag_link
     * @param {string} value
     */
	public set $tag_link(value: string) {
		this.tag_link = value;
	}

    /**
     * Setter $tag_mapping_id
     * @param {string} value
     */
	public set $tag_mapping_id(value: string) {
		this.tag_mapping_id = value;
	}

    /**
     * Setter $tag_name
     * @param {string} value
     */
	public set $tag_name(value: string) {
		this.tag_name = value;
	}

    /**
     * Setter $tag_start_index
     * @param {string} value
     */
	public set $tag_start_index(value: string) {
		this.tag_start_index = value;
	}

    /**
     * Setter $tagged_entity_id
     * @param {string} value
     */
	public set $tagged_entity_id(value: string) {
		this.tagged_entity_id = value;
	}

    /**
     * Setter $tagged_entity_type
     * @param {string} value
     */
	public set $tagged_entity_type(value: string) {
		this.tagged_entity_type = value;
	}


}