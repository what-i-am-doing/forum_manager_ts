import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { ImageService } from '../services/image.service';
import { UserService } from '../services/user.service';
import { FriendService } from '../services/friend.service';
import { NavBarComponent } from '../nav-bar/nav-bar.component';
import { MatMenuTrigger } from '@angular/material/menu';
import { ActivatedRoute } from '@angular/router';
import { PostService } from '../services/post.service';
import { GroupService } from '../services/group.service';
import { AddressService } from '../services/address.service';


@Component({
  selector: 'app-viewprofilepage',
  templateUrl: './viewprofilepage.component.html',
  styleUrls: ['./viewprofilepage.component.css'],
  providers: [UserService, ImageService, AddressService, PostService, GroupService]
})
export class ViewprofilepageComponent implements OnInit {

  @ViewChild('myDiv') myDiv: ElementRef;
  groupId = localStorage.getItem("groupId");
  public showemojis: boolean = false;
  public friends: any;
  public arrowkeyLocation = 0;
  public uploadTrue: boolean = false;
  public active: boolean = false;
  public currentItem: any;
  public show_drop_menu: boolean = false;
  public userName: string;
  public userId: string;
  public user_auth_id: string;
  public status: string;
  public userGroupId: string;
  public displayName: string;
  name = 'Angular 4';
  Message = "default";
  public url: string = '';
  public receivedFriendList: any;
  public user_friendlist;
  public requestresult;
  public userMainDetails;
  public profilePic: any;
  public gender: any;
  public user:string = localStorage.getItem("user");
  firstName:string;
  lastName:string;
  allPosts: Array<any> = [];
  allUserData: Array<any> = [];
  activeUserEducationalData: any;
  public city: any;
  myFriend: boolean = false;
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;

  constructor(private route: ActivatedRoute, private addresService: AddressService, private postService: PostService,
         private imageService: ImageService, private service: UserService,
        private groupService: GroupService, public eleref: ElementRef, public navbar: NavBarComponent, private friendService:FriendService) {

  }

  public sub: any;
  public tempUserId: any;
  ngOnInit() {

    this.groupService.getUserlistByGroup(this.groupId, 0, 100).subscribe(data => this.allUserData = data);
    this.sub = this.route.queryParams.subscribe(params => {
      
      this.myFriend = false;
      this.firstName = null;
      this.lastName = null;
      this.profilePic = null;
      this.tempUserId = params.userId;
      this.getUserDetails(this.tempUserId);
      this.getActiveUserPersonalDetails(this.tempUserId);
      this.getUserEducationalDetails(this.tempUserId);


    });
    this.defaultProfilepic();



  }


  getUserDetails(userID:string) {
    this.service.getUserDetails(userID).subscribe(userdata => {
      
      if (JSON.parse(localStorage.getItem("my_friends")).includes(userID)) 
        this.myFriend = true;
      
      this.firstName = userdata.first_name;
      this.lastName  =  userdata.last_name;
      this.displayName = userdata.user_full_text_name;
      this.userGroupId = userdata.group_id[0];
      this.getFriendListOfActiveUser(this.tempUserId, this.userGroupId, 0, 100);

      if(userdata.image_id && userdata.image_id != 'string')
      this.getProfile(userdata.image_id);

      this.postService.getPostByUser(this.userGroupId, userID).subscribe(
        data => {

          this.allPosts = data;
          this.Message = data.length ? "data got" : "No data found";

        }
      );


    })

  }



  seeUpload() {
    this.uploadTrue = !this.uploadTrue;
  }

  defaultProfilepic() {

    if (this.url == '') {
      this.displayName = this.userName;
    }
  }


  getProfile(image_id:string) {
    
    this.imageService.getImageByType(image_id, 'thumbnail').subscribe(data => {
      
      if (data.length ) this.profilePic = this.URLChange(data[0].image_storage_url);
      else this.getProfileImage(image_id);
      
    })
  }

  getProfileImage(image_id:string) {
        this.imageService.getImage(image_id).subscribe(data => this.profilePic = this.URLChange(data.image_storage_url))
    
  }


  getFriendListOfActiveUser(userId: any, group_id, fromSize, toSize) {

    this.service.getFriendListOfActiveUser(userId, group_id, fromSize, toSize).subscribe(data => {


      this.receivedFriendList = data;
      console.log(this.receivedFriendList);

    })

  }


  URLChange(img: any) {
    console.log("convert image to http");
    console.log(img);
    return img.replace("https", "http");
  }


  ngOnDestroy() {
    this.sub.unsubscribe();
  }


  getActiveUserPersonalDetails(userId) {
    this.service.getUserPersonalDetails(userId).subscribe(data => {
      this.gender = data.gender_type;
      console.log(data.gender_type);
      console.log("gender");
      console.log(this.gender);
      this.getAdress(data.address_id)
    })
  }

  getUserEducationalDetails(userId) {
    this.service.getEducationDetailsDataByUserId(userId).subscribe(data => {
      console.log(data);
      this.activeUserEducationalData = data;

    })
  }

  getAdress(adressId) {
    this.addresService.getAdress(adressId).subscribe(data => {
      console.log('adress data found');
      console.log(data);
      this.city = data.city;
    })
  }


  unFriend() {
    this.friendService.deleteFriend(this.user,this.tempUserId).subscribe(data=>
      {
         console.log(data);
          this.myFriend = false;
      }

    )}

}
