export class UserKYC {

    constructor(){}
    
    private _user_id:string;
    private _kyc_name_by_user:string;
    private _kyc_type_id:string;
    private _kyc_id_value:string;
    private _img_id:string;
    private _stateId:string;
    private _comments:string;
    private _creation_timestamp:number;
    private _last_updated_timestamp:number;
    
        /**
         * Getter user_id
         * @return {string}
         */
        public get user_id(): string {
            return this._user_id;
        }
    
        /**
         * Getter kyc_name_by_user
         * @return {string}
         */
        public get kyc_name_by_user(): string {
            return this._kyc_name_by_user;
        }
    
        /**
         * Getter kyc_type_id
         * @return {string}
         */
        public get kyc_type_id(): string {
            return this._kyc_type_id;
        }
    
        /**
         * Getter kyc_id_value
         * @return {string}
         */
        public get kyc_id_value(): string {
            return this._kyc_id_value;
        }
    
        /**
         * Getter img_id
         * @return {string}
         */
        public get img_id(): string {
            return this._img_id;
        }
    
        /**
         * Getter stateId
         * @return {string}
         */
        public get stateId(): string {
            return this._stateId;
        }
    
        /**
         * Getter comments
         * @return {string}
         */
        public get comments(): string {
            return this._comments;
        }
    
        /**
         * Getter creation_timestamp
         * @return {number}
         */
        public get creation_timestamp(): number {
            return this._creation_timestamp;
        }
    
        /**
         * Getter last_updated_timestamp
         * @return {number}
         */
        public get last_updated_timestamp(): number {
            return this._last_updated_timestamp;
        }
    
        /**
         * Setter user_id
         * @param {string} value
         */
        public set user_id(value: string) {
            this._user_id = value;
        }
    
        /**
         * Setter kyc_name_by_user
         * @param {string} value
         */
        public set kyc_name_by_user(value: string) {
            this._kyc_name_by_user = value;
        }
    
        /**
         * Setter kyc_type_id
         * @param {string} value
         */
        public set kyc_type_id(value: string) {
            this._kyc_type_id = value;
        }
    
        /**
         * Setter kyc_id_value
         * @param {string} value
         */
        public set kyc_id_value(value: string) {
            this._kyc_id_value = value;
        }
    
        /**
         * Setter img_id
         * @param {string} value
         */
        public set img_id(value: string) {
            this._img_id = value;
        }
    
        /**
         * Setter stateId
         * @param {string} value
         */
        public set stateId(value: string) {
            this._stateId = value;
        }
    
        /**
         * Setter comments
         * @param {string} value
         */
        public set comments(value: string) {
            this._comments = value;
        }
    
        /**
         * Setter creation_timestamp
         * @param {number} value
         */
        public set creation_timestamp(value: number) {
            this._creation_timestamp = value;
        }
    
        /**
         * Setter last_updated_timestamp
         * @param {number} value
         */
        public set last_updated_timestamp(value: number) {
            this._last_updated_timestamp = value;
        }
    
    
    }