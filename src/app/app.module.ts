
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { ProfileComponent } from "./profile/profile.component";
import { NavBarComponent } from "./nav-bar/nav-bar.component";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { EducationTypesPipe } from "./profile/filters/educationTypes.pipe";
import { InstituteTypesPipe } from "./profile/filters/instituteTypes.pipe";
import { CourseTypesPipe } from "./profile/filters/courseTypes.pipe";
import { StreamTypesPipe } from "./profile/filters/streamTypes.pipe";
import { ClassTypesPipe } from "./profile/filters/classTypes.pipe";
import { SectionTypesPipe } from "./profile/filters/sectionTypes.pipe";
import { IdTypesPipe } from "./profile/filters/idTypes.pipe";
import { ViewprofileComponent } from "./profile/viewprofile/viewprofile.component";
import { ViewdashboardComponent } from "./dashboard/viewdashboard/viewdashboard.component";
import { FriendlistComponent } from "./friendlist/friendlist.component";
import { FilterPipe } from "./filter/filter.pipe";
import { NgxExtendedPdfViewerModule } from "ngx-extended-pdf-viewer";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatListModule,
  MatCardModule,
  MatIconModule,
  MatAutocompleteModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule
} from "@angular/material";
import { MatDialogModule } from "@angular/material/dialog";
import { GroupmodelComponent } from "./groupmodel/groupmodel.component";
import { AngularMultiSelectModule } from "../../node_modules/angular2-multiselect-dropdown";
import * as Cookies from "es-cookie";
import { SearchresultComponent } from "./searchresult/searchresult.component";
import { MatTabsModule } from "@angular/material/tabs";
import { ReactiveFormsModule } from "@angular/forms";
//import { YoutubePlayerModule } from 'ngx-youtube-player';
import { EmbedVideo } from "ngx-embed-video";
import { ChatModule } from "./chat/chat.module";
import { AdminModule } from "./admin/admin.module";
import { AppRoutingModule } from "./app-routing.module";
import { AccountsModule } from "./accounts/accounts.module";
import { MatMenuModule } from "@angular/material/menu";
import { LayoutModule } from "@angular/cdk/layout";
import { NgxSpinnerModule } from "ngx-spinner";
import { AboutusComponent } from "./ExternalComponent/aboutus/aboutus.component";
import { AboutusmainComponent } from "./ExternalComponent/aboutusmain/aboutusmain.component";
import { CompanyComponent } from "./ExternalComponent/company/company.component";
import { ProductComponent } from "./ExternalComponent/product/product.component";
import { PlatformComponent } from "./ExternalComponent/platform/platform.component";
import { TreeModule } from "angular-tree-component";
import { PostModule } from "./post/post.module";
import { FriendRequestComponent } from "./friend-request/friend-request.component";
import { UserfriendComponent } from "./profile/userfriend/userfriend.component";
import { DocumentComponent } from "./profile/document/document.component";
import { UploadThumbnailComponent } from "./dashboard/viewdashboard/upload-thumbnail/upload-thumbnail.component";
import { ViewprofilepageComponent } from "./viewprofilepage/viewprofilepage.component";
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { SuggestionlistComponent } from './dashboard/viewdashboard/suggestionlist/suggestionlist.component';
import { SearchPeopleCardComponent } from './searchresult/search-people-card/search-people-card.component';
import { SentrequestsComponent } from './friendlist/sentrequests/sentrequests.component';
import { BlockrequestsComponent } from './friendlist/blockrequests/blockrequests.component';
import { DeleterequestsComponent } from './friendlist/deleterequests/deleterequests.component';
import { ChangeGroupComponent } from './change-group/change-group.component';
import { AcceptNotificaionComponent } from './nav-bar/accept-notificaion/accept-notificaion.component';
import { NotificationComponent } from './nav-bar/notification/notification.component';
import { UserDetailComponent } from "./admin/admin/user-detail/user-detail.component";
import { AllnotificationComponent } from './allnotification/allnotification.component';
import { ViewAllNotificationComponent } from './nav-bar/view-all-notification/view-all-notification.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, PipeTransform, Pipe } from '@angular/core';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { WatchVideoComponent } from './searchresult/watch-video/watch-video.component';
import { UserIdleModule } from 'angular-user-idle';
import { MatSlideToggleModule} from '@angular/material/slide-toggle';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { AllFriendsComponent } from './profile/all-friends/all-friends.component';
import { DataTablesModule } from 'angular-datatables';
import { CourseManagerModule } from './course-manager/course-manager.module';
import { PostReviewModule } from "./post-review/post-review.module";
import { ForumManagerModule } from "./forum-manager/forum-manager.module";
import { UserDocumentComponent } from './profile/user-document/user-document.component';
import { ClickOutsideDirective } from './directives/click-outside.directive';

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) { }

  transform(url) {
    var url = url.replace('watch?v=', 'embed/');
    url = url + '' + '?modestbranding=1&;showinfo=0&;autohide=1&;rel=0;'
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ProfileComponent,
    NavBarComponent,
    FilterPipe,
    EducationTypesPipe,
    InstituteTypesPipe,
    CourseTypesPipe,
    StreamTypesPipe,
    ClassTypesPipe,
    SectionTypesPipe,
    IdTypesPipe,
    ViewprofileComponent,
    ViewdashboardComponent,
    FriendlistComponent,
    GroupmodelComponent,
    SearchresultComponent,
    AboutusComponent,
    AboutusmainComponent,
    CompanyComponent,
    ProductComponent,
    PlatformComponent,
    FriendRequestComponent,
    UserfriendComponent,
    DocumentComponent,
    UploadThumbnailComponent,
    ViewprofilepageComponent,
    SuggestionlistComponent,
    SearchPeopleCardComponent,
    SentrequestsComponent,
    BlockrequestsComponent,
    DeleterequestsComponent,
    ChangeGroupComponent,
    AcceptNotificaionComponent,
    UserDetailComponent,
    SafePipe,
    AcceptNotificaionComponent,
    NotificationComponent,
    AllnotificationComponent,
    ViewAllNotificationComponent, WatchVideoComponent, AllFriendsComponent, UserDocumentComponent, ClickOutsideDirective
  ],

  entryComponents: [GroupmodelComponent],

  imports: [
    BrowserModule,
    ChatModule,
    AdminModule,
    PostModule,
    FormsModule,
    LayoutModule,
    HttpClientModule,
    ReactiveFormsModule,
    AngularMultiSelectModule,
    NgxSpinnerModule,
    EmbedVideo,
    BsDatepickerModule.forRoot(),
    BrowserAnimationsModule,
    MatMenuModule,
    MatToolbarModule,
    MatSelectModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    MatDialogModule,
    MatTabsModule,
    MatIconModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    AppRoutingModule,
    AccountsModule,
    LayoutModule,
    NgxExtendedPdfViewerModule,
    MatSlideToggleModule,
    TreeModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot(),
    InfiniteScrollModule,
    NgxIntlTelInputModule,
    UserIdleModule.forRoot({ idle: 300, timeout: 60, ping: 60 }),
    DataTablesModule,
    CourseManagerModule,
    ForumManagerModule,
    PostReviewModule
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
  
}
