import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { MatIconModule,MatFormFieldModule,MatInputModule } from "@angular/material";
import { MatButtonModule} from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule} from '@angular/material/stepper';
import { CourseManagerComponent } from './course-manager/course-manager.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CourseThumbnailComponent } from './course-thumbnail/course-thumbnail.component';
import { CourseViewComponent } from './course-view/course-view.component';
import { DomSanitizer } from '@angular/platform-browser';
import { ModuleViewComponent } from './module-view/module-view.component';
import { SubModuleViewComponent } from './sub-module-view/sub-module-view.component';
import { CourseSearchComponent } from './course-search/course-search.component';
import { ModuleSearchComponent } from './module-search/module-search.component';
import {MatTreeModule} from '@angular/material/tree';
import { Routes, RouterModule } from '@angular/router';
import { ImageService } from "../services/image.service";
import { VideoService } from "../services/video.service";

import { CourseSearchViewComponent } from './course-search-view/course-search-view.component';
import { ModuleSearchViewComponent } from './module-search-view/module-search-view.component';
import { CourseSearchModuleViewComponent } from './course-search-module-view/course-search-module-view.component';
import { JoinedCourseViewComponent } from './joined-course-view/joined-course-view.component';
import { JoinedModuleViewComponent } from './joined-module-view/joined-module-view.component';
import { JoinedSubModuleViewComponent } from './joined-sub-module-view/joined-sub-module-view.component';

import {MatExpansionModule} from '@angular/material/expansion';

// import { CourseViewComponent }
@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) { }

  transform(url) {
    var url = url.replace('watch?v=', 'embed/');
    url = url + '' + '?modestbranding=1&;showinfo=0&;autohide=1&;rel=0;'
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}

const routes:Routes=[
  {path:'appCourseView',component:CourseSearchViewComponent},
  {path:'',component:CourseSearchViewComponent},
  {path:'appModuleView',component:ModuleSearchViewComponent },
  {path:'',component:ModuleSearchViewComponent}
  ]

@NgModule({ 
  declarations: [CourseManagerComponent, CourseThumbnailComponent, CourseViewComponent,SafePipe,
     ModuleViewComponent, SubModuleViewComponent, CourseSearchComponent,CourseSearchModuleViewComponent, ModuleSearchComponent, CourseSearchViewComponent,ModuleSearchViewComponent,JoinedCourseViewComponent,JoinedModuleViewComponent,JoinedSubModuleViewComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatStepperModule,
    MatCardModule,
    NgMultiSelectDropDownModule,
    MatTreeModule,
    MatExpansionModule,
    RouterModule.forChild(routes),
    
  ],
  exports:[CourseManagerComponent, CourseSearchComponent, ModuleSearchComponent , CourseViewComponent, JoinedCourseViewComponent, JoinedModuleViewComponent, JoinedSubModuleViewComponent,CourseSearchModuleViewComponent],
  providers:[ImageService,VideoService]

})
export class CourseManagerModule { }
