import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatGroupFriendListComponent } from './chat-group-friend-list.component';

describe('ChatGroupFriendListComponent', () => {
  let component: ChatGroupFriendListComponent;
  let fixture: ComponentFixture<ChatGroupFriendListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatGroupFriendListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatGroupFriendListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
