import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ForumService {

  private api_url = environment.api_url;
   constructor(private http: HttpClient) { }

  public addForum(addForumObj: any): Observable<any> {        //add new Forum using this function
                                                                //pass Forum obj here
    console.log(" Forum will be created");
    return this.http.post<any>(this.api_url +'/forum/', addForumObj, {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'from_user_id': localStorage.getItem('user')

      })
    })
    .pipe(
        catchError(this.handleError)
      );
    }


  public getForumsForGroup(groupId:any): Observable<any>{
    return this.http.post<any>(this.api_url +'search_by_group?group_id='+groupId+'public&size=100' , {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'from_user_id': localStorage.getItem('user')

      })
    })
    .pipe(
        catchError(this.handleError)
      );
  }

  

 deleteforum(forum_id:string):Observable<any>{               //delete forum using this function
        
         return this.http.delete<any>(this.api_url+'/forum/'+forum_id, {
          headers: new HttpHeaders({
            'Accept': 'application/json',
            'from_user_id': localStorage.getItem('user')
    
          })
        }).pipe(

          catchError(this.handleError)
        );
  }
  updateForum(forum_id:string,update:any):Observable<any>{
    return this.http.put<any>(this.api_url+'/forum/'+forum_id,update, {headers: new HttpHeaders({

      'Content-Type':  'application/json',
      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem("user")
      
    })
  })
    .pipe(
      catchError(this.handleError)
    );
  }

   updateforum_name(forum_name:any,entity_id:string):Observable<any>{            //update forum_name
    return this.http.put<any>(this.api_url+'/forum/'+entity_id,{"forum_name": forum_name}, {headers: new HttpHeaders({

      'Content-Type':  'application/json',
      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem("user")
      
    })
  })
    .pipe(
      catchError(this.handleError)
    );
  }

  updateforum_description(forum_description:any,entity_id:string):Observable<any>{   //update forum_description
    return this.http.put<any>(this.api_url+'/forum/'+entity_id,{ "forum_description":forum_description}, {headers: new HttpHeaders({

      'Content-Type':  'application/json',
      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem("user")
      
    })
  })
    .pipe(
      catchError(this.handleError)
    );
  }
  
  getRequestsForForumByStatus(forum_id:string,group_id:string,req_status:string):Observable<any>{
    return this.http.get<any>(this.api_url+'/forum_request/by_forum/by_status?group_id='+group_id+'&forum_id='+forum_id+'&req_status='+req_status+'&from=0&size=100',{headers: new HttpHeaders({
      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
    })
  } ).pipe(
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }
  //search forum by User_id here
   getForumsCreatedByAdmin(user_id:string,group_id:string):Observable<any>{
             
    return this.http.get<any>(this.api_url+'/forum/created_by_user?group_id='+group_id+'&user_id='+user_id,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }

//search forum by forum_ids here
   getForumsByIds(forum_id:string):Observable<any>{
             
    return this.http.get<any>(this.api_url + '/forum/' +  forum_id, {headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }



    //search Forums by search_text
   searchForumsByText(searchQuery,groupId,from,to):Observable<any>{
        
    return this.http.get<any>(this.api_url+'/forum/search_by_text?group_id='+groupId+'&search_text='+searchQuery+'&from='+from+'&size='+to,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );

   
  
   }

   private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

}
    
