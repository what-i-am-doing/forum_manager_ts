import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupmodelComponent } from './groupmodel.component';

describe('GroupmodelComponent', () => {
  let component: GroupmodelComponent;
  let fixture: ComponentFixture<GroupmodelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupmodelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupmodelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
