import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  private api_url = environment.api_url;

  constructor(private http: HttpClient) { }

  public addCourse(addcourseObj: any): Observable<any> {        //add new course using this function
                                                                //pass course obj here
    return this.http.post<any>(this.api_url +'/course/', addcourseObj, {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'from_user_id': localStorage.getItem('user')

      })
    })
      .pipe(
        catchError(this.handleError)
      );
  }

  deleteCourse(course_id:string):Observable<any>{               //delete course using this function
        
         return this.http.delete<any>(this.api_url+'/course/'+course_id, {
          headers: new HttpHeaders({
            'Accept': 'application/json',
            'from_user_id': localStorage.getItem('user')
    
          })
        }).pipe(

          catchError(this.handleError)
        );
  }

  updateCourse(course_id:string,update:any):Observable<any>{
    return this.http.put<any>(this.api_url+'/course/'+course_id,update, {headers: new HttpHeaders({

      'Content-Type':  'application/json',
      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem("user")
      
    })
  })
    .pipe(
      catchError(this.handleError)
    );
  }

  
    //search course by course_id here
   getCourseById(course_id:string):Observable<any>{
             
    return this.http.get<any>(this.api_url+'/course/'+course_id,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  //  localStorage.setItem('course_id',JSON.stringify('res.course_id'))
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }


    //search course by search_text
   searchCourseByText(searchQuery,groupId,from,to):Observable<any>{
        
    return this.http.get<any>(this.api_url+'/course/search_by_text?group_id='+groupId+'&search_text='+searchQuery+'&from='+from+'&size='+to,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }

   getModulesBySearchText(searchQuery,groupId,from,to):Observable<any>{
        
    return this.http.get<any>(this.api_url+'/module/search_by_text?group_id='+groupId+'&search_text='+searchQuery+'&from='+from+'&size='+to,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }

   getCourseByUser(userID:string , groupID:string):Observable<any>{
    return this.http.get<any>(this.api_url+`/course/created_by_user?group_id=${groupID}&user_id=${userID}&size=100`,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }
   
   createModule(module:any):Observable<any> {
    return this.http.post<any>(this.api_url +'/module/', module, {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'from_user_id': localStorage.getItem('user')

      })
    })
      .pipe(
        catchError(this.handleError)
      );
   }

   getModulesByCourse(course_id:string):Observable<any> {
    //  console.log('plplp',course_id)
    return this.http.get<any>(this.api_url+`/module/search_by_course?group_id=public&course_id=${course_id}&size=10`,{headers: new HttpHeaders({
      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user'),
    })
  } ).pipe(
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }

   ModuleByCourse(course_id:string):Observable<any> {
    //  console.log('plplp',course_id)
    return this.http.get<any>(this.api_url+`/module/search_by_course/top_level?group_id=public&course_id=${course_id}&size=10`,{headers: new HttpHeaders({
      'Accept':        'application/json',
      'from_user_id':  'ok'
    })
  } ).pipe(
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );
   }

   getModulesByIds(module_id:string):Observable<any> {
    return this.http.get<any>(this.api_url+`/module/search_by_module_ids?group_id=public&module_ids=${module_id}&from=0&size=100`,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    );  
   }

   ModuleByParentModule(parent:string,course_id:string):Observable<any> {
    return this.http.get<any>(this.api_url+`/module/search_by_parent_module?group_id=public&course_id=${course_id}&parent_module_id=${parent}&size=10`,{headers: new HttpHeaders({

      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem('user')
      
    })
  } ).pipe(
     
     retry(1), // retry a failed request up to 1 time
     catchError(this.handleError) // then handle the error
    ); 
   }

   updateModule(module_id:string,update:any):Observable<any>{
    return this.http.put<any>(this.api_url+'/module/'+module_id,update, {headers: new HttpHeaders({

      'Content-Type':  'application/json',
      'Accept':        'application/json',
      'from_user_id':  localStorage.getItem("user")
      
    })
  })
    .pipe(
      catchError(this.handleError)
    );
  }

   private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }; 
}