import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TempChatGroupComponent } from './temp-chat-group.component';

describe('TempChatGroupComponent', () => {
  let component: TempChatGroupComponent;
  let fixture: ComponentFixture<TempChatGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TempChatGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TempChatGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
