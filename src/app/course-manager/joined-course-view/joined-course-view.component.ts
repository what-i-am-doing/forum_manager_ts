
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ImageService } from "../../services/image.service";
import { VideoService } from "../../services/video.service";
import { Lightbox, LightboxConfig, LightboxEvent, LIGHTBOX_EVENT, IEvent } from "ngx-lightbox";
import { Subscription } from 'rxjs';
import { Course } from "../../model/course";
import { CourseService } from "../../services/course.service";
import { Route, Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-joined-course-view',
  templateUrl: './joined-course-view.component.html',
  styleUrls: ['./joined-course-view.component.css']
})

export class JoinedCourseViewComponent implements OnInit, OnChanges {

  constructor(private _formBuilder: FormBuilder, private imageService: ImageService,
  private videoService: VideoService,
  private courseService: CourseService,
  private _lightbox: Lightbox,
  private _lightboxEvent: LightboxEvent,
  private _lighboxConfig: LightboxConfig,private router:Router,private route:ActivatedRoute) { }
  basicGroup: FormGroup;
  imgGroup: FormGroup;
  detailGroup: FormGroup;
  courseGroup: FormGroup;
  descGroup: FormGroup;
  extData: FormGroup;
  private _subscription: Subscription;
  coUpdated: boolean = false;
  edit: boolean = false;
  coursePic: string;
  video: any;
  showExt: boolean = false;
  courseModules: any;
  activeLink: boolean = false;
  curModule: any;

  @Input() joinedcourse: any;
  @Input() readable?: boolean;
  
  path:any = [];

  ngOnInit() {
    this.courseModules = undefined;
    this.curModule = undefined;
    this.video = undefined;
    this.coursePic = undefined;

    //     this.route.queryParams.subscribe((obj:any)=>{
    //   // console.log('@@@courses',obj);
    //   this.idPassedFromParams  = obj.courses;
    // });

    // this.courseService.getCourseById(this.idPassedFromParams).subscribe((res:any)=>{
    //     console.log('resssssssssssssssss',res,this.course);
    //     this.course = {};
    //     this.course = res;
    //     console.log('reesssssssssssssssssssss2',this.course);
    // });


    this.basicGroup = this._formBuilder.group({
      basicCtrl: [{ value: this.joinedcourse.course_name, disabled: true }, Validators.required],
      basicCtrl2: [{ value: this.joinedcourse.course_type_id, disabled: true }, Validators.required],
    });

    this.imgGroup = this._formBuilder.group({
      imgCtrl1: [{ value: this.joinedcourse.heading, disabled: true }, Validators.required],
      imgCtrl2: [{ value: this.joinedcourse.status, disabled: true }, Validators.required],

    });

    this.detailGroup = this._formBuilder.group({
      detailCtrl1: [{ value: this.joinedcourse.category, disabled: true }, Validators.required],
      detailCtrl2: [{ value: this.joinedcourse.is_education_content, disabled: true }, Validators.required],
      detailCtrl3: [{ value: this.joinedcourse.language, disabled: true }, Validators.required]

    });

    this.courseGroup = this._formBuilder.group({
      courseCtrl1: [{ value: this.joinedcourse.course, disabled: true }, Validators.required],
      courseCtrl2: [{ value: this.joinedcourse.stream, disabled: true }, Validators.required],
      courseCtrl3: [{ value: this.joinedcourse.class, disabled: true }, Validators.required],
      courseCtrl4: [{ value: this.joinedcourse.sub_class, disabled: true }, Validators.required],
      courseCtrl5: [{ value: this.joinedcourse.subject, disabled: true }, Validators.required],
      courseCtrl6: [{ value: this.joinedcourse.chapter, disabled: true }, Validators.required],
      courseCtrl7: [{ value: this.joinedcourse.concept, disabled: true }, Validators.required]

    });

    this.descGroup = this._formBuilder.group({
      descCtrl1: [{ value: this.joinedcourse.course_description, disabled: true }]
    });

    this.extData = this._formBuilder.group({
      extDataCtrl1: ['']
    });

    if (this.joinedcourse.image_id) {
      this.imageService.getImage(this.joinedcourse.image_id).subscribe(
        img => this.coursePic = this.URLChange(img.image_storage_url)
      );
    }

    if (this.joinedcourse.course_content_list) {
      this.imageService.getContent(this.joinedcourse.course_content_list[0]).subscribe(

        content => {
          if (content.ext_link_data_list) {
            this.videoService.getExtVideo(content.ext_link_data_list[0]).subscribe(
              video_data => {
                this.videoService.getYouTubeVideoData(video_data.video_id).subscribe(
                  video => this.video = video
                )
              }
            );
          }

          if (content.content_images_list) {
            this.joinedcourse._albums = [];
            content.content_images_list.forEach(element => {
              this.imageService.getImage(element).subscribe(imgData => {
                console.log(imgData);
                const src = imgData.image_storage_url.replace("https", "http");
                const caption = imgData.image_name;
                const thumb = imgData.image_storage_url.replace(
                  "https",
                  "http"
                );

                const album = {
                  src: src,
                  caption: caption,
                  thumb: thumb

                };

                this.joinedcourse._albums.push(album);


              });
            });
          }
        }
      );
    }
    this.getCourses();
  }

  getCourses() {
    this.courseService.getCourseByUser(localStorage.getItem('user'),localStorage.getItem('groupId')).subscribe(
      courses => {
        console.log(courses);
        // this.userCourses = courses;
        courses.forEach((element) => {
          // this.TREE_DATA.push({id:element.course_id,name:element.course_name,course:element});
          console.log('________====',this.joinedcourse.course_id,element.course_id)

          if(this.joinedcourse.course_id == element.course_id){
            this.moduleListCall(element.course_id);
          }
        });
         
        // this.dataSource.data = this.TREE_DATA;
        // this.showCourse = courses[0];
      }
    );
    console.log('&&&&arrrrrrrrrrr',this.arrOfModules);
      localStorage.setItem('courseidformodule',this.joinedcourse.course_id);
  }
  arrOfModules=[];
  moduleListCall(courseId){
    console.log('Called....',courseId);
    this.courseService.ModuleByCourse(courseId).subscribe((res:any)=>{
      if(res.length > 0){
        this.arrOfModules.push(res);
      }
    });
  }
  

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    if (!changes.course.firstChange) this.ngOnInit();
  }

  editCourse() {
    this.edit = true;
    this.basicGroup.get('basicCtrl').enable();
    this.basicGroup.get('basicCtrl2').enable();
    this.imgGroup.get('imgCtrl1').enable();
    this.imgGroup.get('imgCtrl2').enable();
    this.detailGroup.get('detailCtrl1').enable();
    this.detailGroup.get('detailCtrl2').enable();
    this.detailGroup.get('detailCtrl3').enable();
    this.courseGroup.get('courseCtrl1').enable();
    this.courseGroup.get('courseCtrl2').enable();
    this.courseGroup.get('courseCtrl3').enable();
    this.courseGroup.get('courseCtrl4').enable();
    this.courseGroup.get('courseCtrl5').enable();
    this.courseGroup.get('courseCtrl6').enable();
    this.courseGroup.get('courseCtrl7').enable();
    this.descGroup.get('descCtrl1').enable();

    console.log(this.basicGroup);

  }

  updateCourse() {


    var course = new Course;
    course.$course_name = this.basicGroup.value.basicCtrl;
    course.$course_type_id = this.basicGroup.value.basicCtrl2;
    course.$heading = this.imgGroup.value.imgCtrl1;
    course.$status = this.imgGroup.value.imgCtrl2;
    course.$category = this.detailGroup.value.detailCtrl1;
    course.$is_education_content = this.detailGroup.value.detailCtrl2;
    course.$language = this.detailGroup.value.detailCtrl3;
    course.$course = this.courseGroup.value.courseCtrl1;
    course.$stream = this.courseGroup.value.courseCtrl2;
    course.$class = this.courseGroup.value.courseCtrl3;
    course.$sub_class = this.courseGroup.value.courseCtrl4;
    course.$subject = this.courseGroup.value.courseCtrl5;
    course.$chapter = this.courseGroup.value.courseCtrl6;
    course.$concept = this.courseGroup.value.courseCtrl7;
    course.$course_description = this.descGroup.value.descCtrl1;

    this.courseService.updateCourse(this.joinedcourse.course_id, course).subscribe(
      () => {
        this.coUpdated = true;
      //  this.updateView();
        this.removeEdit();
      }
    );
  }

 // updateView() {
 //   this.course.course_name = this.basicGroup.value.basicCtrl;
//this.course.course_type_id = this.basicGroup.value.basicCtrl2;
  //  this.course.heading = this.imgGroup.value.imgCtrl1;
  //this.course.status = this.imgGroup.value.imgCtrl2;
    //this.course.category = this.detailGroup.value.detailCtrl1;
 //   this.course.is_education_content = this.detailGroup.value.detailCtrl2;
   // this.course.language = this.detailGroup.value.detailCtrl3;

 //   this.course.course = this.courseGroup.value.courseCtrl1;
 //   this.course.stream = this.courseGroup.value.courseCtrl2;
 //   this.course.class = this.courseGroup.value.courseCtrl3;
 //   this.course.sub_class = this.courseGroup.value.courseCtrl4;
 //   this.course.subject = this.courseGroup.value.courseCtrl5;
 //   this.course.chapter = this.courseGroup.value.courseCtrl6;
 //   this.course.concept = this.courseGroup.value.courseCtrl7;
 //   this.course.course_description = this.descGroup.value.descCtrl1;
//  }

  removeEdit() {
    this.edit = false;
   // this.basicGroup.get('basicCtrl').disable();
    this.basicGroup.get('basicCtrl2').disable();
    this.imgGroup.get('imgCtrl1').disable();
    this.imgGroup.get('imgCtrl2').disable();
    this.detailGroup.get('detailCtrl1').disable();
    this.detailGroup.get('detailCtrl2').disable();
    this.detailGroup.get('detailCtrl3').disable();
    this.courseGroup.get('courseCtrl1').disable();
    this.courseGroup.get('courseCtrl2').disable();
    this.courseGroup.get('courseCtrl3').disable();
    this.courseGroup.get('courseCtrl4').disable();
    this.courseGroup.get('courseCtrl5').disable();
    this.courseGroup.get('courseCtrl6').disable();
    this.courseGroup.get('courseCtrl7').disable();
    this.descGroup.get('descCtrl1').disable();
  }


  URLChange(url: any) {
    return url.replace("https", "http");
  }


  open(index: number, album): void {
    this._subscription = this._lightboxEvent.lightboxEvent$.subscribe(
      (event: IEvent) => this._onReceivedEvent(event)
    );

    // override the default config
    this._lightbox.open(album, index, {
      wrapAround: true,
      showImageNumberLabel: true
    });
  }

  close(): void {
    // close lightbox programmatically
    this._lightbox.close();
  }

  private _onReceivedEvent(event: IEvent): void {
    if (event.id === LIGHTBOX_EVENT.CLOSE) {
      this._subscription.unsubscribe();
    }
  }

  modulesOfCourse() {
    
      this.courseService.ModuleByCourse(this.joinedcourse.course_id).subscribe(
        modules => {
          this.courseModules = modules;
          this.path.push({"id":this.joinedcourse.course_id,"path_name":this.joinedcourse.course_name});
        }
      );
    

    // else {
    //   this.courseService.ModuleByParentModule(this.curModule.module_id,this.course.course_id).subscribe(
    //     modules=>{
    //      this.courseModules = modules;
    //     }
    //   );
    // }
  }

  openModulesofModules(module:any) {
    this.courseService.ModuleByParentModule(module.module_id,this.joinedcourse.course_id).subscribe(
      modules=>{
       this.courseModules = modules;
      }
    );
  }

  showChildModules(event:any) {
     this.courseModules = event[0];
     this.path.push(event[1]);
     this.curModule = undefined;
     
  }

  backButton() {
    this.curModule = undefined;
    this.path.pop();
    if(!this.path.length) 
       this.courseModules = undefined;
    else if (this.path.length == 1) 
      this.courseService.ModuleByCourse(this.joinedcourse.course_id).subscribe(
        modules => this.courseModules = modules
       );
    else
      this.courseService.ModuleByParentModule(this.path[this.path.length -1].id,this.joinedcourse.course_id).subscribe(
        modules=>{
        this.courseModules = modules;
        }
      );
  }


  moduleShow:boolean = false;
  
  moduleView(moduleInfo){
    this.curModule = moduleInfo;
    this.moduleShow =true;
    console.log('moduleInfo',moduleInfo);
  }
}
