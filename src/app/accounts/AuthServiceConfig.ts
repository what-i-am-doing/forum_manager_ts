import {
    SocialLoginModule,
    AuthServiceConfig,
    GoogleLoginProvider
  } from 'angularx-social-login';
  
  export function getAuthServiceConfigs() {
  var    googleClientId='139417300527-o68k9jka1bu8sji6je4b8jof79n0vh87.apps.googleusercontent.com'
    let config = new AuthServiceConfig([
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider(googleClientId)
      }
    ]);
  
    return config;
  }