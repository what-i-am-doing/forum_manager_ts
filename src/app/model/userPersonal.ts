export class UserPersonal {
    
    constructor(){ }

    
    private address_id: string;
    private age: number;
    private comments: string;
    private user_id: string;
    private creation_timestamp: number;
    private date_of_birth: number;
    private gender_type: string;
    private interests_list: [string];
    private last_updated_timestamp: number
    private mobile_number: string;
    private report_card_entry_list: [string];
    private status: string;
    private user_documents_list: [string];
    
     /**
    * Getter $address_id
    * @return {string}
    */
    public get $address_id(): string {
    return this.address_id;
    }
    
    /**
    * Getter $age
    * @return {number}
    */
    public get $age(): number {
    return this.age;
    }
    
    /**
    * Getter $comments
    * @return {string}
    */
    public get $comments(): string {
    return this.comments;
    }
    
    /**
    * Getter $user_id
    * @return {string}
    */
    public get $user_id(): string {
    return this.user_id;
    }
    
    /**
    * Getter $creation_timestamp
    * @return {number}
    */
    public get $creation_timestamp(): number {
    return this.creation_timestamp;
    }
    
    /**
    * Getter $date_of_birth
    * @return {number}
    */
    public get $date_of_birth(): number {
    return this.date_of_birth;
    }
    
    /**
    * Getter $gender_type
    * @return {string}
    */
    public get $gender_type(): string {
    return this.gender_type;
    }
    
    /**
    * Getter $interests_list
    * @return {[string]}
    */
    public get $interests_list(): [string] {
    return this.interests_list;
    }
    
    /**
    * Getter $mobile_number
    * @return {string}
    */
    public get $mobile_number(): string {
    return this.mobile_number;
    }
    
    /**
    * Getter $report_card_entry_list
    * @return {[string]}
    */
    public get $report_card_entry_list(): [string] {
    return this.report_card_entry_list;
    }
    
    
    
    /**
    * Getter $status
    * @return {string}
    */
    public get $status(): string {
    return this.status;
    }
    
    // /**
    // * Getter $user_details_id
    // * @return {string}
    // */
    // public get $user_details_id(): string {
    // return this.user_details_id;
    // }
    
    /**
    * Getter $user_documents_list
    * @return {[string]}
    */
    public get $user_documents_list(): [string] {
    return this.user_documents_list;
    }
    
    /**
    * Setter $address_id
    * @param {string} value
    */
    public set $address_id(value: string) {
    this.address_id = value;
    }
    
    /**
    * Setter $age
    * @param {number} value
    */
    public set $age(value: number) {
    this.age = value;
    }
    
    /**
    * Setter $comments
    * @param {string} value
    */
    public set $comments(value: string) {
    this.comments = value;
    }
    
    /**
    * Setter $user_id
    * @param {string} value
    */
    public set $user_id(value: string) {
    this.user_id = value;
    }
    
    /**
    * Setter $creation_timestamp
    * @param {number} value
    */
    public set $creation_timestamp(value: number) {
    this.creation_timestamp = value;
    }
    
    /**
    * Setter $date_of_birth
    * @param {number} value
    */
    public set $date_of_birth(value: number) {
    this.date_of_birth = value;
    }
    
    /**
    * Setter $gender_type
    * @param {string} value
    */
    public set $gender_type(value: string) {
    this.gender_type = value;
    }
    
    /**
    * Setter $interests_list
    * @param {[string]} value
    */
    public set $interests_list(value: [string]) {
    this.interests_list = value;
    }
    
    /**
    * Setter $mobile_number
    * @param {string} value
    */
    public set $mobile_number(value: string) {
    this.mobile_number = value;
    }
    
    /**
    * Setter $report_card_entry_list
    * @param {[string]} value
    */
    public set $report_card_entry_list(value: [string]) {
    this.report_card_entry_list = value;
    }
    
    
    
    /**
    * Setter $status
    * @param {string} value
    */
    public set $status(value: string) {
    this.status = value;
    }
    
    /**
    * Setter $user_details_id
    * @param {string} value
    */
    // public set $user_details_id(value: string) {
    // this.user_details_id = value;
    // }
    
    /**
    * Setter $user_documents_list
    * @param {[string]} value
    */
    public set $user_documents_list(value: [string]) {
    this.user_documents_list = value;
    }
    
 
    
    }