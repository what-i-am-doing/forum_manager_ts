import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ImageService } from "../../services/image.service";
import { VideoService } from "../../services/video.service";
import { Lightbox, LightboxConfig, LightboxEvent, LIGHTBOX_EVENT, IEvent} from "ngx-lightbox";
import { Subscription } from 'rxjs';
import { CourseService } from "../../services/course.service";
import { Module } from "../../model/module";
import { publish } from 'rxjs/operators';
import {  Router } from '@angular/router';
@Component({
  selector: 'app-module-view',
  templateUrl: './module-view.component.html',
  styleUrls: ['./module-view.component.css']
})

export class ModuleViewComponent implements OnInit ,OnChanges{
 
  @Input() module: any;
  @Input() readable?:boolean = false;
  @Output() childModules = new EventEmitter<any>();
  modulePic:string;
  showExt:boolean = false;
  edit:boolean = false;
  video: any;
  basicGroup: FormGroup;
  imgGroup: FormGroup;
  detailGroup: FormGroup;
  courseGroup: FormGroup;
  descGroup: FormGroup;
  extData:FormGroup;
  modUpdated:boolean = false;
  moduleModules:any;
  private _subscription: Subscription;
  route: any;
  idPassedFromParams: any;
  curSubModule: any;
  
  constructor(private router:Router,private _formBuilder: FormBuilder, private imageService:ImageService, 
    private videoService:VideoService,
    private courseService:CourseService,
    private _lightbox: Lightbox,
    private _lightboxEvent: LightboxEvent,
    private _lighboxConfig: LightboxConfig,
    ) {
       
     
    }
  
  ngOnInit() {
    this.video = undefined;
     this.modulePic = undefined;
     this.moduleModules = undefined;
     this.basicGroup = this._formBuilder.group({
      basicCtrl: [{ value: this.module.module_name, disabled: true }, Validators.required],
      basicCtrl2: [{ value: this.module.module_type, disabled: true }, Validators.required],


    });
    

    this.imgGroup = this._formBuilder.group({
      imgCtrl1: [{ value: this.module.heading, disabled: true }, Validators.required],
      imgCtrl2: [{ value: this.module.language, disabled: true }, Validators.required],

    });

    this.detailGroup = this._formBuilder.group({
      detailCtrl1: [{ value: this.module.category, disabled: true }, Validators.required],
      detailCtrl2: [{ value: this.module.is_education_content, disabled: true }, Validators.required],
      detailCtrl3: [{ value: this.module.status, disabled: true }, Validators.required]

    });

    this.courseGroup = this._formBuilder.group({
      courseCtrl1: [{ value: this.module.course, disabled: true }, Validators.required],
      courseCtrl2: [{ value: this.module.stream, disabled: true }, Validators.required],
      courseCtrl3: [{ value: this.module.class, disabled: true }, Validators.required],
      courseCtrl4: [{ value: this.module.sub_class, disabled: true }, Validators.required],
      courseCtrl5: [{ value: this.module.subject, disabled: true }, Validators.required],
      courseCtrl6: [{ value: this.module.chapter, disabled: true }, Validators.required],
      courseCtrl7: [{ value: this.module.concept, disabled: true }, Validators.required]

    });

    this.descGroup = this._formBuilder.group({
      descCtrl1: [{ value: this.module.module_description, disabled: true }]
    });

    this.extData = this._formBuilder.group({
      extDataCtrl1:['']
    });

    if(this.module.image_id){
      this.imageService.getImage(this.module.image_id).subscribe(
        img => this.modulePic = this.URLChange(img.image_storage_url)
      );
   }

   if(this.module.module_content_list){
      this.imageService.getContent(this.module.module_content_list[0]).subscribe(

        content=> {
          if(content.ext_link_data_list) {
            this.videoService.getExtVideo(content.ext_link_data_list[0]).subscribe(
              video_data=> {
                this.videoService.getYouTubeVideoData(video_data.video_id).subscribe(
                  video => this.video = video
                )
              }
            );
          }

          if (content.content_images_list) {
            this.module._albums = [];
            content.content_images_list.forEach(element => {
              this.imageService.getImage(element).subscribe(imgData => {
                console.log(imgData);
                const src = imgData.image_storage_url.replace("https", "http");
                const caption = imgData.image_name;
                const thumb = imgData.image_storage_url.replace(
                  "https",
                  "http"
                );
                
                const album = {
                  src: src,
                  caption: caption,
                  thumb: thumb
                  
                };
                
                this.module._albums.push(album);
                

              });
            });
          }  
        }
      );
   }
   this.getSubModulesOfCourseModules();
   this.getChildModules();
  }


ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    if (!changes.module.firstChange) this.ngOnInit();
  }
 arrOfSubModules=[];
 getSubModulesOfCourseModules(){
   var course_id=localStorage.getItem('courseIdforjoin');
   console.log(localStorage.getItem('courseIdforjoin'));
  console.log('heyyyyyyyyyyyyyyyyyyyyyyy',this.module.module_id);
 this.courseService.ModuleByParentModule(this.module.module_id,course_id).subscribe((res:any)=>
 {  if(res.length > 0){
   this.arrOfSubModules.push(res);
 }
     console.log('888888888888888',res)
    
 })
}  
  editModule(){
    this.edit = true;
    this.basicGroup.get('basicCtrl').enable();
    this.basicGroup.get('basicCtrl2').enable();
    this.imgGroup.get('imgCtrl1').enable();
    this.imgGroup.get('imgCtrl2').enable();
    this.detailGroup.get('detailCtrl1').enable();
    this.detailGroup.get('detailCtrl2').enable();
    this.detailGroup.get('detailCtrl3').enable();
    this.courseGroup.get('courseCtrl1').enable();
    this.courseGroup.get('courseCtrl2').enable();
    this.courseGroup.get('courseCtrl3').enable();
    this.courseGroup.get('courseCtrl4').enable();
    this.courseGroup.get('courseCtrl5').enable();
    this.courseGroup.get('courseCtrl6').enable();
    this.courseGroup.get('courseCtrl7').enable();
    this.descGroup.get('descCtrl1').enable();
  }
  

  updateModule() {
    var module = new Module;
    module.$module_name = this.basicGroup.value.basicCtrl;
    module.$module_type = this.basicGroup.value.basicCtrl2;
    module.$module_heading = this.imgGroup.value.imgCtrl1;
    module.$status = this.imgGroup.value.imgCtrl2;
    module.$category = this.detailGroup.value.detailCtrl1;
    module.$is_education_content = this.detailGroup.value.detailCtrl2;
    module.$language = this.detailGroup.value.detailCtrl2;
    module.$course = this.courseGroup.value.courseCtrl1;
    module.$stream = this.courseGroup.value.courseCtrl2;
    module.$class = this.courseGroup.value.courseCtrl3;
    module.$sub_class = this.courseGroup.value.courseCtrl4;
    module.$subject = this.courseGroup.value.courseCtrl5;
    module.$chapter = this.courseGroup.value.courseCtrl6;
    module.$concept = this.courseGroup.value.courseCtrl7;
    module.$module_description = this.descGroup.value.descCtrl1;

    this.courseService.updateModule(this.module.module_id, module).subscribe(
      ()=>{
        this.modUpdated = true;
        this.removeEdit();
        this.updateView();
      }
    );
  }
 
  updateView() {
    this.module.module_name = this.basicGroup.value.basicCtrl;
    this.module.module_type = this.basicGroup.value.basicCtrl2;
    this.module.module_heading = this.imgGroup.value.imgCtrl1;
    this.module.status = this.imgGroup.value.imgCtrl2;
    this.module.category = this.detailGroup.value.detailCtrl1;
    this.module.is_education_content = this.detailGroup.value.detailCtrl2;
    this.module.language = this.detailGroup.value.detailCtrl3;
    this.module.course = this.courseGroup.value.courseCtrl1;
    this.module.stream = this.courseGroup.value.courseCtrl2;
    this.module.class = this.courseGroup.value.courseCtrl3;
    this.module.sub_class = this.courseGroup.value.courseCtrl4;
    this.module.subject = this.courseGroup.value.courseCtrl5;
    this.module.chapter = this.courseGroup.value.courseCtrl6;
    this.module.concept = this.courseGroup.value.courseCtrl7;
    this.module.module_description = this.descGroup.value.descCtrl1;
  }

  removeEdit() {
    this.edit = false;
    this.basicGroup.get('basicCtrl').disable();
    this.basicGroup.get('basicCtrl2').disable();
    this.imgGroup.get('imgCtrl1').disable();
    this.imgGroup.get('imgCtrl2').disable();
    this.detailGroup.get('detailCtrl1').disable();
    this.detailGroup.get('detailCtrl2').disable();
    this.detailGroup.get('detailCtrl3').disable();
    this.courseGroup.get('courseCtrl1').disable();
    this.courseGroup.get('courseCtrl2').disable();
    this.courseGroup.get('courseCtrl3').disable();
    this.courseGroup.get('courseCtrl4').disable();
    this.courseGroup.get('courseCtrl5').disable();
    this.courseGroup.get('courseCtrl6').disable();
    this.courseGroup.get('courseCtrl7').disable();
    this.descGroup.get('descCtrl1').disable();
  }

  URLChange(url: any) {
    return url.replace("https", "http");
  }


  open(index: number, album): void {
    this._subscription = this._lightboxEvent.lightboxEvent$.subscribe(
      (event: IEvent) => this._onReceivedEvent(event)
    );

    // override the default config
    this._lightbox.open(album, index, {
      wrapAround: true,
      showImageNumberLabel: true
    });
  }

  close(): void {
    // close lightbox programmatically
    this._lightbox.close();
  }

  private _onReceivedEvent(event: IEvent): void {
    if (event.id === LIGHTBOX_EVENT.CLOSE) {
      this._subscription.unsubscribe();
    }
  }

  getChildModules() {
    this.courseService.ModuleByParentModule(this.module.module_id,this.module.course_id).subscribe(
      modules=>{
      if(modules.length) 
         this.moduleModules = modules;
      else 
         this.moduleModules = undefined;
      }
    );
  }
  
  sendChildModules() {
     
     this.childModules.emit([this.moduleModules,{"id":this.module.module_id , "path_name":this.module.module_name}]);
  }
  SubmoduleShow:boolean = false;
SubmoduleView(SubmoduleInfo){
  this.curSubModule = SubmoduleInfo;
    this.SubmoduleShow =true;
    console.log('SubmoduleInfo',SubmoduleInfo);
}

  
}
