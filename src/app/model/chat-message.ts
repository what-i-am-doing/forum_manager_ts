export class ChatMessage {

    constructor() { };

    
private chat_id: string;
private chat_message: string;
private chat_message_id: string;
private chat_users_list: string[];
private comments: string;
private creation_timestamp: number;
private from_user_id: string;
private group_id:string;
private last_updated_timestamp: number;
private parent_chat_message_id: string;
private status: string;
private tagged_group_id:string [];
private tagged_level_id:string [];
private tagged_tag_id:string [];
private tagged_user_id:string [];
    /**
     * Getter $tagged_user_id
     * @return {string []}
     */
	public get $tagged_user_id(): string [] {
		return this.tagged_user_id;
	}

    /**
     * Setter $tagged_user_id
     * @param {string []} value
     */
	public set $tagged_user_id(value: string []) {
		this.tagged_user_id = value;
	}

    /**
     * Getter $chat_id
     * @return {string}
     */
	public get $chat_id(): string {
		return this.chat_id;
	}

    /**
     * Getter $chat_message
     * @return {string}
     */
	public get $chat_message(): string {
		return this.chat_message;
	}

    /**
     * Getter $chat_message_id
     * @return {string}
     */
	public get $chat_message_id(): string {
		return this.chat_message_id;
	}

    /**
     * Getter $chat_users_list
     * @return {string[]}
     */
	public get $chat_users_list(): string[] {
		return this.chat_users_list;
	}

    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $creation_timestamp
     * @return {number}
     */
	public get $creation_timestamp(): number {
		return this.creation_timestamp;
	}

    /**
     * Getter $from_user_id
     * @return {string}
     */
	public get $from_user_id(): string {
		return this.from_user_id;
	}
    
    /**
     * Getter $group_id
     * @return {string}
     */
	public get $group_id(): string {
		return this.group_id;
	}

    /**
     * Getter $last_updated_timestamp
     * @return {number}
     */
	public get $last_updated_timestamp(): number {
		return this.last_updated_timestamp;
	}

    /**
     * Getter $parent_chat_message_id
     * @return {string}
     */
	public get $parent_chat_message_id(): string {
		return this.parent_chat_message_id;
	}

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Getter $tagged_group_id
     * @return {string []}
     */
	public get $tagged_group_id(): string [] {
		return this.tagged_group_id;
	}

    /**
     * Getter $tagged_level_id
     * @return {string []}
     */
	public get $tagged_level_id(): string [] {
		return this.tagged_level_id;
	}

    /**
     * Getter $tagged_tag_id
     * @return {string []}
     */
	public get $tagged_tag_id(): string [] {
		return this.tagged_tag_id;
	}

    /**
     * Setter $chat_id
     * @param {string} value
     */
	public set $chat_id(value: string) {
		this.chat_id = value;
	}

    /**
     * Setter $chat_message
     * @param {string} value
     */
	public set $chat_message(value: string) {
		this.chat_message = value;
	}

    /**
     * Setter $chat_message_id
     * @param {string} value
     */
	public set $chat_message_id(value: string) {
		this.chat_message_id = value;
	}

    /**
     * Setter $chat_users_list
     * @param {string[]} value
     */
	public set $chat_users_list(value: string[]) {
		this.chat_users_list = value;
	}

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $creation_timestamp
     * @param {number} value
     */
	public set $creation_timestamp(value: number) {
		this.creation_timestamp = value;
	}

    /**
     * Setter $from_user_id
     * @param {string} value
     */
	public set $from_user_id(value: string) {
		this.from_user_id = value;
	}
    
     /**
     * Setter $group_id
     * @param {string} value
     */
	public set $group_id(value: string) {
		this.group_id = value;
    }
    
    /**
     * Setter $last_updated_timestamp
     * @param {number} value
     */
	public set $last_updated_timestamp(value: number) {
		this.last_updated_timestamp = value;
	}

    /**
     * Setter $parent_chat_message_id
     * @param {string} value
     */
	public set $parent_chat_message_id(value: string) {
		this.parent_chat_message_id = value;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}

    /**
     * Setter $tagged_group_id
     * @param {string []} value
     */
	public set $tagged_group_id(value: string []) {
		this.tagged_group_id = value;
	}

    /**
     * Setter $tagged_level_id
     * @param {string []} value
     */
	public set $tagged_level_id(value: string []) {
		this.tagged_level_id = value;
	}

    /**
     * Setter $tagged_tag_id
     * @param {string []} value
     */
	public set $tagged_tag_id(value: string []) {
		this.tagged_tag_id = value;
	}

      

}