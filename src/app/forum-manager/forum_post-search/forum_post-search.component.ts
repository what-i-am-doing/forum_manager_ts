import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-forum_post-search',
  templateUrl: './forum_post-search.component.html',
  styleUrls: ['./forum_post-search.component.css']
})
export class ForumPostSearchComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  @Input() forumpost:any;
  @Output() emitForumPost = new EventEmitter<any>();

  sendForum() {
       this.emitForumPost.emit(this.forumpost);
  }
}
