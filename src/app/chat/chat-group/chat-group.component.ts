import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ChatGroup } from '../../model/chat-group';
import { ChatService } from '../../services/chat.service';

@Component({
  selector: 'app-chat-group',
  templateUrl: './chat-group.component.html',
  styleUrls: ['./chat-group.component.css']
})
export class ChatGroupComponent implements OnInit {

  constructor(private userService:UserService,private chatService:ChatService) { }

  ngOnInit() {
      console.log(this.friendList);
      
  }
  success:any = null;
  groupName:string = "";
  
  userGroup: Array<string> = JSON.parse(localStorage.getItem("user_group"));
  userID: string = localStorage.getItem("user");
  chatusers = [this.userID];
  @ViewChild("upload", { read: ElementRef }) upload: ElementRef;
  @Input() friendList:any;
  selectedFriends:Array<any> = [];
  shiftUser(user: HTMLElement, event: any) {

      if (event.srcElement.checked == true) {

            var container = document.querySelector('.body-right');

            container.firstChild.nextSibling.appendChild(user);
          }

      else {

        var container = document.querySelector('.body-left');
        container.firstChild.appendChild(user);

      }
      

  }

  clickUpload(): void {

    this.upload.nativeElement.click();

  }

  selectedList(event:any){
       console.log(event);
       this.selectedFriends.push(event);
       console.log(this.selectedFriends);
  }

  remove(event:any){
    console.log(event);
    this.selectedFriends = this.selectedFriends.filter(member => member != event);
    console.log(this.selectedFriends);
  }

  submitGroup(){

    console.log("submitted");
    this.selectedFriends.forEach(element => {
         this.chatusers.push(element.user_id);
    });
    var chatGroup = new ChatGroup;
    chatGroup.$chat_group_name = this.groupName;
    chatGroup.$chat_type_id = "GROUP_CHAT";
    chatGroup.$group_id = this.userGroup[0];
    chatGroup.$level_id = "1";
    chatGroup.$tag_id = "1";
    chatGroup.$chat_users_list = this.chatusers;
    chatGroup.$created_by_user_id = this.userID;
    chatGroup.$creation_timestamp = +new Date();
    chatGroup.$status = "ACTIVE";

    this.chatService.createChatGroup(chatGroup).subscribe(data => {

      console.log(data);
      this.success = "Group Created Successfully";
    });
  }
}
