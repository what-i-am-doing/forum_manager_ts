import { Pipe,PipeTransform } from "@angular/core";


@Pipe({

    'name':'educationTypes'
})

export class EducationTypesPipe implements PipeTransform{

transform(value , search ){

   return value.filter(edu => edu.education_type_id === search)[0].education_type_name
}

}