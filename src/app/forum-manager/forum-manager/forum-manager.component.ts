import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Forum } from "../../model/Forum";

import { ImageService } from "../../services/image.service";
import { ForumService } from "../../services/forum.service";
import { Router } from "@angular/router";
@Component({
  selector: 'app-forum-manager',
  templateUrl: './forum-manager.component.html',
  styleUrls: ['./forum-manager.component.css']
})

export class ForumManagerComponent implements OnInit {

  @ViewChild("upload", { read: ElementRef }) upload: ElementRef;
  @ViewChild("upload2", { read: ElementRef }) upload2: ElementRef;
  forum:any;
  isLinear = false;
  basicGroup: FormGroup;
  imgGroup: FormGroup;
  detailGroup: FormGroup;
  forumGroup: FormGroup;
  descGroup: FormGroup;
  userId: string = localStorage.getItem("user");
  groupId: string = localStorage.getItem("groupId");
  forumAdded: boolean = false;
  newForum: boolean = true;
  forumImg: string;
  constructor(private router:Router,private _formBuilder: FormBuilder, private imageService: ImageService, private forumService: ForumService) { }
  color: string = "accent";
  bgColor: string = "primary";
  showForm: boolean = false;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  imgResponse = { status: '', message: 0, filePath: '' };
  imageSRCS: Array<any> = [];
  userForums: Array<any> = [];
  coMoImageSRCS: any;
  path: any = [{ "id": 1, "path_name": "forum" }];
  coMoImageID: string;

  ngOnInit() {

    this.selectedItems = [];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.dropdownList = [
      { item_id: 1, item_text: 'Group1' },
      { item_id: 2, item_text: 'Group2' },
      { item_id: 3, item_text: 'Group3' },
      { item_id: 4, item_text: 'Group4' },
      { item_id: 5, item_text: 'Group5' }
    ];

    this.basicGroup = this._formBuilder.group({
      basicCtrl: ['', Validators.required],
      basicCtrl2: ['', Validators.required],


    });

    this.imgGroup = this._formBuilder.group({
      imgCtrl1: ['', Validators.required],
      imgCtrl2: ['', Validators.required],

    });

    this.detailGroup = this._formBuilder.group({
      detailCtrl1: ['', Validators.required],
      detailCtrl2: ['', Validators.required],
      detailCtrl3: ['', Validators.required]

    });

    this.forumGroup = this._formBuilder.group({
      fourmCtrl1: ['', Validators.required],
      forumCtrl2: ['', Validators.required],
      forumCtrl3: ['', Validators.required],
      forumCtrl4: ['', Validators.required],
      forumCtrl5: ['', Validators.required],
      forumCtrl6: ['', Validators.required],
      forumCtrl7: ['', Validators.required]

    });

    this.descGroup = this._formBuilder.group({
      descCtrl1: ['']
    });

    this.getForums();
    this.getDataForJoinedForums();
  }

  dataForJoinedForums=[];
 
  async getDataForJoinedForums(){
    let a;
     a= localStorage.getItem('UserAllData');
    console.log('aaaaaaaaa',a);

    let b= await JSON.parse(a);
    let c =[];
    c  = b.forums_list;
    console.log('aaaaaaaaa2222',c,b.forums_list);
    c.forEach((element) => {
      // console.log('________====',this.course.course_id,element.course_id) 
        this.joinedForumNameGetByUsingId(element);
    });

    if(this.dataForJoinedForums.length > 0){
      console.log('OOOOOOOOOOOO',this.dataForJoinedForums)
  //    this.callForModuleNameFromId();
  this.getForumDetailCall(c);

    }
 
    
    // console.log('$$',this.courseSelected);
  }
  getForumDetailCall(forum_id){
    return new Promise((resolve,reject)=>{
    
      this.forumService.getForumsByIds(forum_id).subscribe((res:any)=>{
        console.log('resssssssssssssssss',res);
        debugger;
        // this.course = {};
        this.forum = res;
        console.log('reesssssssssssssssssssss2',this.forum);
        resolve(this.forum)
    //    localStorage.setItem('courseIdforjoin',this.idPassedFromParams);
       // let moduleIdList= res.course_modules_list;
        // console.log('hhhhhhhhhhhh',moduleIdList)
        // moduleIdList.forEach((i:any)=>{
        //   this.getModulesById(i);
        // })
    });
    })
    }
    joinedForumNameGetByUsingId(forum_id)
      {
      console.log('Called....',forum_id);
      this.forumService.getForumsByIds(forum_id).subscribe((res:any)=>{
        console.log(res,'qqqqqqqq')
        if(res){
          this.dataForJoinedForums.push(res);
        }
      });
    }

  createForum() {
    var forum = new Forum;
    forum.$forum_name = this.basicGroup.value.basicCtrl;
    forum.$forum_description = this.basicGroup.value.basicCtrl1;
    forum.$forum_type = this.basicGroup.value.detailsCtrl3;
    forum.$custom_tag_line = this.basicGroup.value.basicCtrl2;
    forum.$created_by_user = this.userId;
    forum.$forum_admin_user_id = [this.userId];
    forum.$enabled_for_groups = ["public"];
    if (this.coMoImageID != undefined) forum.$img_id = this.coMoImageID;

    this.addForum(forum);
  }

  addForum(forum: any) {
    this.forumService.addForum(forum).subscribe(
      data => {
        console.log(data);
        this.forumAdded = true;
        forum.$forum_id = data.id;
        this.userForums.push(forum);
        this.basicGroup.reset();
        this.imgGroup.reset();
        this.detailGroup.reset();
        this.forumGroup.reset();
        this.descGroup.reset();
        this.coMoImageSRCS = {};
      }
    );
  }

  forumpostview(){
     this.router.navigate(['/appforumView']);
  }
  clickUpload(): void {
    this.upload.nativeElement.click();
  }

  clickUpload2(): void {
    this.upload2.nativeElement.click();
  }
  onItemSelect(item: any) {
    console.log(item);
  }

  onSelectAll(items: any) {
    console.log(items);
  }

  
      coMoIMG(imageInput: any) {
    var file = imageInput.files[0];
    var fileReader = new FileReader();

    fileReader.addEventListener("load", (event: any) => {
      console.log(event.target.result);
      this.imageService.uploadImage(file).subscribe(
        (res: any) => {
          console.log(res);
          this.imgResponse = res;

          if (typeof res === 'object') {
            if ('id' in res) {

              this.coMoImageID = res.id;
              this.coMoImageSRCS = {
                image_storage_url: fileReader.result,
                content_type: "image/jpeg",
                image_name: file.name,
                image_size: file.size
              };
            }
          }
        },
        err => {
          console.log(err);
        }
      );
    });

    fileReader.readAsDataURL(file);
  }

  getForums() {
    this.forumService.getForumsCreatedByAdmin(this.userId, this.groupId).subscribe(
      forums => {
        // console.log('kal'forums);
        this.userForums = forums;
        console.log('qwwwwert',this.userForums)
      }
    );
  }
}
