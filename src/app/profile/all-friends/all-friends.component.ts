import { Component, OnInit } from '@angular/core';
import { UserService} from '../../services/user.service';

@Component({
  selector: 'app-all-friends',
  templateUrl: './all-friends.component.html',
  styleUrls: ['./all-friends.component.css'],
  providers: [UserService]
})
export class AllFriendsComponent implements OnInit {
  public receivedFriendList:any;   
  userGroupId =JSON.parse(localStorage.getItem('user_group'));
  userId = localStorage.getItem("user");
  constructor(private service:UserService) { }
  public fromSize=0;
  public toSize=100;
  public friendData:Array<any> = [];
  
  ngOnInit() {
      this.getFriendListOfActiveUser(this.userId,this.userGroupId[0],this.fromSize,this.toSize);
  }
  

  getFriendListOfActiveUser(userId:any,group_id,fromSize,toSize){

    this.service.getFriendListOfActiveUser(userId,group_id,fromSize,toSize).subscribe(data=>{
                            this.receivedFriendList=data;
                          
                            
        }) 
    }
}
