import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ImageService } from 'src/app/services/image.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-accept-notificaion',
  templateUrl: './accept-notificaion.component.html',
  styleUrls: ['./accept-notificaion.component.css'],
  providers:[UserService,ImageService]
})
export class AcceptNotificaionComponent implements OnInit {
@Input() acceptedData:any;
public activeUserDetails=[];
public profilePic:any;
public gender:any;
public userId:any;
  constructor(private router:Router,private service:UserService,private imageService:ImageService) { }

  ngOnInit() {
  this.userId=this.acceptedData.to_user_id
this.getActiveUserDetails(this.acceptedData.to_user_id);
this.getActiveUserPersonalDetails(this.acceptedData.to_user_id)




  }





  getActiveUserDetails(userId: any) {
    this.service.getUserDetails(userId).subscribe(receivedUserData => {
     console.log(receivedUserData)
      this.activeUserDetails = receivedUserData;
     
      if(receivedUserData.image_id!=null||receivedUserData.image_id!=undefined){
  this.getProfile(receivedUserData.image_id)
  
      }
  
  
    });
  }

  getProfile(image_id){
    console.log('thumbnail calling')
    console.log(image_id)
      this.imageService.getImageByType(image_id,'thumbnail').subscribe(data=>{
       console.log(data);
        if(data.length>0){
        console.log("thumbnail found");
        console.log(data[0].image_storage_url);
      this.profilePic=this.URLChange(data[0].image_storage_url);
        }
        else{
          this.getProfileImage(image_id);
        }
      })
    }
    getProfileImage(image_id){
      if(image_id!=''||image_id!='string'||image_id!=null){
  
  
      this.imageService.getImage(image_id).subscribe(data=>{
        console.log("image found");
        console.log(data.image_storage_url);
  this.profilePic=this.URLChange(data.image_storage_url);
  
      }
  
   ,err=>{
    console.log("image not found")
  switch(this.gender){
       case 'MALE':
       this.profilePic='assets/images/male.jpg'
       break;
       case 'FEMALE':
       this.profilePic='assets/images/female.jpg';
       break
  }
   }
   )}else{
    switch(this.gender){
      case 'MALE':
      this.profilePic='assets/images/male.jpg'
      break;
      case 'FEMALE':
      this.profilePic='assets/images/female.jpg';
      break
  }
   }
  
    }

      URLChange(img:any){
        
        return img.replace("https" , "http");
    
  }

  getActiveUserPersonalDetails(userId){
    this.service.getUserPersonalDetails(userId).subscribe(data=>{
      console.log("getActiveUserPersonalDetails")
      console.log(data)
       
    this.gender=data.gender_type;
    console.log(this.gender);
    })
    
    }

    seeFriendProfile(){
      this.router.navigate(['/home/userprofile'], { queryParams: { userId:this.userId} });
    }

}
