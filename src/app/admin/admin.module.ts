import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule ,ReactiveFormsModule } from '@angular/forms';
import { MatTreeModule} from '@angular/material/tree';
import { AdminComponent } from './admin/admin.component';
import { ChildrenComponent }from './children/children.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatInputModule,MatListModule, MatIconModule, MatProgressSpinnerModule } from '@angular/material';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { TreeModule } from 'angular-tree-component';
import { CreateNewGroupComponent } from './create-new-group/create-new-group.component';
import {MatMenuModule} from '@angular/material/menu';
import { ProfilePicComponent } from './admin/profile-pic/profile-pic.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { EducationDetailComponent } from './admin/education-detail/education-detail.component';
import { PersonalDetailComponent } from './admin/personal-detail/personal-detail.component';
import { AdminGroupListComponent } from './admin-group-list/admin-group-list.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { DataTablesModule } from 'angular-datatables';
import { PostReviewComponent } from './post-review/post-review.component';
import { LightboxModule } from "ngx-lightbox";
import { NgxExtendedPdfViewerModule } from "ngx-extended-pdf-viewer";
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({

  declarations: [AdminComponent,ChildrenComponent, CreateNewGroupComponent, ProfilePicComponent,  EducationDetailComponent, PersonalDetailComponent, AdminGroupListComponent, PostReviewComponent],
  imports: [
    CommonModule,FormsModule,ReactiveFormsModule,MatTreeModule,MatIconModule, MatInputModule,MatToolbarModule, MatButtonModule, MatSidenavModule, MatListModule, MatIconModule, MatFormFieldModule,
    MatTableModule,MatCheckboxModule,
    MatMenuModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    NgxIntlTelInputModule,
    BsDatepickerModule,
    DataTablesModule,
    LightboxModule,
    NgxExtendedPdfViewerModule,
    TreeModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot()

  ],
  exports:[AdminComponent,ChildrenComponent,CreateNewGroupComponent]

})

export class AdminModule { }
