import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../environments/environment';


@Injectable()
export class GroupService {

    constructor(private http: HttpClient) {}
    api_url = environment.api_url;
  
    public createUserGroup(groupObj,userId):Observable<any> {
  
     
      return this.http.post<any>(this.api_url+'/group/', groupObj,{headers: new HttpHeaders({

        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })
    }).pipe(
        catchError(this.handleError)
      )
    }


    getAllGroupList(clientId):Observable<any>{

        return this.http.get<any>(this.api_url+'/group/search?client_id='+clientId,{headers: new HttpHeaders({

            'Accept':        'application/json',
            'from_user_id':  'user'
            
          })
        }).pipe(
            catchError(this.handleError)
          )


    }

    getGroupDetails(groupId):Observable<any>{

        return this.http.get<any>(this.api_url+'/group/'+groupId,{headers: new HttpHeaders({

            'Accept':        'application/json',
            'from_user_id':  'user'
            
          })
        }).pipe(
            catchError(this.handleError)
          )


    }


userSwitchGroup(switchObj,groupId,userId):Observable<any>{

      return this.http.post<any>(this.api_url+'/user/switch_group?switch_to_group='+groupId,switchObj,{headers: new HttpHeaders({

          'Accept':        'application/json',
          'from_user_id':  userId
          
        })
      }).pipe(
          catchError(this.handleError)
        )


  }
  getUserlistByGroup(groupId,fromsize, tosize):Observable<any>{

    return this.http.get<any>(this.api_url+'/user/by_group?group_id='+groupId+'&from='+fromsize+'&size='+tosize,{headers: new HttpHeaders({

        'Accept':        'application/json',
        'from_user_id':  groupId
        
      })
    }).pipe(
        catchError(this.handleError)
      )


}

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
          // A client-side or network error occurred. Handle it accordingly.
          console.error('An error occurred:', error.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
          'Something bad happened; please try again later.');
      }
    }