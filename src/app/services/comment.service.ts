import { Injectable } from '@angular/core';
import { Post } from '../model/post';
import { HttpClient , HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry ,delay } from 'rxjs/operators';
import { environment } from '../../environments/environment';



@Injectable()
export class CommentService{
    
   
   private api_url = environment.api_url;
    constructor(private http: HttpClient) { }
    
   
    submitComment(comment:any):Observable<any>{

        return this.http.post<any>(this.api_url+'/comment/',comment,{headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept':        'application/json',
            'from_user_id':  localStorage.getItem('user')
            
          })
        }).pipe(

            retry(1),
            catchError(this.handleError)
        );

    }

    getCommentByPost(postID:string,from:number,size:number):Observable<any> {
        return this.http.get<any>(this.api_url+'/comment/search_by_entity?group_id=public&on_entity_type=POST&entity_id='+postID+'&from='+from+'&size='+size,{headers: new HttpHeaders({

            'Accept':        'application/json',
            'from_user_id':  localStorage.getItem('user')
            
          })
        } ).pipe(
           
           retry(1), // retry a failed request up to 1 time
           catchError(this.handleError) // then handle the error
          );
    }

    getCommentByParent(postID:string, parent:string,from:number,size:number):Observable<any> {
        return this.http.get<any>(this.api_url+'/comment/search_by_parent_comment?group_id=public&on_entity_type=POST&entity_id='+postID+'&parent_comment='+parent+'&from='+from+'&size='+size,{headers: new HttpHeaders({

            'Accept':        'application/json',
            'from_user_id':  localStorage.getItem('user')
            
          })
        } ).pipe(
           
           retry(1), // retry a failed request up to 1 time
           catchError(this.handleError) // then handle the error
          );
    }

    updatePostAfterComment(postID:string, body:any):Observable<any> {
        return this.http.put<any>(this.api_url+'/post/'+postID,body,{headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept':        'application/json',
            'from_user_id':  localStorage.getItem('user')
            
          })
        }).pipe(

            retry(1),
            catchError(this.handleError)
        );
    }

    updateComment(comID:string , body):Observable<any> {
        return this.http.put<any>(this.api_url+'/comment/'+comID,body,{headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept':        'application/json',
            'from_user_id':  localStorage.getItem('user')
            
          })
        }).pipe(

            retry(1),
            catchError(this.handleError)
        );
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
          // A client-side or network error occurred. Handle it accordingly.
          console.error('An error occurred:', error.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
          'Something bad happened; please try again later.');
      };

}