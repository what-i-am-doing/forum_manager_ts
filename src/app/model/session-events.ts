export class sessionEvents {
 constructor(){}
 
private action_type: string;
private activity: string;
private activity_id: string;
private comments: string;
private creation_timestamp: string;
private entity: string;
private entity_id: string;
private event_timestamp: string;
private group_id: string;
private last_updated_timestamp: string;
private locality: string;
private locality_id: string;
private session_id: string;
private status: string;
private user_id: string;
private user_session_event_details_id: string;


    /**
     * Getter $action_type
     * @return {string}
     */
	public get $action_type(): string {
		return this.action_type;
	}

    /**
     * Getter $activity
     * @return {string}
     */
	public get $activity(): string {
		return this.activity;
	}

    /**
     * Getter $activity_id
     * @return {string}
     */
	public get $activity_id(): string {
		return this.activity_id;
	}

    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $creation_timestamp
     * @return {string}
     */
	public get $creation_timestamp(): string {
		return this.creation_timestamp;
	}

    /**
     * Getter $entity
     * @return {string}
     */
	public get $entity(): string {
		return this.entity;
	}

    /**
     * Getter $entity_id
     * @return {string}
     */
	public get $entity_id(): string {
		return this.entity_id;
	}

    /**
     * Getter $event_timestamp
     * @return {string}
     */
	public get $event_timestamp(): string {
		return this.event_timestamp;
	}

    /**
     * Getter $group_id
     * @return {string}
     */
	public get $group_id(): string {
		return this.group_id;
	}

    /**
     * Getter $last_updated_timestamp
     * @return {string}
     */
	public get $last_updated_timestamp(): string {
		return this.last_updated_timestamp;
	}

    /**
     * Getter $locality
     * @return {string}
     */
	public get $locality(): string {
		return this.locality;
	}

    /**
     * Getter $locality_id
     * @return {string}
     */
	public get $locality_id(): string {
		return this.locality_id;
	}

    /**
     * Getter $session_id
     * @return {string}
     */
	public get $session_id(): string {
		return this.session_id;
	}

    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Getter $user_id
     * @return {string}
     */
	public get $user_id(): string {
		return this.user_id;
	}

    /**
     * Getter $user_session_event_details_id
     * @return {string}
     */
	public get $user_session_event_details_id(): string {
		return this.user_session_event_details_id;
	}

    /**
     * Setter $action_type
     * @param {string} value
     */
	public set $action_type(value: string) {
		this.action_type = value;
	}

    /**
     * Setter $activity
     * @param {string} value
     */
	public set $activity(value: string) {
		this.activity = value;
	}

    /**
     * Setter $activity_id
     * @param {string} value
     */
	public set $activity_id(value: string) {
		this.activity_id = value;
	}

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $creation_timestamp
     * @param {string} value
     */
	public set $creation_timestamp(value: string) {
		this.creation_timestamp = value;
	}

    /**
     * Setter $entity
     * @param {string} value
     */
	public set $entity(value: string) {
		this.entity = value;
	}

    /**
     * Setter $entity_id
     * @param {string} value
     */
	public set $entity_id(value: string) {
		this.entity_id = value;
	}

    /**
     * Setter $event_timestamp
     * @param {string} value
     */
	public set $event_timestamp(value: string) {
		this.event_timestamp = value;
	}

    /**
     * Setter $group_id
     * @param {string} value
     */
	public set $group_id(value: string) {
		this.group_id = value;
	}

    /**
     * Setter $last_updated_timestamp
     * @param {string} value
     */
	public set $last_updated_timestamp(value: string) {
		this.last_updated_timestamp = value;
	}

    /**
     * Setter $locality
     * @param {string} value
     */
	public set $locality(value: string) {
		this.locality = value;
	}

    /**
     * Setter $locality_id
     * @param {string} value
     */
	public set $locality_id(value: string) {
		this.locality_id = value;
	}

    /**
     * Setter $session_id
     * @param {string} value
     */
	public set $session_id(value: string) {
		this.session_id = value;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}

    /**
     * Setter $user_id
     * @param {string} value
     */
	public set $user_id(value: string) {
		this.user_id = value;
	}

    /**
     * Setter $user_session_event_details_id
     * @param {string} value
     */
	public set $user_session_event_details_id(value: string) {
		this.user_session_event_details_id = value;
	}


}  


 