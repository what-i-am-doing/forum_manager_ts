import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry ,delay } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private api_url = environment.api_url;
    constructor(private http: HttpClient) { }
    
   
    submit121Chat(chat:any):Observable<any>{

        return this.http.post<any>(this.api_url+'/chat_message/',chat,{headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept':        'application/json',
            'from_user_id':  localStorage.getItem('user')
            
          })
        }).pipe(

            retry(1),
            catchError(this.handleError)
        );

    }

    get121Chat(groupID:string,chatID:string ,from:number,size:number):Observable<any> {
      return this.http.get(this.api_url+'/chat_message/search_by_chat?chat_id='+chatID+'&group_id='+groupID+'&from='+from+'&size='+size,{headers: new HttpHeaders({
        
        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })
    }).pipe(
        retry(1),
            catchError(this.handleError)
      );
    }
  
    get121ChatByTime(groupID:string,chatID:string ,time_start:number,time_end:number):Observable<any>{
      return this.http.get(this.api_url+'/chat_message/search_by_chat/async?group_id='+groupID+'&chat_id='+chatID+'&time_start='+time_start+'&time_end='+time_end+'&size=100',{headers: new HttpHeaders({
        
        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })
    }).pipe(
        retry(1),
            catchError(this.handleError)
      );
    }

    createChatGroup(group:any):Observable<any> {

         return this.http.post(this.api_url+'/chat/',group,{headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Accept':        'application/json',
          'from_user_id':  localStorage.getItem('user')
          
        })
      }).pipe(
          retry(1),
              catchError(this.handleError)
        );
    }
   
    getGroupsForUser(userID:string,groupID:string):Observable<any> {
      return this.http.get(this.api_url+'/chat/search_by_member?group_id='+groupID+'&user_id='+userID+'&from=0&size=100',{headers: new HttpHeaders({
        
        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })
    }).pipe(
        retry(1),
            catchError(this.handleError)
      );
    }

    getGroupsByGroup(groupID:string , time_start:number , time_end:number):Observable<any>{
       
      return this.http.get(this.api_url+'/chat/search_by_group?group_id='+groupID+'&time_start='+time_start+'&time_end='+time_end+'&from=0&size=10',{headers: new HttpHeaders({
        
        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })
    }).pipe(
        retry(1),
            catchError(this.handleError)
      );
    }
    
    chatByUser(user:string, time_start:number,time_end:number):Observable<any>{
      return this.http.get(this.api_url+'/chat_message/search_by_user/?group_id=public&user_id='+user+'&time_start='+time_start+'&time_end='+time_end+'&size=100',{headers: new HttpHeaders({
        
        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })
    }).pipe(
        retry(1),
            catchError(this.handleError)
      );
    }

    chatByUserAsync(user:string, time_start:number,time_end:number):Observable<any>{
      return this.http.get(this.api_url+'/chat_message/search_by_user/async?group_id=public&user_id='+user+'&time_start='+time_start+'&time_end='+time_end+'&size=100',{headers: new HttpHeaders({
        
        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })
    }).pipe(
        retry(1),
            catchError(this.handleError)
      );
    }
    
    getChatGroupByID(chatID:string):Observable<any>{
      return this.http.get(this.api_url+'/chat/'+chatID,{headers: new HttpHeaders({
        
        'Accept':        'application/json',
        'from_user_id':  localStorage.getItem('user')
        
      })
    }).pipe(
        retry(1),
            catchError(this.handleError)
      );
    }
    private handleError(error: HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: ${error.error}`);
      }
      // return an observable with a user-facing error message
      return throwError(
        'Something bad happened; please try again later.');
    }; 
}
