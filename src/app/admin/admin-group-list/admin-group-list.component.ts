import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { group } from '@angular/animations';
import { ImageService } from 'src/app/services/image.service';

@Component({
  selector: 'app-admin-group-list',
  templateUrl: './admin-group-list.component.html',
  styleUrls: ['./admin-group-list.component.css'],
  providers:[ImageService]
})
export class AdminGroupListComponent implements OnInit {

  constructor(private imageService:ImageService) { }
  
  @Input() group:any;
  @Output() switchGroup = new EventEmitter<any>();
  grpImage:string;

  ngOnInit() {

     if(this.group.image_id){
        this.imageService.getImage(this.group.image_id).subscribe(
          img => this.grpImage = this.URLChange(img.image_storage_url)
        );
     }
  }

  emitGroup(){
      this.switchGroup.emit(this.group);
  }

  URLChange(url: any) {
    return url.replace("https", "http");
  }

}
