import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-chat-group-friend-list',
  templateUrl: './chat-group-friend-list.component.html',
  styleUrls: ['./chat-group-friend-list.component.css']
})
export class ChatGroupFriendListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  
  @Input() isChecked:boolean;
  @Input() friend:any;
  @Output() isSelected = new EventEmitter<any>();
  @Output() isRemoved = new EventEmitter<any>();

  checkBoxChecked(event:any) {
       console.log(event);
       if (event.srcElement.checked == true)  this.isSelected.emit(this.friend);
            
       else this.isRemoved.emit(this.friend);
       
  }
  
  
}
