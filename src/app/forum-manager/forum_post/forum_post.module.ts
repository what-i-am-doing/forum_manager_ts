import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { AutoCompleteComponent } from 'src/app/forum-manager/forum_post/auto-complete/auto-complete.component';
import { EmojiModule } from 'src/app/lib/emoji/emoji.module';
import { PickerModule } from 'src/app/lib/picker/picker.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ThumbnailComponent } from 'src/app/forum-manager/forum_post/thumbnail/thumbnail.component';

import {
  MatIconModule,
  MatButtonModule,

  MatFormFieldModule,
  MatInputModule,
  MatAutocompleteModule
} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { LightboxModule } from 'ngx-lightbox';
import { ForumPostCommentComponent } from './forum_post_comment/forum_post_comment.component';
import { CreateForumPostComponent } from './create-forum_post/create-forum_post.component';
import { UploadThumbnailComponent } from './upload-thumbnail/upload-thumbnail.component';
import { EditorModule } from '@tinymce/tinymce-angular';
import { MatChipsModule } from '@angular/material/chips';
import { MatMenuModule} from '@angular/material/menu';
import { MatCardModule } from '@angular/material';
import { InfiniteScrollModule } from 'node_modules/ngx-infinite-scroll';
import { HoverListComponent } from './hover-list/hover-list.component';
import { TooltipComponent } from './like-tooltip/like-tooltip.component';
import { ShareTooltipComponent } from './share-tooltip/share-tooltip.component';
import { CommentTooltipComponent } from './comment-tooltip/comment-tooltip.component';
import { ReportTooltipComponent } from './report-tooltip/report-tooltip.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { ModalModule } from 'ngx-bootstrap/modal';
import { LikeModalComponent } from './forum_post/like-modal/like-modal.component';
import { ShareForumPostComponent } from './share-forum_post/share-forum_post.component';
import { ForumPostSnapshotComponent } from './forum_post-snapshot/forum_post-snapshot.component';
import {ForumPostComponent} from 'src/app/forum-manager/forum_post/forum_post/forum_post.component';
import { ForumThumbnailComponent } from './forum-thumbnail/forum-thumbnail.component';
@NgModule({
    declarations: [
      ForumPostComponent,
      ForumPostCommentComponent,
      CreateForumPostComponent,
      UploadThumbnailComponent,
      AutoCompleteComponent ,
      ThumbnailComponent,
      HoverListComponent,
      TooltipComponent,
      ShareTooltipComponent,
      CommentTooltipComponent,
      ReportTooltipComponent,
      LikeModalComponent,
      ShareForumPostComponent,
      ForumPostSnapshotComponent,
      ForumThumbnailComponent,

    ],
    imports: [
      CommonModule,
      FormsModule,
      LightboxModule,
      EditorModule,
      NgxExtendedPdfViewerModule,
      MatIconModule,
      MatButtonModule,
      InfiniteScrollModule,
      MatMenuModule,
      MatCardModule,
      MatFormFieldModule,
      MatInputModule,
      EmojiModule,
      PickerModule,
      MatChipsModule,
      MatAutocompleteModule,
      ReactiveFormsModule,
      TooltipModule,
      ModalModule
    ],
    exports: [ForumPostComponent, CreateForumPostComponent, ShareForumPostComponent,InfiniteScrollModule, MatButtonModule, MatCardModule,
      MatMenuModule]
  })
  export class ForumPostModule {}
