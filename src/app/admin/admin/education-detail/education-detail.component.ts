import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-education-detail',
  templateUrl: './education-detail.component.html',
  styleUrls: ['./education-detail.component.css']
})
export class EducationDetailComponent implements OnInit {
@Input() uesrId;
public  activeUserEducationalData:any;
  constructor(private  service:UserService) { }

  ngOnInit() {
this.getUserEducationalDetails(this.uesrId)
  }

  getUserEducationalDetails(userId){
    
    try{
      this.service.getEducationDetailsDataByUserId(userId).subscribe(data=>{
  
        console.log(data);
        this.activeUserEducationalData=data;

    })
  }catch(err){


    }
  }




}
