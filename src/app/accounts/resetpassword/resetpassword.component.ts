import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router } from "@angular/router";
import { User } from '../../model/user';

import { UserAuth } from '../../model/userAuth';
@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css'],
  providers: [UserService]
})
export class ResetpasswordComponent implements OnInit {

   
    
  
    public userAuthData:any;
    public showOtpSection: boolean = true;
    public showPassSection: boolean = false;
    passnotmatch:boolean=false;
    passchange:boolean=false;
    otpnotmatch:boolean=false;
    showLoginAlertSection:boolean=false;
    userAuthEmail:string;
  constructor(public router: Router, public service: UserService) {

    this.userAuthData=JSON.parse(localStorage.getItem('UserAuthData'));
    this.userAuthEmail=localStorage.getItem('UserAuthEmail');

    console.log(this.userAuthData);
    this.userAuthEmail=localStorage.getItem('UserAuthEmail');

  }

  ngOnInit() {
  }

  updatePassword(newpassword:string, confirmpassword:string) {
    console.log("update password called ");

if(newpassword==null|| confirmpassword==null || newpassword.length<1 || confirmpassword.length<1){

  // alert("Please enter your password");
  

}

   else if (newpassword!=confirmpassword) {

  
    this.show_pass_not_match_error_alert();

    }
    else if(newpassword==confirmpassword) {


      var updateUserPasswordObj={
        "email": this.userAuthEmail,
        "password": newpassword,
        "last_updated_timestamp": Date.now()
        
        }



this.service.updateUserPassword(updateUserPasswordObj,  this.userAuthData.id).subscribe(data => {
   
        console.log(data);

        this.showLoginalertSection();

     }
     
      , error => {

      console.log(error);
     // this.showwerroralert();
  
    }
     
     
     )
      

    }
  }


  verifyOtp(otp){
    if (otp) {
      this.showPassSection = true;
      this.showOtpSection = false;

    }
    else {

     this.show_otp_not_match_error_alert();
    }
  }
  cancelUser() {
    
    this.router.navigate(['/login']);


  }

  close(){
    this.passchange=false;
    this.passnotmatch=false;
   this.otpnotmatch=false;

  }



  show_otp_not_match_error_alert(){

    this.otpnotmatch=true;
    setTimeout(()=> this.otpnotmatch = false,5000)
   
  }

  show_pass_not_match_error_alert(){

    this.passnotmatch=true;
    setTimeout(()=> this.passnotmatch = false,5000)
   
  }

navigateToLogin(){
this.router.navigateByUrl('/login');

  }

  showLoginalertSection(){
this.showPassSection=false;
this.showOtpSection=false;
this.showLoginAlertSection=true;

  }
}
