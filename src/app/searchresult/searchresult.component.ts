import { Component, OnInit, PipeTransform, Pipe } from "@angular/core";
import { UserService } from "../services/user.service";
import { SearchService } from "../services/search.service";
import { FilterPipe } from "../filter/filter.pipe";
import { MatDialog } from "@angular/material";
import { GroupmodelComponent } from "../groupmodel/groupmodel.component";
import { Router, NavigationEnd } from "@angular/router";
import { PostService } from "../services/post.service";
import { Post } from "../model/post";
import { DomSanitizer } from "@angular/platform-browser";
import { ImageService } from "../services/image.service";
import { PdfService } from "../services/pdf.service";
import { VideoService } from "../services/video.service";
import { CourseService } from "../services/course.service";
import { UsersearchService } from "../services/usersearch.service";
import { GroupService } from "../services/group.service";

export class Comment {
  constructor(
    public post_id: string,
    public user_id: string,
    public comment: string,
    public created_timestamp: number,
    public last_updated_timestamp: number,

  ) { }
}

@Component({
  selector: "app-searchresult",
  host: {
    "(document:click)": "functionClick($event)"
  },
  templateUrl: "./searchresult.component.html",
  styleUrls: ["./searchresult.component.css"],
  providers: [
    UserService,
    FilterPipe,
    GroupmodelComponent,
    SearchService,
    PostService,
    PdfService,
    VideoService,
    ImageService,
    UsersearchService,
    GroupService
  ]
})

export class SearchresultComponent implements OnInit {
  navigationSubscription: any;
  videoIDnew: string = "gWjofpeBUU4?modestbranding=1";
  allPosts: Post[];
  initialFilteredPosts: Array<any> = [];
  fetchedPosts: Array<any> = [];
  AllFilteredPosts: Array<any> = [];
  initialFilteredUsers: Array<any> = [];
  public sentFriendRequestListids: Array<any> = [];
  allFilteredUsers: Array<any> = [];
  userId: string = localStorage.getItem("user");
  userGroup: Array<any> = JSON.parse(localStorage.getItem("user_group"));
  userLevel: string = localStorage.getItem("user_level");
  userTag: string = localStorage.getItem("user_tag");
  searchQuery: string = localStorage.getItem("searchquery");
  groupId = localStorage.getItem("groupId");
  active: boolean = false;
  currentThing: any;
  public youtubeId;
  public users: any;
  public searchText: string;
  public term: string;
  public Found_friendlist: any[];
  public friendName = "";
  // public results: any[];
  public videoArray: Array<any> = [];
  public friendids: Array<any> = [];
  public values = "";
  public total_friend: any;
  public groups: any;
  public levels: any;
  public tags: any;
  public showfriendlist = true;
  public flag = false;
  public mouseover = false;
  public duration: number;
  public durationarray = [];
  public durationArrayObject;
  listORgrid: boolean = false;
  public mobileFriendlyZoomPercent = false;
  public hidden: boolean = true;
  allUserData: Array<any> = [];
  public showPostLoader = true;
  public showCourseLoader = true;
  public showModuleLoader = true;
  public showPeopleLoader = true;
  public showVideoLoader = true;
  public fromSizeforPeople: number = 0;
  public toSizeforPeople: number = 5;
  public fromSizeforPost: number = 0;
  public toSizeforPost: number = 5;
  public fromSizeforVidoes: number = 0;
  public toSizeforVidoes: number = 6;
  public showoverlay: boolean = false;
  public groupCourses: any;
  public groupModules: any;
  content: string = 'Share this video with friends';
  copied: boolean = false;
  gotCourse:any;
  gotModule:any;
  
  constructor(private sanitizer: DomSanitizer, private service: UserService, private postService: PostService,
    private searchService: SearchService,
    private groupService: GroupService,
    private videoService: VideoService,
    private courseService: CourseService,
    public modal: MatDialog, private router: Router, private userSearch: UsersearchService
  ) {

    console.log(localStorage.getItem("searchquery"));
    if (localStorage.getItem("searchquery").length > 0) {
    }



    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {

        console.log(JSON.parse(localStorage.getItem("filter")));
        console.log(localStorage.getItem("searchquery"));
        this.searchQuery = localStorage.getItem("searchquery");
        this.allPosts = [];
        this.groupCourses = [];
        this.groupModules = [];
        this.initialFilteredPosts = [];
        this.fetchedPosts = [];
        this.AllFilteredPosts = [];
        this.initialFilteredUsers = [];
        this.allFilteredUsers = [];
        this.videoArray = [];
        this.getPostByGroupsearchtext(0, 100);
        this.getUserByGroupsearchByName(0, 100);
        this.showYoutubeVideos();
        this.getGroupCoursesByText();
        this.getModulesByText();
      }
    });
  }

  // search the input keyword entered by the user into our database's received data array

  //if users click somewhere else in the document
  functionClick() {
    this.flag = false;
  }
  pressKeyboard(event: KeyboardEvent, name) {
    if (event.key === "Enter") {
      this.friendName = name;
    }
  }
  public arrowkeyLocation = 0;

  ngOnInit() {
    this.groupService.getUserlistByGroup(this.groupId, 0, 100).subscribe(data => this.allUserData = data);
    this.showYoutubeVideos();
    try {

      this.getFriendListOfActiveUser(this.userId, this.userGroup[0], 0, 100);
      this.getSentFriendRequestByActiveUser(this.userId, this.userGroup[0]);
    }

    catch (err) { }
    this.getPostByGroupsearchtext(0, 100);
    this.getUserByGroupsearchByName(0, 100);
    this.getGroupCoursesByText();
    this.getModulesByText();
  }

  toggle: boolean = false;
  userName: string = localStorage.getItem("user_name");

  getPostByGroupsearchtext(from: number, to: number) {
    this.fetchedPosts = [];
    this.postService
      .getPostByGroupsearchtext(this.userGroup[0], this.searchQuery, from, to)
      .subscribe(posts => {
        this.showPostLoader = false;
        this.fetchedPosts = posts;
        console.log(this.fetchedPosts);
        this.initialFilteredPosts.length
          ? this.AllFilteredPosts.concat(this.fetchedPosts)
          : (this.initialFilteredPosts = this.AllFilteredPosts = this.fetchedPosts);

        console.log(this.fetchedPosts);
      });
  }

  getUserByGroupsearchByName(from, to) {
    this.userSearch
      .getUserByGroupSearchByName(this.userGroup[0], this.searchQuery, from, to)
      .subscribe(users => {
        this.showPeopleLoader = false;
        console.log(users);
        this.initialFilteredUsers.length
          ? this.allFilteredUsers.concat(users)
          : (this.initialFilteredUsers = this.allFilteredUsers = users);
      });
  }

  getGroupCoursesByText() {
    this.courseService.searchCourseByText(this.searchQuery, this.groupId, 0, 10).subscribe(
      courses => {
        this.groupCourses = courses
        this.showCourseLoader = false;

      }
    );
  }

  getModulesByText() {
    this.courseService.getModulesBySearchText(this.searchQuery, this.groupId, 0, 10).subscribe(
      modules => {
        console.log('@@@@@@QWERTY',modules);
        this.groupModules = modules;
        this.showModuleLoader = false;

      }
    );
  }

  toggle_settings() {
    this.toggle = !this.toggle;
  }

  getDataforPeople() {
    this.getUserByGroupsearchByName(5, 15);
    try {

      this.getFriendListOfActiveUser(this.userId, this.userGroup[0], 0, 100);
      this.getSentFriendRequestByActiveUser(this.userId, this.userGroup[0]);
    } catch (err) {


    }
  }

  // getYoutubeVideoBasedOnQuery(searchQuery) {
  //   console.log(" youtube video called");

  //   this.service.getYoutubeVideoBasedOnQuery(searchQuery).subscribe(data => {
  //     console.log(data);

  //     this.videoArray = Object.values(data);
  //     console.log(this.videoArray);

  //     var ids = [];

  //     var tempdurationarray = [];

  //     for (let i = 0; i <= this.videoArray[5].length; i++) {
  //       try {
  //         // console.log(this.videoArray[5][i].id.videoId);
  //         if (
  //           this.videoArray[5][i] != null &&
  //           this.videoArray[5][i] != undefined
  //         ) {
  //           this.videoArray[5][i].id.videoId = this.createSrc(
  //             this.videoArray[5][i].id.videoId
  //           );

  //           //ids.push(this.createSrc(this.videoArray[5][i].id.videoId));
  //           ids.push(this.videoArray[5][i]);
  //           this.service
  //             .getDurationOfVideos(this.videoArray[5][i].id.videoId)
  //             .subscribe(data => {
  //               tempdurationarray.push(data);
  //             });
  //         }
  //       } catch (err) {
  //         console.log(err);
  //       }
  //     }

  //     this.videoIdArray = ids;

  //     this.durationarray = tempdurationarray;
  //     console.log(this.videoIdArray);
  //     console.log(this.videoIdArray[0].snippet);
  //     console.log(this.videoIdArray[0].snippet.thumbnails);
  //     console.log(this.videoIdArray[0].snippet.thumbnails.default.url);
  //     console.log(this.videoIdArray[0].snippet.thumbnails.default.url[0]);
  //     console.log(this.durationarray);
  //     console.log(this.durationarray.length);

  //     console.log(this.durationarray.indexOf(0, 10));
  //   });
  // }
  //open create group modal box
  showYoutubeVideos() {
    this.getYoutubeVideoBasedOnQuery(localStorage.getItem("searchquery"));
  }

  getYoutubeVideoBasedOnQuery(searchQuery) {
    console.log(" youtube video called");

    this.videoService.getVideobySearchText(searchQuery, this.groupId, 0, 0, 0, 100).subscribe(data => {
      this.showVideoLoader = false;

      this.videoArray = data;
      console.log('video array');
      console.log(this.videoArray);


    })
  }

  openDialog() {
    console.log("inovked ");
    this.modal.open(GroupmodelComponent);
  }

  openServerDialog() {
    let serverDialogRef = this.modal.open(GroupmodelComponent, {
      width: "50%",
      height: "60%",

      data: { name: "lavkush" }
    });
  }

  // show friend list of the users
  showFriendList() {
    console.log(" function called");
    this.showfriendlist == true;
  }

  onChange(event) { }

  createSrc(videoId) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(
      "https://www.youtube.com/embed/" + videoId + "?modestbranding=1"
    );
    // myframe.setAttribute('src', 'https://www.youtube.com/embed/'+'gWjofpeBUU4'+'?modestbranding=1');
    //console.log('https://www.youtube.com/embed/'+videoId+'?enablejsapi=1&amp;origin=http%3A%2F%2Flocalhost%3A4200&amp;widgetid=1&amp;modestbranding=1');
    // myframe.setAttribute('src' ,'https://www.youtube.com/embed/'+'gWjofpeBUU4'+'?modestbranding=1');
    //return 'https://www.youtube.com/embed/'+videoId+'?enablejsapi=1&amp;origin=http%3A%2F%2Flocalhost%3A4200&amp;widgetid=1&amp;modestbranding=1';
  }

  getLocalVidoes() {
    this.service.getLocalVidoesLink().subscribe(localdata => {
      console.log(localdata);

      var result = [];

      var local_data = Object.values(localdata);
      console.log(local_data.length);
      for (let i = 0; i < local_data.length; i++) {
        result.push(local_data[i].videolink);
      }

      console.log(result);
      var newresult = [];
      newresult = result[0].split("v=");
      console.log(newresult);
      this.youtubeId = newresult[1];
      console.log(this.youtubeId);
    });
  }

  getAllFilteredPosts() {
    this.getPostByGroupsearchtext(3, 10);
  }

  seeFriendProfile(userId) {

    this.router.navigate(['/home/userprofile'], { queryParams: { userId: userId } });
  }

  sendFriendRequest(frienduserId, group_id, index) {

    var requestObj = {

      "comments": "string",
      "creation_timestamp": Date.now(),
      "from_user_id": this.userId,
      "group_id": group_id,
      "message": "sending friend Request",
      "relation_type_id": "FRIEND",
      "req_status": "OPEN",
      "status": "ACTIVE",
      "to_user_id": frienduserId


    }


    this.service.sendFriendRequest(requestObj, this.userId).subscribe(data => {


      console.log(data);
      if (data.id) {

        var element = <HTMLInputElement>document.getElementById(frienduserId);

        document.getElementById(frienduserId).innerText = "Request Sent";

        element.disabled = true;
        setTimeout(() =>



          delete this.initialFilteredUsers[index].user_full_text_name,
          2000)

      }

    })

  }

  getSentFriendRequestByActiveUser(userId, groupId) {

    this.service.getSentFriendRequestByActiveUser(userId, groupId).subscribe(data => {

      if (data != null || data != undefined)
        data.forEach(element => {
          if (element.req_status == 'OPEN') {
            this.sentFriendRequestListids.push(element.to_user_id);

          }

        });


    })

  }

  changeUrl(url) {
    let i = 0;
    return url.replace('watch?v=', 'embed/');
  }

  getFriendListOfActiveUser(userId: any, group_id, fromsize, toSize) {
    this.service.getFriendListOfActiveUser(userId, group_id, fromsize, toSize).subscribe(data => {

      if (data != null || data != undefined) {

        data.forEach(element => {
          this.friendids.push(element.friend_user_id);
        });
      }

    });
  }





  seeMorePeople(startIndex, endIndex) {

    if (this.toSizeforPeople < this.initialFilteredUsers.length) {
      this.toSizeforPeople = this.toSizeforPeople + 5;
    }
    else {
      this.fromSizeforPeople = 0;
      this.toSizeforPeople = 5;
    }
  }


  seeMorePosts(startIndex, endIndex) {

    if (this.toSizeforPost < this.initialFilteredPosts.length)
      this.toSizeforPost = this.toSizeforPost + 5;

    else {
      this.fromSizeforPost = 0;
      this.toSizeforPost = 5;
    }
  }

  seeMoreVidoes(startIndex, endIndex) {

    if (this.fromSizeforVidoes < this.videoArray.length) this.toSizeforVidoes = this.toSizeforVidoes + 6;

    else {
      this.fromSizeforVidoes = 0;
      this.toSizeforVidoes = 5;
    }
  }

  playVideo(ev, thumbnailId, videoId) {
    console.log(thumbnailId)
    console.log(document.getElementById(videoId) as HTMLImageElement)
    var result = document.getElementById(videoId) as HTMLImageElement;
    console.log(result.src)
    result.src += "&autoplay=1";
    this.showoverlay = true;
    setTimeout(() => this.toggleVideo(thumbnailId, videoId), 1000);
  }

  toggleVideo(thumbnailId, videoId) {

    document.getElementById(thumbnailId).style.display = 'none';
    document.getElementById(videoId).style.display = 'block';
  }




  showwasha(thumbnailId) {
    document.getElementById('show' + thumbnailId).style.display = 'block';
  }

  dontshowwasha(thumbnailId) {
    document.getElementById('show' + thumbnailId).style.display = 'none';

  }

  copyToClipBoard(str) {
    str = location.protocol + "//" + location.host + '/#/scholars.book/' + str;
    const el = document.createElement('textarea');  // Create a <textarea> element
    el.value = str;                                 // Set its value to the string that you want copied
    el.setAttribute('readonly', '');                // Make it readonly to be tamper-proof
    el.style.position = 'absolute';
    el.style.left = '-9999px';                      // Move outside the screen to make it invisible
    document.body.appendChild(el);                  // Append the <textarea> element to the HTML document
    const selected =
      document.getSelection().rangeCount > 0        // Check if there is any content selected previously
        ? document.getSelection().getRangeAt(0)     // Store selection if found
        : false;                                    // Mark as false to know no selection existed before
    el.select();                                    // Select the <textarea> content
    document.execCommand('copy');                   // Copy - only works as a result of a user action (e.g. click events)
    document.body.removeChild(el);                  // Remove the <textarea> element
    if (selected) {                                 // If a selection existed before copying
      document.getSelection().removeAllRanges();    // Unselect everything on the HTML document
      document.getSelection().addRange(selected);   // Restore the original selection
    }

    this.copied = true;

    setTimeout(() => this.copied = false, 2000);
  }
}

