import { Component, OnInit, Input, ViewChild, ElementRef, OnChanges, SimpleChanges } from 'node_modules/@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from 'node_modules/@angular/forms';
import { ImageService } from '../../services/image.service';
import { UserService } from '../../services/user.service';
import { VideoService } from '../../services/video.service';
import { Lightbox, LightboxConfig, LightboxEvent, LIGHTBOX_EVENT, IEvent } from 'node_modules/ngx-lightbox';
import { Subscription } from 'node_modules/rxjs';
import { ForumService } from '../../services/forum.service';
import { AddressService } from '../../services/address.service';
import { ForumPostService } from '../../services/forum_post.service';
import { Forum } from 'src/app/model/Forum';
import { NgModule } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-forum-view',
  templateUrl: './forum-view.component.html',
  styleUrls: ['./forum-view.component.css']
})

export class ForumViewComponent implements OnInit, OnChanges {
 constructor(private _formBuilder: FormBuilder, private imageService: ImageService,
    private addressService: AddressService,
    private videoService: VideoService,
    private forumService: ForumService,
    private forumpostService: ForumPostService,
    private userService: UserService,
    private _lightbox: Lightbox,
    private _lightboxEvent: LightboxEvent,
    private _lighboxConfig: LightboxConfig,
    private router: Router) { }
  @ViewChild('acceptrequest', { read: ElementRef }) acceptrequest: ElementRef;
  @ViewChild('deleterequest', { read: ElementRef }) deleterequest: ElementRef;
//  @ViewChild('blockrequest', { read: ElementRef }) blockrequest: ElementRef;
  arrofrequest: any = [];
  basicGroup: FormGroup;
  imgGroup: FormGroup;
  detailGroup: FormGroup;
  public acceptuserrequest = false;
  public deleteuserrequest = false;
  public blockuserrequest = false;
  public profilePic: any;

  descGroup: FormGroup;
  private _subscription: Subscription;
  coUpdated = false;
  edit = false;
  forumPic: string;
  video: any;
  showExt = false;
  activeLink = false;
  curForum: any;
  public activeUserPersonalData: any;
  public activeUserEducationalData: any;

  @Input() forum: any;
  @Input() readable = false;

  path: any = [];

 forum_id: any;
  public  a = [];
  public requestresult: any;
  public requestresult1: any;
   public d: any = [];
   public e: any = [];
   userdetails: any;
  image_id: any;
  gender: any;

 public city: any;
  ngOnInit() {

    console.log('_____ooo', this.forum, this.forum.forum_name, this.forum.forum_id);
    this.video = undefined;
    this.forumPic = undefined;
    this.curForum = undefined;
    this.forum_id = this.forum.forum_id;
    localStorage.setItem('forum_id', this.forum_id);

    this.basicGroup = this._formBuilder.group({
      basicCtrl: [{ value: this.forum.forum_name, disabled: true }, Validators.required],
      basicCtrl2: [{ value: this.forum.forum_type_id, disabled: true }, Validators.required],
    });

    this.imgGroup = this._formBuilder.group({
      imgCtrl1: [{ value: this.forum.heading, disabled: true }, Validators.required],
      imgCtrl2: [{ value: this.forum.status, disabled: true }, Validators.required],

    });

    this.detailGroup = this._formBuilder.group({
      detailCtrl1: [{ value: this.forum.category, disabled: true }, Validators.required],
      detailCtrl2: [{ value: this.forum.is_education_content, disabled: true }, Validators.required],


    });
    this.descGroup = this._formBuilder.group({
      descCtrl1: [{ value: this.forum.forum_description, disabled: true }]
    });


    if (this.forum.image_id) {
      this.imageService.getImage(this.forum.image_id).subscribe(
        img => this.forumPic = this.URLChange(img.image_storage_url)
      );
    }
    this.getforumrequest();


}
  forum_name;
  forum_description;
  forum_type;
  custom_tag_line;
  getforumrequest() { 
    let group_id = 'public';

    let req_status = 'OPEN';
    this.forum_name = this.forum.forum_name;
    this.forum_description = this.forum.forum_description;
    this.forum_type = this.forum.forum_type;
    this.custom_tag_line = this.forum.custom_tag_line;
    this.forumService.getRequestsForForumByStatus(this.forum.forum_id, group_id, req_status).subscribe(
      (res) => {
        this.arrofrequest=res;
         console.log('gh', res);


        console.log('hj', this.arrofrequest);
        res.forEach(element => {
         console.log('godzilla',element)
      // let  eff :Array<any>[];
    // eff.push(element.forum_request_id);
      // this.e = eff;
     //  console.log('effffff',eff);
       //     var ghi :Array<any>[];
         //   ghi.push(element.from_user_id);
            this.a.push(element.from_user_id);
            console.log('haha',this.a);});
        this.a.forEach(element=>{

           
            this.userService.getUserDetails(element).subscribe(receivedUserData => {
               // console.log('eleeeemmmmmt', element)
                this.requestresult = receivedUserData ;
                console.log('reeee', this.requestresult);
                 this.d.push(this.requestresult);
                const uniqueArray = this.removeDuplicates(this.d, 'from_user_id');
                this.requestresult1 = uniqueArray ;
                console.log('thissssss',this.d);
                this.d.push(this.requestresult1);
            });
          })
     //  });
   }
      );
      console.log('dddddddddd', this.d);
    }
  getUserFriendDetails(userId: any) {
    this.userService.getUserDetails(userId).subscribe(receivedUserData => {

      this.requestresult  = receivedUserData;
      this.gender = receivedUserData.gender_type;
      this.image_id = receivedUserData.img_id;
      this.getUsersFriendProfileImage(this.image_id);
    });
}
removeDuplicates(myArr, prop) {
    console.log('duplicates in friend request called');
    return myArr.filter((obj, pos, arr) => {
    return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
  });
 }



  getUsersFriendProfileImage(image_id) {
    console.log('thumbnail calling');
    console.log(image_id);
    this.imageService.getImageByType(image_id, 'thumbnail').subscribe(data => {
      console.log(data);
      if (data.length > 0) {
        console.log('thumbnail found');
        console.log(data[0].image_storage_url);
        this.profilePic = this.URLChange(data[0].image_storage_url);
      } else {
        this.getProfileImage(image_id);
      }
    });
  }
  getProfileImage(image_id) {
    if (image_id != '' || image_id != 'string' || image_id != null) {


      this.imageService.getImage(image_id).subscribe(data => {
        console.log('image found');
        console.log(data.image_storage_url);
        this.profilePic = this.URLChange(data.image_storage_url);

      }

        , err => {
          console.log('image not found');
          switch (this.gender) {
            case 'MALE':
              this.profilePic = 'assets/images/male.jpg';
              break;
            case 'FEMALE':
              this.profilePic = 'assets/images/female.jpg';
              break;
          }
        }
      );
    } else {
      switch (this.gender) {
        case 'MALE':
          this.profilePic = 'assets/images/male.jpg';
          break;
        case 'FEMALE':
          this.profilePic = 'assets/images/female.jpg';
          break;
      }
    }

  }


 accecptForumRequest(from_user_id) {
   var userID =from_user_id;
  console.log(from_user_id, 'g63');
  this.arrofrequest.forEach(element=>{
      if(element.from_user_id==from_user_id)
      {

  this.userService.updateForumrequest(element.forum_request_id, { 'forum_request_status': 'ACCEPTED' }).subscribe((data) => {
      console.log(data, 'gooooooo');
      this.acceptrequest.nativeElement.style.display = 'none';
      this.deleterequest.nativeElement.style.display = 'none';
     // this.blockrequest.nativeElement.style.display = 'none';
      this.acceptuserrequest = true;
      alert("Request Accepted ")
     // this.addforumtouserforumlist(userID);
    });
   
      }
   }
  );
  }

  addforumtouserforumlist(forumReqUserId){
    var user_detailsObj = {
      "forums_list": "this.forum_id"
    }
    this.userService.updateUserDetail(user_detailsObj,forumReqUserId).subscribe(
      (res: any) => {
        console.log('forum_list',res);
       });

  }


deleteForumRequest(from_user_id) {
  this.arrofrequest.forEach(element=>{
    if(element.from_user_id==from_user_id)
    {

this.userService.updateForumrequest(element.forum_request_id, {'forum_request_status': 'DECLINED'}).subscribe(data => {
      this.acceptrequest.nativeElement.style.display = 'none';
      this.deleterequest.nativeElement.style.display = 'none';
     // this.blockrequest.nativeElement.style.display = 'none';
      this.deleteuserrequest = true;
      alert('forum joining request declined')
    });
    if(this.deleteuserrequest==true)
    {
      this.getforumrequest();
    }
  }})
  }

// blockFriendRequest(forumrequestId) {
// this.userService.updateForumrequest(forumrequestId, {'req_status': 'BLOCKED'}).subscribe(data => {
//       console.log(data);
//       this.acceptrequest.nativeElement.style.display = 'none';
//       this.deleterequest.nativeElement.style.display = 'none';
//       this.blockrequest.nativeElement.style.display = 'none';
//       this.blockuserrequest = true;
//     });

//   }

  getUserPersonalDetails(userId) {
    try {
      this.userService.getUserPersonalDetails(userId).subscribe(data => {
        console.log(data);
        this.activeUserPersonalData = data;
        this.getAdress(data.address_id);
      });
    } catch (err) {
    }
  }

  getUserEducationalDetails(userId) {

    try {
      this.userService.getEducationDetailsDataByUserId(userId).subscribe(data => {

        console.log(data);
        this.activeUserEducationalData = data;

      });
    } catch (err) {


    }
  }


  seeFriendProfile(userId) {
    this.router.navigate(['/home/userprofile'], { queryParams: { userId: userId } });

  }
  getAdress(adressId) {
    this.addressService.getAdress(adressId).subscribe(data => {
      console.log('adress data in friend request list found');
      console.log(data);

      this.city = data.city;

    });
  }

  ngOnChanges(changes: SimpleChanges) {
    // console.log(changes);
    if (!changes.forum.firstChange) { this.ngOnInit(); }
  }
  ViewForumPosts() {
    // console.log('kpl',this.forum);
    let forum = this.forum;
    let forumID = this.forum.forum_id;


    // console.log('kpl',this.forum);
   // this.router.navigate(['appforumpostview'],{queryParams:{forum:this.forum.forum_id}});
  }

  editforum() {
    this.edit = true;
    this.basicGroup.get('basicCtrl').enable();
    this.basicGroup.get('basicCtrl2').enable();
    this.imgGroup.get('imgCtrl1').enable();
    this.imgGroup.get('imgCtrl2').enable();
    this.detailGroup.get('detailCtrl1').enable();
    this.detailGroup.get('detailCtrl2').enable();
    this.detailGroup.get('detailCtrl3').enable();
    this.descGroup.get('descCtrl1').enable();

    console.log(this.basicGroup);
    console.log(this.imgGroup);
    console.log(this.detailGroup);
    console.log(this.descGroup);

  }

  updateforum() {


let forum = new Forum;
forum.$forum_name = this.basicGroup.value.basicCtrl;
forum.$forum_type = this.basicGroup.value.basicCtrl2;
forum.$enabled_for_groups = this.basicGroup.value.basicCtrl;
forum.$status = this.imgGroup.value.imgCtrl2;
forum.$forum_description = this.descGroup.value.descCtrl1;

    this.forumService.updateForum(this.forum.forum_id, forum).subscribe(
      () => {
        this.coUpdated = true;
        this.updatedForumView();
        this.removeEdit();
      }
    );
  }

  updatedForumView() {
    this.forum.forum_name = this.basicGroup.value.basicCtrl;
    this.forum.forum_type = this.basicGroup.value.basicCtrl2;
    this.forum.heading = this.imgGroup.value.imgCtrl1;
    this.forum.status = this.imgGroup.value.imgCtrl2;
    this.forum.forum_description = this.descGroup.value.descCtrl1;
  }

  removeEdit() {
    this.edit = false;
    this.basicGroup.get('basicCtrl').disable();
    this.basicGroup.get('basicCtrl2').disable();
    this.imgGroup.get('imgCtrl1').disable();
    this.imgGroup.get('imgCtrl2').disable();
    this.detailGroup.get('detailCtrl1').disable();
    this.detailGroup.get('detailCtrl2').disable();
    this.detailGroup.get('detailCtrl3').disable();
    this.descGroup.get('descCtrl1').disable();
  }


  URLChange(url: any) {
    return url.replace('https', 'http');
  }


  open(index: number, album): void {
    this._subscription = this._lightboxEvent.lightboxEvent$.subscribe(
      (event: IEvent) => this._onReceivedEvent(event)
    );

    // override the default config
    this._lightbox.open(album, index, {
      wrapAround: true,
      showImageNumberLabel: true
    });
  }

  close(): void {
    // close lightbox programmatically
    this._lightbox.close();
  }

  private _onReceivedEvent(event: IEvent): void {
    if (event.id === LIGHTBOX_EVENT.CLOSE) {
      this._subscription.unsubscribe();
    }
  }
  backButton() {
    this.curForum = undefined;
     if (!this.path.length) { this.path.pop(); }
      if (this.path.length == 1) {
         this.curForum = undefined;
        } else { (this.path.length == 2) }
        this.curForum = undefined;
  }
}
