import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ConfirmEqualValidatorDirective } from '../../shared/confirm-equal-validator.directive';
import { LoginComponent } from './login/login.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { RouterModule } from '@angular/router';
import { UserVerificationComponent } from './user-verification/user-verification.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { ReactiveFormsModule } from '@angular/forms';
import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { getAuthServiceConfigs } from './AuthServiceConfig';
import { GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider} from "angularx-social-login";
import { SessionOutComponent } from './session-out/session-out.component';

var Google_OAuth_Client_Id='139417300527-o68k9jka1bu8sji6je4b8jof79n0vh87.apps.googleusercontent.com'

const config = new AuthServiceConfig([
{
id: GoogleLoginProvider.PROVIDER_ID,
provider: new GoogleLoginProvider('139417300527-o68k9jka1bu8sji6je4b8jof79n0vh87.apps.googleusercontent.com')
},
{
id: FacebookLoginProvider.PROVIDER_ID,
provider: new FacebookLoginProvider('444849732941061')
},
// {
// id: LinkedInLoginProvider.PROVIDER_ID,
// provider: new LinkedInLoginProvider('78iqy5cu2e1fgr')
// }
]);

export function provideConfig() {
return config;
}



@NgModule({
declarations: [LoginComponent,
ConfirmEqualValidatorDirective,
ForgetpasswordComponent,
ResetpasswordComponent,
UserVerificationComponent,
SessionOutComponent

],
imports: [
CommonModule,ReactiveFormsModule,FormsModule,BsDatepickerModule.forRoot(),RouterModule.forChild(

[

{ path: 'forgetPassword', component: ForgetpasswordComponent },
{ path: 'resetPassword', component: ResetpasswordComponent },
{ path: 'user-verification', component: UserVerificationComponent },
{ path: 'session-out', component: SessionOutComponent }

]

),
NgxIntlTelInputModule,
SocialLoginModule

],
providers:[ {
provide: AuthServiceConfig,
useFactory: provideConfig
}],

exports:[LoginComponent,ForgetpasswordComponent,ResetpasswordComponent]
})
export class AccountsModule { }