import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'children',
  template: ` 
            <div *ngFor="let child of children">
                    <ul>
                      <li>
                       <span class="node-elements" (click)="clickMe($event, child)"> {{child.filename}}</span>
                        <children [children]="child.children" *ngIf="child.children"></children>
                      </li>
                    </ul>
            </div>`,
  styleUrls: ['./children.component.css']
})

export class ChildrenComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    
  }

  @Input() children;

  clickMe(event: Event,child){

    event.stopPropagation();
    this.children = child.children;
    console.log (child.filename);
    console.log("child");
    console.log(child);
    console.log("children");
    console.log(this.children);
   // alert(this.children.length);

  }

}
