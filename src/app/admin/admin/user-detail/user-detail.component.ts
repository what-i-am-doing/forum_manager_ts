import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ImageService } from 'src/app/services/image.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css'],
  providers: [UserService, ImageService]
})
export class UserDetailComponent implements OnInit {
  @Input() userId: any;
  public imageId: any;
  public username: any;
  profilePic: any;
  gender: any;
  constructor(private userService: UserService, private imageService: ImageService, private router: Router) { }

  ngOnInit() {
    this.getUserDetails(this.userId);
    this.getActiveUserPersonalDetails(this.userId);
  }

  getUserDetails(userId) {

    this.userService.getUserDetails(userId).subscribe(data => {

      this.username = data.user_full_text_name;
      this.getProfile(data.image_id);

    })


  }


  getProfile(image_id) {
    this.imageService.getImage(image_id).subscribe(data => {

      this.profilePic = this.URLChange(data.image_storage_url);

    }, err => {

      switch (this.gender) {
        case 'MALE':
          this.profilePic = 'assets/images/male.jpg'
          break;
        case 'FEMALE':
          this.profilePic = 'assets/images/female.jpg';
          break
      }
      this.profilePic = 'assets/images/male.jpg'
    })

  }


  seeFriendProfile(userId) {
    this.router.navigate(['/home/userprofile'], { queryParams: { userId: userId } });
  }


  URLChange(img: any) {

    return img.replace("https", "http");
  }


  getActiveUserPersonalDetails(userId) {
    this.userService.getUserPersonalDetails(userId).subscribe(data => {
      this.gender = data.gender_type;

    })

  }
}
