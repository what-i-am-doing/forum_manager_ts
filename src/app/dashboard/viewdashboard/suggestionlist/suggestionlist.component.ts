import { Component, OnInit, Input } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { UserService } from 'src/app/services/user.service';
import { ImageService } from 'src/app/services/image.service';
import { Router } from "@angular/router";
import { AddressService } from 'src/app/services/address.service';

@Component({
  selector: 'app-suggestionlist',
  templateUrl: './suggestionlist.component.html',
  styleUrls: ['./suggestionlist.component.css'],
  providers: [UserService, CommonService, ImageService]
})
export class SuggestionlistComponent implements OnInit {
  @Input() receivedSuggestoinList: any;
  activeUserAllData: any;
  public userId: any;
  public groups: any;
  public userGroupId: any;
  public profilePic: any;
  fromSize: number = 1;
  toSize: number = 5;
  active: boolean = false;
  public activeUserPersonalData: any;
  public city: any;

  activeUserEducationalData: any;
  public gender: any;
  constructor(private commonService: CommonService,
    private imageService: ImageService,
    private addressService: AddressService,
    private service: UserService, private router: Router) {
    if (this.receivedSuggestoinList != null && this.receivedSuggestoinList != undefined) {
      console.log('this.receivedSuggestoinList');
      console.log(this.receivedSuggestoinList);

    }



  }

  ngOnInit() {

    if (this.receivedSuggestoinList) {

      this.userGroupId = this.receivedSuggestoinList.group_id;
      this.getUserEducationalDetails(this.receivedSuggestoinList.user_id);
      this.getUserPersonalDetails(this.receivedSuggestoinList.user_id);
      if(this.receivedSuggestoinList.image_id && this.receivedSuggestoinList.image_id !='string')
      this.getProfile(this.receivedSuggestoinList.image_id);

      }
  }





  getUserPersonalDetails(userId) {

    try {
      this.service.getUserPersonalDetails(userId).subscribe(data => {

        
        this.gender = data.gender_type;
        this.activeUserPersonalData = data;
        this.getAdress(data.address_id);
        
      })
    } catch (err) {

    }

  }


  getUserEducationalDetails(userId) {

    try {
      this.service.getEducationDetailsDataByUserId(userId).subscribe(data => {
        
        this.activeUserEducationalData = data;

      })
    } catch (err) {


    }
  }

  URLChange(img: any) {

    return img.replace("https", "http");


  }

  // this will method will be used  while using backend service
  sendFriendRequest(touserId) {

    

    var requestObj = {

      "comments": "string",
      "creation_timestamp": Date.now(),
      "from_user_id": localStorage.getItem("user"),
      "group_id": this.userGroupId[0],
      "message": "sending friend Request",
      "relation_type_id": "FRIEND",
      "req_status": "OPEN",
      "status": "ACTIVE",
      "to_user_id": touserId

    }
    

    document.getElementById('sending' + touserId).textContent = "Sending...";

    this.service.sendFriendRequest(requestObj, localStorage.getItem("user")).subscribe(data => {


      console.log(data);
      if (data.id) {
        var element = <HTMLInputElement>document.getElementById(touserId);


        document.getElementById('sending' + touserId).textContent = "Request Sent";

        element.disabled = true;
        var element1 = <HTMLInputElement>document.getElementById('remove' + touserId);
        element1.style.display = 'none';


        setTimeout(() =>

          delete this.receivedSuggestoinList.user_full_text_name,
          2000)

      }





    })

  }

  removeFromSuggestList(touserId) {
    document.getElementById('remove' + touserId).textContent = "Removing...";
    this.toSize = this.toSize - 1;
    delete this.receivedSuggestoinList.user_full_text_name;
  }

  getProfile(image_id) {

    this.imageService.getImageByType(image_id, 'thumbnail').subscribe(data => {

      if (data.length > 0) {

        this.profilePic = this.URLChange(data[0].image_storage_url);
      }
      else {
        this.getProfileImage(image_id);
      }
    })
  }
  
  getProfileImage(image_id) {
    if (image_id != '' || image_id != 'string' || image_id != 'null') {


      this.imageService.getImage(image_id).subscribe(data => {

        this.profilePic = this.URLChange(data.image_storage_url);

      }


      )
    }


  }


  seeFriendProfile(userId) {
    this.router.navigate(['/home/userprofile'], { queryParams: { userId: userId } });
  }


  getAdress(adressId) {
    this.addressService.getAdress(adressId).subscribe(data => {
        this.city = data.city;

    })
  }

  getActiveUserPersonalDetails(userId) {
    this.service.getUserPersonalDetails(userId).subscribe(data => {
      this.gender = data.gender_type;
      this.getAdress(data.address_id)
    })
  }





}
