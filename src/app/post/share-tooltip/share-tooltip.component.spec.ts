import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareTooltipComponent } from './share-tooltip.component';

describe('ShareTooltipComponent', () => {
  let component: ShareTooltipComponent;
  let fixture: ComponentFixture<ShareTooltipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareTooltipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareTooltipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
