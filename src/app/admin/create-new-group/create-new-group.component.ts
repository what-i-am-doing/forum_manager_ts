import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Group } from '../models/group';
import { GroupService } from 'src/app/services/group.service';

@Component({
  selector: 'create-new-group',
  templateUrl: './create-new-group.component.html',
  styleUrls: ['./create-new-group.component.css'],
  providers: [Group,GroupService]
})

export class CreateNewGroupComponent implements OnInit {
  @Output() createGroupEvent = new EventEmitter<any>();
public group=new Group();
  group_name:string;
  custom_tag_line:string;
  comments:string;
  success:boolean = false;
  constructor( private groupService:GroupService) { }

   ngOnInit() {




  }
  



  alert(){

       console.log("{"+this.group_name+ ","+this.custom_tag_line+","+this.comments+"}");
  }




  createGroup(){
 var groupObj=   {
      "client_id": "teTVc2kBd-kimDmnvDxx",
     
     
      "custom_tag_line": this.custom_tag_line,
    
      "group_name": this.group_name,
 
      "status": "ACTIVE"
    }

 
this.groupService.createUserGroup(groupObj,localStorage.getItem('user')).subscribe(data=>{

console.log(data);

this.getGroupDetails(data.id)





  })




  
  }

  getGroupDetails(groupId){


  

  this.groupService.getGroupDetails(groupId).subscribe(data=>{


    console.log(data);
    
    this.createGroupEvent.next(data);

  }
  )
  }
}