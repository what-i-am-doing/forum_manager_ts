import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common'



@Component({

  
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.css']
})
export class AboutusComponent implements OnInit {
  showContactUs:boolean=false;
  showContactSidebar:boolean=false;

  constructor(public router:Router) {

    var images=['assets/img/header_blue_boy.jpg','assets/img/header_redgirl.jpg',
    'assets/img/header_girl_blue.jpg'];

    var numimages=3;
  


    setInterval(function(){

      var  x=(Math.floor(Math.random()*numimages));
    var  url=(images[x]);

try{

 document.getElementById('loopBgImg').style.backgroundImage = 'url('+url+')';
}
catch(err){


}
     
    

    },5000);


    
   }

  ngOnInit() {

   


    
var img="assets/img/backdrop_stories-higherEducation.jpg";
var img2="assets/img/video_california-community-college_full.jpg";
var img3="assets/img/backdrop_stories-k12.jpg";
var img4="assets/img/video_ncdpi_full.jpg";
var img5="assets/img/backdrop_testDrive.png";
var testdrive='assets/img/testDrive.png';
var newmotion="assets/img/backdrop_news.jpg";
var rumblestrip="assets/img/rumble-strip.gif";
var featurecallout="assets/img/backdrop_diamond-red.png";
var gagecallout="assets/img/gauge/2017_Gauge_Canvaslms_310x185.jpg";
var gagecanvas="assets/img/header-finalPoint-change.jpg";

var finalpoint="assets/img/header-finalPoint-engage.jpg"
try{



    document.getElementById('higherEducation_motionBox').style.backgroundImage = 'url('+img+')';
    document.getElementById('video_main').style.backgroundImage = 'url('+img2+')';
    document.getElementById('k12_motionBox').style.backgroundImage = 'url('+img3+')';
    document.getElementById('california').style.backgroundImage = 'url('+img4+')';
    document.getElementById('testDrive').style.backgroundImage = 'url('+img5+')';
    document.getElementById('span').style.backgroundImage = 'url('+testdrive+')';

    document.getElementById('rumblestrip').style.backgroundImage = 'url('+rumblestrip+')';
    document.getElementById('featurescallout').style.backgroundImage='url('+featurecallout+')';
    document.getElementById('gagecallout').style.backgroundImage='url('+gagecallout+')';
    document.getElementById('finalpoint').style.backgroundImage='url('+finalpoint+')';
    document.getElementById('gagecanvas').style.backgroundImage='url('+gagecanvas+')';
    document.getElementById('featurescallouttwo').style.backgroundImage='url('+gagecallout+')';
} catch(err){

  
}




  }

 


  //  navigate to pages
navigateToPages(link:string) {
  console.log(link);

  if(link=='about-us'){
  
  
   this.router.navigateByUrl('/about-us');
  
  }
  else if(link=="login"){
  
  
    this.router.navigateByUrl('/login');
   
  }
  else if(link=="main"){
  
  
    this.router.navigateByUrl('/main');
   
  }
  else if(link=="product"){
  
    this.router.navigateByUrl('/product');
 
  
  }
  else if(link=="platform"){
  
  
    this.router.navigateByUrl('/platform')
    
  
  }
  else if(link=="company"){
  
  
    this.router.navigateByUrl('/company')
    
  
  }
  

  
  }

  showNavbar: boolean = false;

// when menu button is clicked , it diplays left  menu bar
  menuClicked(showNavbar: boolean) {

    localStorage.setItem('leftmenubar', 'showNavbar');

    if (showNavbar == false) {

      this.showNavbar = true;
    }
    else if (showNavbar == true) {
      this.showNavbar = false;
    }

  }




  openNav() {
    this.showContactUs=false;
 

 
    document.getElementById("mySidebar").style.display= "block";
    document.getElementById("mySidebar").style.width = "500px";

    document.getElementById("main").style.marginLeft = "0px";
    this.showContactSidebar=true;
  }

  closeNav() {
    this.showContactUs=true;
    this.showContactSidebar=false;
    document.getElementById("mySidebar").style.display= "none";
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
 
  }

  myEventHandler(){

    document.getElementById("mySidebar").style.display= "none";
    this.showContactUs=true;
  }
  closemenuoptions(){

    this.showNavbar=false;
  }
}
