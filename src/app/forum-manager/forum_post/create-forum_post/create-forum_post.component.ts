import {
    Component,
    OnInit,
    ViewChild,
    ElementRef,
    Output,
    EventEmitter,
    Input
  } from '@angular/core';
  import { Forum_Post } from 'src/app/model/Forum_Post';
  import { Content } from 'src/app/model/content';
  import { ImageService } from 'src/app/services/image.service';
  import { PdfService } from 'src/app/services/pdf.service';
  import { VideoService } from 'src/app/services/video.service';
  import { ForumPostService } from 'src/app/services/forum_post.service';
  import { OpengraphService } from 'src/app/services/opengraph.service';
  import { EmojiEvent, EmojiData } from 'src/app/lib/emoji/public_api';
  import { Renderer2 } from '@angular/core';
  import { UserService } from 'src/app/services/user.service';

  const CUSTOM_EMOJIS = [
    {
      name: 'Party Parrot',
      shortNames: ['parrot'],
      keywords: ['party'],
      imageUrl: './assets/images/parrot.gif'
    },
    {
      name: 'Octocat',
      shortNames: ['octocat'],
      keywords: ['github'],
      imageUrl: 'https://assets-cdn.github.com/images/icons/emoji/octocat.png?v7'
    },
    {
      name: 'Squirrel',
      shortNames: ['shipit', 'squirrel'],
      keywords: ['github'],
      imageUrl: 'https://assets-cdn.github.com/images/icons/emoji/shipit.png?v7'
    },
    {
      name: 'Test Flag',
      shortNames: ['test'],
      keywords: ['test', 'flag'],
      spriteUrl:
        'https://unpkg.com/emoji-datasource-twitter@4.0.4/img/twitter/sheets-256/64.png',
      sheet_x: 1,
      sheet_y: 1,
      size: 64,
      sheetColumns: 52,
      sheetRows: 52
    }
  ];

  @Component({
    selector: "app-create-forum_post",
    templateUrl: './create-forum_post.component.html',
    styleUrls: ['./create-forum_post.component.css'],
    providers: [
      ForumPostService,
      PdfService,
      VideoService,
      ImageService,
      OpengraphService
    ]
  })

  export class CreateForumPostComponent implements OnInit {
   
  
    constructor(
      private forumpostService: ForumPostService,
      private imageService: ImageService,
      private pdfService: PdfService,
      private videoService: VideoService,
      private opengraphService: OpengraphService,
      private renderer: Renderer2,
      private userService: UserService
    ) {
      this.renderer.listen('window', 'click', (e: Event) => {
        if (e.target !== this.toggleButton.nativeElement && this.menu.nativeElement != undefined && e.target !== this.menu.nativeElement && this.emojiclicked == false) {
          this.isMenuOpen = false;
        }
      });
      this.isMenuOpen = false;
    }
    public gender: any;
    public forum_post_content: any;
   
    forum_post_id: any;
    img_id: any;
    originalPostId: any;
  
    public profilePic: any;
    public isMenuOpen = false;
    public Activegender: any;
    firstName: string = localStorage.getItem("first_name");
    lastName: string = localStorage.getItem("last_name");
    userName: string = this.firstName + " " + this.lastName;
    uploadResponse = { status: '', message: 0, filePath: '' };
  //  getUsersDetails(userId: string) {
  //    throw new Error("Method not implemented.");
  //  }
 //   getActiveUserPersonalDetails(userId: string) {
 //     throw new Error("Method not implemented.");
 //   }
 //   getPostsForForum(forumId: string) {
  //    throw new Error("Method not implemented.");
 //   }
    themes = ['google'];
    set = 'google';
    native = true;
    CUSTOM_EMOJIS = CUSTOM_EMOJIS;
    toggleMenu() {
      this.isMenuOpen = !this.isMenuOpen;
    }
    ngOnInit() {
     // this.getUsersDetails(this.userId);
  //    this.getActiveUserPersonalDetails(this.userId);
      //this.getPostsForForum(this.forumId);
      console.log(this.postToEdit);
      if (this.edit) {
        this.postBoxActive = !this.postBoxActive;
        this.myModal.nativeElement.style.zIndex = "10000";
        this.modalback.nativeElement.style.display = "block";
        this.close.nativeElement.style.display = "block";
        this.appear.nativeElement.style.display = "block";
        this.findOGLinks();
      }
      this.content.$content_images_list = [];
      this.content.$content_pdf_list = [];
      this.content.$content_video_list = [];
    }
    @Input() idPassedFromParams: any;
    @Input() friends: any;
    @Input() edit: any;
    @Input() postToEdit?: any;
    @Output() emitUpdate = new EventEmitter<any>();
    @Output() emitForumPost = new EventEmitter<any>();
    @Output() emitState = new EventEmitter<any>();
    @ViewChild("upload", { read: ElementRef }) upload: ElementRef;
    @ViewChild("myModal", { read: ElementRef }) myModal: ElementRef;
    @ViewChild("modalback", { read: ElementRef }) modalback: ElementRef;
    @ViewChild("close", { read: ElementRef }) close: ElementRef;
    @ViewChild("appear", { read: ElementRef }) appear: ElementRef;
    @ViewChild('toggleButton') toggleButton: ElementRef;
    @ViewChild('menu') menu: ElementRef;
    @ViewChild('emoji') emoji: ElementRef
    preview: boolean = false;
    postTagFound: boolean = false;
    postBoxActive: boolean = false;
    tagFriends: boolean = false;
    showemojis: boolean = false;
    beforePrevious: number = 0;
    previousKey: number = 0;
    currentTag: string;
    formatted_message: any;
    heading: any;
    postMessage: string;
    lastLink: string;
    showError: true;
    postTagFriends: Array<any> = [];
    imageSRCS: Array<any> = [];
    enabled_for_groups=["public"];
    pdfData: Array<any> = [];
    videoSRCS: Array<any> = [];
    filteredFriends: Array<string> = [];
    userId: string = localStorage.getItem("user");

    userLevel: string = localStorage.getItem("user_level");
    userTag: string = localStorage.getItem("user_tag");
    forumId: string = localStorage.getItem("forum_id");
// tslint:disable-next-line: member-ordering
  //  forum_post: Forum_Post = new Forum_Post();
    content : Content = new Content();
    mediaUpload: boolean = false;
    taggedUsers: Array<any> = [];
    Message: boolean = false;
    start: number;
    end: number;
    previousThumbnail: number = 0;
    previousType: string;
    curTime: number = +new Date;
    public emojiclicked: boolean = false;
  

    submitPost(): void {
      console.log('tuuuuu',this.formatted_message.length)
      if (this.formatted_message.length > 10000) {
        this.showError = true;
        return;
      }
      var forum_post: Forum_Post = new Forum_Post;
      forum_post.$message = this.postMessage;
      forum_post.$user_id = this.userId;
      forum_post.$original_user_id = this.userId;
      forum_post.$forum_id = this.idPassedFromParams;
      forum_post.$original_post_id = this.originalPostId;
      forum_post.$heading = this.heading;
      forum_post.$formatted_message = this.formatted_message
      forum_post.$enabled_for_groups = this.enabled_for_groups;
      forum_post.$creation_timestamp = +new Date();
      forum_post.$tag_id = this.userTag;
      forum_post.$post_type = "CREATED";
      forum_post.$post_has_data = false;
      forum_post.$status = "ACTIVE";
      // debugger
      // if(this.content.$content_images_list.length){
      //   console.log('JJJJJJJ',this.content.$content_images_list)
      // }
      if (this.content && this.content.$content_images_list) {
           console.log('JJJJJJJ',this.content)
        if (this.previousType == 'image') {
          forum_post.$img_id = this.content.$content_images_list[this.previousThumbnail];
          forum_post.$preview_content_id = this.content.$content_images_list[this.previousThumbnail];
          forum_post.$preview_content_type = "images";
        }
        forum_post.$post_has_data = true;
        // if(this.forum_post_content){

        // }
        console.log('JJJJJJJJJJ222',this.forum_post_content);
        if(this.forum_post_content == undefined){
          this.forumpostService.userfourmpost(forum_post).subscribe(data => {
            console.log('JJJJJJJJJJ2223333',data);
            this.imageSRCS = [];
            this.content.$content_images_list = [];
            this.mediaUpload = false;
            this.Message = false;
            this.formatted_message = "";
            this.heading = "";
            console.log('QQQQQQQQQQQQQQQQQ',data);
            this.getCreatedForumPost(data.id);
          });
        }
        else{
          this.forumpostService.usercontent(this.content).subscribe(content => {
            forum_post.$forum_post_content = content.id;
            this.forumpostService.userfourmpost(forum_post).subscribe(data => {
              console.log('JJJJJJJJJJ2223333',data);
               this.getCreatedForumPost(data.id);
              this.imageSRCS = [];
              this.forum_post_content.$content_images_list = [];
              this.mediaUpload = false;
              this.Message = false;
              this.formatted_message = "";
              this.heading = "";
              console.log('QQQQQQQQQQQQQQQQQ',data);
              this.getCreatedForumPost(data.id);
            });
          });
        }
      } else {
      
        this.forumpostService.userfourmpost(forum_post).subscribe(data => {
  
          this.imageSRCS = [];
          this.mediaUpload = false;
          this.Message = false;
          this.formatted_message = "";
          this.heading = "";
          console.log(data);
          this.getCreatedForumPost(data.id);
        });
      localStorage.setItem('forum_id',this.forumId);
      localStorage.setItem('forum_postID', this.forum_post_id);
      localStorage.setItem('forum_post_img_id',this.img_id);
   //   localStorage.setItem('original_post_id',this.original_post_id);
    //  localStorage.setItem('original_user_id',this.original_user_id);
    //  localStorage.setItem('preview_content_id',this.preview_content_id);
      }
      this.postBoxActive = !this.postBoxActive;
      this.preview = false;
      this.closePostcreate();
    }
    getCreatedForumPost(postID: string) {
      // this.postService.getPostById(postID).subscribe(post => {
      //   console.log(post);
      //   var emitPost = Object.assign({}, post);
      //   this.emitPost.emit(emitPost);
      // });
      
      this.emitForumPost.emit(true);
    }
    original_user_id(original_user_id: any): string {
      throw new Error("Method not implemented.");
    }
    preview_content_id(preview_content_id: any): string {
      throw new Error("Method not implemented.");
    }
    original_post_id(original_post_id: any): string {
      throw new Error("Method not implemented.");
    }
  
  //  getForumPostByIds(forum_post_Id: string) {
      // this.postService.getPostById(postID).subscribe(post => {
      //   console.log(post);
      //   var emitPost = Object.assign({}, post);
      //   this.emitPost.emit(emitPost);
      // });
  //    this.emitPost.emit(true);
  //  }
  
    findLinksInPost(str) {
      var pattern = /(?:(?:https?|ftp):\/\/|\b(?:[a-z\d]+\.))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))?/gi;
      var result = str.match(pattern);
      console.log(result);
      return result;
    }
  
    closePostcreate() {
      this.modalback.nativeElement.style.display = "none";
      this.close.nativeElement.style.display = "none";
      this.appear.nativeElement.style.display = "none";
      this.myModal.nativeElement.style.zIndex = "0";
      this.imageSRCS = [];
      this.content.$content_images_list = [];
      this.mediaUpload = false;
      this.Message = false;
  
      if (this.edit) {
        this.emitState.emit(false);
      }
    }
  
    postFocus(event: Event) {
      console.log(this.imageSRCS);
      console.log(this.content.$content_images_list);
      console.log(this.postToEdit);
      event.preventDefault();
      this.myModal.nativeElement.style.zIndex = "10000";
      this.modalback.nativeElement.style.display = "block";
      this.close.nativeElement.style.display = "block";
      this.appear.nativeElement.style.display = "block";
    }
  
  
    tagInPost(event: any) {
      console.log(this.postTagFound);
      if(!this.postToEdit) this.postMessage = this.formatted_message
        .toString()
        .replace(/<[^>]*>/g, "");
  
      else this.postMessage = this.postToEdit.formatted_message
      .toString()
      .replace(/<[^>]*>/g, "");  
  
      if (this.postMessage.length) this.Message = true;
      else this.Message = false;
      console.log(this.Message);
      event = event.event;
      console.log(event.keyCode);
      if (event.keyCode === 32) {
        this.findOGLinks();
      }
  
      var str = this.postMessage;
      if (this.postTagFound === true) {
        console.log(event.keyCode);
        console.log(str);
  
        if (str.includes("@")) {
          var str = str.substring(str.lastIndexOf("@") + 1);
          this.currentTag = str;
          console.log(str);
          this.filteredFriends = this.friends.filter(word =>
            word.user_full_text_name.startsWith(str)
          );
          console.log(this.filteredFriends);
        } else this.postTagFound = false;
  
      } else {
        if (
          (this.postMessage == "@" || this.beforePrevious == 32 || this.beforePrevious == 8) &&
          (event.key == 2 && this.previousKey == 16)
        ) {
  
          this.postTagFound = true;
          this.start = this.formatted_message.length - 1;
          if (this.friends.length > 6) this.filteredFriends = this.friends.slice(0, 6);
          else this.filteredFriends = this.friends;
          this.currentTag = str.substring(str.lastIndexOf("@") + 1);
        }
      }
  
      this.beforePrevious = this.previousKey;
      this.previousKey = event.keyCode;
      console.log(this.postTagFound);
    }
  
    findOGLinks() {
  
      if(!this.postToEdit) {
        this.postMessage = this.formatted_message
        .toString()
        .replace(/<[^>]*>/g, "");
      }
      
      else this.postMessage = this.postToEdit.formatted_message
      .toString()
      .replace(/<[^>]*>/g, "");
  
      if (this.postMessage.length) this.Message = true;
      else this.Message = false;
  
      if (this.lastLink == undefined) {
        console.log(this.postMessage);
        var filter = this.postMessage.replace(/&nbsp;/g, "");
        var links = this.findLinksInPost(filter);
  
        if (links.length) {
          this.lastLink = links[0];
          var reqURL = encodeURIComponent(this.lastLink);
          console.log(reqURL);
          this.opengraphService.getOgData(reqURL).subscribe(data => {
            console.log(data);
            data = data.hybridGraph;
            var main_container = document.createElement("div");
            main_container.style.border = "1px solid #c5c5c5";
            main_container.style.borderTop = "0px";
            main_container.style.padding = "2px";
            var footer_container = document.createElement("div");
            var heading = document.createElement("h4");
            if (data.title) heading.textContent = data.title;
            footer_container.appendChild(heading);
            var link = document.createElement("h5");
            var link_content = document.createElement("a");
            if (data.url) link_content.textContent = data.url;
            link.appendChild(link_content);
            footer_container.appendChild(link);
            var describe = document.createElement("div");
            if (data.description) describe.textContent = data.description.substring(0, 200) + "...";
            footer_container.appendChild(describe);
            if (data.image) {
              var img = document.createElement("img");
              img.src = data.image;
              img.width = 150;
              img.height = 70;
              main_container.appendChild(img);
  
            }
            var cross = document.createElement("span");
            cross.setAttribute("class", "pull-right glyphicon glyphicon-remove");
            cross.classList.add("cursor");
  
            cross.addEventListener("click", () => {
              this.lastLink = undefined;
              container.removeChild(container.childNodes[0]);
            });
  
            main_container.appendChild(cross);
            main_container.appendChild(footer_container);
            if(this.edit) var container = document.getElementById("appendOGedit");
            else var container = document.getElementById("appendOG");
            container.appendChild(main_container);
            console.log(container.childNodes[0]);
          },
            () => this.lastLink == undefined
          );
        }
      }
  
    }
  
    selectPostfriend(friend) {
      this.end = this.formatted_message.length;
      this.formatted_message = this.replaceBetween(this.start, this.end, "<a contenteditable='false'>@" + friend.user_full_text_name + "</a> &nbsp;", this.formatted_message);
  
      this.taggedUsers.push({
        content:
          '<a contenteditable="false">@' + friend.user_full_text_name + "</a>",
        id: friend.user_id
      });
      this.postTagFound = false;
    }
   // replaceBetween(start: number, end: number, arg2: string, formatted_message: any): any {
   //   throw new Error("Method not implemented.");
   // }
    clickUpload(): void {
      this.upload.nativeElement.click();
    }
  
    processFile(imageInput: any) {
      var file = imageInput.files[0];
      var fileName = file.name;
      var fileSize = file.size;
      var fileReader = new FileReader();
  
      if (file.type.match("image")) {
  
        fileReader.addEventListener("load", (event: any) => {
          console.log(event.target.result);
          this.imageService.uploadImage(file).subscribe(
            (res: any) => {
              console.log(res);
              this.uploadResponse = res;
              
              if (typeof res === 'object') {
                if ('id' in res) {
                  this.mediaUpload = true;
                  this.forum_post_content.$content_images_list.push(res.id);
                  if (this.edit) {
                    console.log("if block executed");
                    this.postToEdit._real_albums = [];
                    this.postToEdit._real_albums.push(
                      {
                        image_storage_url: fileReader.result,
                        content_type: "image/jpeg",
                        image_name: file.name,
                        image_size: file.size
                      }
  
                    );
                    console.log(this.imageSRCS);
  
                  }
                  else {
                    console.log("else block executed");
                    if (!this.imageSRCS.length) {
                      var isPreview = true;
                      this.previousType = "image";
                    }
                    else var isPreview = false;
  
                    this.imageSRCS.push({
                      image_storage_url: fileReader.result,
                      content_type: "image/jpeg",
                      image_name: file.name,
                      image_size: file.size,
                      is_preview: isPreview
                    });
                    console.log(this.imageSRCS);
                  }
                }
              }
            },
            err => {
              console.log(err);
            }
          );
  
  
  
  
        });
  
        fileReader.readAsDataURL(file);
    }
  
        
            err => {
              console.log(err);
            }
        
        }
  
       
         
            
  
    showandhideEmojis(showemojis) {
      if (showemojis == false) {
        this.showemojis = true;
      } else if (showemojis == true) {
        this.showemojis = false;
      }
      this.set = this.themes[0];
    }
  
    setTheme(set: string) {
      this.native = set === 'native';
      this.set = set;
    }
  
    handleClick(event: EmojiEvent) {
      var emoji = event.$event.srcElement;
      var cloneEmoji: HTMLElement = <HTMLElement>emoji.cloneNode(true);
  
      this.formatted_message += cloneEmoji.innerHTML;
  
      // document.getElementById("editor").appendChild(cloneEmoji);
    }
  
    emojiClick($event) {
      console.log(" emoji  clicked");
    }
  
    keyDown(event: KeyboardEvent) {
      if (event.keyCode > 1) {
        this.showemojis = false;
      }
    }
  
    getSelectedFriends(event: any) {
      console.log(event);
      this.postTagFriends = event;
    }
  
    cancelORSubmit() { this.emitState.emit(false); }
  
    updatePost() {
      var x = {
        "formatted_message": this.postToEdit.formatted_message,
        "heading": this.postToEdit.heading,
        "message": this.postToEdit.formatted_message.toString().replace(/<[^>]*>/g, "")
      };
      console.log(x);
      this.forumpostService.updateforum_post(this.postToEdit.post_id, x).subscribe(data => {
        console.log(data);
        this.emitUpdate.emit(x);
  
      }
      );
  
    }
  
    public addEmoji(event: { $event: MouseEvent, emoji: EmojiData }) {
  
      console.log("emoji added");
      let emoticonElement = <HTMLElement><unknown>event.$event.target;
      if (!emoticonElement.style.backgroundImage || emoticonElement.style.backgroundImage === '') {
        emoticonElement = <HTMLElement>emoticonElement.firstChild;
      }
  
  
      console.log(emoticonElement.outerHTML);
      console.log(event.emoji.unified);
      console.log(this.URLChange(emoticonElement.outerHTML))
  
      if (this.formatted_message == undefined) {
        this.formatted_message = "<span style='position: relative;top: 5px' contenteditable='false'>" + this.URLChange(emoticonElement.outerHTML) + "</span";
  
      }
      else {
        this.formatted_message += "<span style='position:relative;top:5px' contenteditable='false'>" + this.URLChange(emoticonElement.outerHTML) + "</span>";
  
      }
  
    }
  
    replaceBetween = function (start, end, what, string) {
      return string.substring(0, start) + what + string.substring(end);
    };
    onSearchChange(value) {
      console.log(" value changes")
      this.formatted_message = value;
    }
  
    URLChange(element) {
      return element.replace("https", "http");
    }
  
    reCreateThumbnail(event: any) {
      console.log(event);
      if (this.previousType == 'image') this.imageSRCS[this.previousThumbnail].is_preview = false;
    
  
      if (event.type == 'image') this.imageSRCS[event.index].is_preview = true;
      
      this.previousType = event.type;
  
    }
  
    emojiCicked() {
      console.log(' emoji clicked true');
      this.emojiclicked = true;
      setTimeout(() => this.emojiclicked = false, 500)
  
    }
  
    onChange(event) {
  
      console.log('value changing');
      this.isMenuOpen = false;
  
    }
    getUsersDetails(userId) {
      this.userService.getUserDetails(userId).subscribe(data => {
  
        this.getProfile(data.image_id);
  
      })
    }
    getProfile(image_id) {
  
      this.imageService.getImage(image_id).subscribe(data => {
        this.profilePic = this.URLChange(data.image_storage_url);
      })
    }
  
    getActiveUserPersonalDetails(userId) {
      this.userService.getUserPersonalDetails(userId).subscribe(data => {
        this.Activegender = data.gender_type;
        console.log(data.gender_type);
  
  
      })
    }
  }
