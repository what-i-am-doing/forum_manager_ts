export class Content{

   constructor(){};

    private comments: string;
    private content_custom_id: string;
    private content_description: string;
    private content_images_list:string [];
    private content_name: string;
    private content_pdf_list: string[];
    private content_video_list:string [];
    private creation_tiestamp: number;
    private ext_link_data_list:string [];
    private formatted_content_description: string;
    private image_id: string;
    private last_updated_tiestamp: number;
    private status: string;
    /**
     * Getter $comments
     * @return {string}
     */
	public get $comments(): string {
		return this.comments;
	}

    /**
     * Getter $content_custom_id
     * @return {string}
     */
	public get $content_custom_id(): string {
		return this.content_custom_id;
	}

    /**
     * Getter $content_description
     * @return {string}
     */
	public get $content_description(): string {
		return this.content_description;
	}

    /**
     * Getter $content_images_list
     * @return {string []}
     */
	public get $content_images_list(): string [] {
		return this.content_images_list;
	}

    /**
     * Getter $content_name
     * @return {string}
     */
	public get $content_name(): string {
		return this.content_name;
	}

    /**
     * Getter $content_pdf_list
     * @return {string[]}
     */
	public get $content_pdf_list(): string[] {
		return this.content_pdf_list;
	}

    /**
     * Getter $content_video_list
     * @return {string []}
     */
	public get $content_video_list(): string [] {
		return this.content_video_list;
	}

    /**
     * Getter $creation_tiestamp
     * @return {number}
     */
	public get $creation_tiestamp(): number {
		return this.creation_tiestamp;
	}
     


    /**
     * Getter $ext_link_data_list
     * @return {string []}
     */
	public get $ext_link_data_list(): string [] {
		return this.ext_link_data_list;
	}

    /**
     * Getter $formatted_content_description
     * @return {string}
     */
	public get $formatted_content_description(): string {
		return this.formatted_content_description;
	}

    /**
     * Setter $ext_link_data_list
     * @param {string []} value
     */
	public set $ext_link_data_list(value: string []) {
		this.ext_link_data_list = value;
	}

    /**
     * Setter $formatted_content_description
     * @param {string} value
     */
	public set $formatted_content_description(value: string) {
		this.formatted_content_description = value;
	}

    /**
     * Getter $image_id
     * @return {string}
     */
	public get $image_id(): string {
		return this.image_id;
	}

    /**
     * Getter $last_updated_tiestamp
     * @return {number}
     */
	public get $last_updated_tiestamp(): number {
		return this.last_updated_tiestamp;
	}

    /**
     * Setter $comments
     * @param {string} value
     */
	public set $comments(value: string) {
		this.comments = value;
	}

    /**
     * Setter $content_custom_id
     * @param {string} value
     */
	public set $content_custom_id(value: string) {
		this.content_custom_id = value;
	}

    /**
     * Setter $content_description
     * @param {string} value
     */
	public set $content_description(value: string) {
		this.content_description = value;
	}

    /**
     * Setter $content_images_list
     * @param {string []} value
     */
	public set $content_images_list(value: string []) {
		this.content_images_list = value;
	}

    /**
     * Setter $content_name
     * @param {string} value
     */
	public set $content_name(value: string) {
		this.content_name = value;
	}

    /**
     * Setter $content_pdf_list
     * @param {string[]} value
     */
	public set $content_pdf_list(value: string[]) {
		this.content_pdf_list = value;
	}

    /**
     * Setter $content_video_list
     * @param {string []} value
     */
	public set $content_video_list(value: string []) {
		this.content_video_list = value;
	}

    /**
     * Setter $creation_tiestamp
     * @param {number} value
     */
	public set $creation_tiestamp(value: number) {
		this.creation_tiestamp = value;
	}

    /**
     * Setter $image_id
     * @param {string} value
     */
	public set $image_id(value: string) {
		this.image_id = value;
	}

    /**
     * Setter $last_updated_tiestamp
     * @param {number} value
     */
	public set $last_updated_tiestamp(value: number) {
		this.last_updated_tiestamp = value;
	}


    /**
     * Getter $status
     * @return {string}
     */
	public get $status(): string {
		return this.status;
	}

    /**
     * Setter $status
     * @param {string} value
     */
	public set $status(value: string) {
		this.status = value;
	}
    
}