import { TestBed } from '@angular/core/testing';

import { TagMappingService } from './tag-mapping.service';

describe('TagMappingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TagMappingService = TestBed.get(TagMappingService);
    expect(service).toBeTruthy();
  });
});
