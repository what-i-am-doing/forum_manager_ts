import { TestBed } from '@angular/core/testing';

import { OpengraphService } from './opengraph.service';

describe('OpengraphService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OpengraphService = TestBed.get(OpengraphService);
    expect(service).toBeTruthy();
  });
});
